<?php

class OAuthTableSeeder extends Seeder
{

  public function run()
  {
    //DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    DB::table('oauth_clients')->truncate();
    $clients = [
      [
        'id'     => 'hotelbonanzaapi',
        'secret' => 'sdgsdfgb4398earuvbaesdf43',
        'name'   => 'Hotel Bonanza API',
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now()
      ]
    ];
    DB::table('oauth_clients')->insert($clients);

    DB::table('oauth_client_scopes')->truncate();
    $client_scopes = [
      [
        'client_id' => 'hotelbonanzaapi',
        'scope_id'   => 'extranet',
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now()
      ]
    ];
    DB::table('oauth_client_scopes')->insert($client_scopes);

    DB::table('oauth_client_endpoints')->delete();
    $clientEndpoints = [
      [
        'client_id'    => 'hotelbonanzaapi',
        'redirect_uri' => 'https://api.hotelbonanza.com',
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now()
      ]
    ];
    DB::table('oauth_client_endpoints')->insert($clientEndpoints);

    //DB::statement('SET FOREIGN_KEY_CHECKS=1;');

  }
}

