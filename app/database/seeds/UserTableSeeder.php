<?php

class UserTableSeeder extends Seeder
{

  public function run()
  {
    DB::table('users')->truncate();
    User::create(
      [
        'username' => 'dave',
        'email'    => 'me@daveblake.co.uk',
        'password' => 'nimzoted'
      ]
    );
  }
}

