<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hotelpoliciespoly extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('hotels__policies', function(Blueprint $table) {
			$table->integer('hotel_id');
			$table->integer('type_id');
			$table->string('type_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('hotels__policies');
	}
}
