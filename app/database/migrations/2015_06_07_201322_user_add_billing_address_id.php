<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAddBillingAddressId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('users', function(Blueprint $table) {
            $table->integer('billing_address_id')->nullable()->after('channelmanager_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('billing_address_id');
        });
	}

}
