<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncreasePhoneNumberLength extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// moved to create
		//DB::statement('ALTER TABLE `hotels` CHANGE COLUMN `tel` `tel` VARCHAR(30) CHARACTER SET \'utf8\' NULL DEFAULT NULL');
		//DB::statement('ALTER TABLE `hotels` CHANGE COLUMN `mobile` `mobile` VARCHAR(30) CHARACTER SET \'utf8\' NULL DEFAULT NULL');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//DB::statement('ALTER TABLE `hotels` CHANGE COLUMN `tel` `tel` VARCHAR(15) CHARACTER SET \'utf8\' NULL DEFAULT NULL');
		//DB::statement('ALTER TABLE `hotels` CHANGE COLUMN `mobile` `mobile` VARCHAR(15) CHARACTER SET \'utf8\' NULL DEFAULT NULL');
	}

}
