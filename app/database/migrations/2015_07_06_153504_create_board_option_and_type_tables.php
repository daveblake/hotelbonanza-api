<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardOptionAndTypeTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
		public function up()
		{
				\Schema::create('board__options', function(Blueprint $table) {
						$table->increments('id');
						$table->string('uuid');
						$table->integer('type_id');
						$table->float('adultprice')->nullable();
						$table->float('childprice')->nullable();
						$table->timestamps();
				});

				\Schema::create('board__types', function(Blueprint $table) {
						$table->increments('id');
						$table->string('uuid');
						$table->string('name');
						$table->timestamps();
				});

				\Schema::create('hotels__hotel_board', function(Blueprint $table) {
						$table->integer('hotel_id');
						$table->integer('board_option_id');
				});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
				\Schema::drop('board__options');
				\Schema::drop('board__types');
				\Schema::drop('hotels__hotel_board');
		}

}
