<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeHotelAddressIdToGuid extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('hotels', function (Blueprint $table) {
			$table->dropColumn('address_id');
		});
		\Schema::table('hotels', function (Blueprint $table) {
			$table->string('address_id',36)->nullable()->after('user_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::table('hotels', function (Blueprint $table) {
			$table->dropColumn('address_id');
		});
		\Schema::table('hotels', function (Blueprint $table) {
			$table->integer('address_id')->nullable()->after('user_id');
		});
	}

}
