<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtaCredentialsToHotel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->string('ota_hotel_id')->nullable();
      $table->index('ota_hotel_id');
      $table->string('ota_password')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->dropIndex('ota_hotel_id');
      $table->dropColumn('ota_hotel_id');
      $table->dropColumn('ota_password');
		});
	}

}
