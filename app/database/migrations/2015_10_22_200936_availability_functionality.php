<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AvailabilityFunctionality extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('availability__rooms', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->integer('room_id');
			$table->string('short_description')->nullable();
			$table->float('rate');
			$table->integer('quantity')->nullable();
			$table->boolean('closed')->default(false)->nullable();
			$table->integer('minimum_stay')->nullable();
			$table->boolean('no_check_in')->default(false)->nullable();
			$table->boolean('no_check_out')->default(false)->nullable();
			$table->boolean('reoccur')->default(false)->nullable();
			$table->string('days')->nullable();
			$table->date('starts')->nullable();
			$table->date('ends')->nullable();
			$table->timestamps();
		});

		\Schema::create('availability__bookings', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->enum('status', ['pending', 'awaiting_confirmation', 'confirmed', 'complete', 'cancelled', 'denied', 'unknown']);
			$table->integer('availability_room_id');
			$table->integer('user_id');
			$table->dateTime('time_booked');
			$table->date('date_from');
			$table->date('date_to');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists('availability__rooms');
		\Schema::dropIfExists('availability__bookings');
	}

}
