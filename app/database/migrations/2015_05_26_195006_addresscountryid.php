<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addresscountryid extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('addresses', function(Blueprint $table) {
            $table->integer('country_id')->nullable()->after('id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::table('addresses', function(Blueprint $table) {
            $table->dropColumn('country_id');
        });
	}

}
