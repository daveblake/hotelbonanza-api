<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BreakfastoptionMealRoomPrice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('breakfast__options', function(Blueprint $table) {
            $table->float('roomprice')->nullable()->after('childprice');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::table('breakfast__options', function(Blueprint $table) {
            $table->dropColumn('roomprice');
        });
	}

}
