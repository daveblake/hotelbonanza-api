<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnsuiteToRoomLayouts extends Migration {

	static $tableName = 'rooms__layouts';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table(static::$tableName, function(Blueprint $table)
		{
			$table->boolean('ensuite')
				->default(0)
				->after('bedroom_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table(static::$tableName, function(Blueprint $table)
		{
			$table->dropColumn('ensuite');
		});
	}

}
