<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Breakfasttable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('breakfast__options', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('type_id');
            $table->decimal('adultprice',19,4)->nullable();
            $table->decimal('childprice',19,4)->nullable();
            $table->timestamps();
        });

        \Schema::create('breakfast__types', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('name');
            $table->timestamps();
        });

        \Schema::create('hotels__hotel_breakfast', function(Blueprint $table) {
            $table->integer('hotel_id');
            $table->integer('breakfast_option_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('breakfast__options');
        \Schema::drop('breakfast__types');
        \Schema::drop('hotels__hotel_breakfast');
	}

}
