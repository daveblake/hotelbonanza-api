<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTypeFeatures extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
				\Schema::create('rooms__features', function(Blueprint $table) {
						$table->increments('id');
						$table->string('uuid');
						$table->string('name');
						$table->timestamps();
				});

				\Schema::create('rooms__room_features', function(Blueprint $table) {
						$table->integer('room_id');
						$table->integer('feature_id');
				});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
				\Schema::drop('rooms__features');
				\Schema::drop('rooms__room_features');
		}

}
