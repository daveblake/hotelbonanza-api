<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomConfigurablesToRoomType extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rooms__types', function (Blueprint $table) {
            //
            $table->boolean('bedrooms')->after('name')->default(false);
            $table->boolean('lounges')->after('bedrooms')->default(false);
            $table->boolean('bathrooms')->after('lounges')->default(false);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rooms__types', function (Blueprint $table) {
            //
            $table->dropColumn('bedrooms');
            $table->dropColumn('lounges');
            $table->dropColumn('bathrooms');
        });
    }

}
