<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RebuildAvailabilityTables extends Migration
{

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    DB::raw('ALTER TABLE `availability__rooms` MODIFY room_id INT(10) UNSIGNED');
    DB::raw('ALTER TABLE `availability__rooms` MODIFY room_rate_id INT(10) UNSIGNED');

    Schema::table('availability__rooms', function (Blueprint $table) {
      $table->dropColumn('short_description');
      $table->dropColumn('minimum_stay');
      $table->dropColumn('reoccur');
      $table->dropColumn('days');
      $table->dropColumn('starts');
      $table->dropColumn('ends');
      $table->date('date');
      $table->index('date');
      $table->index('rate');
      $table->index('quantity');
      $table->index('room_id');
      $table->index('room_rate_id');
      $table->index(['date', 'rate']);
      $table->unique(['room_rate_id', 'date']);
    });

    Schema::table('rooms__rates', function (Blueprint $table) {
      $table->dropColumn('baseprice');
      $table->index('room_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('availability__rooms', function (Blueprint $table) {
      $table->string('short_description');
      $table->integer('minimum_stay');
      $table->boolean('reoccur');
      $table->integer('days');
      $table->date('starts');
      $table->date('ends');
      $table->date('date');
      $table->dropIndex('date');
      $table->dropIndex('rate');
      $table->dropIndex('quantity');
      $table->dropIndex('room_id');
      $table->dropIndex('room_rate_id');
      $table->dropIndex(['date', 'rate']);
      $table->dropUnique(['room_rate_id', 'date']);
    });

    Schema::table('rooms__rates', function (Blueprint $table) {
      $table->decimal('baseprice',19,4);
    });
  }

}
