<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MediaCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('media__categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('name');
            $table->integer('order')->nullable()->default(0);
            $table->timestamps();
        });

        \Schema::create('media__media_category', function(Blueprint $table) {
            $table->integer('media_id');
            $table->integer('category_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('media__categories');
        \Schema::drop('media__media_category');
	}

}
