<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Beds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('rooms__beds', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id');
            $table->string('uuid');
            $table->string('quantity');
            $table->timestamps();
        });

        \Schema::create('rooms__layout_bed', function(Blueprint $table) {
            $table->integer('layout_id');
            $table->integer('bed_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('rooms__beds');
        \Schema::drop('rooms__layout_bed');
	}
}
