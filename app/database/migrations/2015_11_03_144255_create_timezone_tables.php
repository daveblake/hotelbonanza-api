<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimezoneTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 *
	 */
	public function up()
	{
		\Schema::create('timezones', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->string('city');
			$table->string('timezone');
			$table->timestamps();
		});

		\Schema::create('country__timezones', function(Blueprint $table) {
			$table->integer('country_id');
			$table->integer('timezone_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('timezones');
		\Schema::drop('country__timezones');
	}

}
