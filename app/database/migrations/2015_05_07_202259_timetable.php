<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Timetable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('time', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('name');
            $table->timestamps();
        });

        \Schema::create('internet__internet_time', function(Blueprint $table) {
            $table->integer('internet_id');
            $table->integer('time_id');
        });

        \Schema::create('parking__parking_time', function(Blueprint $table) {
            $table->integer('parking_id');
            $table->integer('time_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('time');
        \Schema::drop('internet__internet_time');
        \Schema::drop('parking__parking_time');
	}

}
