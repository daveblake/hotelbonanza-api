<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdjustPriceSizingOnBreakfastOptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// moved to create
		//DB::statement('ALTER TABLE `breakfast__options` CHANGE COLUMN `adultprice` `adultprice` DECIMAL(19,4) NULL DEFAULT NULL');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//DB::statement('ALTER TABLE `breakfast__options` CHANGE COLUMN `adultprice` `adultprice` FLOAT(8,2) NULL DEFAULT NULL');
	}

}
