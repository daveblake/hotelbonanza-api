<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add4DigitsToBookingsPayments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bookings_payments', function(Blueprint $table)
		{
			$table->char('card_number_last_four', 4)->after('card_number')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bookings_payments', function(Blueprint $table)
		{
			$table->dropColumn('card_number_last_four');
		});
	}

}
