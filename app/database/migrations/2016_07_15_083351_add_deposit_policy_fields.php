<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepositPolicyFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('policies__cancellation', function(Blueprint $table)
		{
			$table->integer('calculation_type_id')->nullable();
      $table->boolean('refundable')->nullable()->default(-1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('policies__cancellation', function(Blueprint $table)
		{
			$table->dropColumn('calculation_type_id');
      $table->dropColumn('refundable');
		});
	}

}
