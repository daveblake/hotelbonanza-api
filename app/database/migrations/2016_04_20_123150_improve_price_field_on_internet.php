<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImprovePriceFieldOnInternet extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//moved to create
		//DB::statement('ALTER TABLE `internet` CHANGE COLUMN `price` `price` DECIMAL(19,4) NULL');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//DB::statement('ALTER TABLE `internet` CHANGE COLUMN `price` `price` FLOAT(8,2) NULL');
	}

}
