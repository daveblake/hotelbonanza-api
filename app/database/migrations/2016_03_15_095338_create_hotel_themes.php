<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelThemes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('themes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->string('name');
			$table->timestamps();
		});

		\Schema::create('hotels__hotel_theme', function(Blueprint $table) {
			$table->integer('hotel_id');
			$table->integer('theme_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('themes');
		\Schema::drop('hotels__hotel_theme');
	}

}
