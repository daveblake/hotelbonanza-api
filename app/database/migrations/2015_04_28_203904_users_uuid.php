<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersUuid extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('users', function (Blueprint $table) {

			if (!Schema::hasColumn('users', 'uuid')) {
				$table->string('uuid')->after('id')->nullable();
			}
			$table->softDeletes();
		});

        foreach(User::all() as $user) {
            if($user->uuid == '') {
                $user->uuid = $user->generateUUID();
                $user->save();
            }
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('uuid');
        });
	}

}
