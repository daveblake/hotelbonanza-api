<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoliciesBedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('policies__bed', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('quantity')->nullable();
            $table->integer('maxAge')->nullable();
            $table->enum('bedType', ['existing', 'cot', 'extrabed', 'other'])->nullable();
            $table->float('costPerNight')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('policies__bed');
	}

}
