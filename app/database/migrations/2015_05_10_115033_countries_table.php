<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CountriesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('countries', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('telprefix_id');
            $table->string('uuid');
            $table->string('name');
            $table->string('iso2', 2);
            $table->string('iso3', 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('countries');
    }

}
