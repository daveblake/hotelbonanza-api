<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingPaymentInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings_payments', function(Blueprint $table)
		{
			//
			$table->increments('id');
			$table->string('uuid');
			$table->integer('booking_id');
			$table->text('card_type');
			$table->text('card_number');
			$table->integer('valid_from');
			$table->integer('valid_to');
			$table->smallInteger('issue_number');
			$table->text('cvv');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings_payments');
	}

}
