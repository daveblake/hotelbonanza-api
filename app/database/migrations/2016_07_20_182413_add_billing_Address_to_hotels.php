<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillingAddressToHotels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
      $table->integer('billing_address_id')->nullable();
      $table->string('billing_name')->nullable();
		});

    $users = User::all();
    foreach ($users as $user) {
      foreach($user->hotels as $hotel)
      if ($user->billingAddress instanceof Address && $hotel instanceof Hotel) {
        $hotel->billingAddress()->associate($user->billingAddress);
        $hotel->billing_name = $user->billing_name;
        $hotel->save([],true);
      }
    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->dropColumn('billing_address_id');
			$table->dropColumn('billing_name');
		});
	}

}
