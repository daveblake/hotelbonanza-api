<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hotelroomrelation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('rooms', function(Blueprint $table) {
            $table->integer('hotel_id')->after('id')->nullable();
        });

        $rooms = DB::table('hotels__hotel_room')->distinct()->get();

        // Make all M2M relations One2Many
        foreach($rooms as $data) {
            DB::table('rooms')->where('id', $data->room_id)->update(['hotel_id' => $data->hotel_id]);
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::table('rooms', function(Blueprint $table) {
            $table->dropColumn('hotel_id');
        });
	}

}
