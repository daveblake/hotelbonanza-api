<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Policies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('policies__cancellation', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('charge_id');
            $table->string('uuid');
            $table->string('period');
            $table->string('charge');
            $table->timestamps();
        });

        \Schema::create('policies__charge_types', function(Blueprint $table) {
            $table->integer('id');
            $table->string('uuid');
            $table->string('name');
            $table->timestamps();
        });

        \Schema::create('hotels__hotel_policy_cancellation', function(Blueprint $table) {
            $table->integer('hotel_id');
            $table->integer('policy_cancellation_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('policies__cancellation');
        \Schema::drop('policies__charge_types');
        \Schema::drop('hotels__hotel_policy_cancellation');
	}

}
