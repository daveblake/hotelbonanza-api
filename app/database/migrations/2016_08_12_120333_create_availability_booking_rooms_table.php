<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailabilityBookingRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('availability__bookings_rooms', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('availability__booking_id')->unsigned();
      $table->foreign('availability__booking_id')->references('id')->on('availability__bookings')->onDelete('cascade');
      $table->index('availability__booking_id');
      $table->integer('availability__room_id')->unsigned();
      $table->foreign('availability__room_id')->references('id')->on('availability__rooms');
      $table->index('availability__room_id');
			$table->timestamps();
		});

    Schema::table('availability__bookings',function(Blueprint $table){
      if(Schema::hasColumn('availability__bookings','availability_room_id'))
        $table->dropColumn('availability_room_id');
      if(!Schema::hasColumn('availability__bookings','deleted_at'))
        $table->softDeletes();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('availability__bookings_rooms');

    Schema::table('availability__bookings',function(Blueprint $table){
      $table->integer('availability_room_id')->unsigned();
      $table->dropSoftDeletes();
    });

	}

}
