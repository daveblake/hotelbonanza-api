<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Policiescheckinouttables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('policies__checkin', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->string('from');
			$table->string('to');
			$table->string('label');
			$table->timestamps();
		});

		\Schema::create('policies__checkout', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->string('from');
			$table->string('to');
			$table->string('label');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('policies__checkin');
		\Schema::drop('policies__checkout');
	}

}
