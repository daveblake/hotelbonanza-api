<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChannelManagerOntoHotels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
      $table->integer('channelmanager_id')->nullable();
      $table->string('channelmanagerother')->nullable();
		});

    //migrate data
    $users = User::all();
    foreach ($users as $user) {
      foreach($user->hotels as $hotel) {
        if ($hotel instanceof Hotel) {
          $hotel->channelmanager_id = $user->channelmanager_id;
          $hotel->channelmanagerother = $user->channelmanagerother;
          $hotel->save([], true);
        }
      }
    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
      $table->dropColumn('channelmanager_id');
      $table->dropColumn('channelmanagerother');
		});
	}

}
