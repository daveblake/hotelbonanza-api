<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCancellationPolicyTypeToText extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('policies__cancellation', function(Blueprint $table)
		{
			//
				$table->dropColumn('charge_id');
				$table->string('charge_type')->after('charge')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('policies__cancellation', function(Blueprint $table)
		{
			//
				$table->dropColumn('charge_type');
				$table->integer('charge_id')->default(0);
		});
	}

}
