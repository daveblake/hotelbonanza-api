<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OauthCustomScopesExtranetAdmin extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::table('oauth_scopes')->insert(
			array('id' => 'admin', 'description' => 'Administrator access only.', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now())
		);

		\DB::table('oauth_scopes')->insert(
			array('id' => 'extranet', 'description' => 'Extranet access to majority of API.', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now())
		);

		if($admin = DB::table('oauth_clients')->where('id', 'hotelbonanzaapi')->first()) {
			\DB::table('oauth_client_scopes')->insert(
				array('client_id' => $admin->id, 'scope_id' => 'admin')
			);
		}

		if($extranet_users = \DB::table('oauth_clients')->where('id', '!=', 'hotelbonanzaapi')->get()) {
			foreach((array)$extranet_users as $extranet) {
				\DB::table('oauth_client_scopes')->insert(
					array('client_id' => $extranet->id, 'scope_id' => 'extranet')
				);
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\DB::table('oauth_scopes')->where('id', 'admin')->delete();
		\DB::table('oauth_scopes')->where('id', 'extranet')->delete();
		\DB::table('oauth_client_scopes')->where('scope_id', 'admin')->delete();
		\DB::table('oauth_client_scopes')->where('scope_id', 'extranet')->delete();
	}

}
