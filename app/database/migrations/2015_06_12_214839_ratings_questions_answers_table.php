<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RatingsQuestionsAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('ratings__questions', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('label');
            $table->timestamps();
        });

        \Schema::create('ratings__answers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('question_id');
            $table->float('rating');
            $table->timestamps();
        });

        \Schema::create('hotels__ratings', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('hotel_id');
            $table->integer('user_id');
            $table->tinyInteger('approved');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('comment')->nullable();
            $table->integer('approval_date')->nullable();
            $table->timestamps();
        });

        \Schema::create('hotels__ratings_rating_answer', function(Blueprint $table) {
            $table->integer('rating_id');
            $table->integer('answer_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('ratings__questions');
        \Schema::drop('ratings__answers');
        \Schema::drop('hotels__ratings');
        \Schema::drop('hotels__ratings_rating_answer');
	}

}
