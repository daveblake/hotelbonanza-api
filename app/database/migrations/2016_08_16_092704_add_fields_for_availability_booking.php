<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsForAvailabilityBooking extends Migration
{

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('availability__bookings', function (Blueprint $table) {
      $table->integer('room_id')->after('booking_id');
      $table->integer('room_rate_id')->after('booking_id');
    });
    Schema::table('availability__bookings_rooms', function (Blueprint $table) {
      $table->decimal('rate',19,4)->after('availability__room_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('availability__bookings', function (Blueprint $table) {
      $table->dropColumn('room_id');
      $table->dropColumn('room_rate_id');
    });
    Schema::table('availability__bookings_rooms', function (Blueprint $table) {
      $table->dropColumn('rate');
    });
	}

}
