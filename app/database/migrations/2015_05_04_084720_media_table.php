<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('media', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('url');
            $table->text('description')->nullable();
            $table->enum('type', ['image', 'video', 'document', 'audio', 'media'])->default('media'); // media covers all
            $table->integer('order')->default(1);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('media');
	}

}
