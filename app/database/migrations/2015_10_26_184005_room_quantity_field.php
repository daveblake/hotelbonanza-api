<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoomQuantityField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('rooms', function(Blueprint $table) {
			$table->integer('quantity')->default(1)->after('sub_type_id');
		});

		//moved to create
		//DB::statement('ALTER TABLE `availability__rooms` MODIFY `quantity` INTEGER(11) NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::table('rooms', function(Blueprint $table) {
			$table->dropColumn('quantity');
		});

		//DB::statement('ALTER TABLE `availability__rooms` MODIFY `quantity` INTEGER(11) NOT NULL;');
	}

}
