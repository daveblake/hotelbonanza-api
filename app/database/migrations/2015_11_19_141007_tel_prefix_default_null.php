<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelPrefixDefaultNull extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//moved to create
		//DB::statement('ALTER TABLE `users` MODIFY telephone_prefix_id int DEFAULT NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//DB::statement('ALTER TABLE `users` MODIFY `telephone_prefix_id` int NOT NULL;');
	}

}
