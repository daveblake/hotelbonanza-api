<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftDeletesForHotelAndUser extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		if(!Schema::hasColumn('users','deleted_at')) {
			Schema::table('users', function (Blueprint $table) {
				$table->softDeletes();
			});
		}
		if(!Schema::hasColumn('hotels','deleted_at')) {
			Schema::table('hotels', function (Blueprint $table) {
				$table->softDeletes();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropSoftDeletes();
		});
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->dropSoftDeletes();
		});
	}

}
