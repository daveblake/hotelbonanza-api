<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hotels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('hotels', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id');
            $table->string('hotel_name');
            $table->string('email');
            $table->string('customer_name');
            $table->string('preferred_name');
            $table->string('password');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('hotels');
	}

}
