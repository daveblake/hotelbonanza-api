<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotelsUuid extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        \Schema::table('hotels', function (Blueprint $table) {
            if (!Schema::hasColumn('hotels','uuid')) {
                $table->string('uuid')->after('user_id')->nullable();
            }
            $table->softDeletes();
        });

        foreach(Hotel::all() as $hotel) {
            if($hotel->uuid == '') {
                $hotel->uuid = $hotel->generateUUID();
                $hotel->save();
            }
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::table('hotels', function(Blueprint $table)
        {
            $table->dropColumn('uuid');
        });
	}

}
