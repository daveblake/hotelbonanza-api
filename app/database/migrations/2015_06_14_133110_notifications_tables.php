<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('notifications__notification', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('message');
            $table->tinyInteger('global')->default(1);
            $table->timestamps();
        });

        \Schema::create('notifications__read', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('notification_id');
            $table->timestamps();
        });

        \Schema::create('users__user_notification', function(Blueprint $table) {
            $table->integer('user_id');
            $table->integer('notification_id');
            $table->tinyInteger('deleted')->nullable();
            $table->date('deleted_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('notifications__notification');
        \Schema::drop('notifications__read');
        \Schema::drop('users__user_notification');
	}
}
