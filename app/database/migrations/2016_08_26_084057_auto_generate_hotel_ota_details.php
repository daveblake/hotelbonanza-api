<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AutoGenerateHotelOtaDetails extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		foreach (Hotel::all() as $hotel) {
		  if ($hotel->ota_hotel_id == null) {
        $hotel->generateSetOTACredentials();
        $hotel->save([],true);
      }
    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
