<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoomsRatesLinksCreateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('rooms__rates_link', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('rate_id');
            $table->enum('type', ['percentage', 'price']);
            $table->float('amount');
            $table->float('total');
            $table->text('description')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('rooms__rates_link');
	}

}
