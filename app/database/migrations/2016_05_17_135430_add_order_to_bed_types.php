<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderToBedTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rooms__beds_types', function(Blueprint $table)
		{
			$table->integer('order')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rooms__beds_types', function(Blueprint $table)
		{
			$table->dropColumn('order');
			$table->dropSoftDeletes();
		});
	}

}
