<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotelsCleanup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(\Schema::hasColumn('hotels', 'email') && \Schema::hasColumn('hotels', 'password'))
        {
            \Schema::table('hotels', function(Blueprint $table)
            {
                $table->dropColumn([
                    'email',
                    'password'
                ]);
            });
        }

        \Schema::table('hotels', function(Blueprint $table)
        {
            $table->integer('user_id')->nullable()->after('type_id');
        });
    
        \Schema::table('hotels', function(Blueprint $table)
        {          
            $table->dropColumn('hotel_name');
        });
        \Schema::table('hotels', function(Blueprint $table)
        {          
            $table->dropColumn('customer_name');
        });
        \Schema::table('hotels', function(Blueprint $table)
        {          
            $table->dropColumn('preferred_name');
        });

        \Schema::table('hotels', function(Blueprint $table)
        {
            $table->string('hotel_name')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('preferred_name')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
    {
        if(\Schema::hasColumn('hotels', 'user_id'))
        {
            \Schema::table('hotels', function(Blueprint $table)
            {
                $table->dropColumn('user_id');
            });
        }
    }
}
