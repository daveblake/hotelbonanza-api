<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckinMessageToHotel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
		public function up()
		{
				\Schema::table('hotels', function(Blueprint $table)
				{
						$table->text('checkinmessage')->nullable()->after('parkingavailable');
				});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
				\Schema::table('hotels', function(Blueprint $table)
				{
						$table->dropColumn('checkinmessage');
				});
		}

}
