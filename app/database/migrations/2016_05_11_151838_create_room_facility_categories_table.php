<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomFacilityCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('room_facilities__categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uuid');
			$table->string('name');
			$table->integer('order')->nullable()->default(0);
			$table->timestamps();
		});

		Schema::table('rooms__facilities', function(Blueprint $table)
		{
			$table->boolean('category_id')->nullable()->after('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('room_facilities__categories');

		Schema::table('rooms__facilities', function(Blueprint $table)
		{
			$table->dropColumn('category_id');
		});
	}

}
