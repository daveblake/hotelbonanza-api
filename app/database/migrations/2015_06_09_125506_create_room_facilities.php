<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomFacilities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
		public function up()
		{
				\Schema::create('rooms__facilities', function(Blueprint $table) {
						$table->increments('id');
						$table->string('uuid');
						$table->string('name');
						$table->timestamps();
				});

				\Schema::create('rooms__room_facilities', function(Blueprint $table) {
						$table->integer('room_id');
						$table->integer('facility_id');
				});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
				\Schema::drop('rooms__facilities');
				\Schema::drop('rooms__room_facilities');
		}

}
