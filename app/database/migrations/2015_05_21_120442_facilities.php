<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Facilities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('facilities', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->integer('category_id')->nullable();
			$table->string('name');
			$table->timestamps();
		});

		\Schema::create('facilities__categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->string('name');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('facilities');
		\Schema::drop('facilities__categories');
	}

}
