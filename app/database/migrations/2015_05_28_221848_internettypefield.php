<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Internettypefield extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('internet', function(Blueprint $table) {
			$table->enum('type', ['wifi', 'lan', 'ethernet', 'none', 'other'])->after('area_id')->default('wifi');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::table('internet', function(Blueprint $table) {
			$table->dropColumn('type');
		});
	}

}
