<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rooms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('rooms', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id', false, true);
            $table->integer('sub_type_id', false, true);
            $table->string('uuid');
            $table->string('name');
            $table->integer('sleeps', false, true);
            $table->integer('bedrooms', false, true);
            $table->integer('lounges', false, true);
            $table->integer('bathrooms', false, true);
            $table->boolean('smoking')->default(0);
            $table->boolean('breakfastincluded')->default(0);
            $table->float('roomsize', 30)->nullable();
            $table->string('roomsizeunit', 30)->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('rooms');
	}

}
