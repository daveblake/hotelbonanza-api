<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinStayMaxStayToAvailabilityRooms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('availability__rooms', function(Blueprint $table)
		{
			$table->integer('min_stay')->default(0);
			$table->integer('max_stay')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('availability__rooms', function(Blueprint $table)
		{
      $table->dropColumn('min_stay');
      $table->dropColumn('max_stay');
		});
	}

}
