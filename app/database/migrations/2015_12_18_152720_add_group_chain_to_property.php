<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupChainToProperty extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('hotels', function(Blueprint $table) {
			$table->boolean('group_chain')->nullable();
			$table->text('group_chain_name')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::table('hotels', function(Blueprint $table) {
			$table->dropColumn('group_chain');
			$table->dropColumn('group_chain_name');
		});
	}

}
