<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdjustTimezoneRelationOnHotel extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('hotels', function (Blueprint $table) {
            $table->dropColumn('timezone');
            $table->integer('timezone_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('hotels', function (Blueprint $table) {
            $table->dropColumn('timezone_id');
            $table->string('timezone')->nullable();
        });
    }

}
