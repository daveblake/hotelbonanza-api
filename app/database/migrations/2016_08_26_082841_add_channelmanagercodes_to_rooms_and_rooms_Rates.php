<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChannelmanagercodesToRoomsAndRoomsRates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		foreach(Room::all() as $room) {
		  if ($room->channelmanagercode == null) {
		    $room->channelmanagercode = $room->id;
        $room->save([],true);
      }
    }
		foreach(RoomRate::all() as $roomRate) {
		  if ($roomRate->channelmanagercode == null) {
        $roomRate->channelmanagercode = $roomRate->id;
        $roomRate->save([],true);
      }
    }
		foreach(Booking::all() as $booking) {
		  if ($booking->booking_ref == null) {
        $booking->booking_ref = $booking->id;
        $booking->save([],true);
      }
    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
