<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('locations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamps();
        });

        \Schema::table('addresses', function(Blueprint $table) {
            $table->integer('location_id')->nullable()->after('id');
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('locations');

        \Schema::table('addresses', function(Blueprint $table) {
            $table->dropColumn('location_id');
        });
	}

}
