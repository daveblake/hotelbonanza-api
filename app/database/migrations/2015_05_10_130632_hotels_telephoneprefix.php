<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotelsTelephoneprefix extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('hotels', function(Blueprint $table) {
            $table->dropColumn('telprefix');
            $table->integer('telephone_prefix_id')->after('address_id')->nullable();
            $table->integer('mobile_prefix_id')->after('telephone_prefix_id')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::table('hotels', function(Blueprint $table) {
            $table->dropColumn('telephone_prefix_id');
            $table->dropColumn('mobile_prefix_id');
            $table->string('telprefix'); // telephone prefixes are lost at this stage
        });
	}

}
