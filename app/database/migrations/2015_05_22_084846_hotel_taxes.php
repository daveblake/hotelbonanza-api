<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotelTaxes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('hotels__taxes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('hotel_id');
			$table->integer('calculation_type_id');
			$table->string('uuid');
			$table->string('name', 50);
			$table->float('amount');
			$table->boolean('inclusive');
			$table->timestamps();
		});

		\Schema::create('calculation__types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->string('name');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('hotels__taxes');
		\Schema::drop('calculation__types');
	}

}
