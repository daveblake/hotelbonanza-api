<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBedroomNumberToRoomLayout extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rooms__layouts', function(Blueprint $table)
		{
			$table->integer('bedroom_number')->after('name')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rooms__layouts', function(Blueprint $table)
		{
			$table->dropColumn('bedroom_number');
		});
	}

}
