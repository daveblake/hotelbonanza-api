<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoomRatesWithThroughTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('rooms__rates', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id');
            $table->string('uuid');
            $table->string('channelmanagercode')->nullable();
            $table->float('baseprice')->nullable();
            $table->timestamps();
        });

        \Schema::create('rooms__rates_rate_policycancellation', function(Blueprint $table) {
            $table->integer('rate_id');
            $table->integer('policycancellation_id');
        });

        \Schema::create('rooms__rates_rate_boardoption', function(Blueprint $table) {
            $table->integer('rate_id');
            $table->integer('boardoption_id');
        });

        \Schema::create('rooms__rates_rate_breakfastoption', function(Blueprint $table) {
            $table->integer('rate_id');
            $table->integer('breakfastoption_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('rooms__rates');
        \Schema::drop('rooms__rates_rate_policycancellation');
        \Schema::drop('rooms__rates_rate_boardoption');
        \Schema::drop('rooms__rates_rate_breakfastoption');
	}

}
