<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTableToLinkItems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings', function(Blueprint $table)
		{
			//
			$table->increments('id');
			$table->string('uuid');
			$table->integer('user_id');
			$table->integer('hotel_id');
			$table->date('date_from');
			$table->date('date_to');
			$table->text('additional_info')->nullable();
			$table->decimal('total', 19, 4);
			$table->decimal('deposit', 19, 4)->default(0)->nullable();
			$table->boolean('bonanza_card')->default(false)->nullable();
			$table->boolean('newsletter_signup')->default(false)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::table('availability__bookings', function(Blueprint $table) {
			$table->integer('booking_id')->after('user_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings');
		Schema::table('availability__bookings', function(Blueprint $table) {
			$table->dropColumn('booking_id');
		});
	}

}
