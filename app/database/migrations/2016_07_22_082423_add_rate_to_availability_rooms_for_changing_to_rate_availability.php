<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRateToAvailabilityRoomsForChangingToRateAvailability extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('availability__rooms', function(Blueprint $table)
		{
			$table->integer('room_rate_id')->after('room_id');
      $table->float('single_occupancy_price')->default(0)->after('rate');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('availability__rooms', function(Blueprint $table)
		{
			$table->dropColumn('room_rate_id');
      $table->dropColumn('single_occupancy_price');
		});
	}

}
