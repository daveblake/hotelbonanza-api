<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Internettable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        \Schema::create('internet', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('area_id');
            $table->integer('time_id');
            $table->decimal('price',19,4);
            $table->timestamps();
        });

        \Schema::create('internet__area', function(Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('name');
            $table->timestamps();
        });

        \Schema::create('hotels__hotel_internet', function(Blueprint $table) {
            $table->integer('hotel_id');
            $table->integer('internet_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::drop('internet');
        \Schema::drop('internet__area');
        \Schema::drop('hotels__hotel_internet');
	}
}
