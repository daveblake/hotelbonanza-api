<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillingContactNameToHotel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->string('billing_contact')->nullable()->after('address_id');
			$table->boolean('billing_different')->default(false)->after('address_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->dropColumn('billing_contact');
			$table->dropColumn('billing_different');
		});
	}

}
