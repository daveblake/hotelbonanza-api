<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Paymentstable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('payments__card_types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->string('name');
			$table->timestamps();
		});

		\Schema::create('hotels__payment_options', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('hotel_id');
			$table->string('uuid');
			$table->boolean('bankTransfer')->default(0);
			$table->boolean('paypal')->default(0);
			$table->boolean('cash')->default(0);
			$table->timestamps();
		});

        \Schema::create('hotels__payment_cardtype', function(Blueprint $table) {
            $table->integer('payment_id');
            $table->integer('card_type_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('payments__card_types');
		\Schema::drop('hotels__payment_options');
		\Schema::drop('hotels__payment_cardtype');
	}

}
