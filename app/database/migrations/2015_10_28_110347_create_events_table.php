<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('events', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->string('from');
			$table->string('to');
			$table->integer('address_id')->nullable();
			$table->string('name');
			$table->text('description');
			$table->timestamps();
		});

		\Schema::create('events__media', function(Blueprint $table) {
			$table->integer('event_id');
			$table->integer('media_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::drop('events');
		\Schema::drop('events__media');
	}

}
