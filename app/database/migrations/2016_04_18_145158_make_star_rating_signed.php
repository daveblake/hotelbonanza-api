<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeStarRatingSigned extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			DB::raw('ALTER TABLE `hotels` CHANGE COLUMN `starRating` `starRating` INT(10) NULL');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			DB::raw('ALTER TABLE `hotels` CHANGE COLUMN `starRating` `starRating` INT(10) UNSIGNED NULL');
		});
	}

}
