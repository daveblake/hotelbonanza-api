<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimeInteger extends Migration
{

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (Schema::hasTable('time')) {
      \Schema::drop('time');
    }
    if (Schema::hasTable('internet__internet_time')) {
      \Schema::drop('internet__internet_time');
    }
    if (Schema::hasTable('parking__parking_time')) {
      \Schema::drop('parking__parking_time');
    }

    \Schema::table('internet__area', function (Blueprint $table) {
      $table->integer('time')->after('name')->nullable();
    });

    \Schema::table('parking__area', function (Blueprint $table) {
      $table->integer('time')->after('name')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    \Schema::create('time', function (Blueprint $table) {
      $table->increments('id');
      $table->string('uuid');
      $table->string('name');
      $table->timestamps();
    });

    \Schema::create('internet__internet_time', function (Blueprint $table) {
      $table->integer('internet_id');
      $table->integer('time_id');
    });

    \Schema::create('parking__parking_time', function (Blueprint $table) {
      $table->integer('parking_id');
      $table->integer('time_id');
    });

    \Schema::table('internet__area', function (Blueprint $table) {
      $table->dropColumn('time');
    });

    \Schema::table('parking__area', function (Blueprint $table) {
      $table->dropColumn('time');
    });
  }

}
