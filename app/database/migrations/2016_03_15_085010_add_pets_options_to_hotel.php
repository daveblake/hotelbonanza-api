<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPetsOptionsToHotel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->tinyInteger('pets_allowed')->default(null)->nullable()->after('parking_reservation_required');
			$table->decimal('pets_charge')->nullable()->default(null)->after('pets_allowed');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->dropColumn('pets_allowed');
			$table->dropColumn('pets_charge');
		});
	}

}
