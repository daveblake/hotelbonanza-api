<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixUserOwnerId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//moved to create
		//\DB::statement('ALTER TABLE users__owners MODIFY COLUMN user_id INT');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// No need to for a down migration because this will reintroduce the bug
	}

}
