<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRoompriceFromBoardoption extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('board__options', function (Blueprint $table) {
            if (Schema::hasColumn('board__options','roomprice')) {
                $table->dropColumn('roomprice');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // roomprice is no longer supported
    }

}
