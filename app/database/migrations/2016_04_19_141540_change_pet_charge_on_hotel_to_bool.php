<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePetChargeOnHotelToBool extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->dropColumn('pets_charge');
		});
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->boolean('pets_charge')->after('pets_allowed')->nullable()->default(-1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->dropColumn('pets_charge');
		});
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->decimal('pets_charge')->nullable()->default(null)->after('pets_allowed');
		});
	}

}
