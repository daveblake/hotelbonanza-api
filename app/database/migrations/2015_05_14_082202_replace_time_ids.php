<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReplaceTimeIds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//remove time columns from areas
		\Schema::table('internet__area', function(Blueprint $table) {
			$table->dropColumn('time');
		});

		\Schema::table('parking__area', function(Blueprint $table) {
			$table->dropColumn('time');
		});

		//remove time_ids from internet/parking
		\Schema::table('internet', function(Blueprint $table) {
			$table->dropColumn('time_id');
		});

		\Schema::table('parking', function(Blueprint $table) {
			$table->dropColumn('time_id');
		});

		//create time fields for internet/parking
		\Schema::table('internet', function(Blueprint $table) {
			$table->integer('time')->after('area_id')->nullable();
		});

		\Schema::table('parking', function(Blueprint $table) {
			$table->integer('time')->after('area_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		\Schema::table('internet__area', function(Blueprint $table) {
			$table->integer('time')->after('name');
		});

		\Schema::table('parking__area', function(Blueprint $table) {
			$table->integer('time')->after('name');
		});


		\Schema::table('internet', function(Blueprint $table) {
			$table->dropColumn('time');
		});

		\Schema::table('parking', function(Blueprint $table) {
			$table->dropColumn('time');
		});


		\Schema::table('internet', function(Blueprint $table) {
			$table->integer('time_id')->after('area_id');
		});

		\Schema::table('parking', function(Blueprint $table) {
			$table->integer('time_id')->after('area_id');
		});
	}

}
