<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AvailabilityLinkedRatesAvailability extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::create('availability__rooms_sub_rates', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uuid');
			$table->integer('room_id');
			$table->integer('rate_id');
			$table->float('margin')->nullable()->default(0);
			$table->enum('sub_rate_type', ['price', 'percentage'])->default('price')->nullable();
			$table->timestamps();
		});

		\Schema::create('availability__rooms_linked', function(Blueprint $table) {
			$table->integer('room_id_a');
			$table->integer('room_id_b');
			$table->primary(['room_id_a', 'room_id_b']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists('availability__rooms_sub_rates');
		\Schema::dropIfExists('availability__rooms_linked');
	}

}
