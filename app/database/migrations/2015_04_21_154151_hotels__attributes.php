<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotelsAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\Schema::table('hotels', function(Blueprint $table) {
            $table->integer('roomcount')->after('password')->default(0)->nullable();
            $table->string('tel', 30)->nullable()->after('roomcount');
            $table->string('telprefix', 10)->nullable()->after('tel');
            $table->string('mobile', 30)->nullable()->after('telprefix');
            $table->string('mobileprefix', 10)->nullable()->after('mobile');
            $table->string('reservationEmail', 50)->nullable()->after('mobileprefix');
            $table->string('reservationSkype', 50)->nullable()->after('reservationEmail');
            $table->string('websiteUrl', 255)->nullable()->after('reservationSkype');
            $table->string('shortDescription', 255)->nullable()->after('websiteUrl');
            $table->text('longDescription')->nullable()->after('shortDescription');
            $table->string('tagline', 50)->nullable()->after('longDescription');
            $table->boolean('childrenallowed')->default(true)->nullable()->after('tagline');
            $table->integer('starRating', false, true)->nullable()->after('childrenallowed');
            $table->integer('userRating', false, true)->nullable()->after('starRating');
            $table->string('heroImage')->nullable()->after('userRating');
            $table->boolean('breakfastIncluded')->default(true)->nullable()->after('heroImage');
            $table->boolean('wifiavailable')->default(true)->nullable()->after('breakfastIncluded');
            $table->boolean('parkingavailable')->default(true)->nullable()->after('wifiavailable');
            $table->text('breakfast')->nullable()->after('parkingavailable');
            $table->boolean('extracribs')->default(true)->nullable()->after('breakfast');
            $table->boolean('extrabeds')->default(true)->nullable()->after('extracribs');
            $table->char('currency', 3)->nullable()->after('extrabeds');
            $table->string('mainImage')->nullable()->after('currency');
            $table->string('timezone', 25)->nullable()->after('mainImage');
            $table->enum('status', ['pending', 'live', 'suspended'])->default('pending')->after('timezone');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        \Schema::table('hotels', function(Blueprint $table)
        {
            $table->dropColumn('roomcount');
            $table->dropColumn('tel');
            $table->dropColumn('telprefix');
            $table->dropColumn('mobile');
            $table->dropColumn('mobileprefix');
            $table->dropColumn('reservationEmail');
            $table->dropColumn('reservationSkype');
            $table->dropColumn('websiteUrl');
            $table->dropColumn('shortDescription');
            $table->dropColumn('longDescription');
            $table->dropColumn('tagline');
            $table->dropColumn('childrenallowed');
            $table->dropColumn('starRating');
            $table->dropColumn('userRating');
            $table->dropColumn('heroImage');
            $table->dropColumn('breakfastIncluded');
            $table->dropColumn('wifiavailable');
            $table->dropColumn('parkingavailable');
            $table->dropColumn('breakfast');
            $table->dropColumn('extracribs');
            $table->dropColumn('extrabeds');
            $table->dropColumn('currency');
            $table->dropColumn('mainImage');
            $table->dropColumn('timezone');
            $table->dropColumn('status');
        });
	}

}
