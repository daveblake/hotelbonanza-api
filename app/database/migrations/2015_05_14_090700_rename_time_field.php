<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTimeField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		\Schema::table('internet', function(Blueprint $table) {
			$table->dropColumn('time');
			$table->integer('seconds')->after('area_id')->nullable();
		});

		\Schema::table('parking', function(Blueprint $table) {
			$table->dropColumn('time');
			$table->integer('seconds')->after('area_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		\Schema::table('internet', function(Blueprint $table) {
			$table->dropColumn('seconds');
			$table->integer('time')->after('area_id');
		});

		\Schema::table('parking', function(Blueprint $table) {
			$table->dropColumn('seconds');
			$table->integer('time')->after('area_id');
		});
	}

}
