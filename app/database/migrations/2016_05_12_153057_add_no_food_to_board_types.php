<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoFoodToBoardTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('board__types', function(Blueprint $table)
		{
			$table->boolean('no_food')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('board__types', function(Blueprint $table)
		{
			$table->dropColumn('no_food');
		});
	}

}
