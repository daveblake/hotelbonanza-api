<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 10:16
 */

namespace HB\Helpers;


use Carbon\Carbon;

class DateRangeHelper
{
  /**
   * Compute a range between two dates, and generate
   * a plain array of Carbon objects of each day in it.
   *
   * @param  \Carbon\Carbon  $from
   * @param  \Carbon\Carbon  $to
   * @param  bool  $inclusive
   * @return array|null
   *
   * @author Tristan Jahier
   */
  static function date_range(Carbon $from, Carbon $to, $inclusive = true)
  {
    if ($from->gt($to)) {
      return null;
    }

    // Clone the date objects to avoid issues, then reset their time
    $from = $from->copy()->startOfDay();
    $to = $to->copy()->startOfDay();

    // Include the end date in the range
    if ($inclusive) {
      $to->addDay();
    }
    $step = new \DateInterval('P1D');
    $period = new \DatePeriod($from, $step, $to);

    // Convert the DatePeriod into a plain array of Carbon objects
    $range = [];

    foreach ($period as $day) {
      $range[] = new Carbon($day);
    }

    return ! empty($range) ? $range : null;
  }
}