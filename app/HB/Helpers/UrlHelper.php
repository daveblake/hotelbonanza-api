<?php
namespace HB\Helpers;

/**
 * Class UrlHelper
 * @package HB\Helpers
 */
class UrlHelper
{

  protected static $_subDomains = ['api', 'ota'];

  private static $_scheme = null;
  private static $_subDomain = null;
  private static $_host = null;
  private static $_port = null;

  static function setConsoleDefaults()
  {
    self::$_scheme    = '';
    self::$_subDomain = '';
    self::$_host      = 'localhost';
    self::$_port      = '';
  }

  static function parseUrl()
  {
    if(\App::runningInConsole())
    {
      self::setConsoleDefaults();
      return;
    }

    $parsedURL = parse_url(\Request::fullUrl());

    self::$_scheme = $parsedURL['scheme'];
    self::$_port   = isset($parsedURL['port']) ? $parsedURL['port'] : '';
    self::$_host   = self::removeSubDomain(
      $parsedURL['host'],
      self::$_subDomain,
      self::$_subDomains
    );
  }

  /**
   * @param $domain
   * @param $subDomain
   * @param array $subDomains
   * @return string
     */
  static function removeSubDomain($domain, &$subDomain, $subDomains = [])
  {
    foreach($subDomains as $subDomain)
    {
      if(substr($domain, 0, strlen($subDomain)) == $subDomain)
      {
        return substr($domain, strlen($subDomain) + 1);
      }
    }
    return $domain;
  }

  /**
   * @return null
   */
  static function getScheme()
  {
    if(is_null(self::$_scheme))
    {
      self::parseUrl();
    }

    return self::$_scheme;
  }

  /**
   * @return null
   */
  static function getSubDomain()
  {
    if(is_null(self::$_subDomain))
    {
      self::parseUrl();
    }

    return self::$_subDomain;
  }

  /**
   * Returns the application host without any of
   * the used subDomains
   *
   * @return string
   */
  static function getHost()
  {
    if(is_null(self::$_host))
    {
      self::parseUrl();
    }

    return self::$_host;
  }

  /**
   * @return null
   */
  static function getPort()
  {
    if(is_null(self::$_port))
    {
      self::parseUrl();
    }

    return self::$_port;
  }

  /**
   * @param string $subDomain
   * @return string
     */
  static function buildUrl($subDomain = 'api')
  {
    if(is_null(self::$_host))
    {
      self::parseUrl();
    }

    if($subDomain != '')
    {
      $subDomain = $subDomain . '.';
    }

    $port = self::getPort();
    if($port != '')
    {
      $port = ':' . $port;
    }

    return self::getScheme() . '://' .
    $subDomain . self::getHost() . $port;
  }
} 