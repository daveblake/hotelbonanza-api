<?php

class Media extends BaseModel
{
    protected $table = 'media';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'url',
        'order',
        'description',
        'type',
        'data'
    );

    /**
     * @var array
     */
    protected $with = array(
        'categories'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'url' => 'required',
    ];

    /**
     * Return the URL of the media
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->url;
    }

    /**
     * Morph media object into a hotel
     *
     * @return mixed
     */
    public function hotel()
    {
        return $this->morphedByMany('Hotel', 'mediaable');
    }

    /**
     * Morph media object into a hotel
     *
     * @return mixed
     */
    public function room()
    {
        return $this->morphedByMany('Room', 'mediaable');
    }

    /**
     * Media has many categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('MediaCategory', 'media__media_category', 'media_id', 'category_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'media';
    }
}