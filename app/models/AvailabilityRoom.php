<?php

use Carbon\Carbon;

class AvailabilityRoom extends BaseModel
{
    protected $table = 'availability__rooms';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'room_id', 'room_rate_id', 'created_at','updated_at', 'pivot');

    protected $dates = array(
      'created_at',
      'updated_at',
      'date'
    );

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'rate',
        'single_occupancy_price',
        'quantity',
        'closed',
        'no_check_in',
        'no_check_out',
        'date'
    );

    /**
     * @var array
     */
    //todo: check these
    protected $with = array(
        'sub',
        'linked',
        'rates'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'rate' => 'required',
        'room_id' => 'required',
        'room_rate_id' => 'required',
        'date' => 'required'
    ];

    /**
     * Each Availability Room has one room
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo('Room', 'room_id');
    }

    /**
     * Each Availability Room has one room
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rates()
    {
        return $this->belongsTo('RoomRate', 'room_rate_id');
    }

    /**
     * Each room has many bookings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bookings()
    {
        return $this->belongsToMany('AvailabilityBooking', 'availability__bookings_rooms', 'availability__room_id', 'availability__booking_id')->orderBy('date_from', 'ASC');
    }

    /**
     * Each room can have many sub rates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    //Todo: check this
    public function sub()
    {
        return $this->hasMany('AvailabilityRoomSub', 'room_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    //Todo: check this
    public function linked()
    {
        return $this->belongsToMany('AvailabilityRoom', 'availability__rooms_linked', 'room_id_a', 'room_id_b');
    }

    /**
     * @param \Carbon\Carbon|null $date
     * @return bool
     */
    //Todo: check this
    public function isAvailable(\Carbon\Carbon $date = null)
    {
        $quantity = $this->quantity;

        if(is_null($date)) {
            $date = \Carbon\Carbon::now();
        }

        $bookings = AvailabilityBooking::findByDateRange($date, $date, $this->id)->get();

        return (bool) $quantity > $bookings->count();
    }

    /**
     * @param \Carbon\Carbon|null $date
     * @return integer
     */
    //Todo: check this
    public function qtyAvailable(\Carbon\Carbon $date = null)
    {
        $quantity = $this->quantity;

        if(is_null($date)) {
            $date = \Carbon\Carbon::now();
        }

        $bookings = AvailabilityBooking::findByDateRange($date, $date, $this->id)->get();

      $available = $quantity - $bookings->count();

        return $available > 0 ? $available : 0;
    }

    const EARTH_RADIUS_MILES = 3959;

    /**
     * Finds all available rooms between a date within a certain location.
     * Takes bookings into account.
     * Location passed will whether the hotel is within said radius
     * Limited to 25 rooms per
     *
     * @param Carbon $from
     * @param Carbon $to
     * @param Location $location
     * @param int $radius
     * @param int $limit
     * @return array
     */
    //Todo: currently returns items based on count of room_rate_ids to match consecutive dates, doesn't handle rate linking yet
    public static function findAvailable(Carbon $from, Carbon $to, Location $location = null, $radius = 25, $limit = 25, $offset = 0, $hotelUuid = null, $autoSubDay = true, $adults = 1)
    {
      if ($autoSubDay) {
        //backdate 1 day on the $to as we're assuming dates are nights (would take out an extra day of availability otherwise)
        //this allows for separate checkin and checkout on the same day
        $to->subDay(1);
      }
        $query = "SELECT ar.uuid as uuid, h.uuid as hotel, r.uuid as room, rr.uuid as room_rate, (MIN(ar.quantity) - COALESCE(bq.quantity, 0)) as quantity, MIN(ar.rate) as rate, SUM(ar.rate) as total_rate, single_occupancy_discount, no_check_in, no_check_out, ar.closed 
FROM `availability__rooms` ar
LEFT JOIN `rooms__rates` rr ON rr.id = ar.room_rate_id
LEFT JOIN rooms r on r.id = ar.room_id
LEFT JOIN `hotels` h ON h.id = r.hotel_id";
      //this bit checks existing bookings and reduces the available quantity
      $query .= "
      LEFT JOIN (SELECT abr.availability__room_id as id, COUNT(abr.availability__room_id) as quantity FROM availability__bookings_rooms AS abr
LEFT JOIN availability__bookings AS ab ON abr.availability__booking_id = ab.id
WHERE ab.`status` NOT IN ('cancelled', 'denied')
GROUP BY abr.availability__room_id) bq ON bq.id = ar.id";

      // Join hotels, address and location to get the availability__rooms.room.hotel.location relation
      if(!is_null($location)) {
        $query .= "
LEFT JOIN `addresses` ad ON ad.id = h.address_id
LEFT JOIN `locations` l ON l.id = ad.location_id
            ";
      }

      $query .= "
WHERE r.sleeps >= ?
AND (ar.closed != 1) AND (ar.closed != true)
AND ar.quantity > 0 AND ar.quantity > COALESCE(bq.quantity,0)
AND ar.rate IS NOT NULL AND ar.rate > 0
AND (ar.`date` BETWEEN ? AND ?)
";

        if(!is_null($hotelUuid)) {
            $query .= "
                AND h.uuid = ?
            ";
        }

        // This part of the query calculates the radius the user passes
        // The location object should contain the latitude / longitude if a valid postcode is passed
        // The radius is set to 50 by default, but the user can also pass this.
        if(!is_null($location)) {
            $query .= "
AND
	(
        l.id IN (SELECT id AS distance FROM locations
WHERE (3959 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin(radians(latitude)) ) ) <= ?)
    )
            ";
        }
        //check against current bookings
//$query .= "
//AND ar.quantity > (SELECT count(*) FROM availability__bookings_rooms AS abr
//LEFT JOIN availability__bookings AS ab ON abr.availability__booking_id = ab.id
//WHERE abr.availability__room_id = ar.id
//AND ab.`status` NOT IN ('cancelled', 'denied'))";

        $query .= "
GROUP BY ar.room_rate_id HAVING COUNT(ar.room_rate_id) = ?
";
//HAVING quantity > (SELECT COUNT(*) FROM availability__bookings AS ab
//LEFT JOIN availability__bookings_rooms AS abr ON abr.availability__room_id = ab.id
//WHERE abr.availability__room_id = ar.id
//AND ab.date_from <= '2016-01-20' AND ab.date_to >= '2016-01-23'
//AND ab.status NOT IN ('cancelled', 'denied'))
//
$query .= "LIMIT ? OFFSET ?";

      $radius = 16;

        $startBindings = [
            $adults,
            $from->format('Y-m-d'),
            $to->format('Y-m-d'),
        ];

        if(!is_null($hotelUuid)) {
            $hotelBindings = [
                $hotelUuid
            ];
        } else {
            $hotelBindings = [];
        }

        // Location affects bindings, if passed, set latitude, longitude as below
        if(!is_null($location)) {
            $locationBindings = [
                (float)$location->latitude,
                (float)$location->longitude,
                (float)$location->latitude,
            ];
        } else {
            $locationBindings = [];
        }

        if (!is_null($location)) {
          $finalBindings = [
            $radius,
            $from->diffInDays($to)+1,
//            $from->format('Y-m-d'),
//            $to->format('Y-m-d'),
            $limit,
            $offset
          ];
        } else {
          $finalBindings = [
            $from->diffInDays($to)+1,
//            $from->format('Y-m-d'),
//            $to->format('Y-m-d'),
            $limit,
            $offset
          ];
        }

        $bindings = array_merge($startBindings,$hotelBindings);
        $bindings = array_merge($bindings,$locationBindings);
        $bindings = array_merge($bindings,$finalBindings);

        $results = \DB::select($query, $bindings);

        return $results;
    }

    /**
     * Find many availability__rooms by room_id
     *
     * @param Room $room
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function findByRoom(Room $room)
    {
        return static::query()->where('room_id', $room->id)->get();
    }

    /**
     * Fetch quantity from Room otherwise fetch the overridden value
     *
     * @return string
     */
    //todo: check this if still needed?
    public function getQuantityAttribute()
    {
        $quantity = $this->attributes['quantity'];

        if(is_null($quantity)) {
            if($this->room instanceof Room) {
              $quantity = $this->room->quantity;
            } elseif ($this->rates instanceof RoomRate && $this->rates->room instanceof Room) {
              $quantity = $this->rates->room->quantity;
            }
        }

        return $quantity;
    }

    /**
     * Find many availability__rooms by date & room_id & rate_id
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]|\Illuminate\Database\Eloquent\Model|null|\Illuminate\Database\Eloquent\Builder
     * @throws Exception
     */
    public static function findByDate(Carbon $date, Room $room, RoomRate $roomRate)
    {
        return static::query()->where('date', $date->format('Y-m-d'))->where('room_id', $room->id)->where('room_rate_id', $roomRate->id)->first();
    }

  public static function findForBooking(Carbon $from, Carbon $to, Room $room, RoomRate $roomRate, $autoSubDay = true)
  {
    if ($autoSubDay) {
      //backdate 1 day on the $to as we're assuming dates are nights (would take out an extra day of availability otherwise)
      //this allows for separate checkin and checkout on the same day
      $to->subDay(1);
    }
    $results = static::query()->whereBetween('date', [$from->format('Y-m-d'), $to->format('Y-m-d')])->where('room_id', $room->id)->where('room_rate_id', $roomRate->id)->get();
    if ($results->count() < ($from->diffInDays($to) + 1)) {
      return false;
    }
    return $results;
  }

    /**
     * Find available rooms based on:
     *
     * Not closed
     * Today's date is between "Starts" and "Ends"
     * Days either contains todays shorthand version OR is NULL/false
     *
     * Additional logic required to find the location of the Room (through hotel relation $this->room->hotel)
     *
     * @return mixed
     */
    //todo: check this
    public static function findToday()
    {
        $today = \Carbon\Carbon::today();

        return static::query()
            ->where('rate','>',0)
            ->whereHas('rates',function($query) {
              //nothing needed here?
            })
            ->where(function($query) use ($today) {
                $query
                    ->where('date', $today->toDateString());
            })
            ->where(function($query) {
                $query
                    ->where('closed', null)
                    ->orWhere('closed', false)
                    ->orWhere('closed', 0);
            })
            ->orderBy('rate', 'ASC');
    }

    /**
     * Return all AvailabilityRooms based on the users hotels assignment
     *
     * @param User $user
     * @param int $limit
     * @param int $offset
     */
    //todo: check this
    public static function getMine(User $user, $limit = 10, $offset = 0)
    {
        return AvailabilityRoom::query()
            ->with(['room.hotel','room.subtype','room.rates.room.subtype','room.rates.cancellationpolicies'])
            ->whereHas('room', function($room) use ($user) {
                $room->whereHas('hotel', function($hotel) use ($user) {
                    $hotel->whereIn('id', $user->hotels->lists('id'));
                });
            })
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'availability room';
    }
}