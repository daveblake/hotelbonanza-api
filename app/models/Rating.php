<?php

class Rating extends BaseModel
{
    const API_DEFAULT_LIMIT = 10;
    const API_DEFAULT_OFFSET = 0;

    use Traits\Paginator;

    protected $table = 'hotels__ratings';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','user_id','hotel_id','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'email',
        'comment'
    );

    /**
     * Load relations
     *
     * @var array
     */
    protected $with = array(
        'answers'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [];

    /**
     * Each rating belongs to a hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('Hotel', 'hotel_id');
    }


    /**
     * Each rating belongs to a user?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Each rating has many questions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function answers()
    {
        return $this->belongsToMany('RatingAnswer', 'hotels__ratings_rating_answer', 'answer_id', 'rating_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'rating';
    }

    /**
     * Find all rating and attach rating dynamically
     *
     * @param array $relations
     * @param null $uuid
     * @param bool $approved
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function findManyWithRating($relations = array(), $uuid = null, $approved = null)
    {
        $self = new static();

        $ratings = Rating::with($relations);

        // Must be either ture/false
        if(is_bool($approved)) {
            $ratings->where('approved', $approved);
        }

        $ratings = $ratings
            ->skip($self->getOffset())
            ->take($self->getLimit())
            ->get();

        foreach($ratings as $rating) {
            $rating->setAttribute('rating', static::calculateRating($rating->answers));
        }

        return $ratings;
    }

    /**
     * Override the findByUUIDorFail method to attach the rating
     *
     * @param $uuid
     * @param array $relations
     * @param bool $approved
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return \Illuminate\Database\Eloquent\Model|null|void|static
     */
    public static function findByUUIDorFail($uuid, $relations = array(), $approved = null)
    {
        $rating = parent::findByUUIDorFail($uuid, $relations = array());

        if(is_bool($approved) && !$rating->approved) {
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException('Rating Not Found');
        }

        $rating->setAttribute('rating', static::calculateRating($rating->answers));

        return $rating;
    }

    /**
     * Calculate the rating to n decimal place
     *
     * @param array $answers
     * @param int $decimal
     * @return string
     */
    protected static function calculateRating($answers, $decimal = 1)
    {
        $rating = 0;
        $count = count($answers) > 0 ? count($answers) : 1;

        foreach($answers as $answer) {
            $rating += $answer->rating;
        }

        return number_format(((float)$rating / $count), $decimal, '.', ',');
    }

    public static function calculateRatingFromArray($answers, $decimal = 1)
    {
        $rating = 0;
        $count = count($answers) > 0 ? count($answers) : 1;

        foreach($answers as $answer) {
            $rating += $answer['rating'];
        }

        return number_format(((float)$rating / $count), $decimal, '.', ',');
    }
}