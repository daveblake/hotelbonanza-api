<?php

abstract class BasePolicy extends BaseModel implements Interfaces\PolicyInterface
{
    public function policy()
    {
        return $this->morphOne('Policy', 'type');
    }

    public static $relationships = [];
}