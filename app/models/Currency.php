<?php

class Currency extends BaseModel
{
    protected $table = 'currencies';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'symbol'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required',
        'symbol' => 'required',
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'currency';
    }
}
