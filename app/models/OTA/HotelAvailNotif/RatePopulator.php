<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 14:12
 */

namespace OTA\HotelAvailNotif;


class RatePopulator
{
  protected $bookingLimit = false;
  protected $start = false;
  protected $end = false;
  protected $rateCode = false;
  protected $roomCode = false;
  protected $minStay = false;
  protected $maxStay = false;
  protected $closed = false;
  protected $no_check_in = false;
  protected $no_check_out = false;
  protected $minimumCountWarning = [];

  /**
   * @return boolean
   */
  public function isBookingLimit()
  {
    return $this->bookingLimit != null;
  }
  public function getBookingLimit()
  {
    return $this->bookingLimit;
  }

  /**
   * @param $bookingLimit
   * @return RatePopulator
   */
  public function setBookingLimit($bookingLimit)
  {
    $this->bookingLimit = $bookingLimit;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isStart()
  {
    return $this->start != null;
  }
  public function getStart()
  {
    return $this->start;
  }

  /**
   * @param $start
   * @return RatePopulator
   */
  public function setStart($start)
  {
    $this->start = $start;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isEnd()
  {
    return $this->end != null;
  }
  public function getEnd()
  {
    return $this->end;
  }

  /**
   * @param $end
   * @return RatePopulator
   */
  public function setEnd($end)
  {
    $this->end = $end;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isRateCode()
  {
    return $this->rateCode != null;
  }
  public function getRateCode()
  {
    return $this->rateCode;
  }

  /**
   * @param $rateCode
   * @return RatePopulator
   */
  public function setRateCode($rateCode)
  {
    $this->rateCode = $rateCode;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isRoomCode()
  {
    return $this->roomCode != null;
  }
  public function getRoomCode()
  {
    return $this->roomCode;
  }

  /**
   * @param $roomCode
   * @return RatePopulator
   */
  public function setRoomCode($roomCode)
  {
    $this->roomCode = $roomCode;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isMinStay()
  {
    return $this->minStay != null;
  }
  public function getMinStay()
  {
    return $this->minStay;
  }

  /**
   * @param $minStay
   * @return RatePopulator
   */
  public function setMinStay($minStay)
  {
    $this->minStay = $minStay;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isMaxStay()
  {
    return $this->maxStay != null;
  }
  public function getMaxStay()
  {
    return $this->maxStay;
  }

  /**
   * @param $maxStay
   * @return RatePopulator
   */
  public function setMaxStay($maxStay)
  {
    $this->maxStay = $maxStay;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isClosed()
  {
    return $this->closed != null;
  }
  public function getClosed()
  {
    return $this->closed;
  }

  /**
   * @param $closed
   * @return RatePopulator
   */
  public function setClosed($closed)
  {
    $this->closed = $closed;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isNoCheckIn()
  {
    return $this->no_check_in != null;
  }
  public function getNoCheckIn()
  {
    return $this->no_check_in;
  }

  /**
   * @param $no_check_in
   * @return RatePopulator
   */
  public function setNoCheckIn($no_check_in)
  {
    $this->no_check_in = $no_check_in;
    return $this;
  }

  /**
   * @return boolean
   */
  public function isNoCheckOut()
  {
    return $this->no_check_out != null;
  }
  public function getNoCheckOut()
  {
    return $this->no_check_out;
  }

  /**
   * @param $no_check_out
   * @return RatePopulator
   */
  public function setNoCheckOut($no_check_out)
  {
    $this->no_check_out = $no_check_out;
    return $this;
  }

  /**
   * @return array
   */
public function getMinimumCountWarnings()
{
  return $this->minimumCountWarning;
}/**
 * @param array $minimumCountWarning
 * @return RatePopulator
 */
public function setMinimumCountWarning($minimumCountWarning)
{
  $this->minimumCountWarning = $minimumCountWarning;
  return $this;
}

public function addMinimumCountWarning($item)
{
  $this->minimumCountWarning[] = $item;
  return $this;
}



}