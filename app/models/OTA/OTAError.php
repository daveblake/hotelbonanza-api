<?php
namespace OTA;


use Exceptions\OTA\OTAWarningInvalidStatusException;

class OTAError
{
  protected $_type = null;
  protected $_code = null;
  protected $_recordID = null;
  protected $_status = null;
  protected $_short_text = null;

  public function __construct($shortText = null, $status = null, $recordID = null, $type = null, $code = null)
  {
    $this->_type = $type;
    $this->_code = $code;
    $this->_short_text = $shortText;
    $this->_recordID = $recordID;
    switch ($status) {
      case 'Complete':
      case true:
        $this->setStatusComplete();
        break;
      case 'NotProcessed':
      case false:
        $this->setStatusNotProcessed();
        break;
      case null:
        //do nothing
        break;
      default:
        throw new OTAWarningInvalidStatusException($status.' is not a valid warning status. Should be in \'Complete,NotProcessed\'');
    }
  }

  /**
   * @return null
   */
  public function getType()
  {
    return $this->_type;
  }

  /**
   * @param null $type
   * @return OTAWarning
   */
  public function setType($type)
  {
    $this->_type = $type;
    return $this;
  }

  /**
   * @return null
   */
  public function getCode()
  {
    return $this->_code;
  }

  /**
   * @param null $code
   * @return OTAWarning
   */
  public function setCode($code)
  {
    $this->_code = $code;
    return $this;
  }

  /**
   * @return null
   */
  public function getStatus()
  {
    return $this->_status;
  }

  /**
   * @param null $status
   * @return OTAWarning
   */
  public function setStatusComplete()
  {
    $this->_status = 'Complete';
    return $this;
  }
  /**
   * @param null $status
   * @return OTAWarning
   */
  public function setStatusNotProcessed()
  {
    $this->_status = 'NotProcessed';
    return $this;
  }

  /**
   * @return null
   */
  public function getRecordID()
  {
    return $this->_recordID;
  }

  /**
   * @param null $recordID
   * @return OTAError
   */
  public function setRecordID($recordID)
  {
    $this->_recordID = $recordID;
    return $this;
  }

  /**
   * @return null
   */
  public function getShortText()
  {
    return $this->_short_text;
  }

  /**
   * @param null $short_text
   * @return OTAWarning
   */
  public function setShortText($short_text)
  {
    $this->_short_text = $short_text;
    return $this;
  }

  public function toXML()
  {
    $data = '<Error ';
    if ($this->getType() != null) {
      $data .= 'Type="'.$this->getType().'" ';
    }
    if ($this->getCode() != null) {
      $data .= 'Code="'.$this->getCode().'" ';
    }
    if ($this->getRecordID() != null) {
      $data .= 'RecordID="'.$this->getRecordID().'" ';
    }
    if ($this->getStatus() != null) {
      $data .= 'Status="'.$this->getStatus().'" ';
    }
    if ($this->getShortText() != null) {
      $data .= 'ShortText="'.$this->getShortText().'" ';
    }
    $data .= '/>';
    return $data;
  }


}