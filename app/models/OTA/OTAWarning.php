<?php
namespace OTA;


use Exceptions\OTA\OTAWarningInvalidStatusException;

class OTAWarning
{
  protected $_type = null;
  protected $_code = null;
  protected $_status = null;
  protected $_short_text = null;

  public function __construct($shortText = null, $processedStatus = null, $type = null, $code = null)
  {
    $this->_type = $type;
    $this->_code = $code;
    $this->_short_text = $shortText;
    switch ($processedStatus) {
      case 'Complete':
      case true:
        $this->setStatusComplete();
        break;
      case 'NotProcessed':
      case false:
        $this->setStatusNotProcessed();
        break;
      case null:
        //do nothing
        break;
      default:
        throw new OTAWarningInvalidStatusException($processedStatus.' is not a valid warning status. Should be in \'Complete,NotProcessed\'');
    }
  }

  /**
   * @return null
   */
  public function getType()
  {
    return $this->_type;
  }

  /**
   * @param null $type
   * @return OTAWarning
   */
  public function setType($type)
  {
    $this->_type = $type;
    return $this;
  }

  /**
   * @return null
   */
  public function getCode()
  {
    return $this->_code;
  }

  /**
   * @param null $code
   * @return OTAWarning
   */
  public function setCode($code)
  {
    $this->_code = $code;
    return $this;
  }

  /**
   * @return null
   */
  public function getStatus()
  {
    return $this->_status;
  }

  /**
   * @param null $status
   * @return OTAWarning
   */
  public function setStatusComplete()
  {
    $this->_status = 'Complete';
    return $this;
  }
  /**
   * @param null $status
   * @return OTAWarning
   */
  public function setStatusNotProcessed()
  {
    $this->_status = 'NotProcessed';
    return $this;
  }

  /**
   * @return null
   */
  public function getShortText()
  {
    return $this->_short_text;
  }

  /**
   * @param null $short_text
   * @return OTAWarning
   */
  public function setShortText($short_text)
  {
    $this->_short_text = $short_text;
    return $this;
  }

  public function toXML()
  {
    $data = '<Warning ';
    if ($this->getType() != null) {
      $data .= 'Type="'.$this->getType().'" ';
    }
    if ($this->getCode() != null) {
      $data .= 'Code="'.$this->getCode().'" ';
    }
    if ($this->getStatus() != null) {
      $data .= 'Status="'.$this->getStatus().'" ';
    }
    if ($this->getShortText() != null) {
      $data .= 'ShortText="'.$this->getShortText().'" ';
    }
    $data .= '/>';
    return $data;
  }


}