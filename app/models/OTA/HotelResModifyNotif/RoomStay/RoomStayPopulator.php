<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 15:55
 */

namespace OTA\HotelResModifyNotif\RoomStay;


use OTA\HotelResModifyNotif\RoomStay\RatePlan\RatePlanPopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomRate\RoomRatePopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomType\RoomTypePopulator;
use OTA\HotelResModifyNotif\RoomStay\SpecialRequest\SpecialRequestPopulator;

class RoomStayPopulator
{
  protected $roomTypes = [];
  protected $ratePlans = [];
  protected $roomRates = [];
  protected $guestCounts = [];
  protected $total = null;
  protected $propertyInfo = null;
  protected $resGuestRPHs = [];
  protected $specialRequests = [];
  protected $serviceRPHs = [];
  protected $totalDecimalPlaces = null;
  protected $totalCurrencyCode = null;
  protected $indexNumber = null;

  /**
   * @return array
   */
  public function getRoomTypes()
  {
    return $this->roomTypes;
  }

  /**
   * @param array $roomTypes
   */
  public function setRoomTypes($roomTypes)
  {
    $this->roomTypes = $roomTypes;
    return $this;
  }

  public function addRoomType(RoomTypePopulator $item)
  {
    $this->roomTypes[] = $item;
    return $this;
  }

  /**
   * @return array
   */
  public function getRatePlans()
  {
    return $this->ratePlans;
  }

  /**
   * @param array $ratePlans
   */
  public function setRatePlans($ratePlans)
  {
    $this->ratePlans = $ratePlans;
    return $this;
  }
  public function addRatePlan(RatePlanPopulator $ratePlan)
  {
    $this->ratePlans[] = $ratePlan;
    return $this;
  }

  /**
   * @return array
   */
  public function getRoomRates()
  {
    return $this->roomRates;
  }

  /**
   * @param array $roomRates
   */
  public function setRoomRates($roomRates)
  {
    $this->roomRates = $roomRates;
    return $this;
  }
  public function addRoomRate(RoomRatePopulator $roomRates)
  {
    $this->roomRates[] = $roomRates;
    return $this;
  }

  /**
   * @return array
   */
  public function getGuestCounts()
  {
    return $this->guestCounts;
  }

  /**
   * @param array $guestCounts
   */
  public function setGuestCounts($guestCounts)
  {
    $this->guestCounts = $guestCounts;
    return $this;
  }
  public function addGuestCounts($guestCounts)
  {
    $this->guestCounts[] = $guestCounts;
    return $this;
  }

  /**
   * @return null
   */
  public function getTotal()
  {
    return $this->total;
  }

  /**
   * @param null $total
   */
  public function setTotal($total)
  {
    $this->total = $total;
    return $this;
  }

  /**
   * @return null
   */
  public function getPropertyInfoHotelCode()
  {
    return $this->propertyInfo;
  }

  /**
   * @param null $propertyInfo
   */
  public function setPropertyInfoHotelCode($propertyInfo)
  {
    $this->propertyInfo = $propertyInfo;
    return $this;
  }

  /**
   * @return array
   */
  public function getResGuestRPHs()
  {
    return $this->resGuestRPHs;
  }

  /**
   * @param array $resGuestRPHs
   */
  public function setResGuestRPHs($resGuestRPHs)
  {
    $this->resGuestRPHs = $resGuestRPHs;
    return $this;
  }
  public function addResGuestRPH($resGuestRPH)
  {
    $this->resGuestRPHs[] = $resGuestRPH;
    return $this;
  }

  /**
   * @return array
   */
  public function getSpecialRequests()
  {
    return $this->specialRequests;
  }

  /**
   * @param array $specialRequests
   */
  public function setSpecialRequests($specialRequests)
  {
    $this->specialRequests = $specialRequests;
    return $this;
  }
  public function addSpecialRequest(SpecialRequestPopulator $specialRequests)
  {
    $this->specialRequests[] = $specialRequests;
    return $this;
  }

  /**
   * @return array
   */
  public function getServiceRPHs()
  {
    return $this->serviceRPHs;
  }

  /**
   * @param array $serviceRPHs
   */
  public function setServiceRPHs($serviceRPHs)
  {
    $this->serviceRPHs = $serviceRPHs;
    return $this;
  }
  public function addServiceRPH($serviceRPH)
  {
    $this->serviceRPHs[] = $serviceRPH;
    return $this;
  }

  /**
   * @return null
   */
  public function getTotalDecimalPlaces()
  {
    return $this->totalDecimalPlaces;
  }

  /**
   * @param null $totalDecimalPlaces
   * @return RoomStayPopulator
   */
  public function setTotalDecimalPlaces($totalDecimalPlaces)
  {
    $this->totalDecimalPlaces = $totalDecimalPlaces;
    return $this;
  }

  /**
   * @return null
   */
  public function getTotalCurrencyCode()
  {
    return $this->totalCurrencyCode;
  }

  /**
   * @param null $totalCurrencyCode
   * @return RoomStayPopulator
   */
  public function setTotalCurrencyCode($totalCurrencyCode)
  {
    $this->totalCurrencyCode = $totalCurrencyCode;
    return $this;
  }

  /**
   * @return null
   */
  public function getIndexNumber()
  {
    return $this->indexNumber;
  }

  /**
   * @param null $indexNumber
   * @return RoomStayPopulator
   */
  public function setIndexNumber($indexNumber)
  {
    $this->indexNumber = $indexNumber;
    return $this;
  }

}