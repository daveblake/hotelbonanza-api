<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 16:29
 */

namespace OTA\HotelResModifyNotif\RoomStay\SpecialRequest;


class SpecialRequestPopulator
{
  protected $name = null;
  protected $text = null;

  /**
   * @return null
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param null $name
   * @return SpecialRequestPopulator
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * @return null
   */
  public function getText()
  {
    return $this->text;
  }

  /**
   * @param null $text
   * @return SpecialRequestPopulator
   */
  public function setText($text)
  {
    $this->text = $text;
    return $this;
  }
}