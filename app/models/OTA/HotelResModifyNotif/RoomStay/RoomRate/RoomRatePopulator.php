<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 16:24
 */

namespace OTA\HotelResModifyNotif\RoomStay\RoomRate;


use Carbon\Carbon;
use OTA\HotelResModifyNotif\RoomStay\RoomRate\Rate\RatePopulator;

class RoomRatePopulator
{
  protected $effectiveDate = null;
  protected $ratePlanCode = null;
  protected $rates = [];

  /**
   * @return null
   */
  public function getEffectiveDate()
  {
    return $this->effectiveDate;
  }

  /**
   * @return Carbon
   */
  public function getEffectiveDateAsCarbon()
  {
    return Carbon::createFromFormat('Y-m-d',$this->effectiveDate);
  }

  /**
   * @param null $effectiveDate
   * @return RoomRatePopulator
   */
  public function setEffectiveDate($effectiveDate)
  {
    $this->effectiveDate = $effectiveDate;
    return $this;
  }

  /**
   * @return null
   */
  public function getRatePlanCode()
  {
    return $this->ratePlanCode;
  }

  /**
   * @param null $ratePlanCode
   * @return RoomRatePopulator
   */
  public function setRatePlanCode($ratePlanCode)
  {
    $this->ratePlanCode = $ratePlanCode;
    return $this;
  }

  /**
   * @return array|RatePopulator[]
   */
  public function getRates()
  {
    return $this->rates;
  }

  /**
   * @param array $rates
   * @return RoomRatePopulator
   */
  public function setRates($rates)
  {
    $this->rates = $rates;
    return $this;
  }

  public function addRate(RatePopulator $item)
  {
    $this->rates[] = $item;
    return $this;
  }


}