<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 16:25
 */

namespace OTA\HotelResModifyNotif\RoomStay\RoomRate\Rate;


class RatePopulator
{
  protected $total = null;
  protected $decimalPlaces = null;
  protected $currencyCode = null;

  /**
   * @return null
   */
  public function getTotal()
  {
    return $this->total;
  }

  /**
   * @param null $total
   * @return RatePopulator
   */
  public function setTotal($total)
  {
    $this->total = $total;
    return $this;
  }

  /**
   * @return null
   */
  public function getDecimalPlaces()
  {
    return $this->decimalPlaces;
  }

  /**
   * @param null $decimalPlaces
   * @return RatePopulator
   */
  public function setDecimalPlaces($decimalPlaces)
  {
    $this->decimalPlaces = $decimalPlaces;
    return $this;
  }

  /**
   * @return null
   */
  public function getCurrencyCode()
  {
    return $this->currencyCode;
  }

  /**
   * @param null $currencyCode
   * @return RatePopulator
   */
  public function setCurrencyCode($currencyCode)
  {
    $this->currencyCode = $currencyCode;
    return $this;
  }


}