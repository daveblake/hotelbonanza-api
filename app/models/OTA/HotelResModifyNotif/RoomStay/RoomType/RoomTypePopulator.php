<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 16:19
 */

namespace OTA\HotelResModifyNotif\RoomStay\RoomType;


class RoomTypePopulator
{
  protected $code = null;
  protected $descriptionName = null;
  protected $descriptionText = null;
  protected $descriptionMealPlan = null;
  protected $descriptionMaxChildren = null;
  protected $amenities = [];

  /**
   * @return null
   */
  public function getCode()
  {
    return $this->code;
  }

  /**
   * @param null $code
   * @return RoomTypePopulator
   */
  public function setCode($code)
  {
    $this->code = $code;
    return $this;
  }

  /**
   * @return null
   */
  public function getDescriptionName()
  {
    return $this->descriptionName;
  }

  /**
   * @param null $descriptionName
   * @return RoomTypePopulator
   */
  public function setDescriptionName($descriptionName)
  {
    $this->descriptionName = $descriptionName;
    return $this;
  }

  /**
   * @return null
   */
  public function getDescriptionText()
  {
    return $this->descriptionText;
  }

  /**
   * @param null $descriptionText
   * @return RoomTypePopulator
   */
  public function setDescriptionText($descriptionText)
  {
    $this->descriptionText = $descriptionText;
    return $this;
  }

  /**
   * @return null
   */
  public function getDescriptionMealPlan()
  {
    return $this->descriptionMealPlan;
  }

  /**
   * @param null $descriptionMealPlan
   * @return RoomTypePopulator
   */
  public function setDescriptionMealPlan($descriptionMealPlan)
  {
    $this->descriptionMealPlan = $descriptionMealPlan;
    return $this;
  }

  /**
   * @return null
   */
  public function getDescriptionMaxChildren()
  {
    return $this->descriptionMaxChildren;
  }

  /**
   * @param null $descriptionMaxChildren
   * @return RoomTypePopulator
   */
  public function setDescriptionMaxChildren($descriptionMaxChildren)
  {
    $this->descriptionMaxChildren = $descriptionMaxChildren;
    return $this;
  }

  /**
   * @return array
   */
  public function getAmenities()
  {
    return $this->amenities;
  }

  /**
   * @param array $amenities
   * @return RoomTypePopulator
   */
  public function setAmenities($amenities)
  {
    $this->amenities = $amenities;
    return $this;
  }
  public function addAmenity($amenity) {
    $this->amenities[] = $amenity;
    return $this;
  }

}