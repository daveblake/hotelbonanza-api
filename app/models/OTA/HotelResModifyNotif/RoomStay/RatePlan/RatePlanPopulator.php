<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 16:21
 */

namespace OTA\HotelResModifyNotif\RoomStay\RatePlan;


class RatePlanPopulator
{
  protected $commissionAmount = null;
  protected $commissionDecimalPlaces = null;
  protected $commissionCurrencyCode = null;

  /**
   * @return null
   */
  public function getCommissionAmount()
  {
    return $this->commissionAmount;
  }

  /**
   * @param null $commissionAmount
   * @return RatePlanPopulator
   */
  public function setCommissionAmount($commissionAmount)
  {
    $this->commissionAmount = $commissionAmount;
    return $this;
  }

  /**
   * @return null
   */
  public function getCommissionDecimalPlaces()
  {
    return $this->commissionDecimalPlaces;
  }

  /**
   * @param null $commissionDecimalPlaces
   * @return RatePlanPopulator
   */
  public function setCommissionDecimalPlaces($commissionDecimalPlaces)
  {
    $this->commissionDecimalPlaces = $commissionDecimalPlaces;
    return $this;
  }

  /**
   * @return null
   */
  public function getCommissionCurrencyCode()
  {
    return $this->commissionCurrencyCode;
  }

  /**
   * @param null $commissionCurrencyCode
   * @return RatePlanPopulator
   */
  public function setCommissionCurrencyCode($commissionCurrencyCode)
  {
    $this->commissionCurrencyCode = $commissionCurrencyCode;
    return $this;
  }


}