<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 15:59
 */

namespace OTA\HotelResModifyNotif\ResGuest;


use OTA\HotelResModifyNotif\ResGuest\Profile\ProfilePopulator;

class ResGuestPopulator
{
  protected $RPH = null;
  protected $profiles = [];

  /**
   * @return null
   */
  public function getRPH()
  {
    return $this->RPH;
  }

  /**
   * @param null $RPH
   * @return ResGuestPopulator
   */
  public function setRPH($RPH)
  {
    $this->RPH = $RPH;
    return $this;
  }

  /**
   * @return array
   */
  public function getProfiles()
  {
    return $this->profiles;
  }

  /**
   * @param array $profiles
   * @return ResGuestPopulator
   */
  public function setProfiles($profiles)
  {
    $this->profiles = $profiles;
    return $this;
  }
  public function addProfile(ProfilePopulator $profiles)
  {
    $this->profiles[] = $profiles;
    return $this;
  }


}