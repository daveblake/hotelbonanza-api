<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 16:32
 */

namespace OTA\HotelResModifyNotif\ResGuest\Profile;

use OTA\HotelResModifyNotif\ResGuest\Profile\Address\AddressPopulator;

/**
 * this model currently represents a generation of nodes:
 *   <ProfileInfo>
 *      <Profile>
 *        <Customer>
 *          THIS MODEL
 *        </Customer>
 *      </Profile>
 *  </ProfileInfo>
 *
 */
class ProfilePopulator
{
  protected $name = null;
  protected $surname = null;
  protected $telephone = null;
  protected $email = null;
  protected $address = null;

  /**
   * @return null
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param null $name
   * @return ProfilePopulator
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * @return null
   */
  public function getSurname()
  {
    return $this->surname;
  }

  /**
   * @param null $surname
   * @return ProfilePopulator
   */
  public function setSurname($surname)
  {
    $this->surname = $surname;
    return $this;
  }

  /**
   * @return null
   */
  public function getTelephone()
  {
    return $this->telephone;
  }

  /**
   * @param null $telephone
   * @return ProfilePopulator
   */
  public function setTelephone($telephone)
  {
    $this->telephone = $telephone;
    return $this;
  }

  /**
   * @return null
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param null $email
   * @return ProfilePopulator
   */
  public function setEmail($email)
  {
    $this->email = $email;
    return $this;
  }

  /**
   * @return null
   */
  public function getAddress()
  {
    return $this->address;
  }

  /**
   * @param null $address
   * @return ProfilePopulator
   */
  public function setAddress(AddressPopulator $address)
  {
    $this->address = $address;
    return $this;
  }
}