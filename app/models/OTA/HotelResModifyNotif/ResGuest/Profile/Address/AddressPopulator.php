<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 16:37
 */

namespace OTA\HotelResModifyNotif\ResGuest\Profile\Address;


class AddressPopulator
{
  protected $line1 = null;
  protected $line2 = null;
  protected $line3 = null;
  protected $city = null;
  protected $postcode = null;
  protected $countryName = null;
  protected $countryCode = null;
  protected $companyName = null;

  /**
   * @return null
   */
  public function getLine1()
  {
    return $this->line1;
  }

  /**
   * @param null $line1
   * @return AddressPopulator
   */
  public function setLine1($line1)
  {
    $this->line1 = $line1;
    return $this;
  }

  /**
   * @return null
   */
  public function getLine2()
  {
    return $this->line2;
  }

  /**
   * @param null $line2
   * @return AddressPopulator
   */
  public function setLine2($line2)
  {
    $this->line2 = $line2;
    return $this;
  }

  /**
   * @return null
   */
  public function getLine3()
  {
    return $this->line3;
  }

  /**
   * @param null $line3
   * @return AddressPopulator
   */
  public function setLine3($line3)
  {
    $this->line3 = $line3;
    return $this;
  }

  /**
   * @return null
   */
  public function getCity()
  {
    return $this->city;
  }

  /**
   * @param null $city
   * @return AddressPopulator
   */
  public function setCity($city)
  {
    $this->city = $city;
    return $this;
  }

  /**
   * @return null
   */
  public function getPostcode()
  {
    return $this->postcode;
  }

  /**
   * @param null $postcode
   * @return AddressPopulator
   */
  public function setPostcode($postcode)
  {
    $this->postcode = $postcode;
    return $this;
  }

  /**
   * @return null
   */
  public function getCountryName()
  {
    return $this->countryName;
  }

  /**
   * @param null $countryName
   * @return AddressPopulator
   */
  public function setCountryName($countryName)
  {
    $this->countryName = $countryName;
    return $this;
  }

  /**
   * @return null
   */
  public function getCountryCode()
  {
    return $this->countryCode;
  }

  /**
   * @param null $countryCode
   * @return AddressPopulator
   */
  public function setCountryCode($countryCode)
  {
    $this->countryCode = $countryCode;
    return $this;
  }

  /**
   * @return null
   */
  public function getCompanyName()
  {
    return $this->companyName;
  }

  /**
   * @param null $companyName
   * @return AddressPopulator
   */
  public function setCompanyName($companyName)
  {
    $this->companyName = $companyName;
    return $this;
  }
}