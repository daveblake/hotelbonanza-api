<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 24/08/2016
 * Time: 13:40
 */

namespace OTA\HotelResModifyNotif\ResGlobalInfo\HotelReservationID;


class HotelReservationIDPopulator
{
  protected $id = null;
  protected $date = null;
  protected $source_context = null;

  /**
   * @return null
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param null $id
   * @return HotelReservationIDPopulator
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @return null
   */
  public function getDate()
  {
    return $this->date;
  }

  /**
   * @param null $date
   * @return HotelReservationIDPopulator
   */
  public function setDate($date)
  {
    $this->date = $date;
    return $this;
  }

  /**
   * @return null
   */
  public function getSourceContext()
  {
    return $this->source_context;
  }

  /**
   * @param null $source_context
   * @return HotelReservationIDPopulator
   */
  public function setSourceContext($source_context)
  {
    $this->source_context = $source_context;
    return $this;
  }


}