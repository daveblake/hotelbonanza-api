<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 16:04
 */

namespace OTA\HotelResModifyNotif\ResGlobalInfo;


use OTA\HotelResModifyNotif\ResGlobalInfo\HotelReservationID\HotelReservationIDPopulator;
use OTA\HotelResModifyNotif\ResGlobalInfo\Profile\ProfilePopulator;

class ResGlobalInfoPopulator
{
  protected $comments = [];
  protected $total = null;
  protected $totalDecimalPlaces = null;
  protected $totalCurrencyCode = null;
  protected $hotelReservationIDS = [];
  protected $profiles = [];
  protected $paymentCard_CardCode = null;
  protected $paymentCard_CardNumber = null;
  protected $paymentCard_SeriesCode = null;
  protected $paymentCard_ExpireDate = null;
  protected $paymentCard_CardHolderName = null;

  /**
   * @return array
   */
  public function getComments()
  {
    return $this->comments;
  }

  /**
   * @param array $comments
   * @return ResGlobalInfoPopulator
   */
  public function setComments($comments)
  {
    $this->comments = $comments;
    return $this;
  }
  public function addComment($comment)
  {
    $this->comments[] = $comment;
    return $this;
  }

  /**
   * @return null
   */
  public function getTotal()
  {
    return $this->total;
  }

  /**
   * @param null $total
   * @return ResGlobalInfoPopulator
   */
  public function setTotal($total)
  {
    $this->total = $total;
    return $this;
  }

  /**
   * @return array
   */
  public function getHotelReservationIDS()
  {
    return $this->hotelReservationIDS;
  }

  /**
   * @param array $hotelReservationIDS
   * @return ResGlobalInfoPopulator
   */
  public function setHotelReservationIDS($hotelReservationIDS)
  {
    $this->hotelReservationIDS = $hotelReservationIDS;
    return $this;
  }
  public function addHotelReservationID(HotelReservationIDPopulator $hotelReservationID)
  {
    $this->hotelReservationIDS[] = $hotelReservationID;
    return $this;
  }

  /**
   * @return array
   */
  public function getProfiles()
  {
    return $this->profiles;
  }

  /**
   * @param array $profiles
   * @return ResGlobalInfoPopulator
   */
  public function setProfiles($profiles)
  {
    $this->profiles = $profiles;
    return $this;
  }
  public function addProfile(ProfilePopulator $profile)
  {
    $this->profiles[] = $profile;
    return $this;
  }

  /**
   * @return null
   */
  public function getTotalDecimalPlaces()
  {
    return $this->totalDecimalPlaces;
  }

  /**
   * @param null $totalDecimalPlaces
   * @return ResGlobalInfoPopulator
   */
  public function setTotalDecimalPlaces($totalDecimalPlaces)
  {
    $this->totalDecimalPlaces = $totalDecimalPlaces;
    return $this;
  }

  /**
   * @return null
   */
  public function getTotalCurrencyCode()
  {
    return $this->totalCurrencyCode;
  }

  /**
   * @param null $totalCurrencyCode
   * @return ResGlobalInfoPopulator
   */
  public function setTotalCurrencyCode($totalCurrencyCode)
  {
    $this->totalCurrencyCode = $totalCurrencyCode;
    return $this;
  }


  public function getTotalWithDecimalPlaces()
  {
    if ($this->getTotalDecimalPlaces() > 0) {
      $total = $this->getTotal();
      for ($i=0;$i<$this->getTotalDecimalPlaces();$i++) {
        $total /= 10;
      }
      return $total;
    }
    return $this->total;
  }

  /**
   * @return null
   */
  public function getPaymentCardCardCode()
  {
    return $this->paymentCard_CardCode;
  }

  /**
   * @param null $paymentCard_CardCode
   * @return ResGlobalInfoPopulator
   */
  public function setPaymentCardCardCode($paymentCard_CardCode)
  {
    $this->paymentCard_CardCode = $paymentCard_CardCode;
    return $this;
  }

  /**
   * @return null
   */
  public function getPaymentCardCardNumber()
  {
    return $this->paymentCard_CardNumber;
  }

  /**
   * @param null $paymentCard_CardNumber
   * @return ResGlobalInfoPopulator
   */
  public function setPaymentCardCardNumber($paymentCard_CardNumber)
  {
    $this->paymentCard_CardNumber = $paymentCard_CardNumber;
    return $this;
  }

  /**
   * @return null
   */
  public function getPaymentCardSeriesCode()
  {
    return $this->paymentCard_SeriesCode;
  }

  /**
   * @param null $paymentCard_SeriesCode
   * @return ResGlobalInfoPopulator
   */
  public function setPaymentCardSeriesCode($paymentCard_SeriesCode)
  {
    $this->paymentCard_SeriesCode = $paymentCard_SeriesCode;
    return $this;
  }

  /**
   * @return null
   */
  public function getPaymentCardExpireDate()
  {
    return $this->paymentCard_ExpireDate;
  }

  /**
   * @param null $paymentCard_ExpireDate
   * @return ResGlobalInfoPopulator
   */
  public function setPaymentCardExpireDate($paymentCard_ExpireDate)
  {
    $this->paymentCard_ExpireDate = $paymentCard_ExpireDate;
    return $this;
  }

  /**
   * @return null
   */
  public function getPaymentCardCardHolderName()
  {
    return $this->paymentCard_CardHolderName;
  }

  /**
   * @param null $paymentCard_CardHolderName
   * @return ResGlobalInfoPopulator
   */
  public function setPaymentCardCardHolderName($paymentCard_CardHolderName)
  {
    $this->paymentCard_CardHolderName = $paymentCard_CardHolderName;
    return $this;
  }

  public function hasPaymentInfo()
  {
    if($this->paymentCard_CardNumber != null && $this->paymentCard_ExpireDate != null) {
      return true;
    }
    return false;
  }

}