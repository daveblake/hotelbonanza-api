<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 15:57
 */

namespace OTA\HotelResModifyNotif\Service;


class ServicePopulator
{
protected $RPH = null;
  protected $inventoryCode = null;
  protected $pricingType = null;
  protected $serviceDetailsTimespanDuration = null;
  protected $fees = [];

  /**
   * @return null
   */
  public function getRPH()
  {
    return $this->RPH;
  }

  /**
   * @param null $RPH
   * @return ServicePopulator
   */
  public function setRPH($RPH)
  {
    $this->RPH = $RPH;
    return $this;
  }

  /**
   * @return null
   */
  public function getInventoryCode()
  {
    return $this->inventoryCode;
  }

  /**
   * @param null $inventoryCode
   * @return ServicePopulator
   */
  public function setInventoryCode($inventoryCode)
  {
    $this->inventoryCode = $inventoryCode;
    return $this;
  }

  /**
   * @return null
   */
  public function getPricingType()
  {
    return $this->pricingType;
  }

  /**
   * @param null $pricingType
   * @return ServicePopulator
   */
  public function setPricingType($pricingType)
  {
    $this->pricingType = $pricingType;
    return $this;
  }

  /**
   * @return null
   */
  public function getServiceDetailsTimespanDuration()
  {
    return $this->serviceDetailsTimespanDuration;
  }

  /**
   * @param null $serviceDetailsTimespanDuration
   * @return ServicePopulator
   */
  public function setServiceDetailsTimespanDuration($serviceDetailsTimespanDuration)
  {
    $this->serviceDetailsTimespanDuration = $serviceDetailsTimespanDuration;
    return $this;
  }

  /**
   * @return array
   */
  public function getFees()
  {
    return $this->fees;
  }

  /**
   * @param array $fees
   * @return ServicePopulator
   */
  public function setFees($fees)
  {
    $this->fees = $fees;
    return $this;
  }

  public function addFee($fee) {
    $this->fees[] = $fee;
    return $this;
  }

}