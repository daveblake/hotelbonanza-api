<?php

/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 23/08/2016
 * Time: 15:51
 */
namespace OTA\HotelResModifyNotif;

use OTA\HotelResModifyNotif\ResGlobalInfo\ResGlobalInfoPopulator;
use OTA\HotelResModifyNotif\ResGuest\ResGuestPopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomStayPopulator;
use OTA\HotelResModifyNotif\Service\ServicePopulator;

class HotelResPopulator
{
  protected $roomStays = [];
  protected $services = [];
  protected $resGuests = [];
  protected $resGlobalInfo = null;

  /**
   * @return array
   */
  public function getRoomStays()
  {
    return $this->roomStays;
  }

  /**
   * @param array $roomStays
   * @return HotelResPopulator
   */
  public function setRoomStays($roomStays)
  {
    $this->roomStays = $roomStays;
    return $this;
  }

  public function addRoomStay(RoomStayPopulator $item)
  {
    $this->roomStays[] = $item;
    return $this;
  }

  /**
   * @return array
   */
  public function getServices()
  {
    return $this->services;
  }

  /**
   * @param array $services
   * @return HotelResPopulator
   */
  public function setServices($services)
  {
    $this->services = $services;
    return $this;
  }

  public function addService(ServicePopulator $item)
  {
    $this->services[] = $item;
    return $this;
  }

  /**
   * @return array
   */
  public function getResGuests()
  {
    return $this->resGuests;
  }

  /**
   * @param array $resGuests
   * @return HotelResPopulator
   */
  public function setResGuests(ResGuestPopulator $resGuests)
  {
    $this->resGuests = $resGuests;
    return $this;
  }

  public function addResGuest($item)
  {
    $this->resGuests[] = $item;
    return $this;
  }

  /**
   * @return null
   */
  public function getResGlobalInfo()
  {
    return $this->resGlobalInfo;
  }

  /**
   * @param null $resGlobalInfo
   * @return HotelResPopulator
   */
  public function setResGlobalInfo(ResGlobalInfoPopulator $resGlobalInfo)
  {
    $this->resGlobalInfo = $resGlobalInfo;
    return $this;
  }


}