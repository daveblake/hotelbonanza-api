<?php

abstract class PolicyText extends BasePolicy
{
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'text'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'text' => 'required'
    ];

    /**
     * Process saving (insert/update) policies
     *
     * @return string
     */
    public static function process($uuid = null, array $input)
    {
        if($uuid) {
            $policy = static::findByUUIDorFail($uuid);
            $policy->text = \Input::get('text', $policy->text);
        } else {
            $policy = new static();
            $policy->fill($input);
            $policy->uuid = (string) $policy->generateUUID();
        }

        return $policy;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function policies()
    {
        return $this->morphMany('Policy', 'type');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->policies as $policy) {
            if($policy->hotel) {
                $users[] = $policy->hotel->user;
            }
        }

        return $users;
    }
}