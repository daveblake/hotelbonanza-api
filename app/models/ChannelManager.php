<?php

class ChannelManager extends BaseModel
{
    protected $table = 'channel_managers';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'pivot','created_at','updated_at','order');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'channel manager';
    }
}