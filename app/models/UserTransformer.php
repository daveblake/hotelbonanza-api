<?php

use Illuminate\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Transformer\TransformerInterface;

class UserTransformer implements TransformerInterface
{
	public function transform($response, $transformer, Binding $binding, Request $request)
	{
		// Make a call to your transformation layer to transformer the given response.
	}
}