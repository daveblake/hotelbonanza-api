<?php

namespace Auth;

use League\OAuth2\Server\Grant\PasswordGrant;

/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 13/07/2016
 * Time: 14:29
 */
class PasswordOverrideGrant extends PasswordGrant
{
  protected $identifier = 'password_override';
}