<?php

class Room extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'rooms';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'type_id', 'sub_type_id', 'pivot','created_at','updated_at', 'hotel_id');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'sleeps',
        'single_occupancy_discount',
        'bedrooms',
        'lounges',
        'bathrooms',
        'loungesleeps',
        'smoking',
        'breakfastincluded',
        'roomsize',
        'roomsizeunit',
        'quantity'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required',
        'sleeps' => 'required',
        'bedrooms' => 'required',
        'lounges' => 'required',
        'bathrooms' => 'required',
        'smoking' => 'required',
        'breakfastincluded' => 'required'
    ];

    /**
     * Layouts have many beds
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function layouts()
    {
        return $this->belongsToMany('RoomLayout', 'rooms__room_layout', 'room_id', 'layout_id');
    }

    /**
     * Each Room is assigned to a hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('Hotel');
    }

    /**
     * Each room also has one type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('RoomType', 'type_id');
    }

    /**
     * Each room also has one subtype
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subtype()
    {
        return $this->belongsTo('RoomType', 'sub_type_id');
    }

    /**
     * Rooms have many images
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->morphToMany('Media', 'mediaable');
    }

    /**
     * Rooms have many meals
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function meals()
    {
        return $this->belongsToMany('BreakfastOption', 'rooms__room_meal', 'room_id', 'meal_id');
    }

    /**
     * Rooms have many policies
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function policies()
    {
        return $this->belongsToMany('PolicyCancellation', 'rooms__room_policy', 'room_id', 'policy_id');
    }

    /**
     * Rooms have many features options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function features()
    {
        return $this->belongsToMany('RoomFeature', 'rooms__room_features', 'room_id', 'feature_id');
    }


    /**
     * Rooms have many facilities options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facilities()
    {
        return $this->belongsToMany('RoomFacility', 'rooms__room_facilities', 'room_id', 'facility_id');
    }

    /**
     * Each room has multiple rates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rates()
    {
        return $this->hasMany('RoomRate', 'room_id');
    }

    /**
     * Each room can have multiple bookings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    //Todo: fix this relation, either wrong, or named wrong
    public function bookings()
    {
        return $this->hasMany('AvailabilityRoom', 'room_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'room';
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $hotel = $this->hotel;
        if ($hotel != null) {
            return [$hotel->user];
        }
        return [];
    }

    /**
     * Return all AvailabilityRooms based on the users hotels assignment
     *
     * @param User $user
     * @param int $limit
     * @param int $offset
     */
    public static function getMine(User $user, $limit = 10, $offset = 0)
    {
        return static::query()
            ->with('hotel')
            ->whereHas('hotel', function($hotel) use ($user) {
                $hotel->whereIn('id', $user->hotels->lists('id'));
            })
            ->skip($offset)
            ->take($limit)
            ->get();
    }
}