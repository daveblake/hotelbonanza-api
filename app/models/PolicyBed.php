<?php

class PolicyBed extends BasePolicy
{
    protected $table = 'policies__bed';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'quantity',
        'maxAge',
        'bedType',
        'costPerNight'
    );

    /**
     * @var array
     */
    protected static $rules = [];

    /**
     * @var array
     */
    public static $relationships = [];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'policy bed';
    }

    /**
     * Process saving (insert/update) policies
     *
     * @param null $uuid
     * @param array $data
     * @return string
     */
    public static function process($uuid = null, array $data)
    {
        if($uuid) {
            $policy = static::findByUUIDorFail($uuid);
            $policy->quantity = Input::get('quantity', $policy->quantity);
            $policy->maxAge = Input::get('maxAge', $policy->maxAge);
            $policy->bedType = Input::get('bedType', $policy->bedType);
            $policy->costPerNight = Input::get('costPerNight', $policy->costPerNight);
        } else {
            $policy = new static();
            $policy->fill($data);
            $policy->uuid = (string) $policy->generateUUID();
        }

        return $policy;
    }

}