<?php

class Tax extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'hotels__taxes';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','hotel_id','calculation_type_id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'amount',
        'inclusive'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required',
        'amount' => 'required',
        'inclusive' => 'required',
        'hotel_id' => 'required|exists:hotels,id',
        'calculation_type_id' => 'required|exists:calculation__types,id'
    ];

    /**
     * Each tax has a calculation type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function calculation_type()
    {
        // Continue
        return $this->belongsTo('CalculationType', 'calculation_type_id');
    }

    /**
     * Tax is related to a hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        // Continue
        return $this->belongsTo('Hotel', 'hotel_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'tax';
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        return [$this->hotel->user];
    }
}