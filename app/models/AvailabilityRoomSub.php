<?php

class AvailabilityRoomSub extends BaseModel
{
    protected $table = 'availability__rooms_sub_rates';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'room_id', 'created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'sub_rate_type',
        'margin'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'room_id' => 'required',
        'rate_id' => 'required'
    ];

    /**
     * Each sub room has one AvailabilityRoom
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function room()
    {
        return $this->belongsTo('AvailabilityRoom', 'room_id');
    }

    /**
     * Each sub room has a room rate, could be identical to room_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function roomRate()
    {
        return $this->belongsTo('AvailabilityRoom', 'rate_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'availability room sub';
    }
}