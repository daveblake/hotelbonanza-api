<?php

class Timezone extends BaseModel
{
    protected $table = 'timezones';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at','pivot');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'city',
        'timezone'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'city' => 'required',
        'timezone' => 'required',
    ];

    /**
     * Each country has a timezone
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countries()
    {
        return $this->belongsToMany('Country', 'country__timezones', 'timezone_id', 'country_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'timezone';
    }
}
