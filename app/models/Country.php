<?php

class Country extends BaseModel
{
    protected $table = 'countries';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'telprefix_id','created_at','updated_at', 'pivot');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'iso2',
        'iso3',
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required',
        'iso2' => 'required|size:2|unique:countries,iso2,{{$id}}',
        'iso3' => 'required|size:3|unique:countries,iso3,{{$id}}',
    ];

    /**
     * Each country has a telephone prefix
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function telprefix()
    {
        return $this->belongsTo('TelephonePrefix', 'telprefix_id');
    }


    /**
     * Each country has a districts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function districts()
    {
        return $this->hasMany('AddressDistrict', 'country_id');
    }

    /**
     * Each country has a timezone
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function timezones()
    {
        return $this->belongsToMany('Timezone', 'country__timezones', 'country_id', 'timezone_id')->orderBy('timezones.city');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'country';
    }
}
