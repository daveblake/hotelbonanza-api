<?php

class Facility extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'facilities';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'category_id', 'pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required',
        'category_id' => 'required|exists:facilities__categories,id'
    ];

    /**
     * Each facility is assigned to a category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('FacilityCategory', 'category_id')->orderBy('name', 'asc');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'facility';
    }

    /**
     * Each facility can be assigned to multiple rooms
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hotels()
    {
        return $this->belongsToMany('Hotel', 'hotels__hotel_facility', 'facility_id', 'hotel_id');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->hotels as $hotel) {
            $users[] = $hotel->user;
        }

        return $users;
    }
}