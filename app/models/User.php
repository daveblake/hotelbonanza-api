<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends BaseModel implements UserInterface, RemindableInterface
{
    use UserTrait, RemindableTrait;
    use \Illuminate\Database\Eloquent\SoftDeletingTrait;

    protected $dates = ['deleted_at','locked_at','last_login'];

    /**
     * @var array
     */
    protected static $rules = [
      'username'            => 'required',
      'email'               => 'required|email|unique:users,email,{{$id}}',
      'password'            => 'required',
      'channelmanagerother' => 'min:1'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      'id',
      'password',
      'remember_token',
      'created_at',
      'updated_at',
      //'last_login',
      'channelmanager_id',
      'billing_address_id',
      'telephone_prefix_id'
    ];

    /**
     * Fillable attributes
     *
     * @var array
     */
    protected $fillable = [
      'email',
      'username',
      'password',
      'set_password',
      'telephone',
      'channelmanagerother',
      'title',
      'name',
      'lastname',
      'email_verified',
      'billing_name',
      'last_login'
    ];

    /**
     * @var array
     */
    protected $with = ['notifications'];

    /**
     * Each user can be assigned to many hotels
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hotels()
    {
        return $this->hasMany('Hotel', 'user_id');
    }

    /**
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = \Hash::make($password);
    }

    /**
     * Users have one channelmanager
     * @deprecated as moved onto hotel when users started supporting multiple hotels - 20th Jun 2016
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channelmanager()
    {
        return $this->belongsTo('ChannelManager', 'channelmanager_id');
    }

    /**
     * Each user has a billing address
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function billingAddress()
    {
        return $this->belongsTo('Address', 'billing_address_id');
    }

    /**
     * Hotel telephones have one prefix
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function telprefix()
    {
        return $this->belongsTo('TelephonePrefix', 'telephone_prefix_id');
    }

    public function ratings()
    {
        return $this->hasMany('Rating','user_id');
    }

    /**
     * User may have many notifications (default: unread)
     *
     * @param bool $all
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function notifications($all = false)
    {
        $notifications = $this->belongsToMany('Notification', 'users__user_notification', 'user_id', 'notification_id')
          ->orderBy('notifications__notification.created_at', 'DESC');

        if (!$all) {
            $notifications->has('reads', '<', 1);
        }

        return $notifications;
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'user';
    }
}
