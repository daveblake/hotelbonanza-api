<?php

class RoomRate extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'rooms__rates';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','room_id','pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'channelmanagercode'
    );

    /**
     * @var array
     */
    protected $with = array(
        'room',
        'cancellationpolicies',
        'boardoptions',
        'breakfastoptions',
        'subRates'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'room_id' => 'required'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'room rate';
    }

    /**
     * Each rate must be attached to a room
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo('Room', 'room_id');
    }

    public function availability()
    {
      return $this->hasMany('AvailabilityRoom', 'room_rate_id');
    }
    public function availabilityBetween(\Carbon\Carbon $from, \Carbon\Carbon $to)
    {
      return $this->hasMany('AvailabilityRoom', 'room_rate_id')->whereBetween('date',[$from->format('Y-m-d'), $to->format('Y-m-d')])->get();
    }

    /**
     * Each room rate can have many cancellation policies
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cancellationpolicies()
    {
        return $this->belongsToMany('PolicyCancellation', 'rooms__rates_rate_policycancellation', 'rate_id', 'policycancellation_id');
    }

    /**
     * Each room rate can have many board options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function boardoptions()
    {
        return $this->belongsToMany('BoardOption', 'rooms__rates_rate_boardoption', 'rate_id', 'boardoption_id');
    }

    /**
     * Each room rate can have many breakfast options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function breakfastoptions()
    {
        return $this->belongsToMany('BreakfastOption', 'rooms__rates_rate_breakfastoption', 'rate_id', 'breakfastoption_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subRates()
    {
        return $this->hasMany('RateLink', 'rate_id')->orderBy('total', 'ASC');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        return [$this->room->hotel->user];
    }

    /**
     * Return all RoomRates based on the users hotels assignment
     *
     * @param User $user
     * @param int $limit
     * @param int $offset
     */
    public static function getMine(User $user, $limit = 10, $offset = 0)
    {
      return static::query()
        ->with(['room','room.hotel'])
        ->whereHas('room.hotel', function($hotel) use ($user) {
          $hotel->whereIn('id', $user->hotels->lists('id'));
        })
        ->skip($offset)
        ->take($limit)
        ->get();
    }
}