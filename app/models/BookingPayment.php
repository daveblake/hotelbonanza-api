<?php

/**
 * Created by PhpStorm.
 * User: mgane
 * Date: 08/01/16
 * Time: 11:06
 */
class BookingPayment extends BaseModel
{
    protected $table = 'bookings_payments';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'booking_id', 'created_at', 'updated_at', 'deleted_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'card_type',
        'card_number',
        'valid_from',
        'valid_to',
        'issue_number',
        'cvv'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'booking_id' => 'required',
        //'card_type',
        'card_number' => 'required|min:10',
        'valid_from' => 'digits:4',
        'valid_to' => 'required|digits:4',
        //'issue_number',
        'cvv' => 'required'//|digits:3'
    ];

    /**
     * Each BookingPayment has one Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo('Booking', 'booking_id');
    }


    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'booking payment';
    }

    public function clearAndSave(array $options = array()) {
        $this->card_number = '';
        $this->cvv = '';
        $query = $this->newQueryWithoutScopes();
        $saved = false;
        if ($this->exists)
        {
            $saved = $this->performUpdate($query, $options);
        }
        if ($saved) $this->finishSave($options);
        return $saved;
    }
}