<?php

class PaymentOption extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'hotels__payment_options';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','hotel_id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'bankTransfer',
        'paypal',
        'cash'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'bankTransfer' => 'required',
        'paypal' => 'required',
        'cash' => 'required',
        'hotel_id' => 'required|exists:hotels,id'
    ];

    /**
     * Payment options have many card type options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cardtypes()
    {
        return $this->belongsToMany('CardType', 'hotels__payment_cardtype', 'payment_id', 'card_type_id');
    }

    /**
     * Tax is related to a hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('Hotel', 'hotel_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'payment option';
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        return [$this->hotel->user];
    }
}