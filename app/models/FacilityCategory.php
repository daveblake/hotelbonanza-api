<?php

class FacilityCategory extends BaseModel
{
    protected $table = 'facilities__categories';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required'
    ];

    /**
     * Each category has many facilities
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function facilities()
    {
        return $this->hasMany('Facility', 'category_id')->orderBy('name', 'asc');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'facility category';
    }
}