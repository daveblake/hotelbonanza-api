<?php

class RoomFeature extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'rooms__features';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'room feature';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rooms()
    {
        return $this->belongsToMany('Room', 'rooms__room_features', 'feature_id', 'room_id');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->rooms as $room) {
            $users[] = $room->hotel->user;
        }

        return $users;
    }
}