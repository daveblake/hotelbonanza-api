<?php
/**
 * Client
 *
 * @property string         $id
 * @property string         $secret
 * @property string         $name
 * @property boolean        $auto_approve
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{
  protected $table = 'oauth_clients';
}