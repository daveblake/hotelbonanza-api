<?php

use Illuminate\Database\Eloquent\Model;

class HotelType extends BaseModel
{
    protected $table = 'hotels__types';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array('name', 'description');

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'hotel type';
    }
} 