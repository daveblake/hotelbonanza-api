<?php

class BedType extends BaseModel
{
    use \Illuminate\Database\Eloquent\SoftDeletingTrait;
    
    protected $table = 'rooms__beds_types';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at', 'deleted_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'sleeps',
        'order'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required',
        'sleeps' => 'required',
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'bed type';
    }
}