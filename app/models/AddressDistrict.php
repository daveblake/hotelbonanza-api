<?php

class AddressDistrict extends BaseModel
{
    protected $table = 'address__district';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'country_id', 'created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'country_id',
        'city'
    );

    protected $with = [
        'country'
    ];

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required',
        'city' => 'required'
    ];

    /**
     * Each District has one country
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->belongsTo('Country', 'country_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'address district';
    }
}