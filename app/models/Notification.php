<?php

class Notification extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'notifications__notification';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','global','creator_id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'message',
        'global'
    );

    /**
     * Load relations
     *
     * @var array
     */
    protected $with = array();

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'message' => 'required'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'notification';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reads()
    {
        return $this->hasMany('NotificationRead');
    }

    /**
     * Notifications may have many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('User', 'users__user_notification', 'notification_id', 'user_id');
    }

    /**
     * Get all unread notifications for a specific user
     *
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function userNotificationsUnread($userId)
    {
        return Notification::query()
            ->where('global', true)
            ->orWhereHas('users', function($query) use($userId) {
                $query->where('user_id', $userId);
            })
            ->has('reads', '<', 1)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get all read notifications for a specific user
     *
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function userNotificationsRead($userId)
    {
        return Notification::query()
            ->whereHas('reads', function($query) use($userId) {
                $query->where('user_id', $userId);
            })
            ->has('reads', '>=', 1)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get all unread notifications and global for a specific user
     *
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function userNotificationsUnreadWithGlobal($userId)
    {
        return Notification::query()
            ->whereHas('reads', function($query) use($userId) {
                $query->where('user_id', $userId);
            })
            ->has('reads', '<', 0)
            ->orWhere('global', true)
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}