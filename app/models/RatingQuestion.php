<?php

class RatingQuestion extends BaseModel
{
    protected $table = 'ratings__questions';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'label'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'label' => 'required'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'rating question';
    }
}