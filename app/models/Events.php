<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Events extends BaseModel
{

    /**
     * @var array
     */
    protected static $rules = [
      'from'        => 'required',
      'to'          => 'required',
      'name'        => 'required',
      'description' => 'required'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      'id',
      'address_id',
      'created_at',
      'updated_at'
    ];

    /**
     * Fillable attributes
     *
     * @var array
     */
    protected $fillable = ['from', 'to', 'name', 'description'];

    /**
     * @var array
     */
    protected $with = ['address', 'images'];

    /**
     * Each event has an address
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('Address', 'address_id');
    }

    /**
     * Event may have many images
     *
     * @param bool $all
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->belongsToMany('Media', 'events__media', 'event_id', 'media_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'event';
    }
}
