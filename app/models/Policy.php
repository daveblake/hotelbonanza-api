<?php

class Policy extends BaseModel
{
	protected $table = 'hotels__policies';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('hotel_id', 'type_id');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array();

    /**
     * @var array
     */
    protected static $rules = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function type()
    {
    	return $this->morphTo();
    }

    /**
     * Each Room is assigned to a hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('Hotel');
    }

    public static function name()
    {
    	return 'policy';
    }
}