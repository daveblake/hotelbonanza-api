<?php

class Location extends BaseModel
{
    protected $table = 'locations';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'latitude',
        'longitude'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'latitude' => 'required',
        'longitude' => 'required'
    ];

    /**
     * @param $latitude
     * @param $longitude
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public static function findByLatLngOrCreate($latitude, $longitude)
    {
        // Find
        /*$model = static::query()
            ->where('latitude', $latitude)
            ->where('longitude', $longitude)
            ->first();*/
        
        $model = false;

        // Else create one
        if(!$model) {
            $model = new static();
            $model->latitude = $latitude;
            $model->longitude = $longitude;
            $model->uuid = (string) $model->generateUUID();
            $model->save();
        }

        return $model;
    }

    const POSTCODESIO_API_ENDPOINT = 'http://api.postcodes.io/postcodes';
    const POSTCODESIO_API_QUERY_ENDPOINT = 'api.postcodes.io/postcodes?q=';

    /**
     * Fetch and store lat/lng information from either DB (cache) or postcodes.io
     *
     * @param $postcode
     * @return \Illuminate\Database\Eloquent\Model|Location|null
     */
    public static function findByPostcode($postcode)
    {
        // Format postcode before we find within DB
        $postcode = strtoupper($postcode);
        $postcode = str_replace(' ', '', $postcode);
        $postcode = trim($postcode);

        // If found, use that. Otherwise, make an API call to postcodes.io
        if($found = \DB::table('locations__postcodes')->where('postcode', $postcode)->first()) {
            return static::find($found->location_id);
        } else {
            $client = new GuzzleHttp\Client([
              'exceptions' => false
            ]);
                $res = $client->request('GET', static::POSTCODESIO_API_ENDPOINT . '/' . $postcode);
                if ($res->getStatusCode() == 200) {
                    $result = json_decode((string)$res->getBody());

                    if (isset($result->result->latitude) && isset($result->result->longitude)) {
                        $location = static::findByLatLngOrCreate($result->result->latitude, $result->result->longitude);

                        // Cache for so API call isn't made again
                        \DB::table('locations__postcodes')->insert([
                          'location_id' => $location->id,
                          'postcode'    => $postcode
                        ]);

                        return $location;
                    }
                }

                //try the nearest postcode by using the autocomplete
                if($res->getStatusCode() == 404) {
                    $res = $client->request('GET', static::POSTCODESIO_API_QUERY_ENDPOINT . $postcode);
                    if ($res->getStatusCode() == 200) {
                        $result = json_decode((string)$res->getBody()->getContents());

                        if(is_array($result->result)) {
                            $result = $result->result;
                            $result = head($result);
                        }
                        if(isset($result->latitude) && isset($result->longitude)) {
                            $location =  static::findByLatLngOrCreate($result->latitude, $result->longitude);

                            // Cache for so API call isn't made again
                            \DB::table('locations__postcodes')->insert([
                              'location_id' => $location->id,
                              'postcode' => $postcode
                            ]);

                            return $location;
                        }
                    }
                }
            }

        return false;
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'area';
    }
}