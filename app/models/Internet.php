<?php

class Internet extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'internet';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'area_id', 'pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'price', 'seconds', 'type'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'price' => 'required',
        'seconds' => 'required'
    ];

    /**
     * Each internet option has an area
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo('InternetArea', 'area_id')->orderBy('order','ASC');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'internet type';
    }

    /**
     * Internet options can be assigned to multiple hotels
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hotels()
    {
        return $this->belongsToMany('Hotel', 'hotels__hotel_internet', 'internet_id', 'hotel_id');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->hotels as $hotel) {
            $users[] = $hotel->user;
        }

        return $users;
    }
}