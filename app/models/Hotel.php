<?php

class Hotel extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'hotels';
    use \Illuminate\Database\Eloquent\SoftDeletingTrait;

    protected $dates = ['created_at','updated_at','deleted_at', 'terms_accepted_at'];
    
    protected $all_data = true;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array(
      'id',
      'type_id',
      'user_id',
      'address_id',
      'billing_address_id',
      'mobile_prefix_id',
      'telephone_prefix_id',
      'payment_options_id',
      'timezone_id',
      'channelmanager_id',
      'ota_hotel_id',
      'ota_password'
    );
    protected $hidden_default = array(
      'id',
      'type_id',
      'user_id',
      'address_id',
      'billing_address_id',
      'mobile_prefix_id',
      'telephone_prefix_id',
      'payment_options_id',
      'timezone_id',
      'channelmanager_id',
      'ota_hotel_id',
      'ota_password'
    );

    /**
     * This is used when returning large api collections
     * @var array
     */
    protected $hidden_large = array(
      'id',
      'type_id',
      'user_id',
      'address_id',
      'billing_address_id',
      'mobile_prefix_id',
      'telephone_prefix_id',
      'payment_options_id',
      'timezone_id',
      'preferred_name',
      'billing_contact',
      'billing_different',
      'mobile',
      'reservationSkype',
      'websiteUrl',
      'shortDescription',
      'longDescription',
      'tagline',
      'childrenallowed',
      'starRating',
      'userRating',
      'heroImage',
      'breakfastIncluded',
      'wifiavailable',
      'parkingavailable',
      'reception24hours',
      'parking_reservation_required',
      'pets_allowed',
      'pets_charge',
      'checkinmessage',
      'extracribs',
      'extrabeds',
      'currency',
      'mainimage',
      'vatnumber',
      'group_chain',
      'group_chain_name',
      'terms_name',
      'terms_position',
      'breakfast',
      'mainImage',
      'deleted_at',
      'channelmanager_id',
      'channelmanagerother',
      'ota_hotel_id',
      'ota_password'
    );

    /**
     * Fillable attributes this model allows.
     *
     * Status is omitted because we don't want the user to fill that out.
     * This is controlled elsewhere.
     *
     * @var array
     */
    protected $fillable = array(
        // Main
        'hotel_name',
        'customer_name',
        'preferred_name',

        // Additional
        'address_id',
        'billing_contact',
        'billing_different',
        'billing_name',
        'roomcount',
        'tel',
        'mobile',
        'reservationEmail',
        'reservationSkype',
        'websiteUrl',
        'shortDescription',
        'longDescription',
        'tagline',
        'childrenallowed',
        'starRating',
        'userRating',
        'heroImage',
        'breakfastIncluded',
        'wifiavailable',
        'parkingavailable',
        'reception24hours',
        'parking_reservation_required',
        'pets_allowed',
        'pets_charge',
        'checkinmessage',
        'extracribs',
        'extrabeds',
        'currency',
        'mainimage',
        'vatnumber',
        'channelmanagerother',
        'group_chain',
        'group_chain_name',
        'terms_name',
        'terms_position',
        'progress', //this is set by the client, not the api
        'progress_total', //this is set by the client, not the api
    );

    /**
     * Each hotel has one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Each hotel has one type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('HotelType', 'type_id');
    }

    /**
     * Each hotel has an address
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('Address', 'address_id');
    }

    /**
     * Each hotel has a billing address
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function billingAddress()
    {
        return $this->belongsTo('Address', 'billing_address_id');
    }

    /**
     * Hotels have many rooms
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany('Room','hotel_id');
    }

    /**
     * Hotels have many images
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->morphToMany('Media', 'mediaable');
    }

    /**
     * Hotels have many parking options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parking()
    {
        return $this->belongsToMany('Parking', 'hotels__hotel_parking', 'hotel_id', 'parking_id');
    }

    /**
     * Hotels have many internet options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function internet()
    {
        return $this->belongsToMany('Internet', 'hotels__hotel_internet', 'hotel_id', 'internet_id');
    }

    /**
     * Hotels have many breakfastoptions options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function breakfastoptions()
    {
        return $this->belongsToMany('BreakfastOption', 'hotels__hotel_breakfast', 'hotel_id', 'breakfast_option_id');
    }

    /**
     * Hotels have many boardoptions options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function boardoptions()
    {
        return $this->belongsToMany('BoardOption', 'hotels__hotel_board', 'hotel_id', 'board_option_id');
    }

    /**
     * Hotel telephones have one prefix
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function telprefix()
    {
        return $this->belongsTo('TelephonePrefix', 'telephone_prefix_id');
    }

    /**
     * Hotel telephones have one prefix
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mobprefix()
    {
        return $this->belongsTo('TelephonePrefix', 'mobile_prefix_id');
    }

    /**
     * Hotels have one timezone
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function timezone()
    {
        return $this->belongsTo('Timezone', 'timezone_id');
    }

    /**
     * Hotels have many facilities
     *
     * @todo support different types of policies
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facilities()
    {
        return $this->belongsToMany('Facility', 'hotels__hotel_facility', 'hotel_id', 'facility_id');
    }

    /**
     * Hotels have many taxes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function taxes()
    {
        return $this->hasMany('Tax', 'hotel_id');
    }

    /**
     * Each hotel has set of payment options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentoptions()
    {
        return $this->hasOne('PaymentOption', 'hotel_id');
    }

    /**
     * Hotels have many different types of policies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function policies()
    {
        return $this->hasMany('Policy', 'hotel_id');
    }

    /**
     * Hotels have many feature options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function features()
    {
        return $this->belongsToMany('Feature', 'hotels__hotel_feature', 'hotel_id', 'feature_id');
    }
    
    /**
     * Hotels have many theme options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function themes()
    {
        return $this->belongsToMany('Theme', 'hotels__hotel_theme', 'hotel_id', 'theme_id');
    }

    /**
     * Hotels have many ratings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ratings()
    {
        return $this->hasMany('Rating', 'hotel_id')->where('approved', true)->orderBy('approval_date', 'DESC');
    }

    /**
     * Users have one channel manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channelmanager()
    {
      return $this->belongsTo('ChannelManager', 'channelmanager_id');
    }

    public function bookings()
    {
      return $this->hasMany('Booking');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'hotel';
    }

    /**
     * Get the first sentence of a string.
     *
     * If no ending punctuation is found then $text will
     * be returned as the sentence. If $strict is set
     * to TRUE then FALSE will be returned instead.
     *
     * @param $field
     * @param  string $end Ending punctuation
     * @param null $default
     * @return string|bool     Sentence or FALSE if none was found
     */
    public function firstSentence($field, $default = null, $end = '.?!') {

        if($value = $this->getAttribute($field)) {
            preg_match("/^[^{$end}]+[{$end}]/", $this->getAttribute($field), $result);

            if (empty($result)) {
                return $default;
            }

            return $result[0];
        }

        return $default;
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        return [$this->user];
    }

    /**
     * @return boolean
     */
    public function isAllData()
    {
        return $this->all_data;
    }

    /**
     * @param boolean $all_data
     */
    public function setAllData($all_data)
    {
        $this->all_data = $all_data;
    }

    public function setAttributeVisibility(){
        if($this->all_data){
            $this->setHidden($this->hidden_default);
        } else {
            $this->setHidden($this->hidden_large);
        }
    }
    public function toJson($options = 0) {
        $this->setAttributeVisibility();
        return parent::toJson();
    }
    public function toArray($options = 0) {
        $this->setAttributeVisibility();
        return parent::toArray();
    }

    public function generateSetOTACredentials()
    {
      $this->ota_hotel_id = str_pad($this->id,10,0,STR_PAD_LEFT);
      $this->ota_password = substr(md5($this->generateUUID()),10);
    }
} 