<?php

class RateLink extends BaseModel
{
    protected $table = 'rooms__rates_link';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','rate_id','pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'type',
        'amount',
        'description'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'type' => 'required',
        'amount' => 'required',
        'rate_id' => 'required'
    ];

    /**
     * @var array
     */
    private static $allowedTypes = [
        'price',
        'percentage'
    ];

    const TYPE_PRICE = 'price';
    const TYPE_PERCENTAGE = 'percentage';

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'room rate link';
    }

    public static function boot()
    {
        parent::boot();

        // Automatically call the calculateTotal event once the model has saved.
        RateLink::saving(function($ratelink) {
            $ratelink->calculateTotal();
            return true;
        });

        // Automatically call the calculateTotal event once the model has saved.
        RateLink::creating(function($ratelink) {
            $ratelink->calculateTotal();
            return true;
        });
    }

    /**
     * Try to set the type based on the allowed values
     *
     * @param $type
     */
    public function setTypeAttribute($type)
    {
        if(!in_array($type, static::$allowedTypes)) {
            $this->attributes['type'] = static::TYPE_PRICE;
        } else {
            $this->attributes['type'] = strtolower($type);
        }
    }

    /**
     * Method to automatically update the total after saving the model
     */
    public function calculateTotal()
    {
        if($rate = $this->rate) {
            switch($this->type) {
                case static::TYPE_PERCENTAGE:
                    // Assuming the value is between whole, i.e. 20 not 0.2
                    $this->total = $rate->baseprice + ($rate->baseprice * ($this->amount / 100));
                    break;

                default:
                case static::TYPE_PRICE:
                    $this->total = $rate->baseprice + $this->amount;
                    break;
            }
        }
    }

    /**
     * Each rate must be attached to a room
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rate()
    {
        return $this->belongsTo('RoomRate', 'rate_id');
    }
}