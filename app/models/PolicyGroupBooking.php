<?php

class PolicyGroupBooking extends PolicyText
{
	protected $table = 'policies__group_bookings';

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'policy group booking';
    }
}