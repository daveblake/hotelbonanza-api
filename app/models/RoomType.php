<?php

class RoomType extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'rooms__types';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','created_at','updated_at','order');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'bedrooms',
        'lounges',
        'bathrooms',
        'order'
    );

    /**
     * Fetch the list of sub types
     *
     * @var array
     */
    protected $with = [
        'subtypes'
    ];

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required',
        'bedrooms' => 'boolean',
        'lounges' => 'boolean',
        'bathrooms' => 'boolean'
    ];

    /**
     * Each type can have sub types (children)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subtypes()
    {
        return $this->belongsToMany('RoomType', 'rooms__types_type_subtype', 'type_id', 'sub_type_id')->orderBy('order','ASC')->orderBy('name','ASC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany('Room', 'type_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'room type';
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->rooms as $room) {
            $users[] = $room->hotel->user;
        }

        return $users;
    }
}