<?php

class TelephonePrefix extends BaseModel
{
    protected $table = 'telephone__prefix';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'prefix'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'prefix' => 'required|unique:telephone__prefix,prefix,{{$id}}',
    ];

    /**
     * Each telephone prefix has a country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->hasOne('Country', 'telprefix_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'telephone prefix';
    }
}