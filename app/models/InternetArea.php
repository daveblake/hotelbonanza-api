<?php

class InternetArea extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'internet__area';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'pivot','created_at','updated_at','order');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'internet area';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function internetOptions()
    {
        return $this->hasMany('Internet', 'area_id');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->internetOptions as $options) {
            foreach($options->hotels as $hotel) {
                $users[] = $hotel->user;
            }
        }

        return $users;
    }
}