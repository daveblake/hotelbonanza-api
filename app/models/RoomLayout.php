<?php

class RoomLayout extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'rooms__layouts';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array();

    /**
     * @var array
     */
    protected static $rules = [];

    /**
     * Layouts have many beds
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function beds()
    {
        return $this->belongsToMany('Bed', 'rooms__layout_bed', 'layout_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rooms()
    {
        return $this->belongsToMany('Room', 'rooms__room_layout', 'layout_id', 'room_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'room layout';
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->rooms as $room) {
            $users[] = $room->hotel->user;
        }

        return $users;
    }
}