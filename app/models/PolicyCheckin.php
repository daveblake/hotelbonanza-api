<?php

class PolicyCheckin extends PolicyTime
{
    protected $table = 'policies__checkin';

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'policy checkin';
    }
}