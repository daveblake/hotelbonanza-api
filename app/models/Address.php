<?php

class Address extends BaseModel
{
    protected $table = 'addresses';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'country_id', 'location_id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'line1',
        'line2',
        'line3',
        'town',
        'county',
        'postcode',
    );

    protected $with = [
        'country',
        'district'
    ];

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'line1' => 'required',
        'postcode' => 'required',
        'town' => 'required'
    ];

    /**
     * Each address has one country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('Country', 'country_id');
    }
    /**
     * Each address has one country
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function district()
    {
        return $this->hasOne('AddressDistrict', 'id','adminArea');
    }

    /**
     * Each address has a location
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        // Continue
        return $this->belongsTo('Location', 'location_id');
    }

    /**
     * Get and cache the location where possible
     *
     * @return mixed
     */
    public function getAndCacheLocation()
    {
        if(is_null($this->location_id) || !$this->location) {
            if($geocode = new \kamranahmedse\Geocode($this->oneline())) {
                $location = Location::findByLatLngOrCreate($geocode->getLatitude(), $geocode->getLongitude());
                Owner::takeOwnership($location);
                $this->location()->associate($location);
                $this->save();
            }
        }

        return $this->location;
    }

    /**
     * Return a formatted, oneline version of the address.
     * explode() might return Address line 1, , , Town, Count, Postcode hence why we manually build the string.
     *
     * @return string
     */
    public function oneline()
    {
        // Parts that build the address up
        $parts = ['line1', 'line2', 'line3', 'town', 'county', 'postcode'];

        // Sep and Address to return
        $sep = null; $address = '';

        foreach($parts as $part) {
            if($this->{$part} != '') {
                $address .= $sep . $this->{$part};
                $sep = ', ';
            }
        }

        return $address;
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'address';
    }
}