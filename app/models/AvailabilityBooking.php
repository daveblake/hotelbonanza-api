<?php

class AvailabilityBooking extends BaseModel
{
  const BOOKING_STATE_PENDING = 'pending';
  const BOOKING_STATE_AWAITING_CONFIRMATION = 'awaiting-confirmation';
  const BOOKING_STATE_CONFIRMED = 'confirmed';
  const BOOKING_STATE_COMPLETE = 'complete';
  const BOOKING_STATE_CANCELLED = 'cancelled';
  const BOOKING_STATE_DENIED = 'denied';
  const BOOKING_STATE_UNKNOWN = 'unknown';

  protected $table = 'availability__bookings';

  use \Illuminate\Database\Eloquent\SoftDeletingTrait;

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array('id', 'availability_room_id', 'booking_id', 'user_id', 'room_rate_id', 'room_id', 'created_at', 'updated_at', 'deleted_at', 'rooms.rates');

  /**
   * Fillable attributes this model allows.
   *
   * @var array
   */
  protected $fillable = array(
    'time_booked',
    'date_from',
    'date_to',
    'user_id',
    //'booking_id',
    //'availability_room_id'
  );

  /**
   * Required fields
   *
   * @var array
   */
  protected static $rules = [
    'time_booked' => 'required',
    'date_from'   => 'required',
    'date_to'     => 'required',
    'user_id'     => 'required',
    'booking_id'  => 'required',
  ];

  /**
   * Each booking has multiple Availability Room Dates
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function rooms()
  {
    return $this->belongsToMany('AvailabilityRoom', 'availability__bookings_rooms',
      'availability__booking_id', 'availability__room_id');
  }

  public function room()
  {
    return $this->belongsTo('Room');
  }

  public function rate()
  {
    return $this->belongsTo('RoomRate','room_rate_id');
  }

  /**
   * Each booking has one User
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function user()
  {
    return $this->belongsTo('User', 'user_id');
  }

  /**
   * Each booking has one Booking
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function booking()
  {
    return $this->belongsTo('Booking', 'booking_id');
  }

  /**
   * Find many bookings by user
   *
   * @param User $user
   * @return \Illuminate\Database\Eloquent\Collection|static[]
   */
  public static function findByUser(User $user)
  {
    return static::query()->where('user_id', $user->id)->get();
  }

  /**
   * Find many bookings by a single date
   *
   * @param \Carbon\Carbon $date
   * @return $this
   */
  public static function findByDate(\Carbon\Carbon $date)
  {
    return static::query()->where('date_from', $date->toDateString());
  }

  /**
   * Find many bookings by a date range
   *
   * @param \Carbon\Carbon $from
   * @param \Carbon\Carbon $to
   * @return $this
   */
  //todo: check this
  public static function findByDateRange(\Carbon\Carbon $from, \Carbon\Carbon $to, $room_id = null)
  {
    $query = static::query()
      ->where('date_from', '<=', $from->format('Y-m-d'))
      ->where('date_to', '>=', $to->format('Y-m-d'))
      ->orderBy('date_from', 'ASC');

    if ($room_id) {
      $query->with('rooms')->whereHas('rooms', function ($room) use ($room_id) {
        $room->where('availability__rooms.id', $room_id);
      });
    }

    return $query;
  }

  /**
   * Provide a friendly name of the entity
   *
   * @return string
   */
  public static function name()
  {
    return 'availability booking';
  }
}