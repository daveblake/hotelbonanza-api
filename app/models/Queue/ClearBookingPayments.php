<?php
/**
 * Created by PhpStorm.
 * User: mgane
 * Date: 08/01/16
 * Time: 16:27
 */

namespace Queue;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Queue\Jobs\Job;

class ClearBookingPayments
{
    public function fire($job, $uuid)
    {
        /**
         * @var $job Job
         */
        \DB::reconnect();
        // Remove the bookingPayment details
        try {
            $bookingPayment = \BookingPayment::findByUUIDorFail($uuid);
            if(!$bookingPayment->clearAndSave()) {
                \Log::info("Could not remove booking payment details\r\n");
            }
        } catch (ModelNotFoundException $e) {
            // Details not found to remove, drop job
            $job->delete();
        } catch (\Exception $e) {
            \Log::error($e);
            $job->release(900);
        }
        $job->delete();
    }
}