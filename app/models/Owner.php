<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class Owner extends BaseModel
{
    protected $table = 'users__owners';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','user_id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'model',
        'user_id',
        'uuid'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'model' => 'required',
        'user_id' => 'required|exists:users,id',
        'uuid' => 'required'
    ];

    /**
     * List of Resources that are locked where ownership cannot be taken.
     *
     * @var array
     */
    protected static $locked = [
        'BaseMode',
        'BasePolicy',
        'Owner'
    ];

    /**
     * List of resources that can openly be created but not edited.
     *
     * @var array
     */
    protected static $bypass = [
        'User'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'owner';
    }

    /**
     * Find a model by user_id and model
     *
     * @param $uuid
     * @param $user_id
     * @param BaseModel $model
     * @return \Illuminate\Database\Eloquent\Model|static
     * @throws \Exceptions\ResourceHasNoOwnersException
     */
    public static function findByUserIdAndModel($uuid, $user_id, BaseModel $model)
    {
        $owners = static::findAllOwnersOfResource($uuid, $model);

        // Resource doesn't have a user
        // Throw an exception to allow the BaseModel to catch and create
        // ownership of that resource.
        if($owners->count() == 0) {
            throw new \Exceptions\ResourceHasNoOwnersException();
        }

        // Check the user is an owner of the resource
        if(is_object($model) && $model instanceof BaseModel) {
            return static::query()
                ->where('uuid', $uuid)
                ->where('user_id', $user_id)
                ->where('model', get_class($model))
                ->firstOrFail();
        }

        throw new ModelNotFoundException('User does not have access to this resource');
    }

    /**
     * Find all the owners of this resource
     *
     * @param $uuid
     * @param BaseModel $model
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function findAllOwnersOfResource($uuid, BaseModel $model)
    {
        return static::query()
            ->where('uuid', $uuid)
            ->where('model', get_class($model))
            ->get();
    }

    /**
     * Take ownership of a resource
     *
     * @param BaseModel $model
     * @return static
     * @throws \Exceptions\AccessDeniedException
     */
    public static function takeOwnership(BaseModel $model)
    {
        // Allow certain resources to bypass ACL
        if(!$model->exists && in_array(get_class($model), static::$bypass)) {
            return true;
        }

        // Lock certain resources by disallowing ownership
        if(in_array(get_class($model), static::$locked)) {
            throw new \Exceptions\AccessDeniedException("You don't have access to this resource");
        }

        if($owners = static::findOwnersOfResource($model)) {
            foreach($owners as $owner) {
                Owner::create([
                    'uuid' => $model->uuid,
                    'model' => get_class($model),
                    'user_id' => $owner
                ]);
            }

            // Return whether the user is an owner
            return in_array($model->getCurrentApiUserid(), $owners);
        } else {
            // No users found for this resource.
            // Grant them ownership automatically.
            return Owner::create([
                'uuid' => $model->uuid,
                'model' => get_class($model),
                'user_id' => $model->getCurrentApiUserid()
            ]);
        }
    }

    /**
     * Find all the users that have access to this resource and create ownerships
     *
     * @param BaseModel $model
     * @return array
     */
    public static function findOwnersOfResource(BaseModel $model)
    {
        $ids = [];

        // Check the model implements the ACL AccessInterface Interface.
        // If so, continue to find the owner of this resource.
        if(in_array('Interfaces\\AccessInterface', (array)class_implements($model))) {
            // Invoke the findUser method on the model
            // This should return an array of User objects OR ids
            // Ideally, user objects.
            if($users = $model->findUser()) {
                foreach($users as $user) {
                    if($user instanceof User) {
                        $ids[] = $user->id;
                    } elseif(is_int($user)) {
                        $ids[] = $user;
                    }
                }
            }
        }

        // Uniquify the user IDS
        return array_unique($ids);
    }
}