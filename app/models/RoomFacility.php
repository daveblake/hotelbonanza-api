<?php

class RoomFacility extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'rooms__facilities';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','created_at','updated_at','category_id');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'room facility';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rooms()
    {
        return $this->belongsToMany('Room', 'rooms__room_facilities', 'facility_id', 'room_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('RoomFacilityCategory','category_id');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->rooms as $room) {
            $users[] = $room->hotel->user;
        }

        return $users;
    }
}