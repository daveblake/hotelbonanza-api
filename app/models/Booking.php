<?php

/**
 * Created by PhpStorm.
 * User: mgane
 * Date: 08/01/16
 * Time: 10:48
 */
class Booking extends BaseModel
{
    protected $table = 'bookings';

    use \Illuminate\Database\Eloquent\SoftDeletingTrait;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'user_id', 'hotel_id', 'updated_at', 'deleted_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'date_from',
        'date_to',
        'additional_info',
        'total',
        'deposit',
        'bonanza_card',
        'newsletter_signup'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'date_from' => 'required',
        'date_to' => 'required',
        'user_id' => 'required',
        'hotel_id' => 'required',
        'total' => 'required'
    ];

    /**
     * Each booking has one User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Each booking has one Hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('Hotel', 'hotel_id');
    }

    /**
     * Each booking has many availability bookings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function availabilitybooking()
    {
        return $this->hasMany('AvailabilityBooking', 'booking_id', 'id');
    }

    /**
     * Each booking has one payment details
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function paymentdetails()
    {
        return $this->hasOne('BookingPayment', 'booking_id');
    }

    /**
     * Find many bookings by user
     *
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function findByUser(User $user)
    {
        return static::query()->where('user_id', $user->id)->get();
    }

    /**
     * Find many bookings by hotel
     *
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function findByHotel(Hotel $hotel)
    {
        return static::query()->where('hotel_id', $hotel->id)->get();
    }

    /**
     * Find many bookings by a single date
     *
     * @param \Carbon\Carbon $date
     * @return $this
     */
    public static function findByDate(\Carbon\Carbon $date)
    {
        return static::query()->where('date_from', $date->toDateString());
    }

    /**
     * Find many bookings by a date range
     *
     * @param \Carbon\Carbon $from
     * @param \Carbon\Carbon $to
     * @return $this
     */
    public static function findByDateRange(\Carbon\Carbon $from, \Carbon\Carbon $to)
    {
        $query = static::query()
            ->where('date_from', '<=', $from->format('Y-m-d'))
            ->where('date_to', '>=', $to->format('Y-m-d'))
            ->orderBy('date_from', 'ASC');

        return $query;
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'booking';
    }
}