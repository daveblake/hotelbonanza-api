<?php

class PolicyCheckout extends PolicyTime
{
    protected $table = 'policies__checkout';

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'policy checkout';
    }
}