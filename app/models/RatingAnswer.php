<?php

class RatingAnswer extends BaseModel
{
    protected $table = 'ratings__answers';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','question_id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'rating'
    );

    /**
     * Load relations
     *
     * @var array
     */
    protected $with = array(
        'question'
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'rating' => 'required',
        'question_id' => 'required'
    ];

    /**
     * Each rating has a related question
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('RatingQuestion', 'question_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'rating answer';
    }
}