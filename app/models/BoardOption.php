<?php

class BoardOption extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'board__options';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','type_id', 'pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'adultprice',
        'childprice'
    );

    /**
     * @var array
     */
    protected $with = array(
        'type'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'type_id' => 'required'
    ];

    /**
     * Each board option has a type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('BoardType', 'type_id');
    }

    /**
     * Each board option can have many hotels
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hotels()
    {
        return $this->belongsToMany('Hotel', 'hotels__hotel_board', 'hotel_id', 'board_option_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'board option';
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->hotels as $hotel) {
            $users[] = $hotel->user;
        }

        return $users;
    }


}