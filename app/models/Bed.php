<?php

class Bed extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'rooms__beds';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'type_id', 'pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'quantity'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'quantity' => 'required'
    ];

    /**
     * Each bed has a type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('BedType', 'type_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'bed';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function layouts()
    {
        return $this->belongsToMany('RoomLayout', 'rooms__layout_bed', 'bed_id', 'layout_id');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        if($layouts = $this->layouts) {
            foreach($layouts as $layout) {
                if($rooms = $layout->rooms) {
                    foreach($rooms as $room) {
                        $users[] = $room->hotel->user;
                    }
                }
            }
        }

        return $users;
    }


}