<?php

class PolicyCancellation extends BasePolicy implements \Interfaces\AccessInterface
{
    protected $table = 'policies__cancellation';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'charge_id', 'pivot','calculation_type_id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'period',
        'charge',
        'charge_type',
        'refundable'
    );

    /**
     * @var array
     */
    protected $with = array('calculation_type');

    /**
     * @var array
     */
    protected static $rules = [
        'charge' => 'required',
        'period' => 'required',
        'calculation_type_id' => 'exists:calculation__types,id'
    ];

    /**
     * @var array
     */
    public static $relationships = ['calculation_type'];

    /**
     * Dynamically add relations instead of fillable fields.
     *
     * @return array
     */
    public static function fields()
    {
        $fields = parent::fields();
        //$fields[] = 'type';

        return $fields;
    }

    /**
     * Process saving (insert/update) policies
     *
     * @param null $uuid
     * @param array $input
     * @return string
     */
    public static function process($uuid = null, array $input)
    {
        if($uuid) {
            $policy = static::findByUUIDorFail($uuid);
            $policy->period = \Input::get('period', $policy->period);
            $policy->charge = \Input::get('charge', $policy->charge);
            $policy->charge_type = \Input::get('charge_type', $policy->charge);
            $policy->refundable = \Input::get('refundable', $policy->refundable);
        } else {
            $policy = new static();
            $policy->fill($input);
            $policy->uuid = (string) $policy->generateUUID();
        }
        if($type = \Input::get('calculation_type')) {
            $policy->calculation_type()->associate(CalculationType::findByUUIDorFail($type));
        }

        return $policy;
    }

    /**
     * Each tax has a calculation type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function calculation_type()
    {
      // Continue
      return $this->belongsTo('CalculationType', 'calculation_type_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'policy cancellation';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function policies()
    {
        return $this->morphMany('Policy', 'type');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->policies as $policy) {
            if($policy->hotel) {
                $users[] = $policy->hotel->user;
            }
        }

        return $users;
    }
}