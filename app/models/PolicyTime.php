<?php

abstract class PolicyTime extends BasePolicy implements \Interfaces\AccessInterface
{
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'from',
        'to',
        'label'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'label' => 'required',
    ];

    /**
     * Process saving (insert/update) policies
     *
     * @param null $uuid
     * @param array $input
     * @return string
     */
    public static function process($uuid = null, array $input)
    {
        if($uuid) {
            $policy = static::findByUUIDorFail($uuid);
            $policy->to = \Input::get('to', $policy->to);
            $policy->from = \Input::get('from', $policy->from);
            $policy->label = \Input::get('label', $policy->label);
        } else {
            $policy = new static();
            $policy->fill($input);
            $policy->uuid = (string) $policy->generateUUID();
        }

        return $policy;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function policies()
    {
        return $this->morphMany('Policy', 'type');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->policies as $policy) {
            if($policy->hotel) {
                $users[] = $policy->hotel->user;
            }
        }

        return $users;
    }
}