<?php

class BreakfastOption extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'breakfast__options';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id', 'type_id', 'pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'adultprice',
        'childprice',
        'roomprice'
    );

    /**
     * @var array
     */
    protected static $rules = [];

    /**
     * Each breakfast option has a type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('BreakfastType', 'type_id');
    }

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'breakfast option';
    }

    /**
     * Each Breakfast Option is attached to a Room
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rooms()
    {
        return $this->belongsToMany('Room', 'rooms__room_meal', 'meal_id', 'room_id');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->rooms as $room) {
            $users[] = $room->hotel->user;
        }

        return $users;
    }
}