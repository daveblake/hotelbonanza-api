<?php

class Theme extends BaseModel implements \Interfaces\AccessInterface
{
    protected $table = 'themes';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name',
    );

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required'
    ];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'theme';
    }

    /**
     * Each theme can be assigned to multiple hotels
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hotels()
    {
        return $this->belongsToMany('Hotel', 'hotels__hotel_theme', 'theme_id', 'hotel_id');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->hotels as $hotel) {
            $users[] = $hotel->user;
        }

        return $users;
    }
}