<?php

class PolicyChargeType extends BaseModel
{
    protected $table = 'policies__charge_types';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'name'
    );

    /**
     * @var array
     */
    protected static $rules = [
        'name' => 'required'
    ];

    public static $relationships = [];

    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'policy charge type';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function policies()
    {
        return $this->morphMany('Policy', 'type');
    }

    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser()
    {
        $users = [];

        foreach($this->policies as $policy) {
            if($policy->hotel) {
                $users[] = $policy->hotel->user;
            }
        }

        return $users;
    }
}