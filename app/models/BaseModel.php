<?php

use Way\Database\Model;

abstract class BaseModel extends Model implements Interfaces\BaseInterface
{
    use \Traits\AclTrait;

    const UUID_VERSION = 4;

    /**
     * Generate a uuid. Truly generate it if the uuid column exists.
     *
     * @return \Webpatser\Uuid\Uuid
     */
    public function generateUUID()
    {
        $uuid = \Webpatser\Uuid\Uuid::generate(static::UUID_VERSION);

        if(\Schema::hasColumn($this->table, 'uuid')) {
            // Generate until truly random
            while(\DB::table($this->table)->where('uuid', $uuid)->count() > 0) {
                $uuid = \Webpatser\Uuid\Uuid::generate(static::UUID_VERSION);
            }
        }

        return $uuid;
    }

    /**
     * Find a user by uuid or fail
     *
     * @param $uuid
     * @param array $relations
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public static function findByUUIDorFail($uuid, $relations = array())
    {
        $user = static::query()
            ->where('uuid', $uuid)
            ->with($relations)
            ->first();

        if($user) {
            return $user;
        }

        throw new \Illuminate\Database\Eloquent\ModelNotFoundException(ucfirst(static::name()) . ' not found');
    }

    /**
     * Return an array of fillable items
     *
     * @return array
     */
    public static function fields()
    {
        return (new static)->fillable;
    }

    /**
     * Pre-process the rules by replacing instances of {{$id}} with the model id
     *
     * @return bool
     */
    public function validate()
    {
        foreach(static::$rules as $field => $rules) {
            $rules = explode('|', $rules);

            foreach($rules as &$rule) {
                $rule = explode(':', $rule);

                if($rule[0] == 'unique') {
                    $rule[1] = str_replace('{{$id}}', $this->id, $rule[1]);
                }

                $rule = implode(':', $rule);
            }

            static::$rules[$field] = implode($rules, '|');
        }

        return parent::validate();
    }

    /**
     * Check the user has access to the action
     *
     * @param array $options
     * @return bool
     */
    public function save(array $options = array(), $force = false)
    {
        // Don't ACL the owner model.
        if($this instanceof Owner || $force) {
            return parent::save($options);
        }

        try {
            if($this->exists && !is_null($this->uuid)) {
                // Determine whether the user has access to perform said action
                $this->hasAccess($this->uuid, $this);
            } elseif(!$this->exists) {
                throw new \Exceptions\ResourceHasNoOwnersException();
            }
        } catch(\Exceptions\AccessDeniedException $e) {
            $this->setErrors($e->getMessage());
            return false;
        } catch(\Exceptions\ResourceHasNoOwnersException $e) {
            try {
                if(!Owner::takeOwnership($this)) {
                    throw new \Exceptions\AccessDeniedException('You do not have access to this resource');
                }
            } catch(\Exceptions\AccessDeniedException $e) {
                $this->setErrors($e->getMessage());
                return false;
            }
        }

        return parent::save($options);
    }

    public function hasErrorUnauthorised()
    {
        if ($this->hasErrors()) {
            if (strtolower($this->getErrors()) === 'you do not have access to this resource') {
                return true;
            }
            foreach($this->getErrors() as $error=>$message) {
                if (strtolower($message) === 'you do not have access to this resource') {
                    return true;
                }
            }
        }
        return false;
    }
}