<?php

class NotificationRead extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'notifications__read';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('id','pivot','created_at','updated_at');

    /**
     * Fillable attributes this model allows.
     *
     * @var array
     */
    protected $fillable = array(
        'notification_id',
        'user_id'
    );

    /**
     * Load relations
     *
     * @var array
     */
    protected $with = array();

    /**
     * Required fields
     *
     * @var array
     */
    protected static $rules = [
        'notification_id' => 'required',
        'user_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification()
    {
        return $this->belongsTo('Notification', 'notification_id');
    }

    /**
     * Find by uuid and user_id
     *
     * @param $uuid
     * @param $user_id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function findByUUIDandUserId($uuid, $user_id)
    {
        $notification = static::whereHas('notification', function($query) use($uuid)
            {
                $query->where('notifications__notification.uuid', $uuid);
            })
            ->where('user_id', $user_id)
            ->firstOrFail();

        return $notification;
    }


    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name()
    {
        return 'notification read';
    }
}