<?php

class ApiTokenTest extends TestCase
{
  public function testApiTokenNoCredentials()
  {
    $this->setExpectedException('League\OAuth2\Server\Exception\InvalidRequestException');
    $resultFalse = $this->client->request('POST', '/oauth/access_token');
  }

  public function testApiTokenFalseCredentials()
  {
    $this->setExpectedException('League\OAuth2\Server\Exception\InvalidClientException');
    $resultFalse = $this->client->request('POST', '/oauth/access_token', [
      'grant_type' => 'client_credentials',
      'client_id' => 'hotelbonanzaapi',
      'client_secret' => 'testing',
      'scope' => 'extranet'
    ]);
  }
  
  public function testApiTokenValidCredentials()
  {
    //$this->setExpectedException('League\OAuth2\Server\Exception\InvalidClientException');
    $result = $this->client->request('POST', '/oauth/access_token', [
      'grant_type' => 'client_credentials',
      'client_id' => 'hotelbonanzaapi',
      'client_secret' => 'sdgsdfgb4398earuvbaesdf43',
      'scope' => 'extranet'
    ]);
    $this->assertResponseOk();
  }
  
  public function testGetValidToken()
  {
    $result = $this->client->request('POST', '/oauth/access_token', [
      'grant_type'    => 'client_credentials',
      'client_id'     => 'hotelbonanzaapi',
      'client_secret' => 'sdgsdfgb4398earuvbaesdf43',
      'scope'         => 'extranet'
    ]);
    $response = $this->client->getResponse()->getContent();
    $jsonResponse = json_decode($response);
    $this->assertNotNull($jsonResponse->access_token);
  }
}
