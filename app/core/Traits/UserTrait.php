<?php

namespace Traits;

/**
 * Trait implemented because setting $this->user on the BaseController within __construct meant
 * this error occured (https://github.com/lucadegasperi/oauth2-server-laravel/issues/231) - which
 * has been fixed in 4.x but not 3.x
 *
 * This trait also allows for non-duplicated code
 *
 * Class UserTrait
 * @package Traits
 */
trait UserTrait
{
    /**
     * @var \User
     */
    protected $user;

    /**
     * Return an instance of User based on the user auth token
     *
     * @return \User
     */
    public function getAuthdUser()
    {
        if(is_null($this->user)) {
            $this->user = \User::find($this->getCurrentApiUserid());
        }

        return $this->user;
    }

    /**
     * Return the currently auth'd user ID
     *
     * @return int
     */
    public function getCurrentApiUserid()
    {
        return (int) \Authorizer::getResourceOwnerId();
    }
}