<?php

namespace Traits;

use AvailabilityBooking;
use Carbon\Carbon;
use Input;

trait BookingFilters
{
    /**
     * Find booking based on todays date
     *
     * @param AvailabilityBooking $booking
     * @return bool
     */
    protected function filterTodayBookings(AvailabilityBooking $booking)
    {
        $today = Carbon::now();
        $from  = Carbon::createFromFormat('Y-m-d', $booking->date_from);
        $to    = Carbon::createFromFormat('Y-m-d', $booking->date_to);

        return $today->between($from, $to);
    }

    /**
     * Find booking based on one or many states
     *
     * @param AvailabilityBooking $booking
     * @return bool
     */
    protected function filterStatusBookings(AvailabilityBooking $booking)
    {
        $states = Input::get('state', array());

        if(!is_array($states)) {
            $states = (array) $states;
        }

        $status = array($booking->status);

        return count(array_intersect($states, $status)) == count($status);
    }

    /**
     * Find booking based on a from -> to date range
     *
     * @param AvailabilityBooking $booking
     * @return bool
     */
    protected function filterDateBookings(AvailabilityBooking $booking)
    {
        $find  = Carbon::createFromFormat('Y-m-d', $booking->date_from);
        $from  = Carbon::createFromFormat('Y-m-d', Input::get('from', date('Y-m-d', time())));
        $to    = Carbon::createFromFormat('Y-m-d', $from->format('Y-m-d'));

        return $find->between($from, $to);
    }
}
