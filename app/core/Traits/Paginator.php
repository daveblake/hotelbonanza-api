<?php

namespace Traits;

trait Paginator
{
    /**
     * Return the user defined limit otherwise return the default (10)
     *
     * @return int
     */
    protected function getLimit()
    {
        return (int) \Input::get('limit', static::API_DEFAULT_LIMIT);
    }

    /**
     * Return the user defined offset otherwise return the default (0)
     *
     * @return int
     */
    protected function getOffset()
    {
        return (int) \Input::get('offset', static::API_DEFAULT_OFFSET);
    }
}