<?php

namespace Traits;

use BaseModel;
use Exceptions\AccessDeniedException;
use Exceptions\ResourceHasNoOwnersException;
use Owner;

trait AclTrait
{
    use UserTrait;

    /**
     * Check the user has access to the require resource
     *
     * @param $uuid
     * @param BaseModel $model
     * @return bool
     * @throws AccessDeniedException
     * @throws ResourceHasNoOwnersException
     * @throws \Exception
     */
    public function hasAccess($uuid, BaseModel $model)
    {
        // First off, check the type of OAuth grant the user has.
        // If admin, then return the $model straight away as they have access to EVERYTHING.
        if(\Authorizer::hasScope('admin') === true) {
            return $model;
        }

        // Grab the user ID from authorizer
        $user_id = $this->getCurrentApiUserid();

        try {
            // Presuming the model doesn't throw an exception we're going to assume
            // the user is classed an as owner of the resource.
            if(Owner::findByUserIdAndModel($uuid, $user_id, $model)) {
                return $model;
            }
        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            // User doesn't have access
        } catch(ResourceHasNoOwnersException $e) {
            throw $e;
        }

        throw new AccessDeniedException('You do not have access to this resource');
    }
}