<?php

use Illuminate\Database\Eloquent\Builder;

class CoreJsonResponse
{
    use \Traits\Paginator;

    /**
     * Attempt to provide pagination results within the response.
     *
     * @param Builder $model
     * @param \Illuminate\Database\Eloquent\Collection $collection
     * @param int $limit
     * @return array
     */
    public static function respond(Builder $model, \Illuminate\Database\Eloquent\Collection $collection, $limit = 10)
    {
        $count = $model->get()->count();

        return [
            $model->getModel()->getTable() => $collection,
            'total' => (string) $count,
            'paginated' => (string) $collection->count(),
            'pages' => (string) ceil(((float)$count / (float)$limit))
        ];
    }
}