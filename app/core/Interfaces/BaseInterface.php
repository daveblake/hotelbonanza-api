<?php

namespace Interfaces;

interface BaseInterface
{
    /**
     * Provide a friendly name of the entity
     *
     * @return string
     */
    public static function name();
}