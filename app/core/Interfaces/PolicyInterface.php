<?php

namespace Interfaces;

interface PolicyInterface
{
    /**
     * Process saving (insert/update) policies
     *
     * @param null $uuid
     * @param array $data
     * @return string
     */
    public static function process($uuid = null, array $data);
}