<?php

namespace Interfaces;

interface AccessInterface
{
    /**
     * Find the owner/user of a resource. Should return an array of users
     *
     * @return array
     */
    public function findUser();
}