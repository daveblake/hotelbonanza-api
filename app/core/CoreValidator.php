<?php

class CoreValidator extends Illuminate\Validation\Validator
{
    /**
     * Validate the postcode
     *
     * @param $attribute
     * @param $value
     * @param null $parameters
     * @return bool
     */
    public function validatePostcode($attribute, $value, $parameters = null)
    {
        $regex = "/^((GIR 0AA)|((([A-PR-UWYZ][0-9][0-9]?)|(([A-PR-UWYZ][A-HK-Y][0-9][0-9]?)|(([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY])))) [0-9][ABD-HJLNP-UW-Z]{2}))$/i";
        if(preg_match($regex ,$value)) {
            return true;
        }
        return false;
    }

    /**
     * Alias for validatePostcode (at the moment). Ability to easily override the regex if US is supported at some point.
     *
     * @param $attribute
     * @param $value
     * @param null $parameters
     * @return bool
     */
    public function validateZip($attribute, $value, $parameters = null)
    {
        return $this->validatePostcode($attribute, $value, $parameters);
    }

    /**
     * Alias for validatePostcode
     *
     * @param $attribute
     * @param $value
     * @param null $parameters
     * @return bool
     */
    public function validatePostalcode($attribute, $value, $parameters = null)
    {
        return $this->validatePostcode($attribute, $value, $parameters);
    }

    /**
     * Alias for validatePostcode
     *
     * @param $attribute
     * @param $value
     * @param null $parameters
     * @return bool
     */
    public function validatePostal($attribute, $value, $parameters = null)
    {
        return $this->validatePostcode($attribute, $value, $parameters);
    }
}