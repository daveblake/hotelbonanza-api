<?php

namespace Exceptions;

class ResourceHasNoOwnersException extends \Exception {}