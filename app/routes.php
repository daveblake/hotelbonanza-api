<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
$domain = \HB\Helpers\UrlHelper::getHost();

// ota.hb stuff
Route::group(
  [
    'domain' => 'ota.'.$domain
  ],
  function ()
  {
    /*Route::any(
      '{all}',
      function ($path)
      {

        return <<<HTML
<h1>Hotel Bonanza OTA API</h1>
<p>Check the documentation for valid routes.</p>
HTML;
      }
    )->where('all', '.*');*/
    Route::resource('v1/OTA_HotelAvailNotif', 'OTA\HotelAvailNotifController');
    Route::resource('v1/OTA_HotelRateAmountNotif', 'OTA\HotelRateAmountNotifController');
    Route::resource('v1/OTA_HotelResModifyNotif', 'OTA\HotelResModifyNotifController');
    Route::resource('v1/OTA_HotelResNotif', 'OTA\HotelResNotifController');

  //duped routes from api.hb.tld
    Route::get('/login',
      function () {
        if (Auth::check()) {
          return UserController::OTACredentials(Auth::user());
        } else {
          return View::make('login');
        }
      });
    Route::post(
      '/login',
      function () {
        $data = Input::only('username', 'password');
        if (Auth::attempt($data)) {
          return UserController::OTACredentials(Auth::user());
        } else {
          return Redirect::to('login')
            ->with(
              'message',
              "Your username password combination is invalid. Please try again."
            );
        }
      }
    );

    Route::get('/logout',function(){
      try {
        Session::clear();
        //Auth::logout();
      } catch (Exception $e) {
        //ignore
      }
      return Redirect::to('login')
        ->with(
          'message',
          "You have been logged out"
        );
    });

    //catch all route
    Route::any(
      '{all}',
      function ($path)
      {

        return <<<HTML
<h1>Hotel Bonanza OTA API</h1>
<p>Check the <a href="http://hotelbonanza-otb-docs.readthedocs.io/en/latest/">documentation</a> for valid routes.</p>
HTML;
      }
    )->where('all', '.*');
  }
);

// api.hb stuff

Route::get(
  '/login',
  function ()
  {
    if(Auth::check())
    {
      return Redirect::intended('/');
    }
    else
    {
      return View::make('login');
    }
  }
);

Route::post(
  '/login',
  function ()
  {
    $data = Input::only('username', 'password');
    if(Auth::attempt($data))
    {
      return Redirect::intended('/');
    }
    else
    {
      return Redirect::to('login')
        ->with(
          'message',
          "Your username password combination is invalid. Please try again."
        );
    }
  }
);

Route::get(
  'oauth/authorize',
  [
    'before' => 'check-authorization-params|auth',
    function ()
    {

      $params              = Authorizer::getAuthCodeRequestParams();
      $params['client_id'] = $params['client']->getId();
      unset($params['client']);

      // display a form where the user can authorize the client to access it's data
      return View::make(
        'oauth.authorization-form'
      )->with(
        'params',
        $params
      );
    }
  ]
);

Route::post(
  'oauth/authorize',
  [
    'as'     => 'post oauth authorize',
    'before' => 'csrf|check-authorization-params|auth',
    function ()
    {
      $params['user_id'] = Auth::user()->id;
      $redirectUri       = '';

      // if the user has allowed the client to access its data, redirect back to the client with an auth code
      if(Input::get('approve') !== null)
      {
        $redirectUri = Authorizer::issueAuthCode(
          'user',
          $params['user_id'],
          $params
        );
      }

      // if the user has denied the client to access its data, redirect back to the client with an error message
      if(Input::get('deny') !== null)
      {
        $redirectUri = Authorizer::authCodeRequestDeniedRedirectUri();
      }

      return Redirect::to($redirectUri);
    }
  ]
);

Route::post(
  'oauth/access_token',
  function ()
  {
    return Response::json(Authorizer::issueAccessToken());
  }
);

Route::api(
  ['version' => 'v1', 'protected' => true],
  function ()
  {
      Route::post('revoke', function(){
          try {
              Authorizer::getChecker()->getAccessToken()->expire();
          } catch (Exception $e)
          {
              //Todo: This function should return 200 OK even when access_token is invalid. See: http://tools.ietf.org/html/rfc7009#section-2.1
          }
          return \Illuminate\Http\Response::create(['message'=>'Token revoked OK','status_code'=>200],200);
      });
  }
);

// Users (creation) ability to create without oAuth token
Route::api(
    ['version' => 'v1'],
    function ()
    {
        Route::post('users', 'UserController@create');
    }
);

// Users (retrieval / deletion) needs to have oAuth token
Route::api(
  ['version' => 'v1', 'protected' => true],
  function ()
  {
    Route::get('users', 'UserController@index');
    Route::get('users/current', 'UserController@current');
    Route::get('users/email/{email}', 'UserController@findByEmail');
    Route::get('users/{id}', 'UserController@show');
    Route::put('users/{id}', ['before' => 'oauth:extranet', 'uses' => 'UserController@update']);
    Route::delete('users/{id}', ['before' => 'oauth:admin', 'uses' => 'UserController@destroy']);
  }
);

// Hotel
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        Route::get('hotels', 'HotelController@index');
        Route::post('hotels', 'HotelController@create');
        Route::post('hotels/types', ['before' => 'oauth:admin', 'uses' => 'HotelController@createtypes']);
        Route::get('hotels/types', 'HotelController@types');
        Route::get('hotels/types/{id}', 'HotelController@showtypes');
        Route::delete('hotels/types/{id}', ['before' => 'oauth:admin', 'uses' => 'HotelController@destroytype']);
        Route::get('hotels/{id}/bookings', 'HotelController@bookings');
        Route::get('hotels/{id}', 'HotelController@show');
        Route::put('hotels/{id}', ['before' => 'oauth:extranet', 'uses' => 'HotelController@update']);
        Route::put('hotels/types/{id}', ['before' => 'oauth:admin', 'uses' => 'HotelController@updatetypes']);
        Route::delete('hotels/{id}', ['before' => 'oauth:admin', 'uses' => 'HotelController@destroy']);
    }
);

// Media: Hotels
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        Route::get('media/hotel', 'MediaHotelController@index');
        Route::post('media/hotel/{hotel_id}', ['before' => 'oauth:extranet', 'uses' => 'MediaHotelController@create']);
        Route::get('media/hotel/{hotel_id}', 'MediaHotelController@show');
    }
);

// Media: Rooms
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        Route::get('media/room', 'MediaRoomController@index');
        Route::post('media/room/{room_id}', ['before' => 'oauth:extranet', 'uses' => 'MediaRoomController@create']);
        Route::get('media/room/{room_id}', 'MediaRoomController@show');
    }
);

// Media Category: Categories
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        Route::get('media/categories', 'MediaCategoryController@index');
        Route::post('media/categories', ['before' => 'oauth:admin', 'uses' => 'MediaCategoryController@create']);
        Route::get('media/categories/{id}', 'MediaCategoryController@show');
        Route::put('media/categories/{id}', ['before' => 'oauth:admin', 'uses' => 'MediaCategoryController@update']);
        Route::get('media/categories/{id}/{width}/{height}', 'MediaCategoryController@size');
        Route::delete('media/categoriesa/{id}', ['before' => 'oauth:admin', 'uses' => 'MediaCategoryController@destroy']);
    }
);

// Media
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        Route::get('media', 'MediaController@index');
        Route::post('media', 'MediaController@create');
        Route::get('media/{id}', 'MediaController@show');
        Route::put('media/{id}', ['before' => 'oauth:extranet', 'uses' => 'MediaController@update']);
        Route::get('media/{id}/{width}/{height}', 'MediaController@size');
        Route::delete('media/{id}', ['before' => 'oauth:extranet', 'uses' => 'MediaController@destroy']);
    }
);

// Address
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        Route::get('address', 'AddressController@index');
        Route::post('address', 'AddressController@create');
        Route::get('address/district', 'AddressDistrictController@index');
        Route::post('address/district', 'AddressDistrictController@create');
        Route::get('address/district/{id}', 'AddressDistrictController@show');
        Route::get('address/{id}', 'AddressController@show');
        Route::put('address/{id}', ['before' => 'oauth:extranet', 'uses' => 'AddressController@update']);
        Route::delete('address/{id}', ['before' => 'oauth:extranet', 'uses' => 'AddressController@destroy']);
        Route::delete('address/district/{id}', ['before' => 'oauth:admin', 'uses' => 'AddressDistrictController@destroy']);
    }
);

// Room
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('rooms/types', 'RoomTypeController@index');
        Route::post('rooms/types', ['before' => 'oauth:admin', 'uses' => 'RoomTypeController@create']);
        Route::get('rooms/features', 'RoomFeatureController@index');
        Route::post('rooms/features', ['before' => 'oauth:admin', 'uses' => 'RoomFeatureController@create']);
        Route::get('rooms/facilities/categories', 'RoomFacilityCategoryController@index');
        Route::post('rooms/facilities/categories', ['before' => 'oauth:admin', 'uses' => 'RoomFacilityCategoryController@create']);
        Route::get('rooms/facilities', 'RoomFacilityController@index');
        Route::post('rooms/facilities', ['before' => 'oauth:admin', 'uses' => 'RoomFacilityController@create']);
        Route::get('rooms/layouts', 'RoomLayoutController@index');
        Route::post('rooms/layouts', 'RoomLayoutController@create');
        Route::get('rooms/rates', 'RoomRateController@index');
        Route::post('rooms/rates', 'RoomRateController@create');

        Route::get('rooms/types/{id}', 'RoomTypeController@show');
        Route::put('rooms/types/{id}', ['before' => 'oauth:admin', 'uses' => 'RoomTypeController@update']);
        Route::delete('rooms/types/{id}', ['before' => 'oauth:admin', 'uses' => 'RoomTypeController@destroy']);

        Route::get('rooms/features/{id}', 'RoomFeatureController@show');
        Route::put('rooms/features/{id}', ['before' => 'oauth:admin', 'uses' => 'RoomFeatureController@update']);
        Route::delete('rooms/features/{id}', ['before' => 'oauth:admin', 'uses' => 'RoomFeatureController@destroy']);

        Route::get('rooms/facilities/categories/{id}', 'RoomFacilityCategoryController@show');
        Route::put('rooms/facilities/categories/{id}', ['before' => 'oauth:admin', 'uses' => 'RoomFacilityCategoryController@update']);
        Route::delete('rooms/facilities/categories/{id}', ['before' => 'oauth:admin', 'uses' => 'RoomFacilityCategoryController@destroy']);

        Route::get('rooms/facilities/{id}', 'RoomFacilityController@show');
        Route::put('rooms/facilities/{id}', ['before' => 'oauth:admin', 'uses' => 'RoomFacilityController@update']);
        Route::delete('rooms/facilities/{id}', ['before' => 'oauth:admin', 'uses' => 'RoomFacilityController@destroy']);

        Route::get('rooms/layouts/{id}', 'RoomLayoutController@show');
        Route::put('rooms/layouts/{id}', ['before' => 'oauth:extranet', 'uses' => 'RoomLayoutController@update']);
        Route::delete('rooms/layouts/{id}', ['before' => 'oauth:extranet', 'uses' => 'RoomLayoutController@destroy']);

        Route::get('rooms/rates/{id}', 'RoomRateController@show');
        Route::put('rooms/rates/{id}', ['before' => 'oauth:extranet', 'uses' => 'RoomRateController@update']);
        Route::delete('rooms/rates/{id}', ['before' => 'oauth:extranet', 'uses' => 'RoomRateController@destroy']);
        Route::delete('rooms/rates/sync/{id}', ['before' => 'oauth:extranet', 'uses' => 'RoomRateController@sync']);

        Route::get('rooms', 'RoomController@index');
        Route::post('rooms', 'RoomController@create');
        Route::get('rooms/{id}', 'RoomController@show');
        Route::put('rooms/{id}', ['before' => 'oauth:extranet', 'uses' => 'RoomController@update']);
        Route::delete('rooms/{id}', ['before' => 'oauth:extranet', 'uses' => 'RoomController@destroy']);
    }
);

// Room: Rate Link
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('rates/link', 'RateLinkController@index');
        Route::post('rates/link', 'RateLinkController@create');
        Route::get('rates/link/{id}', 'RateLinkController@show');
        Route::put('rates/link/{id}', ['before' => 'oauth:extranet', 'uses' => 'RateLinkController@update']);
        Route::delete('rates/link/{id}', ['before' => 'oauth:extranet', 'uses' => 'RateLinkController@destroy']);
    }
);

// Bed
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('beds', 'BedController@index');
        Route::post('beds', 'BedController@create');
        Route::get('beds/types', 'BedTypeController@index');
        Route::post('beds/types', 'BedTypeController@create');
        Route::get('beds/types/{id}', 'BedTypeController@show');
        Route::put('beds/types/{id}', ['before' => 'oauth:admin', 'uses' => 'BedTypeController@update']);
        Route::delete('beds/types/{id}', ['before' => 'oauth:admin', 'uses' => 'BedTypeController@destroy']);
        Route::get('beds/{id}', 'BedController@show');
        Route::put('beds/{id}', ['before' => 'oauth:extranet', 'uses' => 'BedController@update']);
        Route::delete('beds/{id}', ['before' => 'oauth:extranet', 'uses' => 'BedController@destroy']);
    }
);

// Internet
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('internet', 'InternetController@index');
        Route::post('internet', 'InternetController@create');
        Route::get('internet/area', 'InternetAreaController@index');
        Route::post('internet/area', 'InternetAreaController@create');
        Route::get('internet/area/{id}', 'InternetAreaController@show');
        Route::put('internet/area/{id}', ['before' => 'oauth:extranet', 'uses' => 'InternetAreaController@update']);
        Route::delete('internet/area/{id}', ['before' => 'oauth:extranet', 'uses' => 'InternetAreaController@destroy']);
        Route::get('internet/{id}', 'InternetController@show');
        Route::put('internet/{id}', ['before' => 'oauth:extranet', 'uses' => 'InternetController@update']);
        Route::delete('internet/{id}', ['before' => 'oauth:extranet', 'uses' => 'InternetController@destroy']);
    }
);

// Parking
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('parking', 'ParkingController@index');
        Route::post('parking', 'ParkingController@create');
        Route::get('parking/area', 'ParkingAreaController@index');
        Route::post('parking/area', 'ParkingAreaController@create');
        Route::get('parking/area/{id}', 'ParkingAreaController@show');
        Route::put('parking/area/{id}', ['before' => 'oauth:extranet', 'uses' => 'ParkingAreaController@update']);
        Route::delete('parking/area/{id}', ['before' => 'oauth:extranet', 'uses' => 'ParkingAreaController@destroy']);
        Route::get('parking/{id}', 'ParkingController@show');
        Route::put('parking/{id}', ['before' => 'oauth:extranet', 'uses' => 'ParkingController@update']);
        Route::delete('parking/{id}', ['before' => 'oauth:extranet', 'uses' => 'ParkingController@destroy']);
    }
);

// Breakfast Options
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('breakfast', 'BreakfastController@index');
        Route::post('breakfast', 'BreakfastController@create');
        Route::get('breakfast/type', 'BreakfastTypeController@index');
        Route::post('breakfast/type', 'BreakfastTypeController@create');
        Route::get('breakfast/type/{id}', 'BreakfastTypeController@show');
        Route::put('breakfast/type/{id}', ['before' => 'oauth:extranet', 'uses' => 'BreakfastTypeController@update']);
        Route::delete('breakfast/type/{id}', ['before' => 'oauth:extranet', 'uses' => 'BreakfastTypeController@destroy']);
        Route::get('breakfast/{id}', 'BreakfastController@show');
        Route::put('breakfast/{id}', ['before' => 'oauth:extranet', 'uses' => 'BreakfastController@update']);
        Route::delete('breakfast/{id}', ['before' => 'oauth:extranet', 'uses' => 'BreakfastController@destroy']);
    }
);

// Board Options
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('board', 'BoardController@index');
        Route::post('board', 'BoardController@create');
        Route::get('board/type', 'BoardTypeController@index');
        Route::post('board/type', 'BoardTypeController@create');
        Route::get('board/type/{id}', 'BoardTypeController@show');
        Route::put('board/type/{id}', ['before' => 'oauth:extranet', 'uses' => 'BoardTypeController@update']);
        Route::delete('board/type/{id}', ['before' => 'oauth:extranet', 'uses' => 'BoardTypeController@destroy']);
        Route::get('board/{id}', 'BoardController@show');
        Route::put('board/{id}', ['before' => 'oauth:extranet', 'uses' => 'BoardController@update']);
        Route::delete('board/{id}', ['before' => 'oauth:extranet', 'uses' => 'BoardController@destroy']);
    }
);

// Countries
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('countries', 'CountriesController@index');
        Route::post('countries', ['before' => 'oauth:admin', 'uses' => 'CountriesController@create']);
        Route::get('countries/{id}', 'CountriesController@show');
        Route::put('countries/{id}', ['before' => 'oauth:admin', 'uses' => 'CountriesController@update']);
        Route::delete('countries/{id}', ['before' => 'oauth:admin', 'uses' => 'CountriesController@destroy']);
    }
);

// Telephone Prefix
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::post('telephone/prefix', 'TelephonePrefixController@create');
        Route::put('telephone/prefix/{id}', ['before' => 'oauth:admin', 'uses' => 'TelephonePrefixController@update']);
        Route::delete('telephone/prefix/{id}', ['before' => 'oauth:admin', 'uses' => 'TelephonePrefixController@destroy']);
    }
);
// Telephone Prefix Public
Route::api(
    ['version' => 'v1', 'protected' => false],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('telephone/prefix', 'TelephonePrefixController@index');
        Route::get('telephone/prefix/{id}', 'TelephonePrefixController@show');
    }
);

// Policies: Charge Types
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('policy/chargetypes', 'PolicyChargeTypesController@index');
        Route::post('policy/chargetypes', 'PolicyChargeTypesController@create');
        Route::get('policy/chargetypes/{id}', 'PolicyChargeTypesController@show');
        Route::put('policy/chargetypes/{id}', ['before' => 'oauth:extranet', 'uses' => 'PolicyChargeTypesController@update']);
        Route::delete('policy/chargetypes/{id}', ['before' => 'oauth:extranet', 'uses' => 'PolicyChargeTypesController@destroy']);
    }
);

// Policies
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('policies/{policy}', 'PoliciesController@index');
        Route::post('policies/{policy}', 'PoliciesController@create');
        Route::get('policies/{policy}/{id}', 'PoliciesController@show');
        Route::put('policies/{policy}/{id}', ['before' => 'oauth:extranet', 'uses' => 'PoliciesController@update']);
        Route::delete('policies/{policy}/{id}', ['before' => 'oauth:extranet', 'uses' => 'PoliciesController@destroy']);
    }
);

// Facilities: Categories
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('facilities/categories', 'FacilityCategoryController@index');
        Route::post('facilities/categories', ['before' => 'oauth:admin', 'uses' => 'FacilityCategoryController@create']);
        Route::get('facilities/categories/{id}', 'FacilityCategoryController@show');
        Route::put('facilities/categories/{id}', ['before' => 'oauth:admin', 'uses' => 'FacilityCategoryController@update']);
        Route::delete('facilities/categories/{id}', ['before' => 'oauth:admin', 'uses' => 'FacilityCategoryController@destroy']);
    }
);

// Facilities
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('facilities', 'FacilityController@index');
        Route::post('facilities', 'FacilityController@create');
        Route::get('facilities/{id}', 'FacilityController@show');
        Route::put('facilities/{id}', ['before' => 'oauth:admin', 'uses' => 'FacilityController@update']);
        Route::delete('facilities/{id}', ['before' => 'oauth:admin', 'uses' => 'FacilityController@destroy']);
    }
);

// Locations
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('locations', 'LocationController@index');
        Route::post('locations', 'LocationController@create');
        Route::get('locations/{id}', 'LocationController@show');
        Route::put('locations/{id}', ['before' => 'oauth:extranet', 'uses' => 'LocationController@update']);
        Route::delete('locations/{id}', ['before' => 'oauth:extranet', 'uses' => 'LocationController@destroy']);
    }
);

// Calculation: Types
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('calculation/types', 'CalculationTypesController@index');
        Route::post('calculation/types/', ['before' => 'oauth:admin', 'uses' => 'CalculationTypesController@create']);
        Route::get('calculation/types/{id}', 'CalculationTypesController@show');
        Route::put('calculation/types/{id}', ['before' => 'oauth:admin', 'uses' => 'CalculationTypesController@update']);
        Route::delete('calculation/types/{id}', ['before' => 'oauth:admin', 'uses' => 'CalculationTypesController@destroy']);
    }
);

// Tax
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('tax', 'TaxController@index');
        Route::post('tax/', 'TaxController@create');
        Route::get('tax/{id}', 'TaxController@show');
        Route::put('tax/{id}', ['before' => 'oauth:extranet', 'uses' => 'TaxController@update']);
        Route::delete('tax/{id}', ['before' => 'oauth:extranet', 'uses' => 'TaxController@destroy']);
    }
);

// Payment: Card Types
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('payment/cardtypes', 'PaymentCardTypesController@index');
        Route::post('payment/cardtypes/', ['before' => 'oauth:extranet', 'uses' => 'PaymentCardTypesController@create']);
        Route::get('payment/cardtypes/{id}', 'PaymentCardTypesController@show');
        Route::put('payment/cardtypes/{id}', ['before' => 'oauth:extranet', 'uses' => 'PaymentCardTypesController@update']);
        Route::delete('payment/cardtypes/{id}', ['before' => 'oauth:extranet', 'uses' => 'PaymentCardTypesController@destroy']);
    }
);

// Payment: Options
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('payment/options', 'PaymentOptionsController@index');
        Route::post('payment/options/', 'PaymentOptionsController@create');
        Route::get('payment/options/{id}', 'PaymentOptionsController@show');
        Route::put('payment/options/{id}', ['before' => 'oauth:extranet', 'uses' => 'PaymentOptionsController@update']);
        Route::delete('payment/options/{id}', ['before' => 'oauth:extranet', 'uses' => 'PaymentOptionsController@destroy']);
    }
);

// Features
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('features', 'FeatureController@index');
        Route::post('features/', ['before' => 'oauth:admin', 'uses' => 'FeatureController@create']);
        Route::get('features/{id}', 'FeatureController@show');
        Route::put('features/{id}', ['before' => 'oauth:admin', 'uses' => 'FeatureController@update']);
        Route::delete('features/{id}', ['before' => 'oauth:admin', 'uses' => 'FeatureController@destroy']);
    }
);

// Themes
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('themes', 'ThemeController@index');
        Route::post('themes/', ['before' => 'oauth:admin', 'uses' => 'ThemeController@create']);
        Route::get('themes/{id}', 'ThemeController@show');
        Route::put('themes/{id}', ['before' => 'oauth:admin', 'uses' => 'ThemeController@update']);
        Route::delete('themes/{id}', ['before' => 'oauth:admin', 'uses' => 'ThemeController@destroy']);
    }
);

// Channel Managers
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('channelmanagers', 'ChannelManagerController@index');
        Route::post('channelmanagers/', ['before' => 'oauth:admin', 'uses' => 'ChannelManagerController@create']);
        Route::get('channelmanagers/{id}', 'ChannelManagerController@show');
        Route::put('channelmanagers/{id}', ['before' => 'oauth:admin', 'uses' => 'ChannelManagerController@update']);
        Route::delete('channelmanagers/{id}', ['before' => 'oauth:admin', 'uses' => 'ChannelManagerController@destroy']);
    }
);

// Ratings: Questions
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('ratings/questions', 'RatingQuestionController@index');
        Route::post('ratings/questions/', 'RatingQuestionController@create');
        Route::get('ratings/questions/{id}', 'RatingQuestionController@show');
        Route::put('ratings/questions/{id}', ['before' => 'oauth:extranet', 'uses' => 'RatingQuestionController@update']);
        Route::delete('ratings/questions/{id}', ['before' => 'oauth:extranet', 'uses' => 'RatingQuestionController@destroy']);
    }
);


// Ratings
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('ratings', 'RatingController@index');
        Route::get('ratings/approved', 'RatingController@approved');
        Route::get('ratings/unapproved', 'RatingController@unapproved');
        Route::post('ratings/', 'RatingController@create');
        Route::post('ratings/approve/{id}', 'RatingController@approve');
        Route::get('ratings/{id}', 'RatingController@show');
        Route::put('ratings/{id}', ['before' => 'oauth:extranet', 'uses' => 'RatingController@update']);
        Route::delete('ratings/{id}', ['before' => 'oauth:extranet', 'uses' => 'RatingController@destroy']);
    }
);

// Events
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('events', 'EventController@index');
        Route::post('events/', ['before' => 'oauth:admin', 'uses' => 'EventController@create']);
        Route::get('events/{id}', 'EventController@show');
        Route::put('events/{id}', ['before' => 'oauth:admin', 'uses' => 'EventController@update']);
        Route::delete('events/{id}', ['before' => 'oauth:admin', 'uses' => 'EventController@destroy']);
    }
);

// Notifications
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('notifications', 'NotificationController@index');
        Route::get('notifications/read', 'NotificationController@read');
        Route::get('notifications/unread', 'NotificationController@unread');
        Route::post('notifications/', ['before' => 'oauth:admin', 'uses' => 'NotificationController@create']);
        Route::get('notifications/{id}', 'NotificationController@show');
        Route::put('notifications/{id}', ['before' => 'oauth:admin', 'uses' => 'NotificationController@update']);
        Route::delete('notifications/{id}', ['before' => 'oauth:admin', 'uses' => 'NotificationController@destroy']);
    }
);

// Currencies
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('currencies', 'CurrencyController@index');
        Route::post('currencies/', ['before' => 'oauth:admin', 'uses' => 'CurrencyController@create']);
        Route::get('currencies/{id}', 'CurrencyController@show');
        Route::put('currencies/{id}', ['before' => 'oauth:admin', 'uses' => 'CurrencyController@update']);
        Route::delete('currencies/{id}', ['before' => 'oauth:admin', 'uses' => 'CurrencyController@destroy']);
    }
);

// Timezones
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('timezones', 'TimezoneController@index');
        Route::post('timezones/', ['before' => 'oauth:admin', 'uses' => 'TimezoneController@create']);
        Route::get('timezones/{id}', 'TimezoneController@show');
        Route::put('timezones/{id}', ['before' => 'oauth:admin', 'uses' => 'TimezoneController@update']);
        Route::delete('timezones/{id}', ['before' => 'oauth:admin', 'uses' => 'TimezoneController@destroy']);
    }
);

// Availability: Rooms
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('availability/rooms', 'AvailabilityRoomController@index');
        Route::get('availability/today', 'AvailabilityRoomController@today'); //working
        Route::post('availability/clone/{id}', ['before' => 'oauth:extranet', 'uses' => 'AvailabilityRoomController@cloneAvailability']);
        Route::get('availability/month/{year}/{month}', 'AvailabilityRoomController@month'); //working
        Route::get('availability/rooms/subs', 'AvailabilityRoomSubController@index');
        Route::post('availability/rooms/subs', 'AvailabilityRoomSubController@create');
        Route::get('availability/rooms/subs/{id}', 'AvailabilityRoomSubController@show');
        Route::put('availability/rooms/subs/{id}', ['before' => 'oauth:extranet', 'uses' => 'AvailabilityRoomSubController@update']);
        Route::delete('availability/rooms/subs/{id}', ['before' => 'oauth:extranet', 'uses' => 'AvailabilityRoomSubController@destroy']);
        Route::post('availability/rooms/', 'AvailabilityRoomController@create'); //should be okay as is
        Route::get('availability/rooms/{id}', 'AvailabilityRoomController@show'); //should be okay as is
        Route::put('availability/rooms/{id}', ['before' => 'oauth:extranet', 'uses' => 'AvailabilityRoomController@update']); //should be okay as is
        Route::delete('availability/rooms/{id}', ['before' => 'oauth:extranet', 'uses' => 'AvailabilityRoomController@destroy']);//should be okay as is
        Route::get('availability', 'AvailabilityRoomController@available'); //working for dates
        Route::get('availability/hotel/{id}', 'AvailabilityRoomController@hotelAvailable'); //working for dates (uses same as @available function)
        Route::post('availability/link', 'AvailabilityRoomController@link'); //should be fine as availability rooms are now controlled by rates
        Route::delete('availability/link', ['before' => 'oauth:extranet', 'uses' => 'AvailabilityRoomController@deleteLink']); //should be fine as availability rooms are now controlled by rates
    }
);

// Availability: Bookings
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('availability/bookings', ['before' => 'oauth:admin','uses' => 'AvailabilityBookingController@index']);
        Route::post('availability/bookings/', 'AvailabilityBookingController@create');
        Route::get('availability/bookings/{id}', ['before' => 'oauth:extranet', 'uses' => 'AvailabilityBookingController@show']);
        Route::put('availability/bookings/{id}', ['before' => 'oauth:extranet', 'uses' => 'AvailabilityBookingController@update']);
        Route::delete('availability/bookings/{id}', ['before' => 'oauth:admin', 'uses' => 'AvailabilityBookingController@destroy']);
    }
);

// Bookings: Payments (must come above bookings for route ordering
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('bookings/payments', ['before' => 'oauth:extranet','uses' => 'BookingPaymentController@index']);
        Route::post('bookings/payments', 'BookingPaymentController@create');
        Route::get('bookings/payments/{id}', ['before' => 'oauth:extranet', 'uses' => 'BookingPaymentController@show']);
        Route::put('bookings/payments/{id}', ['before' => 'oauth:extranet', 'uses' => 'BookingContrBookingPaymentControlleroller@update']);
        Route::delete('bookings/payments/{id}', ['before' => 'oauth:admin', 'uses' => 'BookingPaymentController@destroy']);
    }
);

// Bookings
Route::api(
    ['version' => 'v1', 'protected' => true],
    function ()
    {
        // Order or routes matters because Routes are read top down.
        Route::get('bookings', ['before' => 'oauth:admin','uses' => 'BookingController@index']);
        Route::get('bookings/current', ['before' => 'oauth:extranet','uses' => 'BookingController@current']);
        Route::post('bookings', 'BookingController@create');
        Route::get('bookings/{id}', ['before' => 'oauth:extranet', 'uses' => 'BookingController@show']);
        Route::put('bookings/{id}', ['before' => 'oauth:extranet', 'uses' => 'BookingController@update']);
        Route::delete('bookings/{id}', ['before' => 'oauth:admin', 'uses' => 'BookingController@destroy']);
    }
);


Route::any(
  '{all}',
  function ($path)
  {

    return <<<HTML
<h1>Hotel Bonanza API</h1>
<p>Check the documentation for valid routes.</p>
HTML;
  }
)->where('all', '.*');
