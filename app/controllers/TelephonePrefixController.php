<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class TelephonePrefixController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['country'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $prefixes =  TelephonePrefix::with(static::$relations)
            ->orderBy('prefix','asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return CoreJsonResponse::respond(TelephonePrefix::query(), $prefixes, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(TelephonePrefix::fields());

        $telephone = new TelephonePrefix();
        $telephone->fill($input);
        $telephone->uuid = (string) $telephone->generateUUID();

        if(!$telephone->save())
        {
            return $this->response->errorBadRequest($telephone->getErrors());
        }

        return $this->response->array(['telprefix_id' => $telephone]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $telephone = TelephonePrefix::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Telephone Prefix Not Found');
        }

        return $this->response->array($telephone->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $telephone = TelephonePrefix::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Telephone Prefix Not Found');
        }

        $telephone->prefix = \Input::get('prefix', $telephone->prefix);

        if(!$telephone->save())
        {
            if ($telephone->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($telephone->getErrors());
            }
            return $this->response->errorBadRequest($telephone->getErrors());
        }

        return $this->response->array(['telprefix_id' => $telephone->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $telephone = TelephonePrefix::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Telephone Prefix Not Found');
        }

        $telephone->delete();
    }
}