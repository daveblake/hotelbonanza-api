<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class FacilityController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Cache the count ready to be returned
        $count = Facility::all()->count();

        $facilities = Facility::with('category')
            ->orderBy('name','asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'facilities' => $facilities,
            'total' => (string) $count,
            'paginated' => (string) $facilities->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Facility::fields());

        try
        {
            $facility = new Facility();
            $facility->fill($input);
            $facility->uuid = (string) $facility->generateUUID();

            // Find and associate the category
            $this->associateCategory($facility, Input::get('category'));

            if(!$facility->save())
            {
                return $this->response->errorBadRequest($facility->getErrors());
            }

            return $this->response->array(['facility_id'=>$facility->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $facility = Facility::findByUUIDorFail($id, ['category']);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Facility Not Found');
        }

        return $this->response->array($facility->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $facility = Facility::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Facility Not Found');
        }

        try
        {
            $facility->name = \Input::get('name', $facility->name);

            // Find and associate the category
            $this->associateCategory($facility, Input::get('category'));

            if(!$facility->save())
            {
                if ($facility->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($facility->getErrors());
                }
                return $this->response->errorBadRequest($facility->getErrors());
            }

            return $this->response->array(['facility_id'=>$facility->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $facility = Facility::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Facility Not Found');
        }

        $facility->delete();
    }

    /**
     * Attempt to associate the FacilityType with the current Facility model
     *
     * @param Facility $facility
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateCategory(Facility &$facility, $uuid)
    {
        if(!is_null($uuid)) {
            // Find and associate the category
            $category = FacilityCategory::findByUUIDorFail($uuid);
            $facility->category()->associate($category);
        }
    }
}