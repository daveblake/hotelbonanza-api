<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class HotelController extends \BaseController
{
    use Traits\BookingFilters;

    /**
     * @var array
     */
    protected static $relations = [
        'type',
        'user',
        'user.channelmanager',
        'user.telprefix',
        'rooms',
        'images',
        'images.room',
        'rooms.type',
        'rooms.subtype',
        'rooms.layouts',
        'rooms.layouts.beds',
        'rooms.layouts.beds.type',
        'rooms.policies',
        'rooms.features',
        'rooms.facilities',
        'rooms.images',
        'rooms.rates',
        'rooms.rates.breakfastoptions.type',
        'address',
        'address.location',
        'internet.area',
        'parking.area',
        'breakfastoptions.type',
        'boardoptions.type',
        'telprefix',
        'mobprefix',
        'policies.type',
        'facilities.category',
        'taxes',
        'taxes.calculation_type',
        'paymentoptions.cardtypes',
        'features',
        'themes',
        'ratings',
        'timezone',
        'timezone.countries',
        'channelmanager',
        'billingAddress',
        'billingAddress.location'
    ];

    /**
     * @var array
     */
    protected static $simpleRelations = [
      'type',
      'images',
      'address',
      'address.location',
      'channelmanager',
      'ratings'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = Hotel::all()->count();

        if ($this->getLimit() < 25) {

            $relations = static::$relations;
            $relations['rooms'] = function ($query) {
                /** @var $query Illuminate\Database\Query\Builder */
                $query->select('rooms.*')
                  ->leftJoin('rooms__types', 'rooms__types.id', '=', 'rooms.type_id')
                  ->leftJoin('rooms__types as rst', 'rst.id', '=', 'rooms.sub_type_id')
                  ->orderBy('rooms__types.order', 'asc')->orderBy('rst.order', 'asc');
            };
            $hotels = Hotel::with($relations)
              ->skip($this->getOffset())
              ->take($this->getLimit())
              ->get();

//            foreach ($hotels as $hotel) {
//                if (!$hotel->tagline) {
//                    $hotel->tagline = $hotel->firstSentence('shortDescription');
//                }
//            }

            return [
              'hotels'    => $hotels,
              'total'     => (string)$count,
              'paginated' => (string)$hotels->count(),
              'pages'     => (string)ceil(((float)$count / (float)$this->getLimit()))
            ];
        } else {
            //large hotel array, skip relations
            /**
             * @var $hotels \Illuminate\Database\Eloquent\Collection
             */
            if(null != Input::get('incompletedays', null)) {
              $days = (int) Input::get('incompletedays', null);
              $date = \Carbon\Carbon::now();
              $date = $date->subDays($days);
              $hotels = Hotel::with(['user'])->whereHas('user',function($query) use ($date)
              {
                $query->whereDate('last_login','=',$date->toDateString());

              })
                ->skip($this->getOffset())
                ->where('progress_total', '<=', 9)
                ->where('terms_accepted_at', '=', null)
                ->take($this->getLimit())
                ->get();
            } else {
              $hotels = Hotel::with(['user','address','channelmanager'])->skip($this->getOffset())
                ->take($this->getLimit())
                ->get();
            }
            $hotels->each(function($hotel)
            {
                /**
                 * @var $hotel Hotel
                 */
                $hotel->setAllData(false);
            });
            return CoreJsonResponse::respond(Hotel::query(), $hotels, $this->getLimit());
//            return [
//              'hotels'    => $hotels,
//              'total'     => (string)$count,
//              'paginated' => (string)$hotels->count(),
//              'pages'     => (string)ceil(((float)$count / (float)$this->getLimit()))
//            ];
        }
    }

    /**
     * Return all bookings for said hotel
     *
     * @param string $hotel
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function bookings($hotel)
    {
        // List of filters to apply when looking for bookings against a hotel
        $filters = Input::get('filters', array());

        // Bookings stored temporarily
        $bookings = array();

        // Get the hotel
        $hotel = Hotel::query()->where('uuid', $hotel)->first();

        // Fetch all the booking of each room
        foreach($hotel->rooms as $room) {
            foreach($room->bookings as $availabilityRoom) {
                foreach($availabilityRoom->bookings as $booking) {
                    // Apply $_GET files if found within Traits\BookingFilters
                    if(!empty($filters)) {
                        foreach($filters as $filter) {
                            // Make sure the filter exists
                            $filterMethod = 'filter' . ucfirst($filter) . 'Bookings';
                            if(method_exists($this, $filterMethod)) {
                                // If the filter returns false, don't bother adding to the collection
                                if($this->{$filterMethod}($booking) === false) {
                                    continue 2;
                                }
                            }
                        }

                        // Filters suggest we can add this to the collection
                        $bookings[] = $booking;
                    } else {
                        // Presume we want to see all bookings
                        $bookings[] = $booking;
                    }
                }
            }
        }

        $collection = Collection::make($bookings);

        return $collection;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Hotel::fields());

        $hotel = new Hotel();

        $hotel->fill($input);
        $hotel->uuid = (string) $hotel->generateUUID();
        $hotel->generateSetOTACredentials();

        // One time operation. As it stands, hotels cannot be swapped between users.

        try {
            $hotel->user()->associate(User::findOrFail(\Authorizer::getResourceOwnerId()));
        } catch (ModelNotFoundException $e) {
            //might be the wrong error here?
            return $this->response->errorNotFound('User \''.\Authorizer::getResourceOwnerId().'\' not found');
        }

        // Attempt to associate the area
        try
        {
            $this->handleAssociations($hotel);
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$hotel->save())
        {
            return  $this->response->errorBadRequest($hotel->getErrors());
        }

        try {
            \Mail::later(30,'emails.hotel.welcome', array('hotel' => $hotel), function($message) use($hotel)
            {
                $message->to($hotel->user->email)->subject('Hotel Created');
            });
        } catch(\Exception $e) {}

        return $this->response->array(['hotel_id' => $hotel->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->__adminLoginAsUser();
        try
        {
            $relations = static::$relations;
            $relations['rooms'] = function($query) {
                /** @var $query Illuminate\Database\Query\Builder */
                $query->select('rooms.*')
                    ->leftJoin('rooms__types','rooms__types.id','=','rooms.type_id')
                    ->leftJoin('rooms__types as rst','rst.id','=','rooms.sub_type_id' )
                    ->orderBy('rooms__types.order','asc')->orderBy('rst.order','asc');
            };
            $hotel = Hotel::findByUUIDorFail($id, $relations);
        }
        catch(ModelNotFoundException $e)
        {
            return  $this->response->errorNotFound('Hotel Not Found');
        }

        if(!$hotel->tagline) {
            $hotel->tagline = $hotel->firstSentence('shortDescription');
        }

        return $this->response->array($hotel->toArray());
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function showtypes($id)
    {
        try
        {
            $hoteltype = HotelType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return  $this->response->errorNotFound('Hotel Type Not Found');
        }

        return $this->response->array($hoteltype->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $this->__adminLoginAsUser();
        try
        {
            $hotel = Hotel::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Hotel Not Found');
        }

        // Map fields from input, otherwise use current value
        foreach(Hotel::fields() as $field) {
            $hotel->{$field} = Input::get($field, $hotel->{$field});
        }

        if($hotel->ota_hotel_id == null) {
          $hotel->generateSetOTACredentials();
        }
        
        if (Input::get('terms_accepted_at', null) != null) {
            try {
                $hotel->terms_accepted_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Input::get('terms_accepted_at', null));
            } catch(Exception $e) {
                return  $this->response->errorBadRequest($e->getMessage());
            }
        }

        // Attempt to associate the area
        try
        {
            $this->handleAssociations($hotel);
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$hotel->save())
        {
            if ($hotel->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($hotel->getErrors());
            }
            return  $this->response->errorBadRequest($hotel->getErrors());
        }

        return $this->response->array(['hotel_id'=>$hotel->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->__adminLoginAsUser();
        try
        {
            $hotel = Hotel::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->array(['errors' => 'Hotel Not Found']);
        }

        $hotel->delete();
    }
    
    public function destroytype($id)
    {
        try
        {
            $hotelType = HotelType::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->array(['errors' => 'Hotel type Not Found']);
        }

        $hotelType->delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function types()
    {
        return CoreJsonResponse::respond(HotelType::query(), HotelType::orderBy('name','asc')->get(), $this->getLimit());
    }

    public function createtypes()
    {
        $input = Input::only(HotelType::fields());

        $type = new HotelType();
        $type->fill($input);
        $type->uuid = (string) $type->generateUUID();


        if(!$type->save())
        {
            return $this->response->errorBadRequest($type->getErrors());
        }

        return $this->response->array(['hoteltype_id' => $type->uuid]);
    }

    public function updatetypes($id)
    {
        try
        {
            $type = HotelType::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Hotel Type Not Found');
        }

        $type->name = \Input::get('name', $type->name);
        $type->description = \Input::get('description', $type->description);

        if(!$type->save())
        {
            return $this->response->errorBadRequest($type->getErrors());
        }

        return $this->response->array(['hoteltype_id' => $type->uuid]);
    }

    /**
     * Handle specific Associations for hotel instances
     *
     * @param Hotel $hotel
     */
    protected function handleAssociations(Hotel &$hotel)
    {
        if($type = Input::get('type')) {
            $this->associateType($hotel, $type);
        }

        if($timezone = Input::get('timezone')) {
            $this->associateTimezone($hotel, $timezone);
        }

        if($address = Input::get('address')) {
            $this->associateAddress($hotel, $address);
        }

        if ($billingAddress = Input::get('billing_address')) {
            $this->associateBillingAddress($hotel, $billingAddress);
        }

        if($rooms = Input::get('rooms', [])) {
            $this->associateRooms($hotel, array_unique($rooms));
        }

        if($images = Input::get('images', [])) {
            $this->associateImages($hotel, array_unique($images), false);
        }

        if($internet = Input::get('internet', [])) {
            $this->associateInternet($hotel, array_unique($internet));
        }

        if($parking = Input::get('parking', [])) {
            $this->associateParking($hotel, array_unique($parking));
        }

        if($breakfast = Input::get('breakfast', [])) {
            $this->associateBreakfast($hotel, array_unique($breakfast));
        }

        if($board = Input::get('board', [])) {
            $this->associateBoard($hotel, array_unique($board));
        }

        if($prefix = Input::get('telprefix')) {
            $this->associateTelephonePrefix($hotel, $prefix, 'telephone_prefix_id');
        }

        if($prefix = Input::get('mobprefix')) {
            $this->associateTelephonePrefix($hotel, $prefix, 'mobile_prefix_id');
        }

        if($policies = Input::get('policies', [])) {
            $this->associatePolicies($hotel, array_unique($policies));
        }

        if($facilities = Input::get('facilities', [])) {
            $this->associateFacilities($hotel, array_unique($facilities));
        }

        if($taxes = Input::get('taxes', [])) {
            $this->associateTaxes($hotel, array_unique($taxes));
        }

        if($paymentoptions = \Input::get('paymentoptions', [])) {
            $this->associatePaymentOptions($hotel, array_unique($paymentoptions));
        }

        if($features = Input::get('features', [])) {
            $this->associateFeatures($hotel, array_unique($features));
        }
        
        if($themes = Input::get('themes', [])) {
            $this->associateThemes($hotel, array_unique($themes));
        }

        if ($channelManager = Input::get('channelmanager', null)) {
          $this->associateChannelManager($hotel, $channelManager);
        }
    }

    /**
     * Associate an address with the current hotel
     *
     * @param Hotel $hotel
     * @param $uuid
     */
    protected function associateAddress(Hotel &$hotel, $uuid)
    {
        $hotel->address()->associate(Address::findByUUIDorFail($uuid));
    }

    /**
     * Associate an address with the current hotel
     *
     * @param User $user
     * @param $uuid
     */
    protected function associateBillingAddress(Hotel &$user, $uuid)
    {
      $user->billingAddress()->associate(Address::findByUUIDorFail($uuid));
    }


    /**
     * Associate a timezone with the current hotel
     *
     * @param Hotel $hotel
     * @param $uuid
     */
    protected function associateTimezone(Hotel &$hotel, $uuid)
    {
        $hotel->timezone()->associate(Timezone::findByUUIDorFail($uuid));
    }

    /**
     * Associate a telephone prefix with the current hotel
     *
     * @param Hotel $hotel
     * @param $uuid
     * @param string $field
     */
    protected function associateTelephonePrefix(Hotel &$hotel, $uuid, $field = 'telephone_prefix_id')
    {
        switch($field)
        {
            case 'mobile_prefix_id':
                $hotel->mobprefix()->associate(TelephonePrefix::findByUUIDorFail($uuid));
                break;

            default:
                $hotel->telprefix()->associate(TelephonePrefix::findByUUIDorFail($uuid));
                break;
        }
    }

    /**
     * Associate hotel type with current hotel
     *
     * @param Hotel $hotel
     * @param $uuid
     */
    protected function associateType(Hotel &$hotel, $uuid)
    {
        $hotel->type()->associate(HotelType::findByUUIDorFail($uuid));
    }

    /**
     * Associate one or more rooms with a hotel
     *
     * @param Hotel $hotel
     * @param array $rooms
     * @param bool $sync DEPRECATED, detaching rooms then re-syncing was leaving rooms unattached. Rooms are now deleted removing the requirement to reset the sync
     */
    protected function associateRooms(Hotel &$hotel, array $rooms, $sync = true)
    {
        // attempt to relate a room uuid to the current hotel
        foreach((array)$rooms as $uuid) {
            $hotel->rooms()->save(Room::findByUUIDorFail($uuid));
        }
    }

    /**
     * Associate one or more images with a hotel
     *
     * @param Hotel $hotel
     * @param array $images
     * @param bool $sync
     */
    protected function associateImages(Hotel &$hotel, array $images, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> images in sync
            $hotel->images()->detach($hotel->images->lists('id'));
        }

        // attempt to relate an image uuid to the current hotel
        foreach((array)$images as $uuid) {
          try {
            $hotel->images()->sync([Media::findByUUIDorFail($uuid)->id],false);
          } catch (ModelNotFoundException $e) {
            //skip missing images
          }
        }
    }

    /**
     * Associate one or more internet options with a hotel
     *
     * @param Hotel $hotel
     * @param array $internet
     * @param bool $sync
     */
    protected function associateInternet(Hotel &$hotel, array $internet, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> internet in sync
            $hotel->internet()->detach($hotel->internet->lists('id'));
        }

        // attempt to relate an internet uuid to the current hotel
        foreach((array)$internet as $uuid) {
            $hotel->internet()->attach(Internet::findByUUIDorFail($uuid));
        }
    }

    /**
     * Associate one or more parking options with a hotel
     *
     * @param Hotel $hotel
     * @param array $parking
     * @param bool $sync
     */
    protected function associateParking(Hotel &$hotel, array $parking, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> parking in sync
            $hotel->parking()->detach($hotel->parking->lists('id'));
        }

        // attempt to relate a parking uuid to the current hotel
        foreach((array)$parking as $uuid) {
            $hotel->parking()->attach(Parking::findByUUIDorFail($uuid));
        }
    }

    /**
     * Associate one or more breakfast options with a hotel
     *
     * @param Hotel $hotel
     * @param array $breakfast
     * @param bool $sync
     */
    protected function associateBreakfast(Hotel &$hotel, array $breakfast, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> beds in sync
            $hotel->breakfastoptions()->detach($hotel->breakfastoptions->lists('id'));
        }

        // attempt to relate a breakfast option uuid to the current hotel
        foreach((array)$breakfast as $uuid) {
            $hotel->breakfastoptions()->attach(BreakfastOption::findByUUIDorFail($uuid));
        }
    }
    /**
     * Associate one or more board options with a hotel
     *
     * @param Hotel $hotel
     * @param array $board
     * @param bool $sync
     */
    protected function associateBoard(Hotel &$hotel, array $board, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> beds in sync
            $hotel->boardOptions()->detach($hotel->boardoptions->lists('id'));
        }

        // attempt to relate a breakfast option uuid to the current hotel
        foreach((array)$board as $uuid) {
            $hotel->boardoptions()->attach(BoardOption::findByUUIDorFail($uuid));
        }
    }

    /**
     * Associate one or more policies with a hotel
     *
     * @param Hotel $hotel
     * @param array $policies
     * @param bool $sync
     */
    protected function associatePolicies(Hotel &$hotel, array $policies, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> beds in sync
           \DB::table('hotels__policies')->where('hotel_id', $hotel->id)->delete();
        }

        // attempt to relate a breakfast option uuid to the current hotel
        foreach((array)$policies as $uuid) 
        {
            foreach(PoliciesController::$policies as $policy) 
            {
                try
                {
                    $object = $policy::findByUUIDorFail($uuid);

                    \DB::table('hotels__policies')->insert([
                        'hotel_id' => $hotel->id,
                        'type_id' => $object->id,
                        'type_type' => $policy
                    ]);

                } catch (\Exception $e) {}
            }
        }
    }

    /**
     * Associate one or more facilities with a hotel
     *
     * @param Hotel $hotel
     * @param array $facilities
     * @param bool $sync
     */
    protected function associateFacilities(Hotel &$hotel, array $facilities, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> beds in sync
            $hotel->facilities()->detach($hotel->facilities->lists('id'));
        }

        // attempt to relate a breakfast option uuid to the current hotel
        foreach((array)$facilities as $uuid) {
            $hotel->facilities()->attach(Facility::findByUUIDorFail($uuid));
        }
    }

    /**
     * Associate one or more taxes with a hotel
     *
     * @param Hotel $hotel
     * @param array $taxes
     * @param bool $sync
     */
    protected function associateTaxes(Hotel &$hotel, array $taxes, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> rooms in sync
            $hotel->taxes()->update(['hotel_id'=>0]);
        }

        // attempt to relate a room uuid to the current hotel
        foreach((array)$taxes as $uuid) {
            $hotel->taxes()->save(Tax::findByUUIDorFail($uuid));
        }
    }

    /**
     * Associate payment options with a hotel
     *
     * @param Hotel $hotel
     * @param $uuid
     */
    protected function associatePaymentOptions(Hotel &$hotel, $uuid)
    {
        $hotel->paymentoptions()->save(PaymentOption::findByUUIDorFail($uuid));
    }

    /**
     * Associate one or more features with a hotel
     *
     * @param Hotel $hotel
     * @param array $features
     * @param bool $sync
     */
    protected function associateFeatures(Hotel &$hotel, array $features, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> beds in sync
            $hotel->features()->detach($hotel->features->lists('id'));
        }

        // attempt to relate a breakfast option uuid to the current hotel
        foreach((array)$features as $uuid) {
            $hotel->features()->attach(Feature::findByUUIDorFail($uuid));
        }
    }
    
    /**
     * Associate one or more themes with a hotel
     *
     * @param Hotel $hotel
     * @param array $themes
     * @param bool $sync
     */
    protected function associateThemes(Hotel &$hotel, array $themes, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> beds in sync
            $hotel->themes()->detach($hotel->themes->lists('id'));
        }

        // attempt to relate a breakfast option uuid to the current hotel
        foreach((array)$themes as $uuid) {
            $hotel->themes()->attach(Theme::findByUUIDorFail($uuid));
        }
    }

    protected function associateChannelManager(Hotel &$hotel, $uuid)
    {
      if (!is_null($uuid)) {
        // Find and associate the category
        $channelmanager = ChannelManager::findByUUIDorFail($uuid);
        $hotel->channelmanager()->associate($channelmanager);
      }
    }
}