<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class MediaHotelController extends \MediaController
{
    protected static $relations = 'hotel';

    public function create()
    {
        $args = func_get_args();

        try
        {
            $hotel = Hotel::findByUUIDorFail(array_get($args, 0));
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Hotel Not Found');
        }

        $input = Input::only(Media::fields());

        $media = new Media();
        $media->fill($input);
        $media->uuid = (string) $media->generateUUID();
        $media->order = \Input::get('order', 0); // @todo handle correctly
        $media->type = \Input::get('type', 'media');

        if(!$media->save())
        {
            return $this->response->errorBadRequest($media->getErrors());
        }

        $media->hotel()->save($hotel);

        return $this->response->array(['media_id' => $media->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $hotel = Hotel::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Hotel Not Found');
        }

        try
        {
            $media = Media::whereHas('hotel', function($query) use($hotel) {
                $query->where('id', $hotel->id);
            })->get();
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Media Not Found');
        }

        return $this->response->array($media->toArray());
    }
}