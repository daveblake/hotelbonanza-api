<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class NotificationController extends \BaseController
{
    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // If the administrator has made a request get all notifications.
        if(\Authorizer::hasScope('admin')) {
            return $this->all();
        }

        // Otherwise return global and user specific notifications
        return Notification::userNotificationsUnreadWithGlobal(\Authorizer::getResourceOwnerId());
    }

    /**
     * Display all notifications.
     *
     * @return Response
     */
    public function all()
    {
        return Notification::with(static::$relations)->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Notification::fields());

        try
        {
            $notification = new Notification();
            $notification->fill($input);
            $notification->uuid = (string) $notification->generateUUID();

            $users = Input::get('users', array());

            if(empty($users)) {
                $notification->global = true;
            } else {
                $notification->global = false;
            }

            if(!$notification->save()) {
                return $this->response->errorBadRequest($notification->getErrors());
            }

            $this->handleRelations($notification);

            $notification->save();

            return $this->response->array(['notification_id'=>$notification->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $notification = Notification::findByUUIDorFail($id, static::$relations);

            try {
                $read = NotificationRead::findByUUIDandUserId($id, \Authorizer::getResourceOwnerId());
                $read->touch();
            } catch(ModelNotFoundException $e) {
                // Mark notification as read for user
                $read = $notification->reads()->create([
                    'notification_id' => $notification->id,
                    'user_id' => \Authorizer::getResourceOwnerId()
                ]);
                $read->save();
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Notification Not Found');
        }

        return $this->response->array($notification->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $notification = Notification::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Notification Not Found');
        }

        $notification->message = \Input::get('message', $notification->message);

        if(!$notification->save())
        {
            if ($notification->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($notification->getErrors());
            }
            return $this->response->errorBadRequest($notification->getErrors());
        }

        return $this->response->array(['notification_id'=> $notification->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $notification = Notification::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Notification Not Found');
        }

        $notification->delete();
    }

    /**
     * Only fetch approved ratings
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function read()
    {
        return Notification::userNotificationsRead(\Authorizer::getResourceOwnerId());
    }

    /**
     * Only fetch unapproved ratings
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function unread()
    {
        return Notification::userNotificationsUnread(\Authorizer::getResourceOwnerId());
    }

    /**
     * Handle saving relations for Notifications
     *
     * @param Notification $notification
     */
    protected function handleRelations(Notification &$notification)
    {
        if($users = Input::get('users')) {
            $this->associateUsers($notification, $users);
        }
    }

    /**
     * Associate one or more users with a notification
     *
     * @param Notification $notification
     * @param array $users
     * @param bool $sync
     */
    protected function associateUsers(Notification &$notification, array $users, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the notifications -> user
            $notification->users()->detach($notification->users->lists('id'));
        }

        // attempt to relate a notification to a user
        foreach((array)$users as $uuid) {
            $notification->users()->attach(User::findByUUIDorFail($uuid));
        }
    }
}