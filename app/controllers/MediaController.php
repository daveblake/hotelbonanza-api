<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class MediaController extends \BaseController
{
    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if(!static::$relations) {
            return Media::query()
                ->orderBy('order','asc')
                ->skip($this->getOffset())
                ->take($this->getLimit())
                ->get();
        }

        if(is_string(static::$relations)) {
            return Media::has(static::$relations, '>=', DB::raw(1))->with(static::$relations)
                ->orderBy('order','asc')
                ->skip($this->getOffset())
                ->take($this->getLimit())
                ->get();
        }

        return Media::with(static::$relations)
            ->orderBy('order','asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Media::fields());

        $media = new Media();
        $media->fill($input);
        $media->uuid = (string) $media->generateUUID();
        $media->order = \Input::get('order', 0); // @todo handle correctly
        $media->type = \Input::get('type', 'media');

        if(!$media->save())
        {
            return $this->response->errorBadRequest($media->getErrors());
        }

        try
        {
            if($categories = Input::get('categories', [])) {
                $this->associateCategories($media, $categories);
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $this->response->array(['media_id' => $media->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $media = Media::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Media Not Found');
        }

        return $this->response->array($media->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $media = Media::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Media Not Found');
        }

        $media->url = Input::get('url', $media->url);
        $media->order = Input::get('order', $media->order);
        $media->description = Input::get('description', $media->description);

        try
        {
            if($categories = Input::get('categories', [])) {
                $this->associateCategories($media, $categories);
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        //currently usign desc for rooms
        if (strlen(Input::get('description',$media->description)) > 5) {
            //probably a uuid due to length
            try
            {
                $room = Room::findByUUIDorFail(Input::get('description',$media->description));
                $media->room()->detach();
                $media->room()->save($room);
            }
            catch(ModelNotFoundException $e)
            {
                return $this->response->errorNotFound('Room Not Found');
            }
        } else {
            $media->room()->detach();
        }

        if(!$media->save())
        {
            if ($media->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($media->getErrors());
            }
            return $this->response->errorBadRequest($media->getErrors());
        }

        return $this->response->array(['media_id' => $media->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $media = Media::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Media Not Found');
        }
        $media->hotel()->detach();

        $media->delete();
    }

    /**
     * Process and return a particular size of a media
     *
     * @param $id
     * @param $width
     * @param $height
     * @return \Illuminate\Http\Response
     */
    public function size($id, $width, $height)
    {
        try
        {
            $media = Media::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Media Not Found');
        }

        $file = Config::get('image::path') . $media->uuid . '-' . $width . '-' . $height . '.' . Config::get('image::extension');

        if(file_exists($file)) {
            $image = Image::make($file);
        } elseif(Config::get('image::cache', false)) {
            $image = Image::make($media->url)->resize($width, $height);
            $image->save($file);
        }

        return $image->response(Config::get('image::extension', 'jpg'));
    }

    /**
     * Associate one or more categories with a media
     *
     * @param Media $media
     * @param array $categories
     * @param bool $sync
     */
    protected function associateCategories(Media &$media, array $categories, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the hotel -> rooms in sync
            $media->categories()->detach($media->categories->lists('id'));
        }

        // attempt to relate a room uuid to the current hotel
        foreach((array)$categories as $uuid) {
            if($uuid) {
                $media->categories()->save(MediaCategory::findByUUIDorFail($uuid));
            }
        }
    }
}