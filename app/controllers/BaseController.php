<?php

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;

class BaseController extends Controller
{
	use ControllerTrait;
	use Traits\Paginator;
	use Traits\UserTrait;

	const API_DEFAULT_LIMIT = 10;
	const API_DEFAULT_OFFSET = 0;

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function __adminLoginAsUser()
	{
		try {
			if (\Authorizer::hasScope('admin') === true) {
				if (null != Request::header('admin_logged_in_as_user_id', null)) {
					$user = User::findByUUIDorFail(Request::header('admin_logged_in_as_user_id', null));
					$this->user = $user;
				}
			}
		} catch (Exception $e) {}
	}

}
