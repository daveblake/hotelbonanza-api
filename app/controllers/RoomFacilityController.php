<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RoomFacilityController extends \BaseController
{
    static $relations = [
        'category'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return RoomFacility::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(RoomFacility::fields());

        try
        {
            $facility = new RoomFacility();
            $facility->fill($input);
            $facility->uuid = (string) $facility->generateUUID();

            // Find and associate the category
            $this->associateCategory($facility, Input::get('category', $facility->category_id));

            if(!$facility->save())
            {
                return $this->response->errorBadRequest($facility->getErrors());
            }

            return $this->response->array(['facility_id'=>$facility->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $facility = RoomFacility::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Facility Not Found');
        }

        return $this->response->array($facility->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $facility = RoomFacility::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Facility Not Found');
        }

        $facility->name = \Input::get('name', $facility->name);

        // Find and associate the category
        $this->associateCategory($facility, Input::get('category', $facility->category_id));

        if(!$facility->save())
        {
            if ($facility->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($facility->getErrors());
            }
            return $this->response->errorBadRequest($facility->getErrors());
        }

        return $this->response->array(['facility_id'=>$facility->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $facility = RoomFacility::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Facility Not Found');
        }

        $facility->delete();
    }

    /**
     * Attempt to associate the FacilityCategories with the current Facility model
     *
     * @param Facility $facility
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateCategory(RoomFacility &$facility, $uuid)
    {
        if(!is_null($uuid)) {
            // Find and associate the category
            $category = RoomFacilityCategory::findByUUIDorFail($uuid);
            $facility->category()->associate($category);
        }
    }
}