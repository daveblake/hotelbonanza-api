<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class FacilityCategoryController extends \BaseController
{
    protected static $relations = [
      'facilities'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Cache the count ready to be returned
        $count = FacilityCategory::all()->count();

        $categories = FacilityCategory::with(static::$relations)
            ->orderBy('name', 'asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'facilities__categories' => $categories,
            'total' => (string) $count,
            'paginated' => (string) $categories->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(FacilityCategory::fields());

        try
        {
            $category = new FacilityCategory();
            $category->fill($input);
            $category->uuid = (string) $category->generateUUID();

            if(!$category->save())
            {
                return $this->response->errorBadRequest($category->getErrors());
            }

            return $this->response->array(['category_id'=>$category->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $category = FacilityCategory::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Facility Category Not Found');
        }

        return $this->response->array($category->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $category = FacilityCategory::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Facility Category Not Found');
        }

        try
        {
            $category->name = \Input::get('name', $category->name);

            if(!$category->save())
            {
                if ($category->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($category->getErrors());
                }
                return $this->response->errorBadRequest($category->getErrors());
            }

            return $this->response->array(['category_id'=>$category->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $category = FacilityCategory::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Facility Category Not Found');
        }

        $category->delete();
    }
}