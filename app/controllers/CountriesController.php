<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class CountriesController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['telprefix','districts','timezones'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $countries =  Country::with(static::$relations)
            ->orderBy('name','asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
        $count = Hotel::all()->count();
        return [
          'countries' => $countries,
          'total' => (string) $count,
          'paginated' => (string) $countries->count(),
          'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Country::fields());

        $country = new Country();
        $country->fill($input);
        $country->uuid = (string) $country->generateUUID();

        // Attempt to associate the area with the hotel
        try
        {
            if($prefix = Input::get('telprefix')) {
                $this->associateTelPrefix($country, $prefix);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$country->save())
        {
            return $this->response->errorBadRequest($country->getErrors());
        }

        return $this->response->array(['country_id' => $country->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $country = Country::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Country Not Found');
        }

        return $this->response->array($country->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $country = Country::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Country Not Found');
        }

        $country->name = \Input::get('name', $country->name);
        $country->iso2 = \Input::get('iso2', $country->iso2);
        $country->iso3 = \Input::get('iso3', $country->iso3);

        // Attempt to associate the prefix with the country
        try
        {
            if($prefix = Input::get('telprefix')) {
                $this->associateTelPrefix($country, $prefix);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$country->save())
        {
            if ($country->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($country->getErrors());
            }
            return $this->response->errorBadRequest($country->getErrors());
        }

        return $this->response->array(['country_id' => $country->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $country = Country::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Country Not Found');
        }

        $country->delete();
    }

    /**
     * Associate a telephone prefix with the current model
     *
     * @param Country $country
     * @param $prefix
     */
    protected function associateTelPrefix(Country &$country, $prefix)
    {
        $country->telprefix()->associate(TelephonePrefix::findByUUIDorFail($prefix));
    }
}