<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class MediaCategoryController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return MediaCategory::query()
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(MediaCategory::fields());

        try
        {
            $category = new MediaCategory();
            $category->fill($input);
            $category->uuid = (string) $category->generateUUID();

            if(!$category->save())
            {
                return $this->response->errorBadRequest($category->getErrors());
            }

            return $this->response->array(['category_id'=>$category->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $category = MediaCategory::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Media Category Not Found');
        }

        return $this->response->array($category->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $category = MediaCategory::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Media Category Not Found');
        }

        try
        {
            $category->name = \Input::get('name', $category->name);

            if(!$category->save())
            {
                if ($category->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($category->getErrors());
                }
                return $this->response->errorBadRequest($category->getErrors());
            }

            return $this->response->array(['category_id'=>$category->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $category = MediaCategory::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Media Category Not Found');
        }

        $category->delete();
    }
}