<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class MediaRoomController extends \MediaController
{
    protected static $relations = 'room';

    public function create()
    {
        $args = func_get_args();

        try
        {
            $room = Room::findByUUIDorFail(array_get($args, 0));
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Not Found');
        }

        $input = Input::only(Media::fields());

        $media = new Media();
        $media->fill($input);
        $media->uuid = (string) $media->generateUUID();
        $media->order = \Input::get('order', 0); // @todo handle correctly
        $media->type = \Input::get('type', 'media');

        if(!$media->save())
        {
            return $this->response->errorBadRequest($media->getErrors());
        }

        $media->room()->save($room);

        return $this->response->array(['media_id' => $media->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $room = Room::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Not Found');
        }

        try
        {
            $media = Media::whereHas('room', function($query) use($room) {
                $query->where('id', $room->id);
            })->get();
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Not Found');
        }

        return $this->response->array($media->toArray());
    }
}