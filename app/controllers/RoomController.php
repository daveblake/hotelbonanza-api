<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RoomController extends \BaseController
{
    /**
     * Relations to load by default with each Room object
     *
     * @var array
     */
    private static $relations = [
        'type',
        'subtype',
        'layouts',
        'layouts.beds',
        'layouts.beds.type',
        'images',
        'meals.type',
        'policies',
        'features',
        'facilities',
        'rates'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Room::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Room::fields());

        $room = new Room();
        $room->fill($input);
        $room->uuid = (string) $room->generateUUID();

        if(!$room->save())
        {
            return $this->response->errorBadRequest($room->getErrors());
        }

        try
        {
            $this->handleAssoications($room);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        if($room->channelmanagercode == null) {
          $room->channelmanagercode = $room->id;
          $room->save();
        }

        return $this->response->array(['room_id' => $room->uuid]);
    }

    /**
     * Handle room associations
     *
     * @param $room
     * @return \Illuminate\Http\Response
     */
    protected function handleAssoications(&$room)
    {
        if($type = \Input::get('type')) {
            $this->associateType($room, $type);
        }

        if($subtype = \Input::get('subtype')) {
            $this->associateSubType($room, $subtype);
        }

        if($layouts = \Input::get('layouts', [])) {
            $this->attachLayouts($room, $layouts);
        }

        if($images = \Input::get('images', [])) {
            $this->attachImages($room, $images);
        }

        if($meals = \Input::get('meals', [])) {
            $this->attachMeals($room, $meals);
        }

        if($policies = \Input::get('policies', [])) {
            $this->attachPolicies($room, $policies);
        }
        if($features = \Input::get('features', [])) {
            $this->attachFeatures($room, $features);
        }
        if($facilities = \Input::get('facilities', [])) {
            $this->attachFacilities($room, $facilities);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $room = Room::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Not Found');
        }

        return $this->response->array($room->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $room = Room::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Not Found');
        }

        // Map fields from input, otherwise use current value
        foreach(Room::fields() as $field) {
            $room->{$field} = Input::get($field, $room->{$field});
        }

        try
        {
            $this->handleAssoications($room);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$room->save())
        {
            if ($room->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($room->getErrors());
            }
            return $this->response->errorBadRequest($room->getErrors());
        }

        if($room->channelmanagercode == null) {
          $room->channelmanagercode = $room->id;
          $room->save();
        }

        return $this->response->array(['room_id' => $room->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $room = Room::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Not Found');
        }

        $room->delete();
    }

    /**
     * Attempt to attach a layout to a room
     *
     * @param Room $room
     * @param array $layouts
     * @param bool $sync
     */
    protected function attachLayouts(Room &$room, array $layouts, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the layout -> bed in sync
            $room->layouts()->detach($room->layouts->lists('id'));
        }

        // attempt to relate a bed uuid to the current layout
        foreach((array)$layouts as $uuid) {
            $room->layouts()->attach(RoomLayout::findByUUIDorFail($uuid));
        }
    }

    /**
     * Attempt to attach an image to a room
     *
     * @param Room $room
     * @param array $images
     * @param bool $sync
     */
    protected function attachImages(Room &$room, array $images, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the layout -> bed in sync
            $room->images()->detach($room->images->lists('id'));
        }

        // attempt to relate a bed uuid to the current layout
        foreach((array)$images as $uuid) {
            $room->images()->attach(Media::findByUUIDorFail($uuid));
        }
    }

    /**
     * Attempt to attach a policy to a room
     *
     * @param Room $room
     * @param array $meals
     * @param bool $sync
     */
    protected function attachMeals(Room &$room, array $meals, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the layout -> bed in sync
            $room->meals()->detach($room->meals->lists('id'));
        }

        // attempt to relate a bed uuid to the current layout
        foreach((array)$meals as $uuid) {
            $room->meals()->attach(BreakfastOption::findByUUIDorFail($uuid));
        }
    }

    /**
     * Attempt to attach a policy to a room
     *
     * @param Room $room
     * @param array $policies
     * @param bool $sync
     */
    protected function attachPolicies(Room &$room, array $policies, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the layout -> bed in sync
            $room->policies()->detach($room->policies->lists('id'));
        }

        // attempt to relate a bed uuid to the current layout
        foreach((array)$policies as $uuid) {
            $room->policies()->attach(PolicyCancellation::findByUUIDorFail($uuid));
        }
    }
    /**
     * Attempt to attach a feature to a room
     *
     * @param Room $room
     * @param array $features
     * @param bool $sync
     */
    protected function attachFeatures(Room &$room, array $features, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the layout -> bed in sync
            $room->features()->detach($room->features->lists('id'));
        }

        // attempt to relate a bed uuid to the current layout
        foreach((array)$features as $uuid) {
            $room->features()->attach(RoomFeature::findByUUIDorFail($uuid));
        }
    }
    /**
     * Attempt to attach a facility to a room
     *
     * @param Room $room
     * @param array $facilities
     * @param bool $sync
     */
    protected function attachFacilities(Room &$room, array $facilities, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the layout -> bed in sync
            $room->facilities()->detach($room->facilities->lists('id'));
        }

        // attempt to relate a facility uuid to the current room
        foreach((array)$facilities as $uuid) {
            $room->facilities()->attach(RoomFacility::findByUUIDorFail($uuid));
        }
    }

    /**
     * Associate a type with a room
     *
     * @param Room $room
     * @param string $type
     */
    protected function associateType(Room &$room, $type)
    {
        $room->type()->associate(RoomType::findByUUIDorFail($type));
    }

    /**
     * Associate a type with a room
     *
     * @param Room $room
     * @param string $type
     */
    protected function associateSubType(Room &$room, $type)
    {
        $room->subtype()->associate(RoomType::findByUUIDorFail($type));
    }
}