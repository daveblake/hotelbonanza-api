<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ThemeController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Cache the count ready to be returned
        $count = Theme::all()->count();

        // Get the paginated features
        $themes = Theme::query()
            ->orderBy('name', 'asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'themes' => $themes,
            'total' => (string) $count,
            'paginated' => (string) $themes->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Theme::fields());

        try
        {
            $theme = new Theme();
            $theme->fill($input);
            $theme->uuid = (string) $theme->generateUUID();

            if(!$theme->save())
            {
                return $this->response->errorBadRequest($theme->getErrors());
            }

            return $this->response->array(['theme_id'=>$theme->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $theme = Theme::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Theme Not Found');
        }

        return $this->response->array($theme->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $theme = Theme::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Theme Not Found');
        }

        $theme->name = \Input::get('name', $theme->name);

        if(!$theme->save())
        {
            if ($theme->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($theme->getErrors());
            }
            return $this->response->errorBadRequest($theme->getErrors());
        }

        return $this->response->array(['theme_id'=>$theme->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $theme = Theme::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Theme Not Found');
        }

        $theme->delete();
    }
}