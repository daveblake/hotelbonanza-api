<?php

/**
 * Created by PhpStorm.
 * User: mgane
 * Date: 08/01/16
 * Time: 11:16
 */
class BookingController extends \BaseController
{
    private static $relations = [
        'user',
        'hotel',
        //'paymentdetails',
        'availabilitybooking',
        'availabilitybooking.room',
        'availabilitybooking.rate',
        'availabilitybooking.rooms',
    ];
    private static $cardrelations = [
        'user',
        'hotel',
        'paymentdetails',
        'availabilitybooking',
        'availabilitybooking.room',
        'availabilitybooking.rate',
        'availabilitybooking.rooms',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $bookings = Booking::query()
            ->latest()
            ->with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit());

        $bookings = $bookings->get();

        return CoreJsonResponse::respond(Booking::query(), $bookings->latest, $this->getLimit());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function current()
    {
        try {
            $User = User::findOrFail(\Authorizer::getResourceOwnerId());
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('User Not Found');
        }
        try {
            $Hotel = Hotel::where('user_id', '=', $User->id)->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $Hotel = false;
        }

        $bookings = Booking::query()
            ->latest()
            ->with(static::$cardrelations)
            ->where('user_id','=',$User->id);

        if ($Hotel) {
            $bookings->orWhere('hotel_id','=',$Hotel->id);
        }

        $bookings->skip($this->getOffset())->take($this->getLimit());

        $bookings = $bookings->get();

        return CoreJsonResponse::respond(Booking::query(), $bookings, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Booking::fields());

        try
        {
            $booking = new Booking();
            $booking->fill($input);
            $booking->uuid = (string) $booking->generateUUID();

            //todo: remove this
            //temp try the posted userid
            try {
                $user = User::findByUUIDorFail(Input::get('user_id', Input::get('user', null)));
                $booking->user()->associate($user);
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {}

            // Don't use the associateUser method here because that's for administrators only
            // $this->create() is used within the public domain
            $user = User::find(\Authorizer::getResourceOwnerId());
            if ($user instanceof User) {
                $booking->user()->associate($user);
            }

            // Finally associate the room
            $this->associateHotel($booking, Input::get('hotel_id', Input::get('hotel', null)));

            if(!$booking->save())
            {
                return $this->response->errorBadRequest($booking->getErrors());
            }

            if($booking->booking_ref == null) {
              $booking->booking_ref = $booking->id;
              $booking->save();
            }

            return $this->response->array(['booking_id'=>$booking->uuid]);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $booking = Booking::findByUUIDorFail($id, static::$cardrelations);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Booking Not Found');
        }

        return $this->response->array($booking->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $booking = Booking::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Booking Not Found');
        }

        $input = Input::only(Booking::fields());

        try
        {
            $booking->fill($input);

            if(!$booking->save())
            {
                if ($booking->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($booking->getErrors());
                }
                return $this->response->errorBadRequest($booking->getErrors());
            }

            // Todo: confirm this. Shouldn't need to alter the assigned user / hotel
            //$this->handleRelations($booking);

            if($booking->booking_ref == null) {
              $booking->booking_ref = $booking->id;
              $booking->save();
            }

            return $this->response->array(['booking_id'=>$booking->uuid]);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $booking = AvailabilityBooking::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Booking Not Found');
        }

        $booking->delete();
    }

    /**
     * Attempt to associate the User with the current model
     *
     * @param Booking $booking
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateUser(Booking $booking, $uuid)
    {
        try {
            // Find and associate the bed_type
            $user = User::findByUUIDorFail($uuid);
            $booking->user()->associate($user);
        } catch(Exception $e) {
            throw $e;
        }
    }

    /**
     * Associate Hotel
     *
     * @param Booking $booking
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateHotel(Booking $booking, $uuid)
    {
        try {
            $hotel = Hotel::findByUUIDorFail($uuid);
            $booking->hotel()->associate($hotel);
        } catch(Exception $e) {
            throw $e;
        }
    }

    /**
     * Associate User and Room relations
     *
     * @param AvailabilityBooking $booking
     * @throws Exception
     */
    protected function handleRelations(AvailabilityBooking $booking)
    {
        if($user = Input::get('user_id',Input::get('user', null))) {
            $this->associateUser($booking, $user);
        }

        if($hotel = Input::get('hotel_id',Input::get('hotel', null))) {
            $this->associateHotel($booking, $hotel);
        }
    }


}