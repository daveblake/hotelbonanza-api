<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class AddressDistrictController extends \BaseController
{

    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $addressDistricts = AddressDistrict::with(static::$relations)
            ->orderBy('city', 'asc')
            ->orderBy('name', 'asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        $count = AddressDistrict::all()->count();
        return [
            'address__districts' => $addressDistricts,
            'total' => (string)$count,
            'paginated' => (string)$addressDistricts->count(),
            'pages' => (string)ceil(((float)$count / (float)$this->getLimit()))
        ];
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $input = Input::only(AddressDistrict::fields());

        $address = new AddressDistrict();
        $address->fill($input);
        $address->uuid = (string) $address->generateUUID();

        if($country = \Input::get('country'))
        {
            try
            {
                $this->associateCountry($address, $country);
            }
            catch (ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }
        }

        if(!$address->save())
        {
            return $this->response->errorBadRequest($address->getErrors());
        }

        return $this->response->array(['address_district_id' => $address->uuid]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function update($id)
    {
        //
        try
        {
            $address = AddressDistrict::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Address District Not Found');
        }

        // Map fields from input, otherwise use current value
        foreach(AddressDistrict::fields() as $field) {
            $address->{$field} = \Input::get($field, $address->{$field});
        }

        $address->getAndCacheLocation();

        if($country = \Input::get('country'))
        {
            try
            {
                $this->associateCountry($address, $country);
            }
            catch (ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }
        }

        if(!$address->save())
        {
            if ($address->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($address->getErrors());
            }
            return $this->response->errorBadRequest($address->getErrors());
        }

        return $this->response->array(['address_district_id' => $address->uuid]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        try
        {
            $address = AddressDistrict::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Address District Not Found');
        }

        return $this->response->array($address->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $address = AddressDistrict::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Address District Not Found');
        }

        $address->delete();
    }

    public function associateCountry(AddressDistrict &$address, $uuid)
    {
        $address->country()->associate(Country::findByUUIDorFail($uuid));
    }

}
