<?php

class UserController extends \BaseController
{

    /**
     * @var array
     */
    protected static $relations = ['channelmanager', 'billingAddress.location', 'telprefix', 'ratings', 'ratings.hotel'];

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $users = User::with(static::$relations)
        ->skip($this->getOffset())
        ->take($this->getLimit())
        ->get();

    return CoreJsonResponse::respond(User::query(), $users, $this->getLimit());
  }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = \Input::only(User::fields());

        $User = new User();
        $User->fill($input);
        $User->uuid = (string)$User->generateUUID();
        $User->channelmanagerother = \Input::get('channelmanagerother', $User->channelmanagerother);

        if ($prefix = Input::get('telprefix')) {
            $this->associateTelephonePrefix($User, $prefix, 'telephone_prefix_id');
        }

        // Find and associate the channel manager
        if (null != \Input::get('channelmanager', null)) {
            $this->associateChannelmanager($User, \Input::get('channelmanager'));
        }

        if (!$User->save()) {
            return $this->response->error($User->getErrors(), 400);
        }

        if ($address = Input::get('address')) {
            $this->associateAddress($User, $address);
        }

        try {
            \Mail::later(30,'emails.user.welcome', ['user' => $User], function ($message) use ($User) {
                $message->to($User->email)->subject('Welcome to Hotel Bonanza!');
            });
        } catch (\Exception $e) {
        }

        return $this->response->array(['user_id' => $User->uuid]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try {
            $User = User::findByUUIDorFail($id, static::$relations);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('User Not Found');
        }

        $user = $User->toArray();

        if (isset($user['ratings'])) {
            foreach($user['ratings'] as $key=>$rating) {
                $user['ratings'][$key]['rating'] = Rating::calculateRatingFromArray($rating['answers']);
            }
        }

        try {
            #MultiHotelChange
            $Hotels = Hotel::where('user_id', '=', $User->id)->get();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            //hotel not found, occurs with new users
            $Hotel = new stdClass();
            $Hotel->uuid = null;
            $Hotels = new \Illuminate\Support\Collection([$Hotel]);
        }
        if ($Hotels->count() == 1) {
          $firstHotel = $Hotels->first();
          $user['hotel'] = ['uuid'=> $firstHotel->uuid];
          return $this->response->array($user);
        }

        //#MultiHotelChange
        $user['hotels'] = [];
        foreach ($Hotels as $hotel) {
          $user['hotels'][] = ['uuid' => $hotel->uuid,'name' => $hotel->hotel_name];
        }

        return $this->response->array($user);
    }

    /**
     * Display the specified resource.
     *
     *
     * @return Response
     */
    public function current()
    {
      $this->__adminLoginAsUser();
      try {
        /**
         * @var $User User
         */
          $User = User::findOrFail(\Authorizer::getResourceOwnerId());
      } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
          return $this->response->errorNotFound('User Not Found');
      }
      try {
        #MultiHotelChange
        $Hotels = Hotel::where('user_id', '=', $User->id)->get();
      } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        //hotel not found, occurs with new users
        $Hotel = new stdClass();
        $Hotel->uuid = null;
        $Hotel->hotel_name = '';
        $Hotels = new \Illuminate\Support\Collection([$Hotel]);
      }
      if ($Hotels->count() == 1) {
        $firstHotel = $Hotels->first();
        if ($firstHotel->ota_hotel_id == null) {
          $firstHotel->ota_hotel_id = substr($firstHotel->uuid,0,4).$firstHotel->id;
          $firstHotel->save();
        }
        if ($firstHotel->ota_password == null) {
          $firstHotel->ota_password = Str::random(16);
          $firstHotel->save();
        }
        #MultiHotelChange
        return $this->response->array([
          'user_uuid'    => $User->uuid,
          'hotel_uuid'   => $firstHotel->uuid,
          'ota_id'       => $firstHotel->ota_hotel_id,
          'ota_password' => $firstHotel->ota_password
        ]);
      }

      //#MultiHotelChange
      $hotelUuids = [];
      foreach ($Hotels as $hotel) {
        if ($hotel->ota_hotel_id == null) {
          $hotel->ota_hotel_id = substr($hotel->uuid,0,6).$hotel->id;
          $hotel->save();
        }
        if ($hotel->ota_password == null) {
          $hotel->ota_password = Str::random(24);
          $hotel->save();
        }
        $hotelUuids[] = [
          'hotel_uuid'   => $hotel->uuid,
          'hotel_name'   => $hotel->hotel_name,
          'ota_id'       => $hotel->ota_hotel_id,
          'ota_password' => $hotel->ota_password
        ];
      }
      #MultiHotelChange
      return $this->response->array([
        'user_uuid' => $User->uuid,
        'hotels'    => $hotelUuids
      ]);
    }
    /**
     * Display the specified resource.
     *
     *
     * @return Response
     */
    public function findByEmail($email)
    {
        try {
            $User = User::where('email','=',$email)->get()->first();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('User Not Found');
        }
        try {
            $Hotel = Hotel::where('user_id', '=', $User->id)->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            //hotel not found, occurs with new users
            $Hotel = new stdClass();
            $Hotel->uuid = null;
        }
        #MultiHotelChange
        return $this->response->array([
          'user_uuid'  => $User->uuid,
          'hotel_uuid' => $Hotel->uuid
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
      $this->__adminLoginAsUser();
        try {
            $User = User::findByUUIDorFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('User Not Found');
        }

        $input = Input::only('username', 'password', 'email', 'channelmanager');
        $User->username = \Input::get('username', $User->username);
        $User->title = \Input::get('title', $User->title);
        $User->name = \Input::get('name', $User->name);
        $User->lastname = \Input::get('lastname', $User->lastname);
        $User->email = \Input::get('email', $User->email);
        $User->channelmanagerother = \Input::get('channelmanagerother', $User->channelmanagerother);
        $User->email_verified = \Input::get('email_verified', $User->email_verified);
        $User->billing_name = \Input::get('billing_name', $User->billing_name);

        $User->set_password = \Input::get('set_password', $User->set_password);

        $User->last_login = \Input::get('last_login', $User->last_login);

        if ($input['password'] != null && strlen($input['password']) > 4) {
            $User->password = $input['password'];
        }

        try {
            // Find and associate the channel manager
            if (null != \Input::get('channelmanager', null)) {
                $this->associateChannelmanager($User, \Input::get('channelmanager'));
            }

            if ($address = Input::get('address')) {
                $this->associateAddress($User, $address);
            }

            if ($prefix = Input::get('telprefix')) {
                $this->associateTelephonePrefix($User, $prefix, 'telephone_prefix_id');
            }

            if (null != Input::get('locked_at', null)) {
              if (false != Input::get('locked_at', false)) {
                $User->locked_at = Carbon\Carbon::now();
              } else {
                $User->locked_at = null;
              }
            }

            if (!$User->save()) {
                if ($User->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($User->getErrors());
                }
                return $this->response->error($User->getErrors(), 400);
            }

            return $this->response->array(['user_id' => $User->uuid]);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $User = User::findByUUIDorFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('User Not Found');
        }
        $User->delete();
    }

    protected function associateChannelmanager(User &$user, $uuid)
    {
        if (!is_null($uuid)) {
            // Find and associate the category
            $channelmanager = ChannelManager::findByUUIDorFail($uuid);
            $user->channelmanager()->associate($channelmanager);
        }
    }

    /**
     * Associate an address with the current hotel
     *
     * @param User $user
     * @param $uuid
     */
    protected function associateAddress(User &$user, $uuid)
    {
        $user->billingAddress()->associate(Address::findByUUIDorFail($uuid));
    }

    /**
     * Associate a telephone prefix with the current hotel
     *
     * @param Hotel $hotel
     * @param $uuid
     * @param string $field
     */
    protected function associateTelephonePrefix(User &$user, $uuid, $field = 'telephone_prefix_id')
    {
        $user->telprefix()->associate(TelephonePrefix::findByUUIDorFail($uuid));
    }

    public static function OTACredentials(User $User) {
      try {
        #MultiHotelChange
        $Hotels = Hotel::where('user_id', '=', $User->id)->get();
      } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        //hotel not found, occurs with new users
        $Hotel = new stdClass();
        $Hotel->uuid = null;
        $Hotel->hotel_name = '';
        $Hotels = new \Illuminate\Support\Collection([$Hotel]);
      }
      if ($Hotels->count() == 1) {
        $firstHotel = $Hotels->first();
        if ($firstHotel->ota_hotel_id == null) {
          $firstHotel->ota_hotel_id = substr($firstHotel->uuid,0,4).$firstHotel->id;
          $firstHotel->save();
        }
        if ($firstHotel->ota_password == null) {
          $firstHotel->ota_password = Str::random(16);
          $firstHotel->save();
        }
        #MultiHotelChange
        return View::make('ota_credentials')->with('credentials',[
          'user_uuid'    => $User->uuid,
          'hotels' => [[
          'hotel_uuid'   => $firstHotel->uuid,
          'hotel_name'   => $firstHotel->hotel_name,
          'ota_id'       => $firstHotel->ota_hotel_id,
          'ota_password' => $firstHotel->ota_password]
        ]]);
      }

      //#MultiHotelChange
      $hotelUuids = [];
      foreach ($Hotels as $hotel) {
        if ($hotel->ota_hotel_id == null) {
          $hotel->ota_hotel_id = substr($hotel->uuid,0,6).$hotel->id;
          $hotel->save();
        }
        if ($hotel->ota_password == null) {
          $hotel->ota_password = Str::random(24);
          $hotel->save();
        }
        $hotelUuids[] = [
          'hotel_uuid'   => $hotel->uuid,
          'hotel_name'   => $hotel->hotel_name,
          'ota_id'       => $hotel->ota_hotel_id,
          'ota_password' => $hotel->ota_password
        ];
      }
      #MultiHotelChange
      return View::make('ota_credentials')->with('credentials',[
        'user_uuid' => $User->uuid,
        'hotels'    => $hotelUuids
      ]);
    }
}
