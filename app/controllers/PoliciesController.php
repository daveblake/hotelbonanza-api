<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class PoliciesController extends \BaseController
{
    /**
     * Array of policy drivers
     *
     * @var array
     */
    public static $policies = [
        'bed' => 'PolicyBed',
        'cancellation' => 'PolicyCancellation',
        //'chargetype' => 'PolicyChargeType',
        'checkin' => 'PolicyCheckin',
        'checkout' => 'PolicyCheckout',
        'groupbooking' => 'PolicyGroupBooking'
    ];

    private function policy($policy)
    {
        if(array_key_exists($policy, static::$policies)) {
            return static::$policies[$policy];
        }

        throw new Exception($policy . ' does not exist');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $policy
     * @return Response
     */
    public function index($policy)
    {
        try {
            $class = $this->policy($policy);
        }
        catch(\Exception $e) {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $class::with($this->relations($class))
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Dynamically return an array of the current policy relations
     *
     * @param $class
     * @return array
     */
    protected function relations($class)
    {
        $relations = [];
        if(property_exists($class, 'relationships')) {
            $relations = $class::$relationships;
        }

        return $relations;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $policy
     * @return Response
     */
    public function create($policy)
    {
        // Attempt to associate the charge type with the hotel
        try
        {
            $class = $this->policy($policy);

            // Parse and process the input and policy
            $policy = $class::process(null, Input::only($class::fields()));
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }
        catch(\Exception $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$policy->save())
        {
            return $this->response->errorBadRequest($policy->getErrors());
        }

        return $this->response->array(['policy_id' => $policy->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param $policy
     * @param  int $id
     *
     * @return Response
     */
    public function show($policy, $id)
    {
        try
        {
            $class = $this->policy($policy);
            $policy = $class::findByUUIDorFail($id, $class::$relationships);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound(ucfirst($class::name()) . ' not found');
        }
        catch(\Exception $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $this->response->array($policy->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $policy
     * @param  int $id
     *
     * @return Response
     */
    public function update($policy, $id)
    {
        try
        {
            $class = $this->policy($policy);
            $policy = $class::process($id, Input::only($class::fields()));
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound(ucfirst($class::name()) . ' not found');
        }
        catch(\Exception $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$policy->save())
        {
            if ($policy->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($policy->getErrors());
            }
            return $this->response->errorBadRequest($policy->getErrors());
        }

        return $this->response->array(['policy_id' => $policy->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $policy
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($policy, $id)
    {
        try
        {
            $class = $this->policy($policy);
            $policy = $class::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound(ucfirst($class::name()) . ' not found');
        }
        catch(\Exception $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        $policy->delete();
    }
}