<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ParkingAreaController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = ParkingArea::all()->count();

        $areas =  ParkingArea::orderBy('order','ASC')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'parking__areas' => $areas,
            'total' => (string) $count,
            'paginated' => (string) $areas->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(ParkingArea::fields());

        $area = new ParkingArea();
        $area->fill($input);
        $area->uuid = (string) $area->generateUUID();

        if(!$area->save())
        {
            return $this->response->errorBadRequest($area->getErrors());
        }

        return $this->response->array(['parking_area_id' => $area->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $area = ParkingArea::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Parking Area Not Found');
        }

        return $this->response->array($area->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $area = ParkingArea::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Parking Area Not Found');
        }

        $input = Input::only(ParkingArea::fields());

        $area->fill($input);

        if(!$area->save())
        {
            if ($area->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($area->getErrors());
            }
            return $this->response->errorBadRequest($area->getErrors());
        }

        return $this->response->array(['parking_area_id' => $area->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $area = ParkingArea::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Parking Area Not Found');
        }

        $area->delete();
    }
}