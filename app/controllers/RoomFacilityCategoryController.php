<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RoomFacilityCategoryController extends \BaseController
{
    protected static $relations = [
      'facilities'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Cache the count ready to be returned
        $count = RoomFacilityCategory::all()->count();

        $categories = RoomFacilityCategory::with(static::$relations)
            ->orderBy('order', 'asc')
            ->orderBy('name', 'asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'room_facilities__categories' => $categories,
            'total' => (string) $count,
            'paginated' => (string) $categories->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(RoomFacilityCategory::fields());

        try
        {
            $category = new RoomFacilityCategory();
            $category->fill($input);
            $category->uuid = (string) $category->generateUUID();

            if(!$category->save())
            {
                return $this->response->errorBadRequest($category->getErrors());
            }

            return $this->response->array(['room_facility_category_id'=>$category->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $category = RoomFacilityCategory::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Facility Category Not Found');
        }

        return $this->response->array($category->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $category = RoomFacilityCategory::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Facility Category Not Found');
        }

        try
        {
            $category->name = \Input::get('name', $category->name);

            if(!$category->save())
            {
                if ($category->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($category->getErrors());
                }
                return $this->response->errorBadRequest($category->getErrors());
            }

            return $this->response->array(['room_facility_category_id'=>$category->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $category = RoomFacilityCategory::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Facility Category Not Found');
        }

        $category->delete();
    }
}