<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class AvailabilityBookingController extends \BaseController
{
    private static $relations = [
        'room',
        'rooms',
        'rate',
        'user',
        'booking'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $bookings = AvailabilityBooking::query()
            ->with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit());

        $bookings = $bookings->get();

        return CoreJsonResponse::respond(AvailabilityBooking::query(), $bookings, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(AvailabilityBooking::fields());

        try
        {
            $booking = new AvailabilityBooking();
            $booking->fill($input);
            $booking->uuid = (string) $booking->generateUUID();

            // Pass time of booking along using Carbon
            $today = \Carbon\Carbon::now();
            $booking->time_booked = $today->toDateTimeString();

            //todo: remove this
            //temp try the posted userid
            try {
                $user = User::findByUUIDorFail($input['user_id']);
                $booking->user()->associate($user);
            } catch (ModelNotFoundException $e) {}

            // Don't use the associateUser method here because that's for administrators only
            // $this->create() is used within the public domain
            $user = User::find(\Authorizer::getResourceOwnerId());
            if ($user instanceof User) {
                $booking->user()->associate($user);
            }

            $from = \Carbon\Carbon::createFromFormat('Y-m-d',Input::get('date_from'));
            $to = \Carbon\Carbon::createFromFormat('Y-m-d',Input::get('date_to'));
            $room = Room::findByUUIDorFail(Input::get('room'));
            $roomRate = RoomRate::findByUUIDorFail(Input::get('room_rate'));

            //find availabilityRooms for the booking
            $availabilityRooms = AvailabilityRoom::findForBooking($from,$to,$room,$roomRate);

            if (!$availabilityRooms) {
              return $this->response->errorBadRequest('No longer available');
            }

            $bookAvailabilityUuids = [];
            foreach($availabilityRooms as $ar) {
              $bookAvailabilityUuids[] = $ar->uuid;
            }

            if($booking_obj = Input::get('booking')) {
              $this->associateBooking($booking, $booking_obj);
            }

            if(!$booking->save())
            {
              return $this->response->errorBadRequest($booking->getErrors());
            }

            $this->associateAvailabilityRooms($booking, $bookAvailabilityUuids);

            // Finally associate the room before trying to save
            $this->handleRelations($booking);


            if(!$booking->save())
            {
                return $this->response->errorBadRequest($booking->getErrors());
            }



            return $this->response->array(['availability_booking_id'=>$booking->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $booking = AvailabilityBooking::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Availability Booking Not Found');
        }

        return $this->response->array($booking->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $booking = AvailabilityBooking::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Availability Booking Not Found');
        }

        $input = Input::only(AvailabilityBooking::fields());

        try
        {
            $booking->fill($input);

            $this->handleRelations($booking);

            if(!$booking->save())
            {
                if ($booking->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($booking->getErrors());
                }
                return $this->response->errorBadRequest($booking->getErrors());
            }

            return $this->response->array(['availability_booking_id'=>$booking->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $booking = AvailabilityBooking::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Availability Booking Not Found');
        }

        $booking->delete();
    }

    /**
     * Associate User and Room relations
     *
     * @param AvailabilityBooking $booking
     * @throws Exception
     */
    protected function handleRelations(AvailabilityBooking $booking)
    {
        if($user = Input::get('user')) {
            $this->associateUser($booking, $user);
        }

//        if($room = Input::get('availability_room')) {
//            $this->associateRoom($booking, $room);
//        }

        if($rooms = Input::get('availability_rooms')) {
          $this->associateAvailabilityRooms($booking, $rooms);
        }

        if($room = Input::get('booking')) {
            $this->associateBooking($booking, $room);
        }

        if($room = Input::get('room')) {
            $this->associateHotelRoom($booking, $room);
        }

        if($room = Input::get('room_rate')) {
            $this->associateRoomRate($booking, $room);
        }
    }

    /**
     * Attempt to associate the BedType with the current Bed model
     *
     * @param AvailabilityBooking $booking
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateUser(AvailabilityBooking $booking, $uuid)
    {
        try {
            // Find and associate the bed_type
            $user = User::findByUUIDorFail($uuid);
            $booking->user()->associate($user);
        } catch(Exception $e) {
            throw $e;
        }
    }

    /**
     * Attempt to associate the Room with the current model
     *
     * @param AvailabilityBooking $booking
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     * @deprecated 16th August 2016 on availability change
     */
    protected function associateRoom(AvailabilityBooking $booking, $uuid)
    {
        try {
            $room = AvailabilityRoom::findByUUIDorFail($uuid);
            $booking->rooms()->sync([$room->uuid],false);
        } catch(Exception $e) {
            throw $e;
        }
    }

    /**
     * Attempt to associate the Booking with the current model
     *
     * @param AvailabilityBooking $booking
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateBooking(AvailabilityBooking $booking, $uuid)
    {
        try {
            $book = Booking::findByUUIDorFail($uuid);
            $booking->booking()->associate($book);
        } catch(Exception $e) {
            throw $e;
        }
    }


    protected function associateRoomRate(AvailabilityBooking $booking, $uuid)
    {
        try {
            $rate = RoomRate::findByUUIDorFail($uuid);
            $booking->room()->associate($rate);
        } catch(Exception $e) {
            throw $e;
        }
    }

    protected function associateAvailabilityRooms(AvailabilityBooking $booking, $uuids)
    {
      try {
        $rooms = [];
        foreach ($uuids as $uuid) {
          $room = AvailabilityRoom::findByUUIDorFail($uuid);
          $rooms[$room->id]['rate'] = $room->rate;
          $booking->rooms()->attach($room->id,['rate'=>$room->rate]);
        }
        //$booking->rooms()->sync($rooms,false);
      } catch(Exception $e) {
        throw $e;
      }
    }

    protected function associateHotelRoom(AvailabilityBooking $booking, $uuid)
    {
        try {
            $room = Room::findByUUIDorFail($uuid);
            $booking->room()->associate($room);
        } catch(Exception $e) {
            throw $e;
        }
    }
}