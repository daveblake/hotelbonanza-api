<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class PolicyChargeTypesController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return PolicyChargeType::with(static::$relations)
            ->orderBy('name', 'asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(PolicyChargeType::fields());

        $type = new PolicyChargeType();
        $type->fill($input);
        $type->uuid = (string) $type->generateUUID();

        if(!$type->save())
        {
            return $this->response->errorBadRequest($type->getErrors());
        }

        return $this->response->array(['charge_type_id' => $type]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $type = PolicyChargeType::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Charge Type Not Found');
        }

        return $this->response->array($type->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $type = PolicyChargeType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Charge Type Not Found');
        }

        $type->name = \Input::get('name', $type->name);

        if(!$type->save())
        {
            if ($type->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($type->getErrors());
            }
            return $this->response->errorBadRequest($type->getErrors());
        }

        return $this->response->array(['charge_type_id' => $type->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $type = PolicyChargeType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Charge Type Not Found');
        }

        $type->delete();
    }
}