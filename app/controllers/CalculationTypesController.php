<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class CalculationTypesController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return CalculationType::query()
            ->orderBy('name', 'asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(CalculationType::fields());

        try
        {
            $type = new CalculationType();
            $type->fill($input);
            $type->uuid = (string) $type->generateUUID();

            if(!$type->save())
            {
                return $this->response->errorBadRequest($type->getErrors());
            }

            return $this->response->array(['calculation_type_id'=>$type->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $type = CalculationType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Calculation Type Not Found');
        }

        return $this->response->array($type->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $type = CalculationType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Calculation Type Not Found');
        }

        $type->name = \Input::get('name', $type->name);

        if(!$type->save())
        {
            if ($type->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($type->getErrors());
            }
            return $this->response->errorBadRequest($type->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $type = CalculationType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Calculation Type Not Found');
        }

        $type->delete();
    }
}