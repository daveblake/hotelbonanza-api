<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RoomRateController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['cancellationpolicies', 'boardoptions', 'breakfastoptions.type'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return RoomRate::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->orderBy('baseprice')
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $roomRate = new RoomRate();
        $roomRate->uuid = (string) $roomRate->generateUUID();
        $roomRate->fill(Input::only(RoomRate::fields()));

        try
        {
            $this->associateRoom($roomRate, Input::get('room'));
        }
        catch(ModelNotFoundException $e) {} // let validation kick in
        catch(\Exception $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }

        if(!$roomRate->save())
        {
            return $this->response->errorBadRequest($roomRate->getErrors());
        }

        try
        {
            $this->handleRelations($roomRate);
        }
        catch(\Exception $e)
        {
            // I don't like this, but attach needs an ID in order to work properly.
            $roomRate->delete();

            return $this->response->errorBadRequest($e->getMessage());
        }

        if($roomRate->channelmanagercode == null) {
          $roomRate->channelmanagercode = $roomRate->id;
          $roomRate->save();
        }

        return $this->response->array(['roomrate_id'=> $roomRate->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $roomRate = RoomRate::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Rate Not Found');
        }

        return $this->response->array($roomRate->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $roomRate = RoomRate::findByUUIDorFail($id);
            $this->associateRoom($roomRate, Input::get('room', $roomRate->room->uuid));
            $this->handleRelations($roomRate);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }
        catch(\Exception $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }

        $roomRate->channelmanagercode = Input::get('channelmanagercode', $roomRate->channelmanagercode);

        if(!$roomRate->save())
        {
            if ($roomRate->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($roomRate->getErrors());
            }
            return $this->response->errorBadRequest($roomRate->getErrors());
        }

        if($roomRate->channelmanagercode == null) {
          $roomRate->channelmanagercode = $roomRate->id;
          $roomRate->save();
        }

        return $this->response->array(['roomrate_id'=> $roomRate->uuid]);
    }

    /**
     * Destory Rate links ready to be sync'd
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function sync($id)
    {
        try
        {
            $roomRate = RoomRate::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Rate Not Found');
        }

        $roomRate->subRates()->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $roomRate = RoomRate::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Rate Not Found');
        }

        $roomRate->delete();
    }

    /**
     * Attempt to handle relations via Input
     *
     * @param RoomRate $roomRate
     * @throws \Exception
     */
    protected function handleRelations(RoomRate &$roomRate)
    {
        if($policies = Input::get('policies', [])) {
            if(is_array($policies) && (count($policies) >= 1)) {
                $this->attachCancellationPolicies($roomRate, array_unique($policies));
            } else {
                throw new \Exception('At least 1 cancellation policy is required');
            }
        } else {
            if($roomRate->cancellationpolicies->count() <= 0) {
                throw new \Exception('At least 1 cancellation policy is required');
            }
        }

        if($options = Input::get('options', [])) {
            if(is_array($options) && count($options) >= 1) {
                $this->attachBoardOptions($roomRate, array_unique($options));
            } else {
                throw new \Exception('At least 1 board option is required');
            }
        } else {
            if($roomRate->boardoptions->count() <= 0) {
                throw new \Exception('At least 1 board option is required');
            }
        }

        if($breakfast = Input::get('breakfast', [])) {
            if(is_array($options)) {
                $this->attachBreakfastOptions($roomRate, array_unique($breakfast));
            }
        }
    }

    /**
     * Attempt to associate a room to a room rate
     *
     * @param RoomRate $roomRate
     * @param string $uuid
     */
    protected function associateRoom(RoomRate &$roomRate, $uuid)
    {
        $roomRate->room()->associate(Room::findByUUIDorFail($uuid));
    }

    /**
     * Attempt to attach a cancellation policy to a room rate
     *
     * @param RoomRate $roomRate
     * @param array $policies
     * @param bool $sync
     */
    protected function attachCancellationPolicies(RoomRate &$roomRate, array $policies, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the rate -> bed in sync
            $roomRate->cancellationpolicies()->detach($roomRate->cancellationpolicies->lists('id'));
        }

        // attempt to relate a bed uuid to the current rate
        foreach((array)$policies as $uuid) {
            if(!empty($uuid)) {
                $roomRate->cancellationpolicies()->attach(PolicyCancellation::findByUUIDorFail($uuid));
            }
        }
    }

    /**
     * Attempt to attach a board option(s) to a room rate
     *
     * @param RoomRate $roomRate
     * @param array $options
     * @param bool $sync
     */
    protected function attachBoardOptions(RoomRate &$roomRate, array $options, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the rate -> bed in sync
            $roomRate->boardoptions()->detach($roomRate->boardoptions->lists('id'));
        }

        // attempt to relate a bed uuid to the current rate
        foreach((array)$options as $uuid) {
            if(!empty($uuid)) {
                $roomRate->boardoptions()->attach(BoardOption::findByUUIDorFail($uuid));
            }
        }
    }

    /**
     * Attempt to attach a breakfast option(s) to a room rate
     *
     * @param RoomRate $roomRate
     * @param array $breakfast
     * @param bool $sync
     */
    protected function attachBreakfastOptions(RoomRate &$roomRate, array $breakfast, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the rate -> bed in sync
            $roomRate->breakfastoptions()->detach($roomRate->breakfastoptions->lists('id'));
        }

        // attempt to relate a bed uuid to the current rate
        foreach((array)$breakfast as $uuid) {
            if(!empty($uuid)) {
                $roomRate->breakfastoptions()->attach(BreakfastOption::findByUUIDorFail($uuid));
            }
        }
    }
}
