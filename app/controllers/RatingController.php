<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RatingController extends \BaseController
{
    protected static $relations = ['answers', 'user', 'hotel'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = Rating::all()->count();

        $ratings = Rating::findManyWithRating(static::$relations, null);
        return [
            'hotels__ratings' => $ratings,
            'total' => (string) $count,
            'paginated' => (string) $ratings->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Only fetch approved ratings
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function approved()
    {
        $ratings = Rating::findManyWithRating(static::$relations, null, true);
        return CoreJsonResponse::respond(Rating::where('approved', true), $ratings, $this->getLimit());
    }

    /**
     * Only fetch unapproved ratings
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function unapproved()
    {
        $ratings = Rating::findManyWithRating(static::$relations, null, false);
        return CoreJsonResponse::respond(Rating::where('approved', false), $ratings, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Rating::fields());


        $rating = new Rating();
        $rating->fill($input);
        $rating->uuid = (string)$rating->generateUUID();

        $rating->approved = false;

        try {
            $rating->user()->associate(User::findOrFail(\Authorizer::getResourceOwnerId()));
        } catch (ModelNotFoundException $e) {
            //might be the wrong error here?
            $rating->user_id = 0; //this might be a user that didn't sign up but made a booking
        }
        try {
            if ($hotel = Hotel::findByUUIDorFail(Input::get('hotel'))) {
                $rating->hotel()->associate($hotel);
            }

            if (!$rating->save()) {
                return $this->response->errorBadRequest($rating->getErrors());
            }

            // Save those answers
            foreach (\Input::get('answers', array()) as $uuid => $value) {

                if ($question = RatingQuestion::findByUUIDorFail($uuid)) {

                    $answer = RatingAnswer::create([
                        'rating' => (float)$value
                    ]);

                    $answer->uuid = $answer->generateUUID();
                    $answer->question()->associate($question);
                    $answer->save();
                    $rating->answers()->attach($answer);
                    $rating->save();
                }
            }

            return $this->response->array(['rating_id' => $rating->uuid]);
        } catch (ModelNotFoundException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try {
            $approved = Input::get('approved', true);

            if ($approved !== true) {
                $approved = false;
            }

            $rating = Rating::findByUUIDorFail($id, static::$relations, $approved);
        } catch (ModelNotFoundException $e) {
            return $this->response->errorNotFound('Rating Not Found');
        }

        return $this->response->array($rating->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        // @todo handle this via oAuth because public frontend editing should not be allowed
        return $this->response->errorForbidden('You are not allowed to perform this action');

        try {
            $rating = Rating::findByUUIDorFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response->errorNotFound('Rating Not Found');
        }

        $rating->name = \Input::get('name', $rating->name);
        $rating->email = \Input::get('email', $rating->email);
        $rating->comment = \Input::get('comment', $rating->comment);

        if (!$rating->save()) {
            if ($rating->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($rating->getErrors());
            }
            return $this->response->errorBadRequest($rating->getErrors());
        }

        return $this->response->array(['rating_id' => $rating->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $rating = Rating::findByUUIDorFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response->errorNotFound('Rating Not Found');
        }

        $rating->delete();
    }

    /**
     * Approve the rating
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try {
            $rating = Rating::findByUUIDorFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response->errorNotFound('Rating Not Found');
        }

        $rating->approved = true;
        $rating->approval_date = time();

        // remove rating which is set by findByUUIDorFail method
        unset($rating->rating);

        $rating->save();

        return $this->response->array(['rating_id' => $rating->uuid]);
    }
}