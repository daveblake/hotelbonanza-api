<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class AvailabilityRoomSubController extends \BaseController
{
    protected static $relations = ['room', 'roomRate'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $rooms = AvailabilityRoomSub::query()
            ->with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return CoreJsonResponse::respond(AvailabilityRoomSub::query(), $rooms, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(AvailabilityRoomSub::fields());

        try
        {
            $room = new AvailabilityRoomSub();
            $room->fill($input);
            $room->uuid = (string) $room->generateUUID();
            $this->handleRelations($room);

            if(!$room->save())
            {
                return $this->response->errorBadRequest($room->getErrors());
            }

            return $this->response->array(['availability_room_sub_id'=>$room->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $room = AvailabilityRoomSub::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Availability Room Sub Not Found');
        }

        return $this->response->array($room->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $room = AvailabilityRoomSub::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Availability Room Sub Not Found');
        }

        try
        {
            $room->margin = \Input::get('margin', $room->margin);
            $room->sub_rate_type = \Input::get('sub_rate_type', $room->sub_rate_type);
            $this->handleRelations($room);

            if(!$room->save())
            {
                if ($room->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($room->getErrors());
                }
                return $this->response->errorBadRequest($room->getErrors());
            }

            return $this->response->array(['availability_room_sub_id'=>$room->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param AvailabilityRoomSub $availabilityRoom
     * @throws Exception
     */
    protected function handleRelations(AvailabilityRoomSub $availabilityRoom)
    {
        if($room = Input::get('room')) {
            $this->associateRoom($availabilityRoom, $room);
            $this->associateRate($availabilityRoom, \Input::get('rate', $room));}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $room = AvailabilityRoomSub::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Availability Room Sub Not Found');
        }

        $room->delete();
    }

    /**
     * Attempt to associate the AvailabilityRoom with the current Sub Room
     *
     * @param AvailabilityRoomSub $availabilityRoom
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateRoom(AvailabilityRoomSub $availabilityRoom, $uuid)
    {
        try {
            $room = AvailabilityRoom::findByUUIDorFail($uuid);
            $availabilityRoom->room()->associate($room);
        } catch(Exception $e) {
            throw $e;
        }
    }

    /**
     * Attempt to associate the AvailabilityRoom with the current Sub Room
     *
     * @param AvailabilityRoomSub $availabilityRoom
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateRate(AvailabilityRoomSub $availabilityRoom, $uuid)
    {
        try {
            $room = AvailabilityRoom::findByUUIDorFail($uuid);
            $availabilityRoom->roomRate()->associate($room);
        } catch(Exception $e) {
            throw $e;
        }
    }
}