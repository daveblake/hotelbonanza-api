<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class CurrencyController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = Currency::all()->count();

        $currencies = Currency::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'currencies' => $currencies,
            'total' => (string) $count,
            'paginated' => (string) $currencies->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Currency::fields());

        $currency = new Currency();
        $currency->fill($input);
        $currency->uuid = (string) $currency->generateUUID();

        if(!$currency->save())
        {
            return $this->response->errorBadRequest($currency->getErrors());
        }

        return $this->response->array(['currency_id' => $currency->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $currency = Currency::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Currency Not Found');
        }

        return $this->response->array($currency->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $currency = Currency::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Currency Not Found');
        }

        $currency->name = \Input::get('name', $currency->name);
        $currency->symbol = \Input::get('symbol', $currency->symbol);

        if(!$currency->save())
        {
            if ($currency->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($currency->getErrors());
            }
            return $this->response->errorBadRequest($currency->getErrors());
        }

        return $this->response->array(['currency_id' => $currency->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $currency = Currency::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Currency Not Found');
        }

        $currency->delete();
    }
}