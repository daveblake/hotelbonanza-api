<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ParkingController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['area'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Parking::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Parking::fields());

        $parking = new Parking();
        $parking->fill($input);
        $parking->uuid = (string) $parking->generateUUID();

        // Attempt to associate the area with the hotel
        try
        {
            if($area = Input::get('area')) {
                $this->associateArea($parking, $area);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$parking->save())
        {
            return $this->response->errorBadRequest($parking->getErrors());
        }

        return $this->response->array(['parking_id' => $parking->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $parking = Parking::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Parking Not Found');
        }

        return $this->response->array($parking->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $parking = Parking::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Parking Not Found');
        }

        $parking->price = \Input::get('price', $parking->price);
        $parking->seconds = \Input::get('seconds', $parking->seconds);

        // Attempt to associate the area with the hotel
        try
        {
            if($area = Input::get('area')) {
                $this->associateArea($parking, $area);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$parking->save())
        {
            if ($parking->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($parking->getErrors());
            }
            return $this->response->errorBadRequest($parking->getErrors());
        }

        return $this->response->array(['parking_id' => $parking->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $parking = Parking::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Parking Not Found');
        }

        $parking->delete();
    }

    /**
     * Associate an area with the current model
     *
     * @param Parking $parking
     * @param $area
     */
    protected function associateArea(Parking &$parking, $area)
    {
        $parking->area()->associate(ParkingArea::findByUUIDorFail($area));
    }
}