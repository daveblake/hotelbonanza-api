<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22/08/2016
 * Time: 11:35
 */

namespace OTA;


use Carbon\Carbon;
use Exceptions\OTA\OTAAuthException;
use Exceptions\OTA\OTAParseException;
use HB\Helpers\DateRangeHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HotelRateAmountNotifController extends BaseOTAController
{
  public function index()
  {
    return 'Route not in use';
  }

  public function store()
  {
    try {
      if ($this->auth()) {
        if($this->_root_name != 'OTA_HotelRateAmountNotifRQ') {
          $failedResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Unknown element: '.$this->_root_name.'" ErrorCode="UnrecognizedRoot" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
          return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
        };
        //if auth'd code here
        foreach ($this->parser->get('RateAmountMessages', []) as $RateAmountMessages) {
          //fix single instances
          if(isset($RateAmountMessages['@attributes']) || isset($RateAmountMessages['StatusApplicationControl'])
            || isset($RateAmountMessages['Rates'])) {
            $RateAmountMessages = [$RateAmountMessages];
          }
          foreach ($RateAmountMessages as $RateAmountMessage) {
            $start = false;
            $end = false;
            $roomCode = false;
            $rateCode = false;
            $price = null;
            $single_price = null;
            $identifier = null;
            if(isset($RateAmountMessage['@attributes']) && isset($RateAmountMessage['@attributes']['LocatorID'])) {
              $identifier = $RateAmountMessage['@attributes']['LocatorID'];
            } else {
              $identifier = 'LocatorID not set';
            }
            if (isset($RateAmountMessage['StatusApplicationControl'])
              && isset($RateAmountMessage['StatusApplicationControl']['@attributes'])
              && isset($RateAmountMessage['StatusApplicationControl']['@attributes']['InvTypeCode'])
            ) {
              $attribs = $RateAmountMessage['StatusApplicationControl']['@attributes'];
              $roomCode = $attribs['InvTypeCode'];
              if (isset($attribs['RatePlanCode'])) {
                $rateCode = $attribs['RatePlanCode'];
              }
              if (isset($attribs['Start'])) {
                $start = Carbon::createFromFormat('Y-m-d', $attribs['Start']);
              }
              if (isset($attribs['End'])) {
                $end = Carbon::createFromFormat('Y-m-d', $attribs['End']);
              }
            }
            if (isset($RateAmountMessage['Rates'])) {
              foreach ($RateAmountMessage['Rates'] as $Rates) {
                //fix single instance
                if(isset($Rates['BaseByGuestAmts'])) {
                  $Rates = [$Rates];
                }
                foreach ($Rates as $Rate) {
                  if (isset($Rate['BaseByGuestAmts'])) {
                    foreach ($Rate['BaseByGuestAmts'] as $BaseByGuestAmts) {
                      //fix single instance
                     if(isset($BaseByGuestAmts['@attributes'])) {
                       $BaseByGuestAmts = [$BaseByGuestAmts];
                     }
                      foreach ($BaseByGuestAmts as $BaseByGuestAmt) {
                        if (isset($BaseByGuestAmt['@attributes']) && isset($BaseByGuestAmt['@attributes']['AmountAfterTax'])) {
                          $price = $BaseByGuestAmt['@attributes']['AmountAfterTax'];
                          if (isset($BaseByGuestAmt['@attributes']['DecimalPlaces'])) {
                            $decimalPlaces = (int)$BaseByGuestAmt['@attributes']['DecimalPlaces'];
                            if ($decimalPlaces < 0) {
                              $this->addWarning(new OTAWarning('BaseByGuestAmt cannot have DecimalPlaces below 0, assumed 0', true));
                              $decimalPlaces = 0;
                            }
                            if ($decimalPlaces > 4) {
                              $this->addWarning(new OTAWarning('BaseByGuestAmt cannot have DecimalPlaces above 4, assumed 4', true));
                              $decimalPlaces = 4;
                            }
                            for ($i = 0; $i < $decimalPlaces; $i++) {
                              $price = $price / 10;
                            }
                          }
                          if (isset($BaseByGuestAmt['@attributes']['NumberOfGuests'])) {
                            $guests = (int)$BaseByGuestAmt['@attributes']['NumberOfGuests'];
                            if ($guests < 0) {
                              $this->addWarning(new OTAWarning('BaseByGuestAmt cannot have NumberOfGuests below 1, assumed 1', true));
                              $guests = 1;
                            }
                            if ($guests > 1) {
                              $this->addWarning(new OTAWarning('BaseByGuestAmt cannot have NumberOfGuests above 1, value ignored', true));
                              $guests = null;
                            }
                            if ($guests === 1) {
                              $single_price = $price;
                              $price = null;
                            }
                          }
                          //proceed to update
                          //do update here
                          if ($roomCode && $rateCode && $start && $end) {
                            //update the room and rate code matched between the dates
                            //todo: add with hotel and do where based on credentials
                            try {
                              $roomRate = \RoomRate::whereHas('room', function ($query) use ($roomCode) {
                                $query->where('channelmanagercode', $roomCode);
                              })->where('channelmanagercode', $rateCode)->firstOrFail();
                            } catch (ModelNotFoundException $e) {
                              $this->addWarning(new OTAWarning('Room Rate not found for Locator: ' . $identifier, false));
                              break;
                            }
                            foreach (DateRangeHelper::date_range($start, $end) as $today) {
                              $availabilityRoom = \AvailabilityRoom::findByDate($today, $roomRate->room, $roomRate);
                              //create if not found
                              if (!$availabilityRoom) {
                                $availabilityRoom = new \AvailabilityRoom();
                                $availabilityRoom->room()->associate($roomRate->room);
                                $availabilityRoom->rates()->associate($roomRate);
                                $availabilityRoom->uuid = $availabilityRoom->generateUUID();
                                $availabilityRoom->rate = 0;
                                $availabilityRoom->closed = 0;
                                $availabilityRoom->quantity = 0;
                                $availabilityRoom->date = $today;
                                $availabilityRoom->save([],true);
                              }
                              /**
                               * @var $availabilityRoom \AvailabilityRoom
                               */
                              if ($price) {
                                $availabilityRoom->rate = $price;
                              }
                              if ($single_price) {
                                $availabilityRoom->single_occupancy_price = $single_price;
                              }
                              if ($this->canSave()) {
                                $availabilityRoom->save([], true);
                              }
                            }
                          } //if room code set
                          if ($roomCode && !$rateCode && $start && $end) {
                            //if only roomcode set
                            //todo: add with hotel and do where based on credentials
                            try {
                              $room = \Room::where('channelmanagercode', $roomCode)->firstOrFail();
                            } catch (ModelNotFoundException $e) {
                              $this->addWarning(new OTAWarning('Room not found for Locator: ' . $identifier, false));
                              break;
                            }
                            /**
                             * @var $room \Room
                             */
                            //todo: same as above (refactor)
                            foreach ($room->rates as $roomRate) {
                              /**
                               * @var $roomRate \RoomRate
                               */
                              foreach (DateRangeHelper::date_range($start, $end) as $today) {
                                $availabilityRoom = \AvailabilityRoom::findByDate($today, $roomRate->room, $roomRate);
                                //create if not found
                                if (!$availabilityRoom) {
                                  $availabilityRoom = new \AvailabilityRoom();
                                  $availabilityRoom->room()->associate($roomRate->room);
                                  $availabilityRoom->rates()->associate($roomRate);
                                  $availabilityRoom->uuid = $availabilityRoom->generateUUID();
                                  $availabilityRoom->rate = 0;
                                  $availabilityRoom->closed = 0;
                                  $availabilityRoom->quantity = 0;
                                  $availabilityRoom->date = $today;
                                  $availabilityRoom->save([],true);
                                }
                                /**
                                 * @var $availabilityRoom \AvailabilityRoom
                                 */
                                if ($price) {
                                  $availabilityRoom->rate = $price;
                                }
                                if ($single_price) {
                                  $availabilityRoom->single_occupancy_price = $single_price;
                                }
                                if ($this->canSave()) {
                                  $availabilityRoom->save([], true);
                                }
                              }
                            } //foreach room rates
                          } //if only roomcode set
                        } //if attribs and amountaftertax
                      }//foreach basebyguestamt
                    } //foreach basebyguestamts
                  } //if $Rate['BaseByGuestAmts']
                } //foreach Rate
              } //foreach Rates
            } // if Rates
          } //foreach rateAmountMessage
        } //foreach parser
        $successResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelRateAmountNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05/OTA_HotelAvailNotifRS.xsd" TimeStamp="' . date('c') . '" Target="' . $this->_request_target . '" Version="' . $this->_request_version . '">
  <Success />'.$this->warningsResponse().'
</OTA_HotelRateAmountNotifRS>';
        return \Response::make($successResponse, 200)->header('Content-Type', 'text/xml');
      }
    } catch (OTAParseException $e) {
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Message contains invalid XML" ErrorCode="Malformed" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
    } catch (OTAAuthException $e) {
      $failedResponse = '<?xml version="1.0"?>
<OTA_HotelRateAmountNotifRS xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Version="1.00" xmlns="http://www.opentravel.org/OTA/2003/05" TimeStamp="'.date('c').'">
    <Errors>
        <Error ShortText="Hotel Not Found" RecordID="'.$this->_requestor_type_id.'">Hotel Not Found: '.$this->_requestor_hotel_id.' Please check POS credentials and/or HotelCode</Error>
    </Errors>
</OTA_HotelAvailNotifRS>';
      return \Response::make($failedResponse, 401)->header('Content-Type', 'text/xml');
    } catch (\Exception $e) {
      \Log::info('Failed to process XML:'.$e->getMessage(),[
        'Trace:'=>$e->getTrace(),
        'Code:'=>$e->getCode(),
        'File:'=>$e->getFile(),
        'Line:'=>$e->getLine(),
        'POS Info'=>$this->_requestor_hotel_id,
        'XML'=>$this->parse()
      ]);
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Failed to process request. Request has been logged" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 500)->header('Content-Type', 'text/xml');
    }
  }
}