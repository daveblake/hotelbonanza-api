<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22/08/2016
 * Time: 11:35
 */

namespace OTA;


use Carbon\Carbon;
use Exceptions\OTA\OTAAuthException;
use Exceptions\OTA\OTAParseException;
use HB\Helpers\DateRangeHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OTA\HotelResModifyNotif\HotelResPopulator;
use OTA\HotelResModifyNotif\ResGlobalInfo\HotelReservationID\HotelReservationIDPopulator;
use OTA\HotelResModifyNotif\ResGlobalInfo\ResGlobalInfoPopulator;
use OTA\HotelResModifyNotif\ResGuest\Profile\Address\AddressPopulator;
use OTA\HotelResModifyNotif\ResGuest\Profile\ProfilePopulator;
use OTA\HotelResModifyNotif\ResGuest\ResGuestPopulator;
use OTA\HotelResModifyNotif\RoomStay\RatePlan\RatePlanPopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomRate\Rate\RatePopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomRate\RoomRatePopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomStayPopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomType\RoomTypePopulator;
use OTA\HotelResModifyNotif\RoomStay\SpecialRequest\SpecialRequestPopulator;
use OTA\HotelResModifyNotif\Service\ServicePopulator;

class HotelResModifyNotifController extends BaseOTAController
{
  public function index()
  {
    return 'Route not in use';
  }

  public function store()
  {
    try {
      if ($this->auth()) {
        if ($this->_root_name != 'OTA_HotelResModifyNotifRQ') {
          $failedResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_ErrorRS Status="NotProcessed" Target="' . $this->_request_target . '" ErrorMessage="Unknown element: ' . $this->_root_name . '" ErrorCode="UnrecognizedRoot" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
          return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
        };
        //if auth'd code here
        foreach ($this->parser->get('HotelResModifies', []) as $HotelResModifies) {
          //fix single instances
          if (isset($HotelResModifies['@attributes']) || isset($HotelResModifies['RoomStays'])
            || isset($HotelResModifies['Services'])
          ) {
            $HotelResModifies = [$HotelResModifies];
          }
          foreach ($HotelResModifies as $hotelResModify) {
            /**
             * Each of these has:
             * RoomStays section,
             * Services section,
             * ResGuests section,
             * ResGlobalInfo
             */
            $hotelResModel = new HotelResPopulator();
            if (isset($hotelResModify['RoomStays'])) {
              foreach ($hotelResModify['RoomStays'] as $RoomStays) {
                //fix single instances
                if (isset($RoomStays['@attributes'])) {
                  $RoomStays = [$RoomStays];
                }
                foreach ($RoomStays as $roomStay) {
                  $roomStayModel = new RoomStayPopulator();
                  if (isset($roomStay['@attributes']) && isset($roomStay['@attributes']['IndexNumber'])) {
                    $roomStayModel->setIndexNumber($roomStay['@attributes']['IndexNumber']);
                  }
                  if (isset($roomStay['RoomTypes'])) {
                    foreach ($roomStay['RoomTypes'] as $RoomTypes) {
                      //fix single instance
                      if (isset($RoomTypes['@attributes']) || isset($RoomTypes['RoomDescription'])) {
                        $RoomTypes = [$RoomTypes];
                      }
                      foreach ($RoomTypes as $roomType) {
                        $roomTypeModel = new RoomTypePopulator();
                        if (isset($roomType['@attributes']['RoomTypeCode'])) {
                          $roomTypeModel->setCode($roomType['@attributes']['RoomTypeCode']);
                        }
                        if (isset($roomType['RoomDescription']['@attributes']['Name'])) {
                          $roomTypeModel->setDescriptionName($roomType['RoomDescription']['@attributes']['Name']);
                        }
                        if (isset($roomType['RoomDescription']['Text'])) {
                          $roomTypeModel->setDescriptionText($roomType['RoomDescription']['Text']);
                        }
                        if (isset($roomType['RoomDescription']['MealPlan'])) {
                          $roomTypeModel->setDescriptionMealPlan($roomType['RoomDescription']['MealPlan']);
                        }
                        if (isset($roomType['RoomDescription']['MaxChildren'])) {
                          $roomTypeModel->setDescriptionMaxChildren($roomType['RoomDescription']['MaxChildren']);
                        }
                        if (isset($roomType['Amenities'])) {
                          foreach ($roomType['Amenities'] as $amenities) {
                            if (!is_array($amenities)) {
                              $amenities = [$amenities];
                            }
                            foreach ($amenities as $amenity) {
                              $roomTypeModel->addAmenity($amenity);
                            }
                          } //foreach amenities
                        } // if amenities set
                        $roomStayModel->addRoomType($roomTypeModel);
                      } //foreach roomtypes
                    } // RoomTypes
                  } // isset RoomTypes
                  if (isset($roomStay['RatePlans'])) {
                    foreach ($roomStay['RatePlans'] as $RatePlans) {
                      //fix single instance
                      if (isset($RatePlans['Commission'])) {
                        $RatePlans = [$RatePlans];
                      }
                      foreach ($RatePlans as $ratePlan) {
                        $ratePlanModel = new RatePlanPopulator();
                        if (isset($ratePlan['Commission']) && isset($ratePlan['Commission']['CommissionPayableAmount']) &&
                          isset($ratePlan['Commission']['CommissionPayableAmount']['@attributes'])
                        ) {
                          $ratePlanCommissionPayableAmount = $ratePlan['Commission']['CommissionPayableAmount']['@attributes'];
                          if (isset($ratePlanCommissionPayableAmount['Amount'])) {
                            $ratePlanModel->setCommissionAmount($ratePlanCommissionPayableAmount['Amount']);
                          }
                          if (isset($ratePlanCommissionPayableAmount['DecimalPlaces'])) {
                            $ratePlanModel->setCommissionDecimalPlaces($ratePlanCommissionPayableAmount['DecimalPlaces']);
                          }
                          if (isset($ratePlanCommissionPayableAmount['CurrencyCode'])) {
                            $ratePlanModel->setCommissionCurrencyCode($ratePlanCommissionPayableAmount['CurrencyCode']);
                          }
                        }
                        $roomStayModel->addRatePlan($ratePlanModel);
                      }//foreach rateplan
                    } //foreach rateplans
                  } // isset RatePlans
                  if (isset($roomStay['RoomRates'])) {
                    foreach ($roomStay['RoomRates'] as $RoomRates) {
                      //fix single instance
                      if (isset($RoomRates['@attributes'])) {
                        $RoomRates = [$RoomRates];
                      }
                      foreach ($RoomRates as $roomRate) {
                        $roomRateModel = new RoomRatePopulator();
                        if (isset($roomRate['@attributes']['EffectiveDate'])) {
                          $roomRateModel->setEffectiveDate($roomRate['@attributes']['EffectiveDate']);
                        }
                        if (isset($roomRate['@attributes']['RatePlanCode'])) {
                          $roomRateModel->setRatePlanCode($roomRate['@attributes']['RatePlanCode']);
                        }
                        if (isset($roomRate['Rates'])) {
                          foreach ($roomRate['Rates'] as $roomRateRates) {
                            //fix single instance
                            if (isset($roomRateRates['Total'])) {
                              $roomRateRates = [$roomRateRates];
                            }
                            foreach ($roomRateRates as $roomRateRate) {
                              $roomRateRateModel = new RatePopulator();
                              if (isset($roomRateRate['@attributes']['AmountAfterTax'])) {
                                $roomRateRateModel->setTotal($roomRateRate['@attributes']['AmountAfterTax']);
                              }
                              if (isset($roomRateRate['@attributes']['DecimalPlaces'])) {
                                $roomRateRateModel->setTotal($roomRateRate['@attributes']['DecimalPlaces']);
                              }
                              if (isset($roomRateRate['@attributes']['CurrencyCode'])) {
                                $roomRateRateModel->setTotal($roomRateRate['@attributes']['CurrencyCode']);
                              }
                              $roomRateModel->addRate($roomRateRateModel);
                            } //foreach room rate rate
                          } //foreach roomraterates
                        } //if rates
                        $roomStayModel->addRoomRate($roomRateModel);
                      } //foreach RoomRate
                    } //foreach toomrates
                  } //if roomrates
                  if (isset($roomStay['GuestCounts'])) {
                    foreach ($roomStay['GuestCounts'] as $GuestCounts) {
                      //fix single instance
                      if (isset($GuestCounts['@attributes'])) {
                        $GuestCounts = [$GuestCounts];
                      }
                      foreach ($GuestCounts as $guestCount) {
                        if (isset($guestCount['@attributes']['Count'])) {
                          $roomStayModel->addGuestCounts((int)$guestCount['@attributes']['Count']);
                        }
                      } //foreach guestcount
                    } //foreach guestcounts
                  } // if guestcounts
                  if (isset($roomStay['Total']) && isset($roomStay['Total']['@attributes'])) {
                    if (isset($roomStay['Total']['@attributes']['AmountAfterTax'])) {
                      $roomStayModel->setTotal($roomStay['Total']['@attributes']['AmountAfterTax']);
                    }
                    if (isset($roomStay['Total']['@attributes']['DecimalPlaces'])) {
                      $roomStayModel->setTotalDecimalPlaces($roomStay['Total']['@attributes']['DecimalPlaces']);
                    }
                    if (isset($roomStay['Total']['@attributes']['CurrencyCode'])) {
                      $roomStayModel->setTotalCurrencyCode($roomStay['Total']['@attributes']['CurrencyCode']);
                    }
                  } // if total node
                  if (isset($roomStay['BasicPropertyInfo']) && isset($roomStay['BasicPropertyInfo']['@attributes'])) {
                    if (isset($roomStay['BasicPropertyInfo']['@attributes']['HotelCode'])) {
                      $roomStayModel->setPropertyInfoHotelCode($roomStay['BasicPropertyInfo']['@attributes']['HotelCode']);
                    }
                  } // basicpropertyinfo node
                  if (isset($roomStay['ResGuestRPHs'])) {
                    foreach ($roomStay['ResGuestRPHs'] as $ResGuestRPHs) {
                      //fix single instance
                      if (isset($ResGuestRPHs['@attributes'])) {
                        $ResGuestRPHs = [$ResGuestRPHs];
                      }
                      foreach ($ResGuestRPHs as $resGuestRPH) {
                        if (isset($resGuestRPH['@attributes']['RPH'])) {
                          $roomStayModel->addResGuestRPH($resGuestRPH['@attributes']['RPH']);
                        }
                      } //foreach ResGuestRPH
                    } //foreach ResGuestRPHs
                  } // if ResGuestRPHs
                  if (isset($roomStay['SpecialRequests'])) {
                    foreach ($roomStay['SpecialRequests'] as $SpecialRequests) {
                      //fix single instance
                      if (isset($SpecialRequests['@attributes'])) {
                        $SpecialRequests = [$SpecialRequests];
                      }
                      foreach ($SpecialRequests as $specialRequest) {
                        $specialRequestModel = new SpecialRequestPopulator();
                        if (isset($specialRequest['@attributes']['Name'])) {
                          $specialRequestModel->setName($specialRequest['@attributes']['Name']);
                        }
                        if (isset($specialRequest['Text'])) {
                          $specialRequestModel->setText($specialRequest['Text']);
                        }
                        $roomStayModel->addSpecialRequest($specialRequestModel);
                      } //foreach SpecialRequest
                    } //foreach SpecialRequests
                  } // if SpecialRequests
                  if (isset($roomStay['ServiceRPHs'])) {
                    foreach ($roomStay['ServiceRPHs'] as $ServiceRPHs) {
                      //fix single instance
                      if (isset($ServiceRPHs['@attributes'])) {
                        $ServiceRPHs = [$ServiceRPHs];
                      }
                      foreach ($ServiceRPHs as $serviceRPH) {
                        if (isset($serviceRPH['@attributes']['RPH'])) {
                          $roomStayModel->addServiceRPH($serviceRPH['@attributes']['RPH']);
                        }
                      } //foreach ServiceRPH
                    } //foreach ServiceRPHs
                  } // if ServiceRPHs
                  $hotelResModel->addRoomStay($roomStayModel);
                } //foreach roomstays
              } // foreach roomstays
            } // if roomstays node
            if (isset($hotelResModify['Services'])) {
              foreach ($hotelResModify['Services'] as $Services) {
                //fix single instances
                if (isset($Services['@attributes'])) {
                  $Services = [$Services];
                }
                foreach ($Services as $service) {
                  $serviceModel = new ServicePopulator();
                  if(isset($service['@attributes'])) {
                    if(isset($service['@attributes']['ServiceRPH'])) {
                      $serviceModel->setRPH($service['@attributes']['ServiceRPH']);
                    }
                    if(isset($service['@attributes']['ServiceInventoryCode'])) {
                      $serviceModel->setInventoryCode($service['@attributes']['ServiceInventoryCode']);
                    }
                    if(isset($service['@attributes']['ServicePricingType'])) {
                      $serviceModel->setPricingType($service['@attributes']['ServicePricingType']);
                    }
                    if(isset($service['ServiceDetails'])) {
                      if(isset($service['ServiceDetails']['TimeSpan']) && isset($service['ServiceDetails']['TimeSpan']['@attributes']) &&
                        isset($service['ServiceDetails']['TimeSpan']['@attributes']['Duration'])) {
                        $serviceModel->setServiceDetailsTimespanDuration($service['ServiceDetails']['TimeSpan']['@attributes']['Duration']);
                      }
                      if(isset($service['ServiceDetails']['Fees'])) {
                        foreach ($service['ServiceDetails']['Fees'] as $Fees) {
                          //fix single instance
                          if(isset($Fees['@attributes'])) {
                            $Fees = [$Fees];
                          }
                          foreach ($Fees as $fee) {
                            if(isset($fee['@attributes']) && isset($fee['@attributes']['Amount'])) {
                              $serviceModel->addFee($fee['@attributes']['Amount']);
                            }
                          } //foreach fees
                        } //foreach fees node
                        $serviceModel->setServiceDetailsTimespanDuration($service['ServiceDetails']['TimeSpan']['@attributes']['Duration']);
                      } //if service fees set
                    } //if service details set
                  } //if service attributes set
                  $hotelResModel->addService($serviceModel);
                } //foreach services
              } //foreach services
            } // isset services node
            if (isset($hotelResModify['ResGuests'])) {
              foreach ($hotelResModify['ResGuests'] as $ResGuests) {
                //fix single instances
                if (isset($ResGuests['@attributes'])) {
                  $ResGuests = [$ResGuests];
                }
                foreach ($ResGuests as $resGuest) {
                  $resGuestModel = new ResGuestPopulator();
                  if(isset($resGuest['@attributes']['ResGuestRPH'])) {
                    $resGuestModel->setRPH($resGuest['@attributes']['ResGuestRPH']);
                  }
                  if(isset($resGuest['Profiles'])) {
                    foreach ($resGuest['Profiles'] as $ResGuestProfiles) {
                      //fix single instance
                      if(isset($ResGuestProfiles['@attributes'])) {
                        $ResGuestProfiles = [$ResGuestProfiles];
                      }
                      foreach ($ResGuestProfiles as $resGuestProfile) {
                        $resGuestProfileModel = new ProfilePopulator();
                        if(isset($resGuestProfile['Customer'])) {
                          if(isset($resGuestProfile['Customer']['PersonName'])) {
                            if(isset($resGuestProfile['Customer']['PersonName']['Surname'])) {
                              $resGuestProfileModel->setSurname($resGuestProfile['Customer']['PersonName']['Surname']);
                            } //if resguest profile person name surname
                            if(isset($resGuestProfile['Customer']['PersonName']['GivenName'])) {
                              $resGuestProfileModel->setName($resGuestProfile['Customer']['PersonName']['GivenName']);
                            } //if resguest profile person name GivenName
                          } //if resguest profile person name
                          if(isset($resGuestProfile['Customer']['Telephone'])) {
                            if (isset($resGuestProfile['Customer']['Telephone']['@attributes'])) {
                              if (isset($resGuestProfile['Customer']['Telephone']['@attributes']['PhoneNumber'])) {
                                $resGuestProfileModel->setTelephone($resGuestProfile['Customer']['Telephone']['@attributes']['PhoneNumber']);
                              } //isset telephonenumber
                            } //isset attribs
                          } //isset telephone
                          if(isset($resGuestProfile['Customer']['Email'])) {
                            $resGuestProfileModel->setEmail($resGuestProfile['Customer']['Email']);
                          }
                          if(isset($resGuestProfile['Customer']['Address'])) {
                            $customerAddress = $resGuestProfile['Customer']['Address'];
                            $addressModel = new AddressPopulator();
                            if(isset($customerAddress['AddressLine'])) {
                              $addressModel->setLine1($customerAddress['AddressLine']);
                            }
                            if(isset($customerAddress['CityName'])) {
                              $addressModel->setCity($customerAddress['CityName']);
                            }
                            if(isset($customerAddress['PostalCode'])) {
                              $addressModel->setPostcode($customerAddress['PostalCode']);
                            }
                            if(isset($customerAddress['CountryName'])) {
                              $addressModel->setCountryName($customerAddress['CountryName']);
                            }
                            if(isset($customerAddress['CountryName']['@attributes']) && isset($customerAddress['CountryName']['@attributes']['Code'])) {
                              $addressModel->setCountryCode($customerAddress['CountryName']['@attributes']['Code']);
                            }
                            if(isset($customerAddress['CompanyName'])) {
                              $addressModel->setCompanyName($customerAddress['CompanyName']);
                            }
                            $resGuestProfileModel->setAddress($addressModel);
                          } //if address
                        } //if resguest profile customer
                        $resGuestModel->addProfile($resGuestProfileModel);
                      } //foreach $ResGuestProfiles
                    } //foreach resguest profile
                  } //if resguest profiles node
                  $hotelResModel->addResGuest($resGuestModel);
                } //foreach ResGuests
              } // foreach ResGuests
            } //if ResGuests
            if (isset($hotelResModify['ResGlobalInfo'])) {
              $hotelResGlobalInfo = $hotelResModify['ResGlobalInfo'];
              $hotelResGlobalModel = new ResGlobalInfoPopulator();
                if(isset($hotelResGlobalInfo['Comments'])) {
                  foreach ($hotelResGlobalInfo['Comments'] as $Comments) {
                    //fix single instance
                    if(isset($Comments['Text'])) {
                      $Comments = [$Comments];
                    }
                    foreach ($Comments as $comment) {
                      if (isset($comment['Text'])) {
                        $hotelResGlobalModel->addComment($comment['Text']);
                      }
                    } //foreach comment
                  } //foreach comments
                } //if comments
              if(isset($hotelResGlobalInfo['Total'])) {
                if(isset($hotelResGlobalInfo['Total']['@attributes'])) {
                  if (isset($hotelResGlobalInfo['Total']['@attributes']['AmountAfterTax'])) {
                    $hotelResGlobalModel->setTotal($hotelResGlobalInfo['Total']['@attributes']['AmountAfterTax']);
                  }
                  if (isset($hotelResGlobalInfo['Total']['@attributes']['DecimalPlaces'])) {
                    $hotelResGlobalModel->setTotalDecimalPlaces($hotelResGlobalInfo['Total']['@attributes']['DecimalPlaces']);
                  }
                  if (isset($hotelResGlobalInfo['Total']['@attributes']['CurrencyCode'])) {
                    $hotelResGlobalModel->setTotalCurrencyCode($hotelResGlobalInfo['Total']['@attributes']['CurrencyCode']);
                  }
                }
              } //if globalres total
              if(isset($hotelResGlobalInfo['HotelReservationIDs'])) {
                foreach ($hotelResGlobalInfo['HotelReservationIDs'] as $HotelReservationIDs) {
                  //fix single instance
                  if(isset($HotelReservationIDs['@attributes'])) {
                    $HotelReservationIDs = [$HotelReservationIDs];
                  }
                  foreach ($HotelReservationIDs as $hotelReservationID) {
                    if (isset($hotelReservationID['@attributes'])) {
                      $hotelResIDModel = new HotelReservationIDPopulator();
                      if (isset($hotelReservationID['@attributes']['ResID_Value'])) {
                        $hotelResIDModel->setId($hotelReservationID['@attributes']['ResID_Value']);
                      }
                      if (isset($hotelReservationID['@attributes']['ResID_Date'])) {
                        $hotelResIDModel->setDate($hotelReservationID['@attributes']['ResID_Date']);
                      }
                      if (isset($hotelReservationID['@attributes']['ResID_SourceContext'])) {
                        $hotelResIDModel->setSourceContext($hotelReservationID['@attributes']['ResID_SourceContext']);
                      }
                      $hotelResGlobalModel->addHotelReservationID($hotelResIDModel);
                    }
                  } //foreach $hotelReservationID
                } //foreach $HotelReservationIDs
              } //if HotelReservationIDs
              //todo profiles
              if(isset($hotelResGlobalInfo['Profiles'])) {
                foreach ($hotelResGlobalInfo['Profiles'] as $ResGlobalProfiles) {
                  //fix single instance
                  if(isset($ResGlobalProfiles['@attributes'])) {
                    $ResGlobalProfiles = [$ResGlobalProfiles];
                  }
                  foreach ($ResGlobalProfiles as $resGlobalProfile) {
                    $resGlobalProfileModel = new \OTA\HotelResModifyNotif\ResGlobalInfo\Profile\ProfilePopulator();
                    if(isset($resGlobalProfile['Customer'])) {
                      if(isset($resGlobalProfile['Customer']['PersonName'])) {
                        if(isset($resGlobalProfile['Customer']['PersonName']['Surname'])) {
                          $resGlobalProfileModel->setSurname($resGlobalProfile['Customer']['PersonName']['Surname']);
                        } //if resguest profile person name surname
                        if(isset($resGuestProfile['Customer']['PersonName']['GivenName'])) {
                          $resGlobalProfileModel->setName($resGlobalProfile['Customer']['PersonName']['GivenName']);
                        } //if resguest profile person name GivenName
                      } //if resguest profile person name
                      if(isset($resGlobalProfile['Customer']['Telephone'])) {
                        if (isset($resGlobalProfile['Customer']['Telephone']['@attributes'])) {
                          if (isset($resGlobalProfile['Customer']['Telephone']['@attributes']['PhoneNumber'])) {
                            $resGlobalProfileModel->setTelephone($resGlobalProfile['Customer']['Telephone']['@attributes']['PhoneNumber']);
                          } //isset telephonenumber
                        } //isset attribs
                      } //isset telephone
                      if(isset($resGlobalProfile['Customer']['Email'])) {
                        $resGlobalProfileModel->setEmail($resGlobalProfile['Customer']['Email']);
                      }
                      if(isset($resGlobalProfile['Customer']['Address'])) {
                        $customerAddress = $resGlobalProfile['Customer']['Address'];
                        $addressModel = new \OTA\HotelResModifyNotif\ResGlobalInfo\Profile\Address\AddressPopulator();
                        if(isset($customerAddress['AddressLine'])) {
                          $addressModel->setLine1($customerAddress['AddressLine']);
                        }
                        if(isset($customerAddress['CityName'])) {
                          $addressModel->setCity($customerAddress['CityName']);
                        }
                        if(isset($customerAddress['PostalCode'])) {
                          $addressModel->setPostcode($customerAddress['PostalCode']);
                        }
                        if(isset($customerAddress['CountryName'])) {
                          $addressModel->setCountryName($customerAddress['CountryName']);
                        }
                        if(isset($customerAddress['CountryName']['@attributes']) && isset($customerAddress['CountryName']['@attributes']['Code'])) {
                          $addressModel->setCountryCode($customerAddress['CountryName']['@attributes']['Code']);
                        }
                        if(isset($customerAddress['CompanyName'])) {
                          $addressModel->setCompanyName($customerAddress['CompanyName']);
                        }
                        $resGlobalProfileModel->setAddress($addressModel);
                      } //if address
                    } //if resguest profile customer
                    $hotelResGlobalModel->addProfile($resGlobalProfileModel);
                  } //foreach $ResGuestProfiles
                } //foreach resguest profile
              } //if resguest profiles node

              $hotelResModel->setResGlobalInfo($hotelResGlobalModel);
            } //if ResGlobalInfo

            //todo: process against data here
            //find bookings
            foreach ($hotelResGlobalModel->getHotelReservationIDS() as $bookingIDModel) {
              /**
               * @var $bookingIDModel HotelReservationIDPopulator
               */
              $bookingID = $bookingIDModel->getId();
              $booking = \Booking::where('booking_ref',$bookingID)->firstOrFail();

              $booking->total = $hotelResGlobalModel->getTotalWithDecimalPlaces();

              if(!empty($hotelResGlobalModel->getComments())) {
                $booking->additional_info = '';
                foreach ($hotelResGlobalModel->getComments() as $comment) {
                  $booking->additional_info .= $comment ."\r\n";
                  }
              }
              //update the booking user (other info in the globalres block
              /**
               * @var $user \User
               * @var $address \Address
               */
              $user = $booking->user;
              $address = $user->billingAddress ?: new \Address();
              /**
               * @var $firstUserModel  \OTA\HotelResModifyNotif\ResGlobalInfo\Profile\ProfilePopulator
               */
              $firstUserModel = head($hotelResGlobalModel->getProfiles());
              if($firstUserModel->getName() != null) {
                $user->name = $firstUserModel->getName();
              }
              if($firstUserModel->getSurname() != null) {
                $user->lastname = $firstUserModel->getSurname();
              }
              if($firstUserModel->getTelephone() != null) {
                //todo handle phone input
                //set phone here
              }
              if($firstUserModel->getEmail() != null) {
                //todo: validate input
                $user->email = $firstUserModel->getEmail();
              }
              if($firstUserModel->getAddress() != null) {
                /**
                 * @var $userAddress \OTA\HotelResModifyNotif\ResGlobalInfo\Profile\Address\AddressPopulator
                 */
                $userAddress = $firstUserModel->getAddress();
                if($userAddress->getLine1() != null) {
                  $address->line1 = $userAddress->getLine1();
                }
                if($userAddress->getLine2() != null) {
                  $address->line2 = $userAddress->getLine2();
                }
                if($userAddress->getLine3() != null) {
                  $address->line3 = $userAddress->getLine3();
                }
                if($userAddress->getCity() != null) {
                  $address->town = $userAddress->getCity();
                }
                if($userAddress->getCountryCode() != null) {
                  //todo handle country relation on address
                }
                if($userAddress->getPostcode() != null) {
                  $address->postcode = $userAddress->getPostcode();
                }
                //todo: where does company name map to?
              }
              //todo: need to handle guest names

              //todo: service handlers, is HB managing these?
              //todo: is the frontend booking / checkout going to support additional extras (services)? - large amounts of time involved to include these

              //todo: room adjustment

                //todo: we don't support room type amenities at the moment
                //todo: we don't support room max children
                //todo: we dont support roomDescription Name attribs.
                //todo: is just using the roomtypecode okay? how do we map this with a rate plan? do we search prices?
                //todo: do we ignore this node?

              //process rates
              if(!empty($hotelResModel->getRoomStays())) {
                foreach($hotelResModel->getRoomStays() as $roomStay) {
                  /**
                   * @var $roomStay RoomStayPopulator
                   */
                  if(!empty($roomStay->getRoomRates())) {
                    foreach ($roomStay->getRoomRates() as $roomRate) {
                      /**
                       * @var $roomRate RoomRatePopulator
                       * @var $rate \RoomRate|ModelNotFoundException
                       * @var $booking \Booking
                       */
                      $rate = \RoomRate::where('channelmanagercode', $roomRate->getRatePlanCode())->firstOrFail();
                      $date = $roomRate->getEffectiveDateAsCarbon();
                      $availabilityRoom = $rate->availability()->where('date',$date->format('Y-m-d'))->firstOrFail();
                      //todo: if not exist? create new?

                      //todo: figure out which availabilityBooking to update? or create a new one?

                      //todo: resume here
                    }
                  }
                }
              }



              /**
               * todo: handle changelog somehow
               * todo Save the following:
               * $booking
               * $user
               * $address
               */
            }

            //todo end process against data here

          } //foreach hotelresmodifier
        } //foreach parser
        $successResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelResModifyNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05/OTA_HotelAvailNotifRS.xsd" TimeStamp="' . date('c') . '" Target="' . $this->_request_target . '" Version="' . $this->_request_version . '">
  <Success />' . $this->warningsResponse() . '
</OTA_HotelResModifyNotifRS>';
        return \Response::make($successResponse, 200)->header('Content-Type', 'text/xml');
      }
    } catch (OTAParseException $e) {
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="' . $this->_request_target . '" ErrorMessage="Message contains invalid XML" ErrorCode="Malformed" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
    } catch (OTAAuthException $e) {
      $failedResponse = '<?xml version="1.0"?>
<OTA_HotelResModifyNotifRS xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Version="1.00" xmlns="http://www.opentravel.org/OTA/2003/05" TimeStamp="' . date('c') . '">
    <Errors>
        <Error ShortText="Hotel Not Found" RecordID="' . $this->_requestor_type_id . '">Hotel Not Found: ' . $this->_requestor_hotel_id . ' Please check POS credentials and/or HotelCode</Error>
    </Errors>
</OTA_HotelResModifyNotifRS>';
      return \Response::make($failedResponse, 401)->header('Content-Type', 'text/xml');
    } catch (\Exception $e) {
      \Log::info('Failed to process XML:'.$e->getMessage(),[
        'Trace:'=>$e->getTrace(),
        'Code:'=>$e->getCode(),
        'File:'=>$e->getFile(),
        'Line:'=>$e->getLine(),
        'POS Info'=>$this->_requestor_hotel_id,
        'XML'=>$this->parse()
      ]);
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Failed to process request. Request has been logged" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 500)->header('Content-Type', 'text/xml');
    }
  }
}