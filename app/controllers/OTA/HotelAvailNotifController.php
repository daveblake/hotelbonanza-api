<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22/08/2016
 * Time: 11:34
 */

namespace OTA;
use Carbon\Carbon;
use Exceptions\OTA\OTAAuthException;
use Exceptions\OTA\OTAParseException;
use HB\Helpers\DateRangeHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Nathanmac\Utilities\Parser\Parser;
use OTA\HotelAvailNotif\RatePopulator;

class HotelAvailNotifController extends BaseOTAController
{
  public function index()
  {
    return 'Route not in use';
  }

  public function store()
  {
    try {
      if ($this->auth()) {
        if($this->_root_name != 'OTA_HotelAvailNotifRQ') {
          $failedResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Unknown element: '.$this->_root_name.'" ErrorCode="UnrecognizedRoot" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
          return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
        };
        foreach ($this->parser->get('AvailStatusMessages', []) as $key => $AvailStatusMessages) {
          //fix single instances
          if(isset($AvailStatusMessages['@attributes']) || isset($AvailStatusMessages['StatusApplicationControl'])) {
            $AvailStatusMessages = [$AvailStatusMessages];
          }
          foreach ($AvailStatusMessages as $availStatusMessage) {
            $availModel = new RatePopulator();
            if (isset($availStatusMessage['StatusApplicationControl'])
              && isset($availStatusMessage['StatusApplicationControl']['@attributes'])
              && isset($availStatusMessage['StatusApplicationControl']['@attributes']['InvTypeCode'])
            ) {
              $attribs = $availStatusMessage['StatusApplicationControl']['@attributes'];
              $availModel->setRoomCode($attribs['InvTypeCode']);
              if (isset($attribs['RatePlanCode'])) {
                $availModel->setRateCode($attribs['RatePlanCode']);
              }
              try {
                if (isset($attribs['Start'])) {
                  $availModel->setStart(Carbon::createFromFormat('Y-m-d', $attribs['Start']));
                }
                if (isset($attribs['End'])) {
                  $availModel->setEnd(Carbon::createFromFormat('Y-m-d', $attribs['End']));
                }
              } catch (\InvalidArgumentException $e) {
                $this->addError(new OTAError('Invalid date format, dates must be yyyy-mm-dd', false));
                $failedResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Bad date input" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
                $failedResponse .= $this->errorsResponse();
                $failedResponse .= '</OTA_ErrorRS>';
                return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');

              }
            }
            if (isset($availStatusMessage['LengthsOfStay'])) {
              foreach ($availStatusMessage['LengthsOfStay'] as $LengthsOfStay) {
                //fix single instance
                if(isset($LengthsOfStay['@attributes'])) {
                  $LengthsOfStay = [$LengthsOfStay];
                }
                foreach ($LengthsOfStay as $LOS) {
                  if (isset($LOS['@attributes']) && isset($LOS['@attributes']['Time']) && isset($LOS['@attributes']['MinMaxMessageType'])) {
                    switch ($LOS['@attributes']['MinMaxMessageType']) {
                      case 'SetMinLOS':
                        $availModel->setMinStay($LOS['@attributes']['Time']);
                        break;
                      case 'SetMaxLOS':
                        $availModel->setMaxStay($LOS['@attributes']['Time']);
                        break;
                      default:
                        //do nothing
                    }
                  }
                }
              }
            }
            if (isset($availStatusMessage['RestrictionStatus'])) {
              if (isset($availStatusMessage['RestrictionStatus']['@attributes']) && isset($availStatusMessage['RestrictionStatus']['@attributes']['Status'])) {
                switch ($availStatusMessage['RestrictionStatus']['@attributes']['Status']) {
                  case 'Open':
                    $status = 0;
                    break;
                  case 'Close':
                    $status = 1;
                    break;
                  default:
                    $status = false;
                }
                if ($status && isset($availStatusMessage['RestrictionStatus']['@attributes']['Restriction'])) {
                  switch ($availStatusMessage['RestrictionStatus']['@attributes']['Restriction']) {
                    case 'Departure':
                      $availModel->setNoCheckOut($status);
                      break;
                    case 'Arrival':
                      $availModel->setNoCheckIn($status);
                      break;
                    default:
                      $status = false;
                  }
                } else {
                  //non arrive / depart based open close
                  $availModel->setClosed($status);
                }
              }
            }
            if (isset($availStatusMessage['@attributes']) && isset($availStatusMessage['@attributes']['BookingLimit'])) {
              $availModel->setBookingLimit($availStatusMessage['@attributes']['BookingLimit']);
              if ($availModel->getBookingLimit() < 0) {
                $availModel->setBookingLimit(0);
                $this->addWarning(
                  new OTAWarning('Booking limit should not be below 0, assumed 0.',true)
                );
              }
            }
            $this->processRatesForDates($availModel);
            if($this->hasErrors()) {
              $failedResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Bad date input" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
              $failedResponse .= $this->errorsResponse();
              $failedResponse .= '</OTA_ErrorRS>';
              return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
            }
          } //foreach status message
        } //loop on statuses
        $successResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelAvailNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05/OTA_HotelAvailNotifRS.xsd" TimeStamp="' . date('c') . '" Target="' . $this->_request_target . '" Version="' . $this->_request_version . '">
  <Success />'.$this->warningsResponse().'
</OTA_HotelAvailNotifRS>';
        return \Response::make($successResponse, 200)->header('Content-Type', 'text/xml');
      } //if auth
    } catch (OTAParseException $e) {
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Message contains invalid XML" ErrorCode="Malformed" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
    } catch (OTAAuthException $e) {
      $failedResponse = '<?xml version="1.0"?>
<OTA_HotelAvailNotifRS xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Version="1.00" xmlns="http://www.opentravel.org/OTA/2003/05" TimeStamp="'.date('c').'">
    <Errors>
        <Error ShortText="Hotel Not Found" RecordID="'.$this->_requestor_type_id.'">Hotel Not Found: '.$this->_requestor_hotel_id.' Please check POS credentials and/or HotelCode</Error>
    </Errors>
</OTA_HotelAvailNotifRS>';
      return \Response::make($failedResponse, 401)->header('Content-Type', 'text/xml');
    } catch (\Exception $e) {
      \Log::info('Failed to process XML:'.$e->getMessage(),[
        'Trace:'=>$e->getTrace(),
        'Code:'=>$e->getCode(),
        'File:'=>$e->getFile(),
        'Line:'=>$e->getLine(),
        'POS Info'=>$this->_requestor_hotel_id,
        'XML'=>$this->parse()
      ]);
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Failed to process request. Request has been logged" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 500)->header('Content-Type', 'text/xml');
    }
  }

  private function processRatesForDates(RatePopulator $ratePopulate)
  {
    //todo: add with hotel and do where based on credentials
    if ($ratePopulate->isRoomCode() && $ratePopulate->isRateCode() && $ratePopulate->isStart() && $ratePopulate->isEnd()) {
      $roomCode = $ratePopulate->getRoomCode();
      $rateCode = $ratePopulate->getRateCode();
      try {
        $roomRate = \RoomRate::whereHas('room', function ($query) use ($roomCode, $rateCode) {
          $query->where('channelmanagercode', $roomCode);
        })->where('channelmanagercode', $rateCode)->firstOrFail();
      } catch (ModelNotFoundException $e) {
        $this->addError(new OTAError('Room / Rate plan not found for: '.$roomCode.' / '.$rateCode,false));
        return false;
      }

      $this->processDateRange($roomRate,$ratePopulate);
    } elseif ($ratePopulate->isRoomCode() && !$ratePopulate->isRateCode() && $ratePopulate->isStart() && $ratePopulate->isEnd()) {

      $roomCode = $ratePopulate->getRoomCode();
      try {
        $room = \Room::where('channelmanagercode', $roomCode)->firstOrFail();
      }
      catch (ModelNotFoundException $e) {
        $this->addError(new OTAError('Room not found for: '.$roomCode,false));
        return false;
      }
      /**
       * @var $room \Room
       */
      foreach ($room->rates as $roomRate) {
        $this->processDateRange($roomRate,$ratePopulate);
      }
    }
  }

  private function processDateRange(\RoomRate $roomRate, RatePopulator $ratePopulator) {
    foreach (DateRangeHelper::date_range($ratePopulator->getStart(), $ratePopulator->getEnd()) as $today) {
      /**
       * @var $today Carbon
       */
      $availabilityRoom = \AvailabilityRoom::findByDate($today, $roomRate->room, $roomRate);
      //create if not found
      if (!$availabilityRoom) {
        $availabilityRoom = new \AvailabilityRoom();
        $availabilityRoom->room()->associate($roomRate->room);
        $availabilityRoom->rates()->associate($roomRate);
        $availabilityRoom->uuid = $availabilityRoom->generateUUID();
        $availabilityRoom->rate = 0;
        $availabilityRoom->closed = 0;
        $availabilityRoom->quantity = 0;
        $availabilityRoom->date = $today;
        $availabilityRoom->save();
      }
      /**
       * @var $availabilityRoom \AvailabilityRoom
       */
      if ($ratePopulator->isBookingLimit()) {
        //todo: check bookings here that quantity can go this low
        $existingBookingsCount = $availabilityRoom->bookings->count();
        if($existingBookingsCount > (int) $ratePopulator->getBookingLimit()) {
          $ratePopulator->addMinimumCountWarning($today->format('Y-m-d'));
          $availabilityRoom->quantity = $availabilityRoom->bookings->count();
        } else {
          $availabilityRoom->quantity = (int) $ratePopulator->getBookingLimit();
        }
      }
      if ($ratePopulator->isMinStay()) {
        $availabilityRoom->min_stay = (int)$ratePopulator->getMinStay();
      }
      if ($ratePopulator->isMaxStay()) {
        $availabilityRoom->max_stay = (int)$ratePopulator->getMaxStay();
      }
      if ($ratePopulator->isClosed()) {
        $availabilityRoom->closed = $ratePopulator->getClosed();
      }
      if ($ratePopulator->isNoCheckOut()) {
        $availabilityRoom->no_check_out = $ratePopulator->getNoCheckOut();
      }
      if ($ratePopulator->isNoCheckIn()) {
        $availabilityRoom->no_check_in = $ratePopulator->getNoCheckIn();
      }
      if ($this->canSave()) {
        $availabilityRoom->save([], true);
      }
    }
    if (!empty($ratePopulator->getMinimumCountWarnings()) && count($ratePopulator->getMinimumCountWarnings()) > 0) {
      $message = 'Rooms to Sell were set below the Minimum Contracted rooms for these dates: ';
      foreach ($ratePopulator->getMinimumCountWarnings() as $date) {
        $message .= $date.', ';
      }
      substr($message, 0, -3);
      $message .= '. The values have been amended to the Minimum Contracted Rooms.';
      $this->addWarning(new OTAWarning($message,true));
    }
  }
}