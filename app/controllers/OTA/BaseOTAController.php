<?php
namespace OTA;

use Dingo\Api\Routing\ControllerTrait;
use Exceptions\OTA\OTAAuthException;
use Exceptions\OTA\OTAParseException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Nathanmac\Utilities\Parser\Exceptions\ParserException;
use Nathanmac\Utilities\Parser\Parser;
use Traits\UserTrait;

class BaseOTAController extends \Controller
{
  use ControllerTrait;
  use UserTrait;

  protected $parser = null;
  protected $_hotel = null;
  protected $_request_timestamp = null;
  protected $_requestor_type_id = null;
  protected $_requestor_hotel_id = null;
  protected $_request_target = 'Test';
  protected $_request_version = '1.00';
  protected $_root_name = null;
  protected $_warnings = [];
  protected $_errors = [];

  public function __construct()
  {
    $this->parser = new Parser();
  }


  /**
   * @param null $input
   * @return array
   * @throws OTAParseException
   */
  public function parse($input = null)
  {
    try {
      $this->parseInfo();
      $data = $this->parser->payload();
    } catch (\Exception $e) {
      throw new OTAParseException('Couldn\'t parse XML');
    }
    return $data;
  }

  public function auth($input = null)
  {
    try {
      $this->parseInfo();
      $hotel_id = $this->parser->get('POS.Source.RequestorID.@attributes.ID', null);
      $password = $this->parser->get('POS.Source.RequestorID.@attributes.MessagePassword', null);
      if ($hotel_id == null || $password == null) {
        throw new OTAAuthException('POS Credentials not supplied');
      }
      $this->_requestor_type_id = $this->parser->get('POS.Source.RequestorID.@attributes.Type', null);
      $this->_requestor_hotel_id = $hotel_id;
      $this->_hotel = \Hotel::where('ota_hotel_id',$hotel_id)->where('ota_password',$password)->firstOrFail();
      $user = $this->_hotel->user;
      \Auth::setUser($user);
      return true;
    } catch (ParserException $e) {
      throw new OTAParseException('Failed to Parse XML');
    } catch (ModelNotFoundException $e) {
      throw new OTAAuthException('Failed to authenticate');
    }
  }

  public function canSave()
  {
    $status = false;
    switch ($this->_request_target) {
      case 'Production':
        $status = true;
        break;
      case 'Test':
        $status = false;
        break;
      default:
        $status = false;
    }
    return $status;
  }

  public function addWarning(OTAWarning $warning)
  {
    $this->_warnings[] = $warning;
  }

  public function warningsResponse()
  {
    if (count($this->_warnings)>0) {
      $responseElement = '<Warnings>';
      foreach ($this->_warnings as $warning) {
        /**
         * @var $warning OTAWarning
         */
        $responseElement .= $warning->toXML();
      }
      $responseElement .= '</Warnings>';
      return $responseElement;
    } else {
      return '';
    }
  }

  public function addError(OTAError $error)
  {
    $this->_errors[] = $error;
  }

  public function errorsResponse()
  {
    if (count($this->_errors)>0) {
      $responseElement = '<Errors>';
      foreach ($this->_errors as $error) {
        /**
         * @var $error OTAError
         */
        $responseElement .= $error->toXML();
      }
      $responseElement .= '</Errors>';
      return $responseElement;
    } else {
      return '';
    }
  }

  public function hasErrors()
  {
    if (count($this->_errors) > 0){
      return true;
    }
    return false;
  }

  private function parseInfo()
  {
    $this->_root_name = simplexml_load_string(file_get_contents('php://input'))->getName();
    $this->_request_timestamp = $this->parser->get('@attributes.TimeStamp',date('c'));
    $this->_request_target = $this->parser->get('@attributes.Target','Test');
    $this->_request_version = $this->parser->get('@attributes.Version','1.00');
  }
}