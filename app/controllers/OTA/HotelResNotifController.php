<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 22/08/2016
 * Time: 11:34
 */

namespace OTA;

use Carbon\Carbon;
use Exceptions\OTA\OTAAuthException;
use Exceptions\OTA\OTAParseException;
use HB\Helpers\DateRangeHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Encryption\DecryptException;
use Nathanmac\Utilities\Parser\Parser;
use OTA\HotelAvailNotif\RatePopulator;
use OTA\HotelResModifyNotif\HotelResPopulator;
use OTA\HotelResModifyNotif\ResGlobalInfo\HotelReservationID\HotelReservationIDPopulator;
use OTA\HotelResModifyNotif\ResGlobalInfo\ResGlobalInfoPopulator;
use OTA\HotelResModifyNotif\ResGuest\Profile\ProfilePopulator;
use OTA\HotelResModifyNotif\ResGuest\ResGuestPopulator;
use OTA\HotelResModifyNotif\RoomStay\RatePlan\RatePlanPopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomRate\RoomRatePopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomStayPopulator;
use OTA\HotelResModifyNotif\RoomStay\RoomType\RoomTypePopulator;
use OTA\HotelResModifyNotif\RoomStay\SpecialRequest\SpecialRequestPopulator;
use OTA\HotelResModifyNotif\Service\ServicePopulator;

class HotelResNotifController extends BaseOTAController
{
  public function index()
  {
    return "route not in use";
  }

  public function store()
  {
    try {
      if ($this->auth()) {
        if ($this->_root_name != 'OTA_HotelResNotifRQ') {
          $failedResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_ErrorRS Status="NotProcessed" Target="' . $this->_request_target . '" ErrorMessage="Unknown element: ' . $this->_root_name . '" ErrorCode="UnrecognizedRoot" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
          return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
        };
        //if date set, not this function
        if(\Input::get('last_change',false)) {
          return $this->lastChange(\Input::get('last_change',null));
        }
        //if id set, not this function
        if(\Input::get('id',false)) {
          return $this->getById(\Input::get('id',null));
        }
        $bookings = [];
        foreach ($this->parser->get('HotelReservations', []) as $key => $HotelReservations) {
          //fix single instances
          if (isset($HotelReservations['RoomStays']) || isset($HotelReservations['Services'])) {
            $HotelReservations = [$HotelReservations];
          }
          foreach ($HotelReservations as $hotelReservation) {
            $hotelResModel = new HotelResPopulator();
            if (isset($hotelReservation['RoomStays'])) {
              foreach ($hotelReservation['RoomStays'] as $RoomStays) {
                //fix single instances
                if (isset($RoomStays['@attributes'])) {
                  $RoomStays = [$RoomStays];
                }
                foreach ($RoomStays as $roomStay) {
                  $roomStayModel = new RoomStayPopulator();
                  if (isset($roomStay['@attributes']) && isset($roomStay['@attributes']['IndexNumber'])) {
                    $roomStayModel->setIndexNumber($roomStay['@attributes']['IndexNumber']);
                  }
                  if (isset($roomStay['RoomTypes'])) {
                    foreach ($roomStay['RoomTypes'] as $RoomTypes) {
                      //fix single instance
                      if (isset($RoomTypes['@attributes']) || isset($RoomTypes['RoomDescription'])) {
                        $RoomTypes = [$RoomTypes];
                      }
                      foreach ($RoomTypes as $roomType) {
                        $roomTypeModel = new RoomTypePopulator();
                        if (isset($roomType['@attributes']['RoomTypeCode'])) {
                          $roomTypeModel->setCode($roomType['@attributes']['RoomTypeCode']);
                        }
                        if (isset($roomType['RoomDescription']['@attributes']['Name'])) {
                          $roomTypeModel->setDescriptionName($roomType['RoomDescription']['@attributes']['Name']);
                        }
                        if (isset($roomType['RoomDescription']['Text'])) {
                          $roomTypeModel->setDescriptionText($roomType['RoomDescription']['Text']);
                        }
                        if (isset($roomType['RoomDescription']['MealPlan'])) {
                          $roomTypeModel->setDescriptionMealPlan($roomType['RoomDescription']['MealPlan']);
                        }
                        if (isset($roomType['RoomDescription']['MaxChildren'])) {
                          $roomTypeModel->setDescriptionMaxChildren($roomType['RoomDescription']['MaxChildren']);
                        }
                        if (isset($roomType['Amenities'])) {
                          foreach ($roomType['Amenities'] as $amenities) {
                            if (!is_array($amenities)) {
                              $amenities = [$amenities];
                            }
                            foreach ($amenities as $amenity) {
                              $roomTypeModel->addAmenity($amenity);
                            }
                          } //foreach amenities
                        } // if amenities set
                        $roomStayModel->addRoomType($roomTypeModel);
                      } //foreach roomtypes
                    } // RoomTypes
                  } // isset RoomTypes
                  if (isset($roomStay['RatePlans'])) {
                    foreach ($roomStay['RatePlans'] as $RatePlans) {
                      //fix single instance
                      if (isset($RatePlans['Commission'])) {
                        $RatePlans = [$RatePlans];
                      }
                      foreach ($RatePlans as $ratePlan) {
                        $ratePlanModel = new RatePlanPopulator();
                        if (isset($ratePlan['Commission']) && isset($ratePlan['Commission']['CommissionPayableAmount']) &&
                          isset($ratePlan['Commission']['CommissionPayableAmount']['@attributes'])
                        ) {
                          $ratePlanCommissionPayableAmount = $ratePlan['Commission']['CommissionPayableAmount']['@attributes'];
                          if (isset($ratePlanCommissionPayableAmount['Amount'])) {
                            $ratePlanModel->setCommissionAmount($ratePlanCommissionPayableAmount['Amount']);
                          }
                          if (isset($ratePlanCommissionPayableAmount['DecimalPlaces'])) {
                            $ratePlanModel->setCommissionDecimalPlaces($ratePlanCommissionPayableAmount['DecimalPlaces']);
                          }
                          if (isset($ratePlanCommissionPayableAmount['CurrencyCode'])) {
                            $ratePlanModel->setCommissionCurrencyCode($ratePlanCommissionPayableAmount['CurrencyCode']);
                          }
                        }
                        $roomStayModel->addRatePlan($ratePlanModel);
                      }//foreach rateplan
                    } //foreach rateplans
                  } // isset RatePlans
                  if (isset($roomStay['RoomRates'])) {
                    foreach ($roomStay['RoomRates'] as $RoomRates) {
                      //fix single instance
                      if (isset($RoomRates['@attributes'])) {
                        $RoomRates = [$RoomRates];
                      }
                      foreach ($RoomRates as $roomRate) {
                        $roomRateModel = new RoomRatePopulator();
                        if (isset($roomRate['@attributes']['EffectiveDate'])) {
                          $roomRateModel->setEffectiveDate($roomRate['@attributes']['EffectiveDate']);
                        }
                        if (isset($roomRate['@attributes']['RatePlanCode'])) {
                          $roomRateModel->setRatePlanCode($roomRate['@attributes']['RatePlanCode']);
                        }
                        if (isset($roomRate['Rates'])) {
                          foreach ($roomRate['Rates'] as $roomRateRates) {
                            //fix single instance
                            if (isset($roomRateRates['Total'])) {
                              $roomRateRates = [$roomRateRates];
                            }
                            foreach ($roomRateRates as $roomRateRate) {
                              $roomRateRateModel = new \OTA\HotelResModifyNotif\RoomStay\RoomRate\Rate\RatePopulator();
                              if(isset($roomRateRate['Total']['@attributes'])){
                                if (isset($roomRateRate['Total']['@attributes']['AmountAfterTax'])) {
                                  $roomRateRateModel->setTotal($roomRateRate['Total']['@attributes']['AmountAfterTax']);
                                }
                                if (isset($roomRateRate['Total']['@attributes']['DecimalPlaces'])) {
                                  $roomRateRateModel->setDecimalPlaces($roomRateRate['Total']['@attributes']['DecimalPlaces']);
                                }
                                if (isset($roomRateRate['Total']['@attributes']['CurrencyCode'])) {
                                  $roomRateRateModel->setCurrencyCode($roomRateRate['Total']['@attributes']['CurrencyCode']);
                                }
                              }
                              $roomRateModel->addRate($roomRateRateModel);
                            } //foreach room rate rate
                          } //foreach roomraterates
                        } //if rates
                        $roomStayModel->addRoomRate($roomRateModel);
                      } //foreach RoomRate
                    } //foreach toomrates
                  } //if roomrates
                  if (isset($roomStay['GuestCounts'])) {
                    foreach ($roomStay['GuestCounts'] as $GuestCounts) {
                      //fix single instance
                      if (isset($GuestCounts['@attributes'])) {
                        $GuestCounts = [$GuestCounts];
                      }
                      foreach ($GuestCounts as $guestCount) {
                        if (isset($guestCount['@attributes']['Count'])) {
                          $roomStayModel->addGuestCounts((int)$guestCount['@attributes']['Count']);
                        }
                      } //foreach guestcount
                    } //foreach guestcounts
                  } // if guestcounts
                  if (isset($roomStay['Total']) && isset($roomStay['Total']['@attributes'])) {
                    if (isset($roomStay['Total']['@attributes']['AmountAfterTax'])) {
                      $roomStayModel->setTotal($roomStay['Total']['@attributes']['AmountAfterTax']);
                    }
                    if (isset($roomStay['Total']['@attributes']['DecimalPlaces'])) {
                      $roomStayModel->setTotalDecimalPlaces($roomStay['Total']['@attributes']['DecimalPlaces']);
                    }
                    if (isset($roomStay['Total']['@attributes']['CurrencyCode'])) {
                      $roomStayModel->setTotalCurrencyCode($roomStay['Total']['@attributes']['CurrencyCode']);
                    }
                  } // if total node
                  if (isset($roomStay['BasicPropertyInfo']) && isset($roomStay['BasicPropertyInfo']['@attributes'])) {
                    if (isset($roomStay['BasicPropertyInfo']['@attributes']['HotelCode'])) {
                      $roomStayModel->setPropertyInfoHotelCode($roomStay['BasicPropertyInfo']['@attributes']['HotelCode']);
                    }
                  } // basicpropertyinfo node
                  if (isset($roomStay['ResGuestRPHs'])) {
                    foreach ($roomStay['ResGuestRPHs'] as $ResGuestRPHs) {
                      //fix single instance
                      if (isset($ResGuestRPHs['@attributes'])) {
                        $ResGuestRPHs = [$ResGuestRPHs];
                      }
                      foreach ($ResGuestRPHs as $resGuestRPH) {
                        if (isset($resGuestRPH['@attributes']['RPH'])) {
                          $roomStayModel->addResGuestRPH($resGuestRPH['@attributes']['RPH']);
                        }
                      } //foreach ResGuestRPH
                    } //foreach ResGuestRPHs
                  } // if ResGuestRPHs
                  if (isset($roomStay['SpecialRequests'])) {
                    foreach ($roomStay['SpecialRequests'] as $SpecialRequests) {
                      //fix single instance
                      if (isset($SpecialRequests['@attributes'])) {
                        $SpecialRequests = [$SpecialRequests];
                      }
                      foreach ($SpecialRequests as $specialRequest) {
                        $specialRequestModel = new SpecialRequestPopulator();
                        if (isset($specialRequest['@attributes']['Name'])) {
                          $specialRequestModel->setName($specialRequest['@attributes']['Name']);
                        }
                        if (isset($specialRequest['Text'])) {
                          $specialRequestModel->setText($specialRequest['Text']);
                        }
                        $roomStayModel->addSpecialRequest($specialRequestModel);
                      } //foreach SpecialRequest
                    } //foreach SpecialRequests
                  } // if SpecialRequests
                  if (isset($roomStay['ServiceRPHs'])) {
                    foreach ($roomStay['ServiceRPHs'] as $ServiceRPHs) {
                      //fix single instance
                      if (isset($ServiceRPHs['@attributes'])) {
                        $ServiceRPHs = [$ServiceRPHs];
                      }
                      foreach ($ServiceRPHs as $serviceRPH) {
                        if (isset($serviceRPH['@attributes']['RPH'])) {
                          $roomStayModel->addServiceRPH($serviceRPH['@attributes']['RPH']);
                        }
                      } //foreach ServiceRPH
                    } //foreach ServiceRPHs
                  } // if ServiceRPHs
                  $hotelResModel->addRoomStay($roomStayModel);
                } //foreach roomstays
              } // foreach roomstays
            } // if roomstays node
            if (isset($hotelReservation['Services'])) {
              foreach ($hotelReservation['Services'] as $Services) {
                //fix single instances
                if (isset($Services['@attributes'])) {
                  $Services = [$Services];
                }
                foreach ($Services as $service) {
                  $serviceModel = new ServicePopulator();
                  if (isset($service['@attributes'])) {
                    if (isset($service['@attributes']['ServiceRPH'])) {
                      $serviceModel->setRPH($service['@attributes']['ServiceRPH']);
                    }
                    if (isset($service['@attributes']['ServiceInventoryCode'])) {
                      $serviceModel->setInventoryCode($service['@attributes']['ServiceInventoryCode']);
                    }
                    if (isset($service['@attributes']['ServicePricingType'])) {
                      $serviceModel->setPricingType($service['@attributes']['ServicePricingType']);
                    }
                    if (isset($service['ServiceDetails'])) {
                      if (isset($service['ServiceDetails']['TimeSpan']) && isset($service['ServiceDetails']['TimeSpan']['@attributes']) &&
                        isset($service['ServiceDetails']['TimeSpan']['@attributes']['Duration'])
                      ) {
                        $serviceModel->setServiceDetailsTimespanDuration($service['ServiceDetails']['TimeSpan']['@attributes']['Duration']);
                      }
                      if (isset($service['ServiceDetails']['Fees'])) {
                        foreach ($service['ServiceDetails']['Fees'] as $Fees) {
                          //fix single instance
                          if (isset($Fees['@attributes'])) {
                            $Fees = [$Fees];
                          }
                          foreach ($Fees as $fee) {
                            if (isset($fee['@attributes']) && isset($fee['@attributes']['Amount'])) {
                              $serviceModel->addFee($fee['@attributes']['Amount']);
                            }
                          } //foreach fees
                        } //foreach fees node
                        $serviceModel->setServiceDetailsTimespanDuration($service['ServiceDetails']['TimeSpan']['@attributes']['Duration']);
                      } //if service fees set
                    } //if service details set
                  } //if service attributes set
                  $hotelResModel->addService($serviceModel);
                } //foreach services
              } //foreach services
            } // isset services node
            if (isset($hotelReservation['ResGuests'])) {
              foreach ($hotelReservation['ResGuests'] as $ResGuests) {
                //fix single instances
                if (isset($ResGuests['@attributes'])) {
                  $ResGuests = [$ResGuests];
                }
                foreach ($ResGuests as $resGuest) {
                  $resGuestModel = new ResGuestPopulator();
                  if (isset($resGuest['@attributes']['ResGuestRPH'])) {
                    $resGuestModel->setRPH($resGuest['@attributes']['ResGuestRPH']);
                  }
                  if (isset($resGuest['Profiles'])) {
                    foreach ($resGuest['Profiles'] as $ResGuestProfiles) {
                      //fix single instance
                      if (isset($ResGuestProfiles['@attributes'])) {
                        $ResGuestProfiles = [$ResGuestProfiles];
                      }
                      foreach ($ResGuestProfiles as $resGuestProfile) {
                        $resGuestProfileModel = new ProfilePopulator();
                        if (isset($resGuestProfile['Customer'])) {
                          if (isset($resGuestProfile['Customer']['PersonName'])) {
                            if (isset($resGuestProfile['Customer']['PersonName']['Surname'])) {
                              $resGuestProfileModel->setSurname($resGuestProfile['Customer']['PersonName']['Surname']);
                            } //if resguest profile person name surname
                            if (isset($resGuestProfile['Customer']['PersonName']['GivenName'])) {
                              $resGuestProfileModel->setName($resGuestProfile['Customer']['PersonName']['GivenName']);
                            } //if resguest profile person name GivenName
                          } //if resguest profile person name
                          if (isset($resGuestProfile['Customer']['Telephone'])) {
                            if (isset($resGuestProfile['Customer']['Telephone']['@attributes'])) {
                              if (isset($resGuestProfile['Customer']['Telephone']['@attributes']['PhoneNumber'])) {
                                $resGuestProfileModel->setTelephone($resGuestProfile['Customer']['Telephone']['@attributes']['PhoneNumber']);
                              } //isset telephonenumber
                            } //isset attribs
                          } //isset telephone
                          if (isset($resGuestProfile['Customer']['Email'])) {
                            $resGuestProfileModel->setEmail($resGuestProfile['Customer']['Email']);
                          }
                          if (isset($resGuestProfile['Customer']['Address'])) {
                            $customerAddress = $resGuestProfile['Customer']['Address'];
                            $addressModel = new AddressPopulator();
                            if (isset($customerAddress['AddressLine'])) {
                              $addressModel->setLine1($customerAddress['AddressLine']);
                            }
                            if (isset($customerAddress['CityName'])) {
                              $addressModel->setCity($customerAddress['CityName']);
                            }
                            if (isset($customerAddress['PostalCode'])) {
                              $addressModel->setPostcode($customerAddress['PostalCode']);
                            }
                            if (isset($customerAddress['CountryName'])) {
                              $addressModel->setCountryName($customerAddress['CountryName']);
                            }
                            if (isset($customerAddress['CountryName']['@attributes']) && isset($customerAddress['CountryName']['@attributes']['Code'])) {
                              $addressModel->setCountryCode($customerAddress['CountryName']['@attributes']['Code']);
                            }
                            if (isset($customerAddress['CompanyName'])) {
                              $addressModel->setCompanyName($customerAddress['CompanyName']);
                            }
                            $resGuestProfileModel->setAddress($addressModel);
                          } //if address
                        } //if resguest profile customer
                        $resGuestModel->addProfile($resGuestProfileModel);
                      } //foreach $ResGuestProfiles
                    } //foreach resguest profile
                  } //if resguest profiles node
                  $hotelResModel->addResGuest($resGuestModel);
                } //foreach ResGuests
              } // foreach ResGuests
            } //if ResGuests
            if (isset($hotelReservation['ResGlobalInfo'])) {
              $hotelResGlobalInfo = $hotelReservation['ResGlobalInfo'];
              $hotelResGlobalModel = new ResGlobalInfoPopulator();
              if (isset($hotelResGlobalInfo['Comments'])) {
                foreach ($hotelResGlobalInfo['Comments'] as $Comments) {
                  //fix single instance
                  if (isset($Comments['Text'])) {
                    $Comments = [$Comments];
                  }
                  foreach ($Comments as $comment) {
                    if (isset($comment['Text'])) {
                      $hotelResGlobalModel->addComment($comment['Text']);
                    }
                  } //foreach comment
                } //foreach comments
              } //if comments
              //todo guarantee > guaranteesAccepted > GuaranteeAccepted > PaymentCard > CardHolderName
              if (isset($hotelResGlobalInfo['Guarantee'])) {
                foreach ($hotelResGlobalInfo['Guarantee'] as $Guarantees) {
                  //fix single instance
                  if (isset($Guarantees['GuaranteeAccepted'])) {
                    $Guarantees = [$Guarantees];
                  }
                  foreach ($Guarantees as $GuaranteesAccepted) {
                    //fix single instance
                    if (isset($GuaranteeAccepted['PaymentCard'])) {
                      $GuaranteesAccepted = [$GuaranteesAccepted];
                    }
                    foreach ($GuaranteesAccepted as $GuaranteeAccepted) {
                      //fix single instance
                      if (isset($GuaranteeAccepted['PaymentCard'])) {
                        $GuaranteeAccepted = [$GuaranteeAccepted];
                      }
                      foreach ($GuaranteeAccepted as $GA) {
                        if(isset($GA['PaymentCard']) && isset($GA['PaymentCard']['@attributes'])) {
                          $GAPcA = $GA['PaymentCard']['@attributes'];
                          if(isset($GAPcA['CardCode'])) {
                            $hotelResGlobalModel->setPaymentCardCardCode($GAPcA['CardCode']);
                          }
                          if(isset($GAPcA['CardNumber'])) {
                            $hotelResGlobalModel->setPaymentCardCardNumber($GAPcA['CardNumber']);
                          }
                          if(isset($GAPcA['SeriesCode'])) {
                            $hotelResGlobalModel->setPaymentCardSeriesCode($GAPcA['SeriesCode']);
                          }
                          if(isset($GAPcA['ExpireDate'])) {
                            $hotelResGlobalModel->setPaymentCardExpireDate($GAPcA['ExpireDate']);
                          }
                        }
                        if(isset($GA['PaymentCard']) && isset($GA['PaymentCard']['CardHolderName'])) {
                          $hotelResGlobalModel->setPaymentCardCardHolderName($GA['PaymentCard']['CardHolderName']);
                        }
                      } //foreach Guarantee
                    } //foreach Guarantee
                  } //foreach Guarantee
                } //foreach Guarantee
              } //if Guarantee
              if (isset($hotelResGlobalInfo['Total'])) {
                if (isset($hotelResGlobalInfo['Total']['@attributes'])) {
                  if (isset($hotelResGlobalInfo['Total']['@attributes']['AmountAfterTax'])) {
                    $hotelResGlobalModel->setTotal($hotelResGlobalInfo['Total']['@attributes']['AmountAfterTax']);
                  }
                  if (isset($hotelResGlobalInfo['Total']['@attributes']['DecimalPlaces'])) {
                    $hotelResGlobalModel->setTotalDecimalPlaces($hotelResGlobalInfo['Total']['@attributes']['DecimalPlaces']);
                  }
                  if (isset($hotelResGlobalInfo['Total']['@attributes']['CurrencyCode'])) {
                    $hotelResGlobalModel->setTotalCurrencyCode($hotelResGlobalInfo['Total']['@attributes']['CurrencyCode']);
                  }
                }
              } //if globalres total
              if (isset($hotelResGlobalInfo['HotelReservationIDs'])) {
                foreach ($hotelResGlobalInfo['HotelReservationIDs'] as $HotelReservationIDs) {
                  //fix single instance
                  if (isset($HotelReservationIDs['@attributes'])) {
                    $HotelReservationIDs = [$HotelReservationIDs];
                  }
                  foreach ($HotelReservationIDs as $hotelReservationID) {
                    if (isset($hotelReservationID['@attributes'])) {
                      $hotelResIDModel = new HotelReservationIDPopulator();
                      if (isset($hotelReservationID['@attributes']['ResID_Value'])) {
                        $hotelResIDModel->setId($hotelReservationID['@attributes']['ResID_Value']);
                      }
                      if (isset($hotelReservationID['@attributes']['ResID_Date'])) {
                        $hotelResIDModel->setDate($hotelReservationID['@attributes']['ResID_Date']);
                      }
                      if (isset($hotelReservationID['@attributes']['ResID_SourceContext'])) {
                        $hotelResIDModel->setSourceContext($hotelReservationID['@attributes']['ResID_SourceContext']);
                      }
                      $hotelResGlobalModel->addHotelReservationID($hotelResIDModel);
                    }
                  } //foreach $hotelReservationID
                } //foreach $HotelReservationIDs
              } //if HotelReservationIDs
              //todo profiles
              if (isset($hotelResGlobalInfo['Profiles'])) {
                foreach ($hotelResGlobalInfo['Profiles'] as $ResGlobalProfiles) {
                  //fix single instance
                  if (isset($ResGlobalProfiles['@attributes'])) {
                    $ResGlobalProfiles = [$ResGlobalProfiles];
                  }
                  foreach ($ResGlobalProfiles as $resGlobalProfile) {
                    $resGlobalProfileModel = new \OTA\HotelResModifyNotif\ResGlobalInfo\Profile\ProfilePopulator();
                    if (isset($resGlobalProfile['Customer'])) {
                      if (isset($resGlobalProfile['Customer']['PersonName'])) {
                        if (isset($resGlobalProfile['Customer']['PersonName']['Surname'])) {
                          $resGlobalProfileModel->setSurname($resGlobalProfile['Customer']['PersonName']['Surname']);
                        } //if resguest profile person name surname
                        if (isset($resGuestProfile['Customer']['PersonName']['GivenName'])) {
                          $resGlobalProfileModel->setName($resGlobalProfile['Customer']['PersonName']['GivenName']);
                        } //if resguest profile person name GivenName
                      } //if resguest profile person name
                      if (isset($resGlobalProfile['Customer']['Telephone'])) {
                        if (isset($resGlobalProfile['Customer']['Telephone']['@attributes'])) {
                          if (isset($resGlobalProfile['Customer']['Telephone']['@attributes']['PhoneNumber'])) {
                            $resGlobalProfileModel->setTelephone($resGlobalProfile['Customer']['Telephone']['@attributes']['PhoneNumber']);
                          } //isset telephonenumber
                        } //isset attribs
                      } //isset telephone
                      if (isset($resGlobalProfile['Customer']['Email'])) {
                        $resGlobalProfileModel->setEmail($resGlobalProfile['Customer']['Email']);
                      }
                      if (isset($resGlobalProfile['Customer']['Address'])) {
                        $customerAddress = $resGlobalProfile['Customer']['Address'];
                        $addressModel = new \OTA\HotelResModifyNotif\ResGlobalInfo\Profile\Address\AddressPopulator();
                        if (isset($customerAddress['AddressLine'])) {
                          $addressModel->setLine1($customerAddress['AddressLine']);
                        }
                        if (isset($customerAddress['CityName'])) {
                          $addressModel->setCity($customerAddress['CityName']);
                        }
                        if (isset($customerAddress['PostalCode'])) {
                          $addressModel->setPostcode($customerAddress['PostalCode']);
                        }
                        if (isset($customerAddress['CountryName'])) {
                          $addressModel->setCountryName($customerAddress['CountryName']);
                        }
                        if (isset($customerAddress['CountryName']['@attributes']) && isset($customerAddress['CountryName']['@attributes']['Code'])) {
                          $addressModel->setCountryCode($customerAddress['CountryName']['@attributes']['Code']);
                        }
                        if (isset($customerAddress['CompanyName'])) {
                          $addressModel->setCompanyName($customerAddress['CompanyName']);
                        }
                        $resGlobalProfileModel->setAddress($addressModel);
                      } //if address
                    } //if resguest profile customer
                    $hotelResGlobalModel->addProfile($resGlobalProfileModel);
                  } //foreach $ResGuestProfiles
                } //foreach resguest profile
              } //if resguest profiles node

              $hotelResModel->setResGlobalInfo($hotelResGlobalModel);
            } //if ResGlobalInfo

            //todo: process against data here
            //find bookings
            foreach ($hotelResGlobalModel->getHotelReservationIDS() as $bookingIDModel) {
              /**
               * @var $bookingIDModel HotelReservationIDPopulator
               */
              $bookingID = $bookingIDModel->getId();
              try {
                $booking = \Booking::where('booking_ref', $bookingID)->firstOrFail();
                //todo: handle existing booking ID
                throw new \Exception('Booking ID already in use');
              } catch (\Exception $e) {
                $booking = new \Booking();
                $booking->uuid = $booking->generateUUID();
                $booking->booking_ref = $bookingID;
              }

              $booking->total = $hotelResGlobalModel->getTotalWithDecimalPlaces();

              if (!empty($hotelResGlobalModel->getComments())) {
                $booking->additional_info = '';
                foreach ($hotelResGlobalModel->getComments() as $comment) {
                  $booking->additional_info .= $comment . "\r\n";
                }
              }
              //update the booking user (other info in the globalres block
              /**
               * @var $user \User
               * @var $address \Address
               */
              $user = new \User();
              $user->uuid = $user->generateUUID();
              $address = new \Address();
              $address->uuid = $address->generateUUID();
              /**
               * @var $firstUserModel  \OTA\HotelResModifyNotif\ResGlobalInfo\Profile\ProfilePopulator
               */
              $firstUserModel = head($hotelResGlobalModel->getProfiles());
              if ($firstUserModel->getName() != null) {
                $user->name = $firstUserModel->getName();
              }
              if ($firstUserModel->getSurname() != null) {
                $user->lastname = $firstUserModel->getSurname();
              }
              if ($firstUserModel->getTelephone() != null) {
                //todo handle phone input
                //set phone here
              }
              if ($firstUserModel->getEmail() != null) {
                //todo: validate input
                $user->email = $firstUserModel->getEmail();
              } else {
                $user->email = $user->uuid.'@channelmanager.bookings';
              }
              $user->username = $user->email;
              $user->password = $user->uuid;
              if ($firstUserModel->getAddress() != null) {
                /**
                 * @var $userAddress \OTA\HotelResModifyNotif\ResGlobalInfo\Profile\Address\AddressPopulator
                 */
                $userAddress = $firstUserModel->getAddress();
                if ($userAddress->getLine1() != null) {
                  $address->line1 = $userAddress->getLine1();
                }
                if ($userAddress->getLine2() != null) {
                  $address->line2 = $userAddress->getLine2();
                }
                if ($userAddress->getLine3() != null) {
                  $address->line3 = $userAddress->getLine3();
                }
                if ($userAddress->getCity() != null) {
                  $address->town = $userAddress->getCity();
                }
                if ($userAddress->getCountryCode() != null) {
                  //todo handle country relation on address
                }
                if ($userAddress->getPostcode() != null) {
                  $address->postcode = $userAddress->getPostcode();
                }
                //todo: where does company name map to?
              }
              //todo: need to handle guest names

              //todo: service handlers, is HB managing these?
              //todo: is the frontend booking / checkout going to support additional extras (services)? - large amounts of time involved to include these

              if(!$address->save([],true)){
                $this->addWarning(new OTAWarning('Failed to save profile address, potentially missing some fields',true));
                //dd($address->getErrors());
              }
              if(!$user->save([],true)){
                $this->addWarning(new OTAWarning('Failed to save user profile, potentially missing some fields', true));
                //dd($user->getErrors());
              }
              $user->billingAddress()->associate($address);
              $booking->user()->associate($user);
              $booking->hotel()->associate($this->_hotel);

              $runningTotal = null;

              //todo: we don't support room type amenities at the moment
              //todo: we don't support room max children
              //todo: we dont support roomDescription Name attribs.
              //todo: is just using the roomtypecode okay? how do we map this with a rate plan? do we search prices?
              //todo: do we ignore this node?

              //process rates
              if (!empty($hotelResModel->getRoomStays())) {
                foreach ($hotelResModel->getRoomStays() as $roomStay) {
                  //find availabilityRooms for the booking
                  $availabilityBooking = new \AvailabilityBooking();
                  $availabilityBooking->uuid = (string) $availabilityBooking->generateUUID();
                  $availabilityBooking->user()->associate($user);
                  // Pass time of booking along using Carbon
                  $today = \Carbon\Carbon::now();
                  //todo: set this as per xml input
                  $availabilityBooking->time_booked = $today->toDateTimeString();
                  //todo: figure out start and end for availability booking / booking
                  /**
                   * @var $roomStay RoomStayPopulator
                   */
                  if (!empty($roomStay->getRoomRates())) {
                    foreach ($roomStay->getRoomRates() as $roomRate) {
                      /**
                       * @var $roomRate RoomRatePopulator
                       * @var $rate \RoomRate|ModelNotFoundException
                       * @var $booking \Booking
                       */
                      //todo: handle xml error for missing rate
                      $rate = \RoomRate::where('channelmanagercode', $roomRate->getRatePlanCode())->firstOrFail();
                      $date = $roomRate->getEffectiveDateAsCarbon();

                      //handle booking dates
                      //todo: only update availabilkity bookings for a roomrate
                      if($booking->date_from == null) {
                        $booking->date_from = $date;
                        $availabilityBooking->date_from = $date;
                      }
                      if($booking->date_to == null) {
                        $booking->date_to = $date;
                        $availabilityBooking->date_to = $date;
                      }
                      if ($date->lt($booking->date_from)) {
                        $booking->date_from = $date;
                        $availabilityBooking->date_from = $date;
                      }
                      if ($date->gt($booking->date_to)) {
                        $booking->date_to = $date;
                        $availabilityBooking->date_to = $date;
                      }
                      try {
                        $availabilityRoom = $rate->availability()->where('date', $date->format('Y-m-d'))->firstOrFail();
                      } catch (ModelNotFoundException $e) {
                        //todo: is creating this missing date correct, probably throw warning that room not available
                        $availabilityRoom = new \AvailabilityRoom();
                        $availabilityRoom->room()->associate($rate->room);
                        $availabilityRoom->rates()->associate($rate);
                        $availabilityRoom->uuid = $availabilityRoom->generateUUID();
                        $availabilityRoom->rate = 0;
                        $availabilityRoom->closed = 0;
                        $availabilityRoom->quantity = 0;
                        $availabilityRoom->date = $date->format('Y-m-d');
                        $availabilityRoom->save([], true);
                      }
                      $availabilityBooking->room()->associate($availabilityRoom->room);
                      $availabilityBooking->rate()->associate($availabilityRoom->rates);
                      //get rate
                      $total = null;
                      foreach ($roomRate->getRates() as $roomRateRate) {
                        //todo: how do we handle multiple rates here?
                        $total = $availabilityRoom->rate;
                        if($roomRateRate->getTotal() != null) {
                          $total = $roomRateRate->getTotal();
                          if($roomRateRate->getDecimalPlaces() > 0) {
                            for ($i=0;$i<$roomRateRate->getDecimalPlaces();$i++) {
                              $total = $total / 10;
                            }
                          }
                        }
                      }
                      $runningTotal += $total;
                      if(!$booking->save([],true)) {
                        \Log::info('OTA Booking failed to save for booking:'.$booking->id,$booking->getErrors());
                        $this->addError(new OTAError('Failed to store booking info, some fields were missing',false));
                        $booking->delete();
                        $user->delete();
                        goto responsePoint;
                      }
                      $availabilityBooking->booking()->associate($booking);
                      //todo: check total against totalnode
                      //todo check basicpropertyinfohotelcode
                      if(!$availabilityBooking->save([],true)){
                        \Log::info('OTA Availability Booking failed to save for booking:'.$booking->id,$availabilityBooking->getErrors());
                        $this->addError(new OTAError('Failed to store booking room info, some fields were missing',false));
                        $booking->delete();
                        $user->delete();
                        goto responsePoint;
                      }
                      $availabilityBooking->rooms()->attach($availabilityRoom->id,['rate'=>$total]);
                      }
                    } //isset roomrates
                  }  // roomstays
                } //get roomstays
              $booking->total = $runningTotal;
              $booking->save([],true);
              if ($booking->booking_ref == '') {
                $booking->booking_ref = $booking->id;
                $booking->save([],true);
              }

              //fill payment info
              if($hotelResGlobalModel->hasPaymentInfo()) {
                $payment = new \BookingPayment();

                $payment->card_number = $hotelResGlobalModel->getPaymentCardCardNumber();
                $payment->card_number_last_four = substr($payment->card_number,-4);
                $payment->cvv = $hotelResGlobalModel->getPaymentCardSeriesCode();
                $payment->valid_to = $hotelResGlobalModel->getPaymentCardExpireDate();
                $payment->uuid = (string) $payment->generateUUID();

                // Associate the booking
                $payment->booking()->associate($booking);

                // do some basic encryption
                /**
                 * @var $hotel \Hotel
                 */
                $hotel = $payment->booking->hotel;

                \Crypt::setKey(substr(\Config::get('app.key').$hotel->id,-32));
                //\Crypt::setCipher('AES');

                $payment->card_number = \Crypt::encrypt($payment->card_number);
                $payment->cvv = \Crypt::encrypt($payment->cvv);

                \Crypt::setKey(\Config::get('app.key'));

                if(!$payment->save([], true))
                {
                  \Log::info('OTA PaymentInfo not save for booking: '.$booking->id,$payment->getErrors());
                  $this->addWarning(new OTAWarning('Failed to store payment info, some fields were missing',true));
                }
              }

              /**
               * todo: handle changelog somehow
               */
              $bookings[] = $booking;

              if(!$this->canSave()) {
                //test mode, delete booking
                $booking->delete();
                $address->delete();
                $user->delete();
              }
            }

          } //loop on hotelreservation
        } //loop on main node
        //todo: create response builder class to allow for earlier responses
        responsePoint:
        $successResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelResNotifRS xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05/OTA_HotelAvailNotifRS.xsd" TimeStamp="' . date('c') . '" Target="' . $this->_request_target . '" Version="' . $this->_request_version . '">
  ';
if ($this->hasErrors()) {
  $successResponse .= $this->errorsResponse();
} else {
  $successResponse .= '<Success />';
  $successResponse .= $this->responseReservations($bookings);
}
$successResponse .= $this->warningsResponse() . '
</OTA_HotelResNotifRS>';
        //todo: add reservation ids in here
        return \Response::make($successResponse, 200)->header('Content-Type', 'text/xml');
      } //if auth
    } catch (OTAParseException $e) {
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="' . $this->_request_target . '" ErrorMessage="Message contains invalid XML" ErrorCode="Malformed" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 400)->header('Content-Type', 'text/xml');
    } catch (OTAAuthException $e) {
      $failedResponse = '<?xml version="1.0"?>
<OTA_HotelResNotifRS xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Version="1.00" xmlns="http://www.opentravel.org/OTA/2003/05" TimeStamp="' . date('c') . '">
    <Errors>
        <Error ShortText="Hotel Not Found" RecordID="' . $this->_requestor_type_id . '">Hotel Not Found: ' . $this->_requestor_hotel_id . ' Please check POS credentials and/or HotelCode</Error>
    </Errors>
</OTA_HotelResNotifRS>';
      return \Response::make($failedResponse, 401)->header('Content-Type', 'text/xml');
    } catch (\Exception $e) {
      \Log::info('Failed to process XML:'.$e->getMessage(),[
        'Trace:'=>$e->getTrace(),
        'Code:'=>$e->getCode(),
        'File:'=>$e->getFile(),
        'Line:'=>$e->getLine(),
        'POS Info'=>$this->_requestor_hotel_id,
        'XML'=>$this->parse()
      ]);
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Failed to process request. Request has been logged" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 500)->header('Content-Type', 'text/xml');
    }
  }

  public function lastChange($date = null)
  {
    try {
      try {
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $date);
      } catch (\InvalidArgumentException $e) {
        try {
          $date = Carbon::createFromFormat('Y-m-d', $date);
        } catch (\InvalidArgumentException $e) {
          //todo: throw invalid date response
        }
      }

      if($date->diffInDays(Carbon::today())>14) {
        //todo: throw invalid date too old exception (more than 2 weeks ago)
      }

      /**
       * @var $hotel \Hotel
       */
      $hotel = $this->_hotel;
      $bookings = $hotel->bookings()->where('updated_at','>',$date->format('Y-m-d H:i:s'))->get();
      //todo: does this need ResStatus?
      $response = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelResNotifRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRQ.xsd" TimeStamp="'.date('c').'" Target="Production" Version="1.00">
<HotelReservations>';
      if(count($bookings) > 0) {
        $response .= '
        <HotelReservation>
            <ResGlobalInfo>
                <HotelReservationIDs>';
      }
      foreach ($bookings as $booking) {
        /**
         * @var $booking \Booking
         */
        //todo: add res_id_source?
        $response .= '<HotelReservationID ResID_Value="'.$booking->booking_ref.'" ResID_Type="14"/>';
      }

      if(count($bookings) > 0) {
$response .= '</HotelReservationIDs>
            </ResGlobalInfo>
        </HotelReservation>';
      }

      $response .= '</HotelReservations>
</OTA_HotelResNotifRQ>';

      return \Response::make($response, 200)->header('Content-Type', 'text/xml');

    } catch (\Exception $e) {
      \Log::info('Failed to process XML:'.$e->getMessage(),[
        'Trace:'=>$e->getTrace(),
        'Code:'=>$e->getCode(),
        'File:'=>$e->getFile(),
        'Line:'=>$e->getLine(),
        'POS Info'=>$this->_requestor_hotel_id,
        'XML'=>$this->parse()
      ]);
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Failed to process request. Request has been logged" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 500)->header('Content-Type', 'text/xml');
    }
  }


  public function getById($bookingID = null)
  {
    try {
      try {
        $hotel = $this->_hotel;
        //todo: check hotel id too
        $booking = \Booking::where('booking_ref',$bookingID)->firstOrFail();
      } catch (ModelNotFoundException $e) {
        //todo: throw reservation not found error
        $this->addError(new OTAError('Booking not found for that reference'));
        $failedResponse = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_ErrorRS Status="NotProcessed" Target="' . $this->_request_target . '" ErrorMessage="Booking Not Found" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
        $failedResponse .= $this->errorsResponse();
        $failedResponse .= '</OTA_ErrorRS>';
        return \Response::make($failedResponse, 404)->header('Content-Type', 'text/xml');
        //throw $e;
      }

      /**
       * @var $hotel \Hotel
       * @var $booking \Booking
       */
      //todo: does this need ResStatus?
      $response = '<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelResNotifRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRQ.xsd" TimeStamp="'.date('c').'" Target="Production" Version="1.00">
<HotelReservations>';
        $response .= '
        <HotelReservation>
            <ResGlobalInfo>
                <HotelReservationIDs>';
        /**
         */
        //todo: add res_id_source?
        $response .= '<HotelReservationID ResID_Value="'.$booking->booking_ref.'" ResID_Type="14"/>';
$response .= '</HotelReservationIDs>';

      if(!empty($booking->additional_info)){
        $response .= '<Comments><Comment><Text>'.trim($booking->additional_info).'</Text></Comment></Comments>';
      }
      if(!empty($booking->total)){
        //todo: update currency code
        $response .= '<Total AmountAfterTax="'.($booking->total*100).'" DecimalPlaces="2" CurrencyCode="GBP"></Total>';
      }
      if($booking->paymentdetails) {
        /**
         * @var $payment \BookingPayment
         */
        $payment = $booking->paymentdetails;
        //todo: set card codes
        //todo: set cardholder name
        try {
          \Crypt::setKey(substr(\Config::get('app.key') . $hotel->id, -32));

          $payment->card_number = \Crypt::decrypt($payment->card_number);
          $payment->cvv = \Crypt::decrypt($payment->cvv);

          \Crypt::setKey(\Config::get('app.key'));
          $response .= '<Guarantee>
                    <GuaranteesAccepted>
                        <GuaranteeAccepted>
                            <PaymentCard CardCode="" CardNumber="' . $payment->card_number . '" SeriesCode="' . $payment->cvv . '" ExpireDate="' . $payment->valid_to . '">
                            </PaymentCard>
                        </GuaranteeAccepted>
                    </GuaranteesAccepted>
                </Guarantee>';
          //remove payment info
          $payment->card_number = Carbon::now()->__toString();
          $payment->cvv = Carbon::now()->__toString();
          $payment->valid_from = str_pad($payment->valid_from, 4, "0", STR_PAD_LEFT);
          $payment->valid_to = str_pad($payment->valid_to, 4, "0", STR_PAD_LEFT);

          if (!$payment->save([], true)) {
            $this->addWarning(new OTAWarning('Failed to store payment information, possible fields missing',true));
            //dd($payment->getErrors());
          }
        } catch (DecryptException $e) {
          //data has already been accessed
        }
      }
      //add profile and address info
      if($booking->user) {
        /**
         * @var $user \User
         * @var $address \Address
         */
        $user = $booking->user;
        //todo: add telephone prefix to telephone
        //todo: return correct country code
        $email = ends_with($user->email,'@channelmanager.bookings') ? '' : $user->email;
        $address = $user->billingAddress ?: new \Address();
        $response .= '
                <Profiles>
                    <ProfileInfo>
                        <Profile>
                            <Customer>
                                <PersonName>
                                    <GivenName>'.$user->name.'</GivenName>
                                    <Surname>'.$user->lastname.'</Surname>
                                </PersonName>
                                <Telephone PhoneNumber="'.$user->telephone.'" />
                                <Email>'.$email.'</Email>
                                <Address>
                                    <AddressLine>'.$address->line1.'</AddressLine>
                                    <CityName>'.$address->town.'</CityName>
                                    <PostalCode>'.$address->postcode.'</PostalCode>
                                    <CountryName Code="GB" />
                                    <CompanyName></CompanyName>
                                </Address>
                            </Customer>
                        </Profile>
                    </ProfileInfo>
                </Profiles>';
      }

      $response .= '</ResGlobalInfo>';

      //add roomstays node
      if(count($booking->availabilitybooking) > 0) {
        $response .= '<RoomStays>';
        foreach ($booking->availabilitybooking as $k => $availabilityBooking) {
          $total = 0;
          /**
           * @var $availabilityBooking \AvailabilityBooking
           * @var $room \Room
           * @var $availabilityRoom \AvailabilityRoom
           * @var $roomRate \RoomRate
           */
          $response .= '<RoomStay IndexNumber="'.$k.'">
                <RoomTypes>';
          $room = $availabilityBooking->room ?: $availabilityBooking->rooms()->first()->room;
          /**
           * todo add these fields to room:
          <Text></Text>
          <MealPlan>All meals and select beverages are included in the room rate.</MealPlan>
          <MaxChildren>0</MaxChildren>
           * todo: add amenities to room
          <Amenities>
          <Amenity></Amenity>
          </Amenities>
           */
          $response .= '<RoomType RoomTypeCode="'.$room->channelmanagercode.'">
                        <RoomDescription Name="'.$room->name.'">
                        </RoomDescription>
                    </RoomType>';
          $response .= '</RoomTypes>';
          //todo: fix rateplans
//          $response .= '<RatePlans>';
//          foreach ($availabilityBooking->rooms as $availabilityRoom) {
//            $response .= '<RatePlan>
//                        <Commission>
//                            <CommissionPayableAmount Amount="4500" DecimalPlaces="2" CurrencyCode="GBP" />
//                        </Commission>
//                    </RatePlan>';
//            }
//          $response .= '</RatePlans>';
          $response .= '<RoomRates>';
          foreach ($availabilityBooking->rooms as $availabilityRoom) {
            $roomRate = $availabilityRoom->rates ?: new \RoomRate();
            $response .= '<RoomRate EffectiveDate="'.$availabilityRoom->date->format('Y-m-d').'" RatePlanCode="'.$roomRate->channelmanagercode.'">
                        <Rates>
                            <Rate>
                                <Total AmountAfterTax="'.$availabilityRoom->rate.'" DecimalPlaces="0" CurrencyCode="GBP" />
                            </Rate>
                        </Rates>
                    </RoomRate>';
            $total += $availabilityRoom->rate;
          }
          $response .= '</RoomRates>';


          //todo: add guestcounts
//                <GuestCounts>
//                    <GuestCount Count="2" />
//                </GuestCounts>
          $response .= '<Total AmountAfterTax="'.$total.'" DecimalPlaces="0" CurrencyCode="GBP" />';


          //todo: add resguestrphs

          //todo: add specialrequests

          //todo: add serviceRPHS

          //todo: add services

          //todo: add resguests

          $response .= '<BasicPropertyInfo HotelCode="'.$hotel->ota_hotel_id.'" />';
//          $response .= '<ResGuestRPHs>
//                    <ResGuestRPH RPH="1" />
//                </ResGuestRPHs>
//                <ServiceRPHs>
//                    <ServiceRPH RPH="1" />
//                </ServiceRPHs>';

          $response .= '</RoomStay>';
        }
        $response .= '</RoomStays>';
      }

      $response .= '</HotelReservation>';









      $response .= '</HotelReservations>
</OTA_HotelResNotifRQ>';

      return \Response::make($response, 200)->header('Content-Type', 'text/xml');

    } catch (\Exception $e) {
      \Log::info('Failed to process XML:'.$e->getMessage(),[
        'Trace:'=>$e->getTrace(),
        'Code:'=>$e->getCode(),
        'File:'=>$e->getFile(),
        'Line:'=>$e->getLine(),
        'POS Info'=>$this->_requestor_hotel_id,
        'XML'=>$this->parse()
      ]);
      $failedResponse = '<OTA_ErrorRS Status="NotProcessed" Target="'.$this->_request_target.'" ErrorMessage="Failed to process request. Request has been logged" Severity="High" xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />';
      return \Response::make($failedResponse, 500)->header('Content-Type', 'text/xml');
    }
  }

  //todo: is this needed?
  private function processRatesForDates(RatePopulator $ratePopulate)
  {
    //todo: add with hotel and do where based on credentials
    if ($ratePopulate->isRoomCode() && $ratePopulate->isRateCode() && $ratePopulate->isStart() && $ratePopulate->isEnd()) {
      $roomCode = $ratePopulate->getRoomCode();
      $rateCode = $ratePopulate->getRateCode();
      $roomRate = \RoomRate::whereHas('room', function ($query) use ($roomCode) {
        $query->where('channelmanagercode', $roomCode);
      })->where('channelmanagercode', $rateCode)->firstOrFail();

      $this->processDateRange($roomRate, $ratePopulate);
    } elseif ($ratePopulate->isRoomCode() && !$ratePopulate->isRateCode() && $ratePopulate->isStart() && $ratePopulate->isEnd()) {

      $roomCode = $ratePopulate->getRoomCode();
      $room = \Room::where('channelmanagercode', $roomCode)->firstOrFail();
      /**
       * @var $room \Room
       */
      foreach ($room->rates as $roomRate) {
        $this->processDateRange($roomRate, $ratePopulate);
      }
    }
  }

  //todo: is this needed?
  private function processDateRange(\RoomRate $roomRate, RatePopulator $ratePopulator)
  {
    foreach (DateRangeHelper::date_range($ratePopulator->getStart(), $ratePopulator->getEnd()) as $today) {
      /**
       * @var $today Carbon
       */
      $availabilityRoom = \AvailabilityRoom::findByDate($today, $roomRate->room, $roomRate);
      //create if not found
      if (!$availabilityRoom) {
        $availabilityRoom = new \AvailabilityRoom();
        $availabilityRoom->room()->associate($roomRate->room);
        $availabilityRoom->rates()->associate($roomRate);
        $availabilityRoom->uuid = $availabilityRoom->generateUUID();
        $availabilityRoom->rate = 0;
        $availabilityRoom->closed = 0;
        $availabilityRoom->quantity = 0;
        $availabilityRoom->date = $today;
        $availabilityRoom->save();
      }
      /**
       * @var $availabilityRoom \AvailabilityRoom
       */
      if ($ratePopulator->isBookingLimit()) {
        //todo: check bookings here that quantity can go this low
        $existingBookingsCount = $availabilityRoom->bookings->count();
        if ($existingBookingsCount > (int)$ratePopulator->getBookingLimit()) {
          $ratePopulator->addMinimumCountWarning($today->format('Y-m-d'));
          $availabilityRoom->quantity = $availabilityRoom->bookings->count();
        } else {
          $availabilityRoom->quantity = (int)$ratePopulator->getBookingLimit();
        }
      }
      if ($ratePopulator->isMinStay()) {
        $availabilityRoom->min_stay = (int)$ratePopulator->getMinStay();
      }
      if ($ratePopulator->isMaxStay()) {
        $availabilityRoom->max_stay = (int)$ratePopulator->getMaxStay();
      }
      if ($ratePopulator->isClosed()) {
        $availabilityRoom->closed = $ratePopulator->getClosed();
      }
      if ($ratePopulator->isNoCheckOut()) {
        $availabilityRoom->no_check_out = $ratePopulator->getNoCheckOut();
      }
      if ($ratePopulator->isNoCheckIn()) {
        $availabilityRoom->no_check_in = $ratePopulator->getNoCheckIn();
      }
      if ($this->canSave()) {
        $availabilityRoom->save([], true);
      }
    }
    if (!empty($ratePopulator->getMinimumCountWarnings()) && count($ratePopulator->getMinimumCountWarnings()) > 0) {
      $message = 'Rooms to Sell were set below the Minimum Contracted rooms for these dates: ';
      foreach ($ratePopulator->getMinimumCountWarnings() as $date) {
        $message .= $date . ', ';
      }
      substr($message, 0, -3);
      $message .= '. The values have been amended to the Minimum Contracted Rooms.';
      $this->addWarning(new OTAWarning($message, true));
    }
  }

  /**
   * @param \Booking[] $bookings
   * @return string
   */
  private function responseReservations($bookings = [])
  {
    $response = '<HotelReservations>';
    $response .= '<HotelReservation>';
    $response .= '<ResGlobalInfo>';
    $response .= '<HotelReservationIDs>';
    foreach ($bookings as $booking) {
      /**
       * @var $booking \Booking
       */
      $response .= '<HotelReservationID ';
      $response .= 'ResID_Value="'.$booking->booking_ref.'" Res_ID_Type="14"';
      $response .= '/>';
    }
    $response .= '</HotelReservationIDs>';
    $response .= '</ResGlobalInfo>';
    $response .= '</HotelReservation>';
    $response .= '</HotelReservations>';


    return $response;
  }
}