<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class TaxController extends \BaseController
{
    protected static $relations = ['hotel', 'calculation_type'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Tax::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Tax::fields());

        try
        {
            $tax = new Tax();
            $tax->fill($input);
            $tax->uuid = (string) $tax->generateUUID();

            try
            {
                $this->handleRelations($tax);
            }
            catch(ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }

            if(!$tax->save())
            {
                return $this->response->errorBadRequest($tax->getErrors());
            }

            return $this->response->array(['tax_id'=>$tax->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $tax = Tax::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Tax Not Found');
        }

        return $this->response->array($tax->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $tax = Tax::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Tax Not Found');
        }

        $tax->name = \Input::get('name', $tax->name);
        $tax->amount = \Input::get('amount', $tax->amount);
        $tax->inclusive = \Input::get('inclusive', $tax->inclusive);

        if(!$tax->save())
        {
            if ($tax->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($tax->getErrors());
            }
            return $this->response->errorBadRequest($tax->getErrors());
        }

        try
        {
            $this->handleRelations($tax);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $this->response->array(['tax_id'=> $tax->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $tax = Tax::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Tax Not Found');
        }

        $tax->delete();
    }

    /**
     * Handle relations specific to Tax model
     *
     * @param  Tax $tax
     *
     * @return void
     */
    protected function handleRelations(&$tax)
    {
        if($hotel = \Input::get('hotel')) {
            $tax->hotel()->associate(Hotel::findByUUIDorFail($hotel));
        }

        if($type = \Input::get('calculation_type')) {
            $tax->calculation_type()->associate(CalculationType::findByUUIDorFail($type));
        }
    }
}