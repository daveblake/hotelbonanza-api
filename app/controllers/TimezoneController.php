<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class TimezoneController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['countries'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = Timezone::all()->count();

        $timezones = Timezone::with(static::$relations)
            ->orderBy('city', 'asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'timezones' => $timezones,
            'total' => (string) $count,
            'paginated' => (string) $timezones->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = \Input::only(Timezone::fields());

        $timezone = new Timezone();
        $timezone->fill($input);
        $timezone->uuid = (string) $timezone->generateUUID();

        if(!$timezone->save())
        {
            return $this->response->errorBadRequest($timezone->getErrors());
        }

        // Find and associate the country
        $this->associateCountry($timezone, \Input::get('countries',[]));

        return $this->response->array(['timezone_id' => $timezone->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $timezone = Timezone::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Timezone Not Found');
        }

        return $this->response->array($timezone->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $timezone = Timezone::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Timezone Not Found');
        }

        $timezone->city = \Input::get('city', $timezone->city);
        $timezone->timezone = \Input::get('timezone', $timezone->timezone);

        // Find and associate the country
        $this->associateCountry($timezone, \Input::get('countries', []));

        if(!$timezone->save())
        {
            if ($timezone->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($timezone->getErrors());
            }
            return $this->response->errorBadRequest($timezone->getErrors());
        }

        return $this->response->array(['timezone_id' => $timezone->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $timezone = Timezone::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Timezone Not Found');
        }

        $timezone->delete();
    }

    /**
     * Attempt to associate the Timezone with the a Country model
     *
     * @param Timezone $timezone
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateCountry(Timezone &$timezone, $uuid)
    {
        if(!is_null($uuid)) {
            // Find and associate the country
            $country = Country::findByUUIDorFail($uuid);
            $timezone->countries()->attach($country->id);
        }
    }
}