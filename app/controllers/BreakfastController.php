<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class BreakfastController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['type'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $options = BreakfastOption::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return CoreJsonResponse::respond(BreakfastOption::query(), $options, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(BreakfastOption::fields());

        $breakfast = new BreakfastOption();
        $breakfast->fill($input);
        $breakfast->uuid = (string) $breakfast->generateUUID();

        // Attempt to associate the area with the hotel
        try
        {
            if($type = Input::get('type')) {
                $this->associateType($breakfast, $type);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$breakfast->save())
        {
            return $this->response->errorBadRequest($breakfast->getErrors());
        }

        return $this->response->array(['breakfast_option_id' => $breakfast->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $breakfast = BreakfastOption::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Breakfast Option Not Found');
        }

        return $this->response->array($breakfast->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $breakfast = BreakfastOption::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Breakfast Option Not Found');
        }

        $breakfast->adultprice = \Input::get('adultprice', $breakfast->adultprice);
        $breakfast->childprice = \Input::get('childprice', $breakfast->childprice);
        $breakfast->roomprice = \Input::get('roomprice', $breakfast->roomprice);

        // Attempt to associate the area with the hotel
        try
        {
            if($type = Input::get('type')) {
                $this->associateType($breakfast, $type);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$breakfast->save())
        {
            if ($breakfast->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($breakfast->getErrors());
            }
            return $this->response->errorBadRequest($breakfast->getErrors());
        }

        return $this->response->array(['breakfast_option_id' => $breakfast->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $breakfast = BreakfastOption::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Breakfast Option Not Found');
        }

        $breakfast->delete();
    }

    /**
     * Associate a type with the current model
     *
     * @param \BreakfastOption $breakfast
     * @param $type
     */
    protected function associateType(BreakfastOption &$breakfast, $type)
    {
        $breakfast->type()->associate(BreakfastType::findByUUIDorFail($type));
    }
}