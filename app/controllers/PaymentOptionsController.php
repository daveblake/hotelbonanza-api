<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentOptionsController extends \BaseController
{
    protected static $relations = ['hotel', 'cardtypes'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return PaymentOption::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(PaymentOption::fields());

        try
        {
            $option = new PaymentOption();
            $option->fill($input);
            $option->uuid = (string) $option->generateUUID();

            // Associate the hotel
            $hotel = \Input::get('hotel');
            $this->associateHotel($option, $hotel); // required so validation doesn't fail

            if(!$option->save())
            {
                return $this->response->errorBadRequest($option->getErrors());
            }

            try 
            {
                $this->handleRelations($option);
            }
            catch(ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }

            return $this->response->array(['payment_option_id'=>$option->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $option = PaymentOption::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Payment Option Not Found');
        }

        return $this->response->array($option->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $option = PaymentOption::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Payment Option Not Found');
        }

        $option->bankTransfer = \Input::get('bankTransfer', $option->bankTransfer);
        $option->paypal = \Input::get('paypal', $option->paypal);
        $option->cash = \Input::get('cash', $option->cash);

        if(!$option->save())
        {
            if ($option->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($option->getErrors());
            }
            return $this->response->errorBadRequest($option->getErrors());
        }

        try 
        {
            $this->handleRelations($option);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $this->response->array(['payment_option_id'=> $option->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $option = PaymentOption::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Payment Option Not Found');
        }

        $option->delete();
    }

    protected function handleRelations(PaymentOption &$option)
    {
        if($hotel = \Input::get('hotel')) {
            $this->associateHotel($option, $hotel);
        }
        
        $this->associateCardTypes($option, \Input::get('cardtypes', []));
    }

    /**
     * Attach the hotel to the payment option
     *
     * @param PaymentOption $option
     * @param string $uuid
     * @param bool $sync
     */
    protected function associateHotel(PaymentOption &$option, $uuid, $sync = true)
    {
        $option->hotel()->associate(Hotel::findByUUIDorFail($uuid));
    }

    /**
     * Associate one or more card types with a payment option
     *
     * @param PaymentOption $option
     * @param array $cardtypes
     * @param bool $sync
     */
    protected function associateCardTypes(PaymentOption &$option, array $cardtypes, $sync = true)
    {
        if($sync) {
            // first, detach all.
            $option->cardtypes()->detach($option->cardtypes->lists('id'));
        }

        // attempt to relate
        foreach((array)$cardtypes as $uuid) {
            $option->cardtypes()->attach(CardType::findByUUIDorFail($uuid));
        }
    }
}