<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class FeatureController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Cache the count ready to be returned
        $count = Feature::all()->count();

        // Get the paginated features
        $features = Feature::query()
            ->orderBy('name', 'asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'features' => $features,
            'total' => (string) $count,
            'paginated' => (string) $features->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Feature::fields());

        try
        {
            $feature = new Feature();
            $feature->fill($input);
            $feature->uuid = (string) $feature->generateUUID();

            if(!$feature->save())
            {
                return $this->response->errorBadRequest($feature->getErrors());
            }

            return $this->response->array(['feature_id'=>$feature->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $feature = Feature::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Feature Not Found');
        }

        return $this->response->array($feature->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $feature = Feature::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Feature Not Found');
        }

        $feature->name = \Input::get('name', $feature->name);

        if(!$feature->save())
        {
            if ($feature->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($feature->getErrors());
            }
            return $this->response->errorBadRequest($feature->getErrors());
        }

        return $this->response->array(['feature_id'=>$feature->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $feature = Feature::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Feature Not Found');
        }

        $feature->delete();
    }
}