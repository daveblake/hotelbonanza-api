<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class BedTypeController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = BedType::all()->count();

        $types = BedType::query()
            ->orderBy('order','asc')
            ->orderBy('name','asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'rooms__beds_types' => $types,
            'total' => (string) $count,
            'paginated' => (string) $types->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(BedType::fields());

        $bedType = new BedType();
        $bedType->fill($input);
        $bedType->uuid = (string) $bedType->generateUUID();

        if(!$bedType->save())
        {
            return $this->response->errorBadRequest($bedType->getErrors());
        }

        return $this->response->array(['bedtype_id' => $bedType->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $bedType = BedType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Bed Type Not Found');
        }

        return $this->response->array($bedType->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $bedType = BedType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Bed Type Not Found');
        }

        $input = Input::only(BedType::fields());
        $input['name'] = Input::get('name', $bedType->name);

        $bedType->fill($input);

        if(!$bedType->save())
        {
            if ($bedType->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($bedType->getErrors());
            }
            return $this->response->errorBadRequest($bedType->getErrors());
        }

        return $this->response->array(['bedtype_id'=>$bedType->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $bedType = BedType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Bed Type Not Found');
        }

        $bedType->delete();
    }
}