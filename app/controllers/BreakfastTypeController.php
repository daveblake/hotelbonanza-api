<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class BreakfastTypeController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $types = BreakfastType::with(static::$relations)
            ->orderBy('name','asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return CoreJsonResponse::respond(BreakfastType::query(), $types, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(BreakfastType::fields());

        $type = new BreakfastType();
        $type->fill($input);
        $type->uuid = (string) $type->generateUUID();


        if(!$type->save())
        {
            return $this->response->errorBadRequest($type->getErrors());
        }

        return $this->response->array(['breakfast_type_id' => $type->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $type = BreakfastType::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Breakfast Type Not Found');
        }

        return $this->response->array($type->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $type = BreakfastType::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Breakfast Type Not Found');
        }

        $type->name= \Input::get('name', $type->name);

        if(!$type->save())
        {
            if ($type->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($type->getErrors());
            }
            return $this->response->errorBadRequest($type->getErrors());
        }

        return $this->response->array(['breakfast_type_id' => $type->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $type = BreakfastType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Breakfast Type Not Found');
        }

        $type->delete();
    }
}