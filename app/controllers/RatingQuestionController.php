<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RatingQuestionController extends \BaseController
{
    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return RatingQuestion::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(RatingQuestion::fields());

        try
        {
            $question = new RatingQuestion();
            $question->fill($input);
            $question->uuid = (string) $question->generateUUID();

            if(!$question->save())
            {
                return $this->response->errorBadRequest($question->getErrors());
            }

            return $this->response->array(['question_id'=>$question->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $question = RatingQuestion::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Rating Question Not Found');
        }

        return $this->response->array($question->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $question = RatingQuestion::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Rating Question Not Found');
        }

        $question->label = \Input::get('label', $question->label);

        if(!$question->save())
        {
            if ($question->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($question->getErrors());
            }
            return $this->response->errorBadRequest($question->getErrors());
        }

        return $this->response->array(['question_id'=> $question->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $question = RatingQuestion::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Rating Question Not Found');
        }

        $question->delete();
    }
}