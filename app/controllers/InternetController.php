<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class InternetController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['area'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Internet::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Internet::fields());

        $internet = new Internet();
        $internet->fill($input);
        $internet->uuid = (string) $internet->generateUUID();

        // Attempt to associate the area
        try
        {
            if($area = Input::get('area')) {
                $this->associateArea($internet, $area);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$internet->save())
        {
            return $this->response->errorBadRequest($internet->getErrors());
        }

        return $this->response->array(['internet_id' => $internet->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $internet = Internet::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Internet Not Found');
        }

        return $this->response->array($internet->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $internet = Internet::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Internet Not Found');
        }

        $internet->price = \Input::get('price', $internet->price);
        $internet->type = \Input::get('type', $internet->type);
        $internet->seconds = \Input::get('seconds', $internet->seconds);

        // Attempt to associate the area
        try
        {
            if($area = Input::get('area')) {
                $this->associateArea($internet, $area);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$internet->save())
        {
            if ($internet->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($internet->getErrors());
            }
            return $this->response->errorBadRequest($internet->getErrors());
        }

        return $this->response->array(['internet_id' => $internet->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $internet = Internet::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Internet Not Found');
        }

        $internet->delete();
    }

    /**
     * Associate an area with the current model
     *
     * @param Internet $internet
     * @param $area
     */
    protected function associateArea(Internet &$internet, $area)
    {
        $internet->area()->associate(InternetArea::findByUUIDorFail($area));
    }
}