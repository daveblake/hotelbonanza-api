<?php
class EventController extends \BaseController
{

    /**
     * @var array
     */
    protected static $relations = ['address','address.location','images'];

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $fromDate = \Input::get('fromDate', date('Y/m/d H:s'));

    $events = Events::with(static::$relations)
        ->skip($this->getOffset())
        ->take($this->getLimit())
        ->where('from','>', $fromDate) // TODO: make this query safe!
        ->orderBy('from','ASC')
        ->get();

    return CoreJsonResponse::respond(Events::query(), $events, $this->getLimit());
  }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = \Input::only(Events::fields());

        $Event = new Events();
        $Event->fill($input);
        $Event->uuid = (string)$Event->generateUUID();

        if (!$Event->save()) {
            return $this->response->error($Event->getErrors(), 400);
        }

        if ($address = \Input::get('address')) {
            $this->associateAddress($Event, $address);
        }

        if($images = \Input::get('images', [])) {
            $this->attachImages($Event, $images);
        }

        return $this->response->array(['event_id' => $Event->uuid]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try {
            $Event = Events::findByUUIDorFail($id, static::$relations);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('Event Not Found');
        }

        $event = $Event->toArray();

        return $this->response->array($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {

        try {
            $Event = Events::findByUUIDorFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('Event Not Found');
        }

        $Event->from = \Input::get('from', $Event->from);
        $Event->to = \Input::get('to', $Event->to);
        $Event->name = \Input::get('name', $Event->name);
        $Event->description = \Input::get('description', $Event->description);

        try {

            if ($address = \Input::get('address')) {
                $this->associateAddress($Event, $address);
            }

            if($images = \Input::get('images', [])) {
                $this->attachImages($Event, $images);
            }

            if (!$Event->save()) {
                if ($Event->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($Event->getErrors());
                }
                return $this->response->error($Event->getErrors(), 400);
            }

            return $this->response->array(['event_id' => $Event->uuid]);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $Event = Events::findByUUIDorFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('Event Not Found');
        }
        $Event->delete();
    }

    /**
     * Associate an address with the current hotel
     *
     * @param User $user
     * @param $uuid
     */
    protected function associateAddress(Events &$event, $uuid)
    {
        $event->address()->associate(Address::findByUUIDorFail($uuid));
    }

    /**
     * Attempt to attach an image to an event
     *
     * @param Events $event
     * @param array $images
     * @param bool $sync
     */
    protected function attachImages(Events &$event, array $images, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the events -> image in sync
            $event->images()->detach($event->images->lists('id'));
        }

        // attempt to relate an image
        foreach((array)$images as $uuid) {
            $event->images()->attach(Media::findByUUIDorFail($uuid));
        }
    }
}
