<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class LocationController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Location::query()
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Location::fields());

        try
        {
            $location = new Location();
            $location->fill($input);
            $location->uuid = (string) $location->generateUUID();

            if(!$location->save())
            {
                return $this->response->errorBadRequest($location->getErrors());
            }

            return $this->response->array(['location_id'=>$location->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $location = Location::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Location Not Found');
        }

        return $this->response->array($location->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $location = Location::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Location Not Found');
        }

        try
        {
            $location->latitude = \Input::get('latitude', $location->latitude);
            $location->longitude = \Input::get('longitude', $location->longitude);

            if(!$location->save())
            {
                if ($location->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($location->getErrors());
                }
                return $this->response->errorBadRequest($location->getErrors());
            }

            return $this->response->array(['location_id'=>$location->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $location = Location::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Location Not Found');
        }

        $location->delete();
    }
}