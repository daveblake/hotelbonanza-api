<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class BedController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $beds = Bed::with('type')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return CoreJsonResponse::respond(Bed::query(), $beds, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Bed::fields());

        try
        {
            $bed = new Bed();
            $bed->fill($input);
            $bed->uuid = (string) $bed->generateUUID();

            // Find and associate the bed_type
            $this->associateBedType($bed, Input::get('bed_type'));

            if(!$bed->save())
            {
                return $this->response->errorBadRequest($bed->getErrors());
            }

            return $this->response->array(['bed_id'=>$bed->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $bed = Bed::findByUUIDorFail($id, ['type']);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Bed Not Found');
        }

        return $this->response->array($bed->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $bed = Bed::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Bed Not Found');
        }

        $input = Input::only(Bed::fields());

        try
        {
            $bed->fill($input);

            // Find and associate the bed_type
            if($bed_type = Input::get('bed_type')) {
                $this->associateBedType($bed, $bed_type);
            }

            if(!$bed->save())
            {
                if ($bed->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($bed->getErrors());
                }
                return $this->response->errorBadRequest($bed->getErrors());
            }

            return $this->response->array(['bed_id'=>$bed->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $bed = Bed::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Bed Not Found');
        }

        $bed->delete();
    }

    /**
     * Attempt to associate the BedType with the current Bed model
     *
     * @param Bed $bed
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateBedType(Bed &$bed, $uuid)
    {
        // Only the fly validation because we don't store this.
        $validator = Validator::make(
            ['bed_type' => \Input::get('bed_type')],
            ['bed_type' => 'required|exists:rooms__beds_types,uuid']
        );

        try
        {
            if(!$validator->fails())
            {
                // Find and associate the bed_type
                $bedType = BedType::findByUUIDorFail($uuid);
                $bed->type()->associate($bedType);
            }
            else
            {
                throw new ModelNotFoundException('Unable to associate Bed Type: ' . $uuid);
            }
        }
        catch(ModelNotFoundException $e)
        {
            throw $e;
        }
    }
}