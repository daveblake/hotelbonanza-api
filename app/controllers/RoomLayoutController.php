<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RoomLayoutController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['beds', 'beds.type'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return RoomLayout::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $roomLayout = new RoomLayout();
        $roomLayout->uuid = (string) $roomLayout->generateUUID();
        $roomLayout->bedroom_number = Input::get('bedroom_number', null);
        $roomLayout->ensuite = (bool) Input::get('ensuite', 0);

        if(!$roomLayout->save())
        {
            return $this->response->errorBadRequest($roomLayout->getErrors());
        }

        // Attempt to attach a list of beds
        // After the roomLayout has been saved.
        try
        {
            $this->attachBeds($roomLayout, \Input::get('beds', array()));
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $this->response->array(['roomlayout_id'=> $roomLayout->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $roomLayout = RoomLayout::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Layout Not Found');
        }

        return $this->response->array($roomLayout->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $roomLayout = RoomLayout::findByUUIDorFail($id);
            $roomLayout->bedroom_number = Input::get('bedroom_number', null);
            $roomLayout->ensuite = (bool) Input::get('ensuite', 0);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Layout Not Found');
        }

        // Attempt to attach a list of beds
        try
        {
            $this->attachBeds($roomLayout, \Input::get('beds', array()));
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$roomLayout->save())
        {
            if ($roomLayout->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($roomLayout->getErrors());
            }
            return $this->response->errorBadRequest($roomLayout->getErrors());
        }

        return $this->response->array(['roomlayout_id'=> $roomLayout->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $roomLayout = RoomLayout::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Layout Not Found');
        }

        $roomLayout->delete();
    }

    /**
     * Attempt to attach a bed to a room layout
     *
     * @param RoomLayout $roomLayout
     * @param array $beds
     * @param bool $sync
     */
    protected function attachBeds(RoomLayout &$roomLayout, array $beds, $sync = true)
    {
        if($sync) {
            // first, detach all. This will keeps the layout -> bed in sync
            $roomLayout->beds()->detach($roomLayout->beds->lists('id'));
        }

        // attempt to relate a bed uuid to the current layout
        foreach((array)$beds as $uuid) {
            $roomLayout->beds()->attach(Bed::findByUUIDorFail($uuid));
        }
    }
}