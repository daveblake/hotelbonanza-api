<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class AvailabilityRoomController extends \BaseController
{
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $this->__adminLoginAsUser();
    if (!$this->getAuthdUser() instanceof User) {
      // Todo: is this the right code? Does it say a user couldn't be matched to the current token?
      return Response::make('Unauthorized', 401);
    }

    $rooms = AvailabilityRoom::getMine($this->getAuthdUser());

    return CoreJsonResponse::respond(AvailabilityRoom::query(), $rooms, $this->getLimit());
  }

  /**
   * Get available bookings based on the users input.
   *
   * Hard filters:
   *
   * Availability (i.e. is available)
   * Location (passed by the user, if not return all)
   * Date from and to (or today if not passed)
   *
   * @return array
   */
  //todo: check this
  public function available()
  {
    $this->__adminLoginAsUser();
    try {
      $from = \Carbon\Carbon::createFromFormat('Y-m-d', \Input::get('from'));
      $to = \Carbon\Carbon::createFromFormat('Y-m-d', \Input::get('to'));
    } catch (\Exception $e) {
      $from = \Carbon\Carbon::now();
      $to = $from->copy();
    }

    $location = null;

    // Only set the location if postcode is passed and doesn't throw an exception
    if ($postcode = Input::get('postcode', false)) {
      try {
        $location = Location::findByPostcode(\Input::get('postcode'));
      } catch (Exception $e) {
      }
    }

    if (!$location) {
      $location = null;
    }

    // Get the results based on user input
    $results = AvailabilityRoom::findAvailable(
      $from,
      $to,
      $location,
      Input::get('radius', 25),
      Input::get('limit', 25),
      Input::get('offset', 0),
      null,
      true,
      Input::get('adults', 1)
    );

    return ['availability' => $results];
  }

  /**
   * Same as normal available call, but hotel specific
   * @return array
   */
  //todo: check this
  public function hotelAvailable($id)
  {
    $this->__adminLoginAsUser();
    try {
      /**
       * @var $hotel Hotel
       */
      $hotel = Hotel::findByUUIDorFail($id);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound('Hotel Not Found');
    }

    try {
      $from = \Carbon\Carbon::createFromFormat('Y-m-d', \Input::get('from'));
      $to = \Carbon\Carbon::createFromFormat('Y-m-d', \Input::get('to'));
    } catch (\Exception $e) {
      $from = \Carbon\Carbon::now();
      $to = $from->copy()->addDay();
    }

    try {
      $location = $hotel->address->location;
    } catch (Exception $e) {
      $location = null;
    }


    // Get the results based on user input
    $results = AvailabilityRoom::findAvailable(
      $from,
      $to,
      $location,
      Input::get('radius', 25),
      Input::get('limit', 25),
      Input::get('offset', 0),
      $hotel->uuid
    );

    return ['availability' => $results];
  }

  /**
   * Get the day by day details for each year/month
   *
   * @return array
   */
  //todo: check this:
  public function month($year, $month)
  {
    $this->__adminLoginAsUser();
    if (!$this->getAuthdUser() instanceof User) {
      // Todo: is this the right code? Does it say a user couldn't be matched to the current token?
      return Response::make('Unauthorized', 401);
    }

    $results = [];

    // Create carbon objects for start and end of month
    $from = \Carbon\Carbon::createFromDate($year, $month)->startOfMonth();
    $to = $from->copy()->endOfMonth();

    $availabilityRooms = AvailabilityRoom::getMine($this->getAuthdUser());
    $roomRates = RoomRate::getMine($this->getAuthdUser());
    $rooms = Room::getMine($this->getAuthdUser(), 9999);
    $diff = array_diff($availabilityRooms->lists('room_rate_id'), $roomRates->lists('id'));

    if ($diff) {
      //there's rates without any availability at all, create some availability
      foreach ($diff as $id) {
        if ($id) {
          $actualRoomRate = RoomRate::findOrFail($id);
          if ($actualRoomRate) {

            $diff = $from->diffInDays($to);
            $searchDate = $from->copy();
            $room = $actualRoomRate->room;

            for ($i = 0; $i <= $diff; $i++) {
              $availabilityRoom = AvailabilityRoom::findByDate($searchDate, $room, $actualRoomRate);
              if (!$availabilityRoom) {
                $availabilityRoom = new AvailabilityRoom();
                $availabilityRoom->room()->associate($room);
                $availabilityRoom->rates()->associate($actualRoomRate);
                $availabilityRoom->uuid = $availabilityRoom->generateUUID();
                $availabilityRoom->rate = 0;
                $availabilityRoom->closed = 0;
                $availabilityRoom->quantity = 0;
                $availabilityRoom->min_stay = 0;
                $availabilityRoom->max_stay = 0;
                $availabilityRoom->date = $searchDate;
                $availabilityRoom->save();
              }
              $searchDate->addDay();
            }
          }
        }
      }
    }

    //generate output
    //output format is:
    // $rooms[]['room_id']
    // $rooms[]['room_name']
    // $rooms[$roomitem]['rates']['rate_id']
    // $rooms[$roomitem]['rates'][]['cancellation_policies']<data>
    // $rooms[$room_id]['rates'][]['breakfasts']<data>
    // $rooms[$room_id]['rates'][]['boards']<data>
    // $rooms[$room_id]['rates'][]['dates']<data>
    foreach ($rooms as $room) {
      /**
       * @var $room Room
      */
      $resultRoom = [];
      $resultRoom['room_id'] = $room->uuid;
      $resultRoom['room_name'] = $room->name;
      $resultRoom['room_ota_id'] = $room->channelmanagercode;
      $resultRoom['hotel_id'] = $room->hotel->uuid;
      //break into rates
      foreach ($room->rates as $rate) {
        /**
         * @var $rate RoomRate
         */
        $resultRate = [];
        $resultRate['rate_id'] = $rate->uuid;
        $resultRate['rate_ota_id'] = $rate->channelmanagercode;
        foreach ($rate->cancellationpolicies as $canpol) {
          /**
           * @var $canpol PolicyCancellation
           */
          $resultRate['cancellation_policies'][] = [
            'id'          => $canpol->uuid,
            'period'      => $canpol->period,
            'charge'      => $canpol->charge,
            'charge_type' => $canpol->charge_type,
            'refundable'  => $canpol->refundable
          ];
        }
        foreach ($rate->boardoptions as $boardOption) {
          /**
           * @var $boardOption BoardOption
           */
          $resultRate['board_options'][] = [
            'id'         => $boardOption->uuid,
            'type'       => [
              'id'      => $boardOption->type->uuid,
              'name'    => $boardOption->type->name,
              'no_food' => $boardOption->type->no_food
            ],
            'adultprice' => $boardOption->adultprice,
            'childprice' => $boardOption->childprice,
          ];
        }
        foreach ($rate->breakfastoptions as $breakfastOption) {
          /**
           * @var $breakfastOption BreakfastOption
           */
          if (isset($breakfastOption->uuid) && isset($breakfastOption->type) && isset($breakfastOption->type->uuid)) {
            $resultRate['breakfast_options'][$breakfastOption->uuid] = [
              'id'         => $breakfastOption->uuid,
              'type'       => [
                'id'   => $breakfastOption->type->uuid,
                'name' => $breakfastOption->type->name
              ],
              'adultprice' => $breakfastOption->adultprice,
              'childprice' => $breakfastOption->childprice,
              'roomprice'  => $breakfastOption->childprice,
            ];
          }
        }
        //now to add availability
        for ($i = 1; $i <= $from->daysInMonth; $i++) {
          try {
            $today = \Carbon\Carbon::createFromDate($from->year, $month, $i)->setTime(0, 0, 0);
            $availabilityRoom = AvailabilityRoom::findByDate($today, $room, $rate);
            //create if not found
            if (!$availabilityRoom) {
              $availabilityRoom = new AvailabilityRoom();
              $availabilityRoom->room()->associate($room);
              $availabilityRoom->rates()->associate($rate);
              $availabilityRoom->uuid = $availabilityRoom->generateUUID();
              $availabilityRoom->rate = 0;
              $availabilityRoom->closed = 0;
              $availabilityRoom->quantity = 0;
              $availabilityRoom->min_stay = 0;
              $availabilityRoom->max_stay = 0;
              $availabilityRoom->date = $today;
              $availabilityRoom->save();
            }
            /**
             * @var $availabilityRoom AvailabilityRoom
             */
              $qtyAvailable = $availabilityRoom->quantity - $availabilityRoom->bookings->count();
            $resultDate = [
              'availability_room_id'  => $availabilityRoom->uuid,
              'rate'                  => (float)$availabilityRoom->rate,
              'single_occupancy_rate' => (float)$availabilityRoom->single_occupancy_rate,
              'quantity'              => (int) $availabilityRoom->quantity,
              'quantity_available'    => (int) $qtyAvailable,
              'closed'                => (bool)$availabilityRoom->closed,
              'no_check_in'           => (bool)$availabilityRoom->no_check_in,
              'no_check_out'          => (bool)$availabilityRoom->no_check_out,
              'min_stay'              => $availabilityRoom->min_stay,
              'max_stay'              => $availabilityRoom->max_stay,
              'sold_out'              => $availabilityRoom->isAvailable($today) ? false : true,
              'helper'                => [
                'state' => $availabilityRoom->closed ? 'closed' : ($qtyAvailable > 0 ? 'available' : 'sold-out')
              ]
            ];
            $resultRate['days'][$i] = $resultDate;
          } catch (\Exception $e) {
//            \Log::info('failed to show availability for :' . $today->toDateString(), [
//              'availabilityRoom' => $availabilityRoom
//            ]);
            throw $e;
          }
        }

        //add rate to room
        $resultRoom['rates'][] = $resultRate;
      }

      $results[] = $resultRoom;
    }


    return ['availability' => $results];


//    // Get all of the user auth'd rooms
//    //$rooms = AvailabilityRoom::getMine($this->getAuthdUser());
//    $roomRates = AvailabilityRoom::getMine($this->getAuthdUser());
//    //$actualRooms = Room::getMine($this->getAuthdUser());
//    $actualRates = RoomRate::getMine($this->getAuthdUser());
//
//    $diff = array_diff($actualRates->lists('id'), $roomRates->lists('room_rate_id'));
//
//    if ($diff) {
//      foreach ($diff as $id) {
//        $actualRoomRate = RoomRate::findOrFail($id);
//        if ($actualRoomRate) {
//          $availabilityRoom = new AvailabilityRoom();
//          $availabilityRoom->room()->associate($actualRoomRate->room);
//          $availabilityRoom->rates()->associate($actualRoomRate);
//          $availabilityRoom->uuid = $availabilityRoom->generateUUID();
//          $availabilityRoom->rate = $actualRoomRate->baseprice;
//          $availabilityRoom->reoccur = true;
//          $availabilityRoom->starts = '0000-00-00';
//          $availabilityRoom->ends = '0000-00-00';
//          $availabilityRoom->save();
//        }
//      }
//    }
//
//    $availabilityRooms = AvailabilityRoom::getMine($this->getAuthdUser(), 9999);
//
//    $rooms = Room::getMine($this->getAuthdUser(), 9999);
//
//    // Iterate though and set accordingly
//    foreach ($rooms as $room) {
//      /**
//       * @var $room Room
//       */
//      foreach ($room->rates as $rate) {
//        /**
//         * @var $rate RoomRate
//         */
//        foreach ($rate->availability as $availabilityRoom) {
//          //$rid = $room->uuid;
//          $rid = $availabilityRoom->uuid;
//          /**
//           * @var $availabilityRoom AvailabilityRoom
//           */
//          // Loop through the amount of days each month has
//          for ($i = 1; $i <= $from->daysInMonth; $i++) {
//
//            // Create todays object
//            $today = \Carbon\Carbon::createFromDate($from->year, $month, $i)->setTime(0, 0, 0);
//
//            // Starts is always set
//            if ($availabilityRoom->ends == '0000-00-00' || is_null($availabilityRoom->ends) || $availabilityRoom->ends == '' || empty($availabilityRoom->ends)) {
//              $starts = \Carbon\Carbon::createFromFormat('Y-m-d', $availabilityRoom->starts)->setTime(0, 0, 0);
//            } else {
//              $starts = $availabilityRoom->starts;
//            }
//
//            // Rooms might be blank or 0000-00-00 - check for this
//            // Otherwise create an object when the room offer ends
//            if ($availabilityRoom->ends == '0000-00-00' || is_null($availabilityRoom->ends) || $availabilityRoom->ends == '' || empty($availabilityRoom->ends)) {
//              $ends = $to->copy()->setTime(0, 0, 0);
//            } else {
//              $ends = $availabilityRoom->ends;
//            }
//
//            // Check $today is available during $starts and $ends
//            // Records can override each other. Ordered by id ASC.
//            $results[$rid]['room_id'] = $room->uuid;
//            $results[$rid]['availability_room_id'] = $availabilityRoom->uuid;
//            $results[$rid]['hotel_id'] = $room->hotel->uuid;
//            $results[$rid]['room_name'] = $room->name;
//            $results[$rid]['rates'][$rate->uuid]['rate_id'] = $rate->uuid;
//            foreach ($rate->cancellationpolicies as $canpol) {
//              /**
//               * @var $canpol PolicyCancellation
//               */
//              $results[$rid]['rates'][$rate->uuid]['cancellation_policies'][$canpol->uuid] = [
//                'id'          => $canpol->uuid,
//                'period'      => $canpol->period,
//                'charge'      => $canpol->charge,
//                'charge_type' => $canpol->charge_type,
//                'refundable'  => $canpol->refundable
//              ];
//            }
//            foreach ($rate->boardoptions as $boardOption) {
//              /**
//               * @var $boardOption BoardOption
//               */
//              $results[$rid]['rates'][$rate->uuid]['board_options'][$boardOption->uuid] = [
//                'id'         => $boardOption->uuid,
//                'type'       => [
//                  'id'      => $boardOption->type->uuid,
//                  'name'    => $boardOption->type->name,
//                  'no_food' => $boardOption->type->no_food
//                ],
//                'adultprice' => $boardOption->adultprice,
//                'childprice' => $boardOption->childprice,
//              ];
//            }
//            foreach ($rate->breakfastoptions as $breakfastOption) {
//              /**
//               * @var $breakfastOption BreakfastOption
//               */
//              if (isset($breakfastOption->uuid) && isset($breakfastOption->type) && isset($breakfastOption->type->uuid)) {
//                $results[$rid]['rates'][$rate->uuid]['breakfast_options'][$breakfastOption->uuid] = [
//                  'id'         => $breakfastOption->uuid,
//                  'type'       => [
//                    'id'   => $breakfastOption->type->uuid,
//                    'name' => $breakfastOption->type->name
//                  ],
//                  'adultprice' => $breakfastOption->adultprice,
//                  'childprice' => $breakfastOption->childprice,
//                  'roomprice'  => $breakfastOption->childprice,
//                ];
//              }
//            }
//            $results[$rid]['rates'][$rate->uuid]['room_id'] = $room->uuid;
//            $results[$rid]['rates'][$rate->uuid]['room_name'] = $room->name;
//            $results[$rid]['rates'][$rate->uuid]['rate_id'] = $rate->uuid;
//            $results[$rid]['rates'][$rate->uuid]['availability_room_id'] = $availabilityRoom->uuid;
//            if ($today->between($starts, $ends)) {
//              //todo: maybe move these up?
////                    $results[$rid][$rate->uuid][$availabilityRoom->uuid]['room_id'] = $room->uuid;
////                    $results[$rid][$rate->uuid][$availabilityRoom->uuid]['room_name'] = $room->name;
////                    $results[$rid][$rate->uuid][$availabilityRoom->uuid]['rate_id'] = $rate->uuid;
////                    $results[$rid][$rate->uuid][$availabilityRoom->uuid]['availability_room_id'] = $availabilityRoom->uuid;
//              $results[$rid]['rates'][$rate->uuid]['days'][$i] = [
//                'availability_room_id'  => $availabilityRoom->uuid,
//                'rate'                  => (float)$availabilityRoom->rate,
//                'single_occupancy_rate' => (float)$availabilityRoom->single_occupancy_rate,
//                'quantity'              => (int)$availabilityRoom->quantity,
//                'closed'                => (bool)$availabilityRoom->closed,
//                'minimum_stay'          => (int)$availabilityRoom->minimum_stay,
//                'no_check_in'           => (bool)$availabilityRoom->no_check_in,
//                'no_check_out'          => (bool)$availabilityRoom->no_check_out,
//                'sold_out'              => $availabilityRoom->isAvailable($today) ? false : true,
//                'helper'                => [
//                  'state' => ($availabilityRoom->isAvailable($today) ? ($availabilityRoom->closed ? 'closed' : 'available') : 'sold-out')
//                ]
//              ];
//            } else {
//              // Assume closed that day
//              if (!isset($results[$rid]['rates'][$rate->uuid]['days'][$i])) {
////                      $results[$rid][$rate->uuid][$availabilityRoom->uuid]['room_id'] = $room->uuid;
////                      $results[$rid][$rate->uuid][$availabilityRoom->uuid]['room_name'] = $room->name;
////                      $results[$rid][$rate->uuid][$availabilityRoom->uuid]['rate_id'] = $rate->uuid;
////                      $results[$rid][$rate->uuid][$availabilityRoom->uuid]['availability_room_id'] = $availabilityRoom->uuid;
//                $results[$rid]['rates'][$rate->uuid]['days'][$i] = array(
//                  'availability_room_id'  => $availabilityRoom->uuid,
//                  'rate'                  => 0,
//                  'single_occupancy_rate' => 0,
//                  'quantity'              => 0,
//                  'closed'                => true,
//                  'minimum_stay'          => 0,
//                  'no_check_in'           => false,
//                  'no_check_out'          => false,
//                  'sold_out'              => false,
//                  'helper'                => [
//                    'state' => 'closed'
//                  ]
//                );
//              }
//            }
//          } //end for
//        } // end rate->availabilityroom
//      } // end rates
//    } //end room
//
//    return ['availability' => $results];
  }

  //todo: update this
  public function cloneAvailability($id)
  {
    $this->__adminLoginAsUser();
    try {
      $actualRoom = Room::findByUUIDorFail($id);

      try {
        $room = AvailabilityRoom::query()
          ->where('starts', \Input::get('starts'))
          ->where('ends', \Input::get('ends'))
          ->where('room_id', $actualRoom->id)
          ->firstOrFail();
      } catch (\Exception $e) {
        $room = AvailabilityRoom::query()->where('room_id', $actualRoom->id)->firstOrFail();
        $room = $room->replicate();
        $room->uuid = (string)$room->generateUUID();
      }

      // Only prefill these when cloning
      $room->fill(\Input::only([
        'starts',
        'ends',
        'closed',
        'max_stay',
        'min_stay',
        'no_check_in',
        'no_check_out'
      ]));

      if (!$room->save()) {
        if ($room->hasErrorUnauthorised()) {
          return $this->response->errorUnauthorized($room->getErrors());
        }
        return $this->response->errorBadRequest($room->getErrors());
      }

      $this->handleRelations($room);

      return $this->response->array(['availability_room_id' => $room->uuid]);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound('Availability Room Not Found');
    }
  }

  /**
   * Find today's available rooms
   *
   * @return array
   */
  //todo: check this
  public function today()
  {
    $this->__adminLoginAsUser();
    $rooms = AvailabilityRoom::findToday();

    return CoreJsonResponse::respond($rooms, $rooms->get(), $this->getLimit());
  }

  /**
   * Link 2 room availabilities together
   *
   * @return \Illuminate\Http\Response
   */
  //todo: check this
  public function link()
  {
    $this->__adminLoginAsUser();
    $a = Input::get('a', false);
    $b = Input::get('b', false);

    if ((!$a || !$b) || $a == $b) {
      return $this->response->errorBadRequest('Unable to link the same room availability');
    }

    try {
      $roomA = AvailabilityRoom::findByUUIDorFail($a);
      $roomB = AvailabilityRoom::findByUUIDorFail($b);

      \DB::table('availability__rooms_linked')->insert([
        'room_id_a' => $roomA->id,
        'room_id_b' => $roomB->id
      ]);

    } catch (\Exception $e) {
      return $this->response->errorBadRequest($e->getMessage());
    }

    return $this->response->created();
  }

  /**
   * Delete linked availability
   *
   * @return mixed
   */
  //todo: check this
  public function deleteLink()
  {
    $this->__adminLoginAsUser();
    try {
      $a = AvailabilityRoom::findByUUIDorFail(\Input::get('a'));
      $b = AvailabilityRoom::findByUUIDorFail(\Input::get('b'));

      \DB::table('availability__rooms_linked')
        ->where('room_id_a', $a->id)
        ->where('room_id_b', $b->id)
        ->delete();

      return ['deleted' => true];
    } catch (\Exception $e) {
    }

    return ['deleted' => false];
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response|\Illuminate\Http\Response
   */
  //todo: update this
  public function create()
  {
//    \Log::info('create availabiliity data:',Input::all());
    $this->__adminLoginAsUser();

    try {
      $starts = Input::get('starts', false);
      $ends = Input::get('ends', false);
      // If the user has specified a start/end end, turn off occurrence
      if ($starts !== false && $ends !== false) {
        $starts = \Carbon\Carbon::createFromFormat('Y-m-d', $starts);
        $ends = \Carbon\Carbon::createFromFormat('Y-m-d', $ends);
      } // On available for that day
      elseif ($starts !== false && $ends === false) {
        $starts = \Carbon\Carbon::createFromFormat('Y-m-d', $starts);
        $ends = $starts->copy()->addDay();
      } elseif ($starts === false && $ends !== false) {
        $ends = \Carbon\Carbon::createFromFormat('Y-m-d', $ends);
        $starts = \Carbon\Carbon::now();
      } else {
        $starts = \Carbon\Carbon::today();
        $ends = $starts->copy();
      }

      $diff = $starts->diffInDays($ends);
      if ($diff > 100) {
        return $this->response->errorBadRequest('Date range cannot be more than 100 days');
      }
      $searchDate = $starts->copy();
      $room = Room::findByUUIDorFail(Input::get('room'));
      $roomRate = RoomRate::findByUUIDorFail(Input::get('room_rate'));

      $updatedAvailabilityRooms = [];

      for ($i = 0; $i <= $diff; $i++) {
        $availabilityRoom = AvailabilityRoom::findByDate($searchDate, $room, $roomRate);
//        \Log::info('found ava room to update:',[$availabilityRoom]);
        if (!$availabilityRoom) {
          $availabilityRoom = new AvailabilityRoom();
          $availabilityRoom->uuid = (string)$room->generateUUID();
        }
        //update the fields manually as we're probably doing bulk update
        $rate = Input::get('rate',isset($availabilityRoom->rate) && !empty($availabilityRoom->rate) ? $availabilityRoom->rate : 0);
        if ($rate === "") {
          $rate = $availabilityRoom->rate;
        }
//        \Log::info('rate is:',[$rate]);
        $availabilityRoom->date = $searchDate;
        $availabilityRoom->rate = $rate;
        $availabilityRoom->single_occupancy_price = Input::get('single_occupancy_price',$availabilityRoom->single_occupancy_price?: 0);
        $quantity = Input::get('quantity',$availabilityRoom->quantity);
        if ($quantity === "") {
          $quantity = $availabilityRoom->quantity;
        }
        $availabilityRoom->quantity = $quantity;
//        \Log::info('quantity is:',[$availabilityRoom->quantity, Input::get('quantity')]);
        $availabilityRoom->closed = Input::get('closed',$availabilityRoom->closed);
        $availabilityRoom->no_check_in = Input::get('no_check_in',$availabilityRoom->no_check_in);
        $availabilityRoom->no_check_out = Input::get('no_check_out',$availabilityRoom->no_check_out);
        $availabilityRoom->min_stay = Input::get('min_stay',$availabilityRoom->min_stay);
        $availabilityRoom->max_stay = Input::get('max_stay',$availabilityRoom->max_stay);

        $this->handleRelations($availabilityRoom);
//        \Log::info('ava room to save:',[$availabilityRoom]);
        if (!$availabilityRoom->save()) {
//          \Log::error('availability error',[$availabilityRoom->getErrors()]);
          return $this->response->errorBadRequest($availabilityRoom->getErrors());
        }
        $updatedAvailabilityRooms[] = $availabilityRoom->uuid;
        $searchDate->addDay();
      }

      return $this->response->array(['availability_room_id_list' => $updatedAvailabilityRooms]);
    } catch (ModelNotFoundException $e) {
//      \Log::error('availability error',[$e->getMessage(),$e->getTrace()]);
      return $this->response->errorBadRequest($e->getMessage());
    } catch (\Exception $e) {
//      \Log::error('availability error',[$e->getMessage(),$e->getTrace()]);
      //todo: update this
      return $this->response->errorBadRequest($e->getMessage());
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  //todo: check this
  public function show($id)
  {
    $this->__adminLoginAsUser();
    try {
      $room = AvailabilityRoom::findByUUIDorFail($id);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound('Availability Room Not Found');
    }

    return $this->response->array($room->toArray());
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  //todo: update this
  public function update($id)
  {
    try {
      $room = AvailabilityRoom::findByUUIDorFail($id);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound('Availability Room Not Found');
    }

    try {
      if ($rate = \Input::get('rate')) {
        if (!is_null($rate) && $rate != '' && is_numeric($rate)) {
          $room->rate = $rate;
        }
      }

      if ($quantity = \Input::get('quantity', $room->quantity)) {
        if (!is_null($quantity) && $quantity !== '' && is_numeric($quantity)) {
          $booked_qty = $room->bookings->count();
          $room->quantity = $booked_qty+$quantity;
        } else if($quantity == 0 || is_null($quantity) || $quantity === '0') {
          $room->quantity = $room->bookings->count();
        }
      }

      if ($date = \Input::get('date')) {
        if (!is_null($date) && $date != '') {
          try {
            $dateFormatted = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date);
          } catch (Exception $e) {
            $dateFormatted = \Carbon\Carbon::createFromFormat('Y-m-d', $date);
          }
          $room->date = $dateFormatted->format('Y-m-d');
        }
      }

      $room->closed = Input::get('closed',$room->closed);
      $room->no_check_in = Input::get('no_check_in',$room->no_check_in);
      $room->no_check_out = Input::get('no_check_out',$room->no_check_out);
      $room->min_stay = Input::get('min_stay',$room->min_stay);
      $room->max_stay = Input::get('max_stay',$room->max_stay);

      $this->handleRelations($room);

      if (!$room->save()) {
        if ($room->hasErrorUnauthorised()) {
          return $this->response->errorUnauthorized($room->getErrors());
        }
        return $this->response->errorBadRequest($room->getErrors());
      }

      return $this->response->array(['availability_room_id' => $room->uuid]);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorBadRequest($e->getMessage());
    }
  }

  /**
   * @param AvailabilityRoom $availabilityRoom
   * @throws Exception
   */
  //todo: check this
  protected function handleRelations(AvailabilityRoom $availabilityRoom)
  {
    $this->__adminLoginAsUser();
    if ($room = Input::get('room')) {
      $this->associateRoom($availabilityRoom, $room);
    }
    if ($roomRate = Input::get('room_rate')) {
      $this->associateRoomRate($availabilityRoom, $roomRate);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  //todo: check this
  public function destroy($id)
  {
    $this->__adminLoginAsUser();
    try {
      $room = AvailabilityRoom::findByUUIDorFail($id);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound('Availability Room Not Found');
    }

    $room->delete();
  }

  /**
   *
   * @param AvailabilityRoom $availabilityRoom
   * @param $uuid
   * @throws Exception
   * @throws Illuminate\Database\Eloquent\ModelNotFoundException
   * @return bool
   */
  protected function associateRoom(AvailabilityRoom $availabilityRoom, $uuid)
  {
    $this->__adminLoginAsUser();
    try {
      $room = Room::findByUUIDorFail($uuid);
      $availabilityRoom->room()->associate($room);
    } catch (Exception $e) {
      throw $e;
    }
  }

  /**
   *
   * @param AvailabilityRoom $availabilityRoom
   * @param $uuid
   * @throws Exception
   * @throws Illuminate\Database\Eloquent\ModelNotFoundException
   * @return bool
   */
  protected function associateRoomRate(AvailabilityRoom $availabilityRoom, $uuid)
  {
    $this->__adminLoginAsUser();
    try {
      $roomRate = RoomRate::findByUUIDorFail($uuid);
      $availabilityRoom->rates()->associate($roomRate);
    } catch (Exception $e) {
      throw $e;
    }
  }
}