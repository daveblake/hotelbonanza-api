<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RoomTypeController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Find out which are the true parents by assume those that aren't sub types
        $parents = \DB::table('rooms__types_type_subtype')->distinct()->lists('sub_type_id');

        $types = RoomType::query()
            ->whereNotIn('id', $parents)
            ->orderBy('order','ASC')
            ->orderBy('name','ASC')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return CoreJsonResponse::respond(RoomType::whereNotIn('id', $parents), $types, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(RoomType::fields());

        $roomType = new RoomType();
        $roomType->fill($input);
        $roomType->uuid = (string) $roomType->generateUUID();

        if(!$roomType->save())
        {
            return $this->response->errorBadRequest($roomType->getErrors());
        }

        try
        {
            $this->handleAssociations($roomType);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $this->response->array(['roomtype_id'=> $roomType->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $roomType = RoomType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Type Not Found');
        }

        return $this->response->array($roomType->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $roomType = RoomType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Type Not Found');
        }

        $roomType->name         = Input::get('name', $roomType->name);
        $roomType->bedrooms     = Input::get('bedrooms', $roomType->bedrooms);
        $roomType->lounges      = Input::get('lounges', $roomType->lounges);
        $roomType->bathrooms    = Input::get('bathrooms', $roomType->bathrooms);
        $roomType->order        = Input::get('order', $roomType->order);

        if(!$roomType->save())
        {
            if ($roomType->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($roomType->getErrors());
            }
            return $this->response->errorBadRequest($roomType->getErrors());
        }

        try
        {
            $this->handleAssociations($roomType);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $this->response->array(['roomtype_id'=>$roomType->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $roomType = RoomType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Room Type Not Found');
        }

        $roomType->delete();
    }

    /**
     * Handle roomType associations
     *
     * @param RoomType $roomType
     * @return \Illuminate\Http\Response
     */
    protected function handleAssociations(RoomType &$roomType)
    {
        if($subtype = Input::get('subtype'))
        {
            if (is_array($subtype)) {
                // Don't allow room types to be children of themselves
                foreach ($subtype as $subtypeUuid) {
                    if ($subtypeUuid != $roomType->uuid && !$roomType->subtypes->contains($subtypeUuid)) {
                        $this->associateSubtype($roomType, $subtypeUuid);
                    }
                }
            } else {
                $this->associateSubtype($roomType, $subtype);
            }
        }
    }

    protected function associateSubtype(RoomType &$roomType, $uuid)
    {
        try {
            $roomType->subtypes()->attach(RoomType::findByUUIDorFail($uuid));
        } catch (\Illuminate\Database\QueryException $e) {}
    }

}