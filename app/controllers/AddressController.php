<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class AddressController extends \BaseController
{
    protected static $relations = [
      'location',
      'district',
      'country'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $addresses = Address::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return CoreJsonResponse::respond(Address::query(), $addresses, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(Address::fields());

        $address = new Address();
        $address->fill($input);
        $address->uuid = (string) $address->generateUUID();
        $address->getAndCacheLocation();

        if(!$address->save())
        {
            return $this->response->errorBadRequest($address->getErrors());
        }

        if($country = \Input::get('country'))
        {
            try
            {
                $this->associateCountry($address, $country);
            }
            catch (ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }
        }

        if($district = \Input::get('adminArea'))
        {
            try
            {
                $this->associateDistrict($address, $district);
            }
            catch (ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }
        }

        if(!$address->save())
        {
            return $this->response->errorBadRequest($address->getErrors());
        }

        return $this->response->array(['address_id' => $address->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $address = Address::findByUUIDorFail($id, static::$relations);

            if(!$address->location) {
                $address->getAndCacheLocation();
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Address Not Found');
        }

        return $this->response->array($address->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            /**
             * @var $address Address
             */
            $address = Address::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Address Not Found');
        }

        // Map fields from input, otherwise use current value
        foreach(Address::fields() as $field) {
            $address->{$field} = Input::get($field, $address->{$field});
        }

        $address->getAndCacheLocation();

        if($country = \Input::get('country'))
        {
            \Log::info('country:'.$country);
            try
            {
                $this->associateCountry($address, $country);
            }
            catch (ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }
        }

        if($district = \Input::get('adminArea'))
        {
            try
            {
                $this->associateDistrict($address, $district);
            }
            catch (ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }
        }

        if(!$address->save())
        {

            if($address->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($address->getErrors());
            }

            return $this->response->errorBadRequest($address->getErrors());
        }

        return $this->response->array(['address_id' => $address->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $address = Address::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Address Not Found');
        }

        $address->delete();
    }

    /**
     * Associate a country with an address
     *
     * @param Address $address
     * @param $uuid
     */
    public function associateCountry(Address &$address, $uuid)
    {
        $address->country()->associate(Country::findByUUIDorFail($uuid));
    }

    /**
     * Associate a district / borough with an address
     *
     * @param Address $address
     * @param $uuid
     */
    public function associateDistrict(Address &$address, $uuid)
    {
        $address->district()->associate(AddressDistrict::findByUUIDorFail($uuid));
    }

}