<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class BoardTypeController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = BoardType::all()->count();

        $types = BoardType::with(static::$relations)
            ->orderBy('name','asc')
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'board__types' => $types,
            'total' => (string) $count,
            'paginated' => (string) $types->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(BoardType::fields());

        $type = new BoardType();
        $type->fill($input);
        $type->uuid = (string) $type->generateUUID();


        if(!$type->save())
        {
            return $this->response->errorBadRequest($type->getErrors());
        }

        return $this->response->array(['board_type_id' => $type->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $type = BoardType::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Board Type Not Found');
        }

        return $this->response->array($type->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $type = BoardType::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Board Type Not Found');
        }

        $type->name= \Input::get('name', $type->name);
        $type->no_food = \Input::get('no_food', $type->no_food);

        if(!$type->save())
        {
            if ($type->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($type->getErrors());
            }
            return $this->response->errorBadRequest($type->getErrors());
        }

        return $this->response->array(['board_type_id' => $type->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $type = BoardType::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Board Type Not Found');
        }

        $type->delete();
    }
}