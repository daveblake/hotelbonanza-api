<?php

/**
 * Created by PhpStorm.
 * User: mgane
 * Date: 08/01/16
 * Time: 11:38
 */
class BookingPaymentController extends \BaseController
{
    private static $relations = [
        'booking'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $payments = BookingPayment::query()
            ->with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit());

        $payments = $payments->get();

        return CoreJsonResponse::respond(BookingPayment::query(), $payments, $this->getLimit());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(BookingPayment::fields());

        try
        {
            $payment = new BookingPayment();
            $payment->fill($input);

            $payment->card_number_last_four = substr($payment->card_number,-4);

            $payment->uuid = (string) $payment->generateUUID();

            // Associate the booking
            $this->associateBooking($payment, Input::get('booking', Input::get('booking_id', null)));

            // do some basic encryption
            /**
             * @var $hotel Hotel
             */
            $hotel = $payment->booking->hotel;

            \Crypt::setKey(substr(Config::get('app.key').$hotel->id,-32));
            //\Crypt::setCipher('AES');

            $payment->card_number = \Crypt::encrypt($payment->card_number);
            $payment->cvv = \Crypt::encrypt($payment->cvv);

            \Crypt::setKey(Config::get('app.key'));

            if(!$payment->save())
            {
                return $this->response->errorBadRequest($payment->getErrors());
            }

            // change the owner to the hotel for reading
            if($owners = Owner::findOwnersOfResource($payment)) {
                // owners exist for the resource.
            } else {
                // No users found for this resource.
                // Grant the ownership to the hotel for reading
                Owner::create([
                    'uuid' => $payment->uuid,
                    'model' => get_class($payment),
                    'user_id' => $hotel->user->id
                ]);
            }

            $date = \Carbon\Carbon::now()->addWeeks(2);

            Queue::later($date, 'Queue\ClearBookingPayments', array('uuid' => $payment->uuid));

            return $this->response->array(['booking_payment_id'=>$payment->uuid]);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $payment = BookingPayment::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Booking Payment Not Found');
        }

        //try to get user permissions
        try {
            $User = User::findOrFail(\Authorizer::getResourceOwnerId());
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->response->errorNotFound('User Not Found');
        }
        try {
            /**
             * @var $Hotel Hotel
             */
            $Hotel = Hotel::where('user_id', '=', $User->id)->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $Hotel = false;
        }
        // if current hotel
        if ($Hotel && $Hotel->id == $payment->booking->hotel->id) {
            \Crypt::setKey(substr(Config::get('app.key').$Hotel->id,-32));
            //\Crypt::setCipher('AES');

            $payment->card_number = \Crypt::decrypt($payment->card_number);
            $payment->cvv = \Crypt::decrypt($payment->cvv);

            \Crypt::setKey(Config::get('app.key'));
        }

        return $this->response->array($payment->toArray());
    }

    // Todo: do we need an update?
    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $payment = BookingPayment::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Booking Payment Not Found');
        }

        $input = Input::only(BookingPayment::fields());

        try
        {
            $payment->fill($input);

            $payment->card_number_last_four = substr($payment->card_number,-4);

            if(!$payment->save())
            {
                if ($payment->hasErrorUnauthorised()) {
                    return $this->response->errorUnauthorized($payment->getErrors());
                }
                return $this->response->errorBadRequest($payment->getErrors());
            }

            // Todo: confirm this. Shouldn't need to alter the assigned booking
            //$this->handleRelations($payment, Input::get('booking', Input::get('booking_id', null)));

            return $this->response->array(['booking_payment_id'=>$payment->uuid]);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $payment = BookingPayment::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Booking Payment Not Found');
        }

        $payment->delete();
    }
    /**
     * Attempt to associate the Booking with the current model
     *
     * @param BookingPayment $payment
     * @param $uuid
     * @throws Exception
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     * @return bool
     */
    protected function associateBooking(BookingPayment $payment, $uuid)
    {
        try {
            // Find and associate the bed_type
            $booking = Booking::findByUUIDorFail($uuid);
            $payment->booking()->associate($booking);
        } catch(Exception $e) {
            throw $e;
        }
    }

}