<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class BoardController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = ['type'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = BoardOption::all()->count();

        $options = BoardOption::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();

        return [
            'board__options' => $options,
            'total' => (string) $count,
            'paginated' => (string) $options->count(),
            'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(BoardOption::fields());

        $breakfast = new BoardOption();
        $breakfast->fill($input);
        $breakfast->uuid = (string) $breakfast->generateUUID();

        // Attempt to associate the area with the hotel
        try
        {
            if($type = Input::get('type')) {
                $this->associateType($breakfast, $type);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$breakfast->save())
        {
            return $this->response->errorBadRequest($breakfast->getErrors());
        }

        return $this->response->array(['board_option_id' => $breakfast->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $breakfast = BoardOption::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Board Option Not Found');
        }

        return $this->response->array($breakfast->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $breakfast = BoardOption::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Board Option Not Found');
        }

        $breakfast->adultprice = \Input::get('adultprice', $breakfast->adultprice);
        $breakfast->childprice = \Input::get('childprice', $breakfast->childprice);

        // Attempt to associate the area with the hotel
        try
        {
            if($type = Input::get('type')) {
                $this->associateType($breakfast, $type);
            }
        }
        catch(ModelNotFoundException $e)
        {
            //might be the wrong error here?
            return $this->response->errorNotFound($e->getMessage());
        }

        if(!$breakfast->save())
        {
            if ($breakfast->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($breakfast->getErrors());
            }
            return $this->response->errorBadRequest($breakfast->getErrors());
        }

        return $this->response->array(['board_option_id' => $breakfast->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $breakfast = BoardOption::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Board Option Not Found');
        }

        $breakfast->delete();
    }

    /**
    /**
     * Associate a type with the current model
     *
     * @param \BreakfastOption $breakfast
     * @param $type
     */
    protected function associateType(BoardOption &$breakfast, $type)
    {
        $breakfast->type()->associate(BoardType::findByUUIDorFail($type));
    }
}