<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class RateLinkController extends \BaseController
{
    protected static $relations = ['rate'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return RateLink::with(static::$relations)
            ->skip($this->getOffset())
            ->take($this->getLimit())
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(RateLink::fields());

        try
        {
            $ratelink = new RateLink();
            $ratelink->fill($input);
            $ratelink->uuid = (string) $ratelink->generateUUID();

            try
            {
                $this->handleRelations($ratelink);
            }
            catch(ModelNotFoundException $e)
            {
                return $this->response->errorNotFound($e->getMessage());
            }

            if(!$ratelink->save())
            {
                return $this->response->errorBadRequest($ratelink->getErrors());
            }

            return $this->response->array(['ratelink_id'=>$ratelink->uuid]);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $ratelink = RateLink::findByUUIDorFail($id, static::$relations);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Rate Link Not Found');
        }

        return $this->response->array($ratelink->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $ratelink = RateLink::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Rate Link Not Found');
        }

        $ratelink->type = \Input::get('type', $ratelink->type);
        $ratelink->amount = \Input::get('amount', $ratelink->amount);

        if(!$ratelink->save())
        {
            if ($ratelink->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($ratelink->getErrors());
            }
            return $this->response->errorBadRequest($ratelink->getErrors());
        }

        try
        {
            $this->handleRelations($ratelink);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound($e->getMessage());
        }

        return $this->response->array(['ratelink_id'=> $ratelink->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $ratelink = RateLink::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Rate Link Not Found');
        }

        $ratelink->delete();
    }

    /**
     * Handle relations specific to RateLink model
     *
     * @param  RateLink $ratelink
     *
     * @return void
     */
    protected function handleRelations(&$ratelink)
    {
        if($rate = \Input::get('rate')) {
            $ratelink->rate()->associate(RoomRate::findByUUIDorFail($rate));
        }
    }
}