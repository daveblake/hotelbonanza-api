<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ChannelManagerController extends \BaseController
{
    /**
     * @var array
     */
    protected static $relations = [];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $count = ChannelManager::all()->count();

        $channelManagers = ChannelManager::orderBy('order','asc')->orderBy('name','asc')
          ->skip($this->getOffset())
          ->take($this->getLimit())
          ->get();

        return [
          'channel_managers' => $channelManagers,
          'total' => (string) $count,
          'paginated' => (string) $channelManagers->count(),
          'pages' => (string) ceil(((float)$count / (float)$this->getLimit()))
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::only(ChannelManager::fields());

        $channelmanager = new ChannelManager();
        $channelmanager->fill($input);
        $channelmanager->uuid = (string) $channelmanager->generateUUID();

        if(!$channelmanager->save())
        {
            return $this->response->errorBadRequest($channelmanager->getErrors());
        }

        return $this->response->array(['channelmanager_id' => $channelmanager->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try
        {
            $channelmanager = ChannelManager::findByUUIDorFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Channel Manager Not Found');
        }

        return $this->response->array($channelmanager->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try
        {
            $channelmanager = ChannelManager::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Channel Manager Not Found');
        }

        $channelmanager->name = \Input::get('name', $channelmanager->name);

        if(!$channelmanager->save())
        {
            if ($channelmanager->hasErrorUnauthorised()) {
                return $this->response->errorUnauthorized($channelmanager->getErrors());
            }
            return $this->response->errorBadRequest($channelmanager->getErrors());
        }

        return $this->response->array(['channelmanager_id' => $channelmanager->uuid]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $channelmanager = ChannelManager::findByUUIDorFail($id);
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return $this->response->errorNotFound('Channel Manager Not Found');
        }

        $channelmanager->delete();
    }
}