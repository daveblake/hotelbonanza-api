<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */
    'default' => 'mysql',

    'connections' => array(
        'sqlite' => [
            'driver' => 'sqlite',
            'database' => storage_path('database/production.sqlite'),
            'prefix' => '',
        ],
        'mysql' => array(
            'driver' => 'mysql',
            'host'      => 'localhost',
            'database'  => 'hotelb_db',
            'username'  => 'hotelbonanza',
            'password'  => 'iGr%0r99',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''/*,
            'options' => [
                PDO::MYSQL_ATTR_SSL_KEY    => storage_path('../config/HB/database/client-key.pem'),
                PDO::MYSQL_ATTR_SSL_CERT    => storage_path('../config/HB/database/client-cert.pem'),
                PDO::MYSQL_ATTR_SSL_CA    => storage_path('../config/HB/database/server-ca.pem')
            ]*/
        ),/*
        'mysql' => array(
            'driver' => 'mysql',
            'host'      => '192.168.1.21',
            'database'  => 'hotelb_api',
            'username'  => 'root',
            'password'  => 'nimzoted',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ),*/

        'pgsql' => array(
            'driver' => 'pgsql',
            'host' => 'localhost',
            'database' => 'homestead',
            'username' => 'homestead',
            'password' => 'secret',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
        ),

    ),

);
