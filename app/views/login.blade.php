<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>API Login</title>

  <!-- Bootstrap -->
  <link rel="stylesheet"
        href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

  <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
  <link
      href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600'
      rel='stylesheet' type='text/css'>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script
      src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style type="text/css" media="screen">
    body {
      padding-top: 100px;
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Hotelbonanza Api</a>
    </div>

    <!--/.nav-collapse -->
  </div>
</nav>

<div class="container">

  <div class="panel panel-primary clearfix">
    <div class="panel-heading">
      <h3 class="panel-title">Login to get started</h3>
    </div>
    <div class="panel-body">

      <form id="login" action="/login" method="post" class="form-horizontal">
        <input type="hidden" name="_token" value="<?= csrf_token(); ?>">

        <div class="form-group">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
              <input type="text" class="form-control" id="username"
                     placeholder="Username" name="username">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <input type="password" class="form-control" id="password"
                     placeholder="Password" name="password">
            </div>
          </div>
        </div>
        <?php if ( Session::get('message') ): ?>
        <div class="errorbox">
          <span class="error" style="color:red">{{{Session::get('message')}}}</span>
        </div>
        <?php endif; ?>

        <div class="pull-right">
          <input class="btn btn-lg btn-primary" type="submit" value="Login"/>
        </div>
      </form>
    </div>

  </div>

</div>
<!-- /.container -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
    src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>
