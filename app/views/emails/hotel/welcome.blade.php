<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Congratulations!</h2>

<p>You have just registered {{ $hotel->name }}! We'll send you another email shortly once your hotel application has been approved.</p>
</body>
</html>
