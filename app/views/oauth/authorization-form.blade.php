<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>API Login</title>

  <!-- Bootstrap -->
  <link rel="stylesheet"
        href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

  <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
  <link
      href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600'
      rel='stylesheet' type='text/css'>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script
      src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style type="text/css" media="screen">
    body {
      padding-top: 100px;
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Hotelbonanza Api</a>
    </div>

    <!--/.nav-collapse -->
  </div>
</nav>

<div class="container">


  <div class="panel panel-primary clearfix">
    <div class="panel-heading">
      <h3 class="panel-title">Approve API Access</h3>
    </div>
    <div class="panel-body">

      <div class="row">
        <div class="col-sm-2">
          {{ Form::open(array('url'=> URL::route('post oauth authorize', $params))) }}
          {{ Form::hidden('approve',1) }}
          {{ Form::submit('approve', array('class'=>'btn btn-large btn-success')) }}
          {{ Form::close() }}
        </div>
        <div class="col-sm-2">
          {{ Form::open(array('url'=> URL::route('post oauth authorize', $params))) }}
          {{ Form::hidden('deny',1) }}
          {{ Form::submit('deny', array('class'=>'btn btn-large btn-danger')) }}
          {{ Form::close() }}
        </div>
      </div>





    </div>

  </div>

</div>
<!-- /.container -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
    src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>
