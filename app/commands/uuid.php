<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class uuid extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'uuid:generate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a UUID.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info(\Webpatser\Uuid\Uuid::generate(BaseModel::UUID_VERSION));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
