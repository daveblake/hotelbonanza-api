FROM ganey/google-mvm-php:2.1

#ADD . /var/www

# Add docker files
ADD docker/ /

# Add memcached config to
RUN echo "env[MEMCACHE_PORT_11211_TCP_ADDR] = '${MEMCACHE_PORT_11211_TCP_ADDR:=/var/run/memcached.sock}'" >> /etc/php-fpm.d/env.conf; echo "env[MEMCACHE_PORT_11211_TCP_PORT] = '${MEMCACHE_PORT_11211_TCP_PORT:=0}'" >> /etc/php-fpm.d/env.conf;

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/bin; chmod 400 /root/.ssh/id_rsa; yum install -y openssh openssh-clients; ssh-keyscan -T 60 bitbucket.org >> /root/.ssh/known_hosts; yum install -y beanstalkd

# change current dir
WORKDIR /var/www

# Get project from git, install composer stuff
RUN git init .; git remote add origin git@bitbucket.org:pulse8/hotelbonanza-api.git; git pull origin master; composer.phar install

# Fix file perms
RUN chown nobody:nobody -R /var/www/app/storage; chmod 777 -R /var/www/app/storage; chown nobody:nobody -R /var/lib/nginx/tmp; chmod 777 -R /var/lib/nginx/tmp

# Log Fixes
RUN ln -s /var/log/nginx/error.log /var/log/app_engine/nginx-error.log; ln -s /var/log/php-fpm/error.log /var/log/app_engine/phpfpm-error.log; ln -s /var/www/app/storage/logs/laravel.log /var/log/app_engine/custom_logs/project.log
